<?php

use App\Jobs\SendEmail;
use App\Models\ClassUser;
use App\Models\License;
use App\Models\Role;
use App\Models\School;
use App\Models\User;
use App\Models\UserPreferences;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoutingController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Test Routes for

// // TODO:: Remove from production
//

//CHANGE_MODULE
Route::get('/', function () {
    return redirect('/login');
});
Route::get('sendmail', [\App\Http\Controllers\GeneralController::class, 'sendmail'])->name('sendmail');


Route::get('ggg', function () {

    // Path to the project's root folder
    echo base_path().'<br>';

// Path to the 'app' folder
    echo app_path().'<br>';

// Path to the 'public' folder
    echo public_path().'<br>';

// Path to the 'storage' folder
    echo storage_path().'<br>';

// Path to the 'storage/app' folder
    echo storage_path('app').'<br>';

    return '';
//    $userid = 0;// auth()->user()->id;
//    $token = session('game_token');
//    if (!$token)
//    {
//        return back();
//    }
//    $response =  \Http::withOptions([
//        'verify' => false,
//    ])->withHeaders([
//        'Authorization' => $token
//        //'Authorization' => '61e678d17ca3af16a33271cf'
//    ])->get('https://rlapi.neetisolutions.com/dashboard/getObituaryData/'.$userid);
//    return json_decode($response->body(), true);

   // $value = $this->getGamedata();
    return view('gamedata/obituarydata');
});
Route::get('ddf', function () {

    $userRules = Collect(User::$rules);

    $schoolRules = School::$rules;
//    return $schoolRules;
//    unset($schoolRules['address1']);
//    unset($schoolRules['address2']);

    $schoolRules  =    array_merge($schoolRules,['address1' => 'required|string|max:200','address2' => 'required|string|max:200']);



    return $schoolRules;
    $bbc = $userRules->merge([ 'password' => 'required|string|min:6|max:15|confirmed',
        'email' => 'required|email|min:3|max:100',
//            'address1' => 'required|string|max:200','address2' => 'required|string|max:200'
    ]);
    return $bbc->merge(School::$rules)->toArray();   $student->assignRole($role);

});




Route::get('import', function () {
    $schoolid =14;
    $school = \App\Models\School::find($schoolid);
    // Get all Imported students
    //  $licenses = License::where('school_schid' ,'=',$schoolid)->whereDate('expiry_date', '>=', Carbon::now())->get();
    $licenses = License::where('school_schid','=', $schoolid)->where('available_lives', '>', 0)->whereDate('expiry_date', '>=', Carbon::now())->get();


dd($licenses);

    foreach ($licenses as $lic )
    {
        $lic_count = $lic->available_lives;

        // Load all imported students
        $students = User::where(['school_schid'=>$schoolid, 'account_status'=>'Imported'])->get();

        foreach ($students as $student)
        {

            if($lic_count > 0)
            {
                $student->assignRole('Student');
                $student->account_status = 'Active';
                $student->deallocated = 0;  // Allocate Student
                $student->purchaseflag = "Yes";
                $student->account_expiry_date = $lic->expiry_date;
                $student->license_licid = $lic->licid;
                //$student->save();

                $lic_count --;
            }
        }

        $lic->available_lives = $lic_count;
     //
    }


    // if license is not present then deallocate those students
//    $students = User::where(['school_schid'=>$schoolid, 'account_status'=>'Imported'])->get();
//    foreach ($students as $student)
//    {
//        $student->assignRole('Student');
//        $student->account_status = 'Active';
//        $student->save();
//    }
//
//
//    $total = License::where('school_schid', '=', $schoolid)->sum('licences_available');
//    $school->available_licenses = $total;
//    $school->save();

    // End Student activation process

    // dd($import->errors());

});



// // TODO:: Remove from production
//
//Route::post('import', function () {
//    $dt = \Carbon\Carbon::now();
//    $user = \App\Models\User::find(auth()->user()->id);
//    $user->account_expiry_date = $dt->addYear()->toDateString();
//    $user->save();
//    dd($dt->addYear()->toDateTimeString());
//});


// TODO:: Remove from production
Route::get('abc', function () {
    $schoolid = get_school_id();

    $count = User::where('school_schid' ,'=',$schoolid)->whereIn('drole', array('Teacher', 'School Admin'))->count();

    dd($count);
//
//    $abc = DB::table('licenses')->where('school_schid' ,'=',$schoolid)
//        ->whereDate('expiry_date', '>=', Carbon::now())->get();
//
//


    $balance = DB::table('licenses')->where('school_schid' ,'=',$schoolid)
        ->whereDate('expiry_date', '>=', Carbon::now())->sum('available_lives');

    dd($balance);

});

// TODO:: Remove from production
Route::get('adminp', function () {
    $role = \App\Models\Role::find(1);
    $role->givePermissionTo('Show bugs');

});

//// TODO:: Remove from production
Route::get('per222222', function () {

//   $usr =  \App\Models\User::find(20);
//    $usr->assignRole('Admin');
//
//    dd('sdds');
//
//
//    $role_id = 0;
//    $mainPermissions = ['licenses'];
//    $mainPermissions = ['organizations','products','schools','users','orders','assignments','licenses','products'];
//    //$mainPermissions = ['roles','permissions','orders'];
//    $menuPermissions = ['assignments','bugs'];
//    $role_id = 1;
    // $mainPermissions = ['licenses'];

    //Organization Admin  -- 2
//    $mainPermissions = [ 'schools','users','teachers','orders'];
//   // $extraPermission = 'buy licence';
//    $menuPermissions = ['buy licence','orders','licenses'];
//    $role_id = 2;

    //School Admin  -- 3
//    $mainPermissions = ['orders','teachers','students','classes','lessonPlans'];
//    $extraPermission = 'buy licence';
//    $menuPermissions = ['buy licence','Other Teachers Classes','orders','licenses'];
//    $role_id = 3;


    // Teacher  -- 4
    $mainPermissions = ['students', 'classes', 'lessonPlans'];
    $extraPermission = '';
    $menuPermissions = ['Other Teachers Classes', 'lessonPlans'];
    $role_id = 4;

    // Student  -- 5
//     $mainPermissions = ['classes','lessonPlans'];
//     $extraPermission = '';
//     $role_id = 5;

    // Gamer  -- 6
//    $mainPermissions = ['orders'];
//   // $extraPermission = 'buy licence';
//    $menuPermissions = ['buy licence','orders','licenses'];
//    $role_id = 6;

    // Test  -- 7
//    $mainPermissions = ['classes','lessonPlans'];
//    $extraPermission = 'buy licence';
//    $role_id = 7;


    //$mainPermissions = ['lessonPlans'];
    //Menu
    // $mainPermissions = [ 'teachers','orders','students','buy licence'];
    // \Artisan::call('cache:clear');

    // $mainPermissions =  ['Other Teachers Classes'];
    $insertAllPermissions = [];
    $assign_permissions_to_roles = [];


    foreach ($mainPermissions as $mainPermission) {
        $insertAllPermissions[] = ['name' => 'View ' . $mainPermission . ' menu'];
        $assign_permissions_to_roles[] = 'View ' . $mainPermission . ' menu';

        $insertAllPermissions[] = ['name' => 'Create ' . $mainPermission];
        $insertAllPermissions[] = ['name' => 'Read ' . $mainPermission];
        $insertAllPermissions[] = ['name' => 'Edit ' . $mainPermission];
        $insertAllPermissions[] = ['name' => 'Delete ' . $mainPermission];
//
//
//
////        // For Assign roles
        $assign_permissions_to_roles[] = 'Create ' . $mainPermission;
        $assign_permissions_to_roles[] = 'Read ' . $mainPermission;
        $assign_permissions_to_roles[] = 'Edit ' . $mainPermission;
        $assign_permissions_to_roles[] = 'Delete ' . $mainPermission;

//
//                $insertAllPermissions[] = ['Create '.$mainPermission];
//                $insertAllPermissions[] = ['Read '.$mainPermission];
//                $insertAllPermissions[] = ['Edit '.$mainPermission];
//                $insertAllPermissions[] = ['Delete '.$mainPermission];

    }


    // $insertAllPermissions[] = ['name' => 'View '.$extraPermission.' menu'];
    foreach ($menuPermissions as $menu) {
        $assign_permissions_to_roles[] = 'View ' . $menu . ' menu';
    }


    //  \App\Models\Permission::insert($insertAllPermissions);


    $permissions = \App\Models\Permission::whereIn('name', $assign_permissions_to_roles)->get();
    $rolePermission = [];

    // dd($permissions);
    foreach ($permissions as $permission) {
        $rolePermission [] = ['permission_id' => $permission->id, 'role_id' => $role_id];
    }


    DB::table('role_has_permissions')->where('role_id', '=', $role_id)->delete();

//dd($rolePermission);
    DB::table('role_has_permissions')->insert($rolePermission);
//
    \Artisan::call('config:clear');
    \Artisan::call('permission:cache-reset');
    return $permissions;


    print_r($permissions_ids);


    // Spatie\Permission\Models\Permission::create([['name' => 'edit articles'],['name' => 'edit articles']]);


//    //admin
//    //$role = \Spatie\Permission\Models\Role::findByName('Admin');
    $role = \App\Models\Role::find(1);
    //$role->givePermissionTo('Create students');
    $role->syncPermissions($assign_permissions_to_roles);
//
//    //Organization Admin
//    $role = \App\Models\Role::find(2);
//    $role->syncPermissions($assign_permissions_to_roles);
//
//    // School Admin
//    $role = \App\Models\Role::find(3);
//    $role->syncPermissions($assign_permissions_to_roles);
//
//    // School Teacher
//    $role = \App\Models\Role::find(4);
//    $role->syncPermissions($assign_permissions_to_roles);
//
//    // School Student
//    $role = \App\Models\Role::find(5);
//    $role->syncPermissions($assign_permissions_to_roles);
//
//    // Gamer
//    $role = \App\Models\Role::find(6);
//    $role->syncPermissions($assign_permissions_to_roles);

    return $insertAllPermissions;
});



// End Test Routes


// Application Routes

Route::get('paypalreturn', array('as' => 'paypal.paypalreturn', 'uses' => 'PaypalController@returnpPaypal'));
Route::get('paypalcancel', array('as' => 'paypal.paypalcancel', 'uses' => 'PaypalController@cancelPaypal'));


Route::get('import2', [App\Http\Controllers\ImportExportController::class, 'import2'])->name('import2');
Route::get('remove', [App\Http\Controllers\ImportExportController::class, 'remove'])->name('remove');


Route::get('register-school', [App\Http\Controllers\SchoolController::class, 'showRegistrationForm'])->name('register-school');
Route::post('register-school', [App\Http\Controllers\SchoolController::class, 'register'])->name('register-school-store');

Route::get('register-university-dept', [App\Http\Controllers\SchoolController::class, 'showRegistrationForm'])->name('register-university-dept');

Route::get('register-family', [App\Http\Controllers\SchoolController::class, 'showRegistrationForm'])->name('register-family');


Route::get('register-organisation', [\App\Http\Controllers\OrganizationController::class, 'showRegistrationForm'])->name('register-organisation');
Route::post('register-organisation', [\App\Http\Controllers\OrganizationController::class, 'register'])->name('register-organisation-store');

Route::get('register-university', [\App\Http\Controllers\OrganizationController::class, 'showRegistrationForm'])->name('register-university');


Route::get('register-gamer', [\App\Http\Controllers\Auth\RegisteredUserController::class, 'register_gamer_form'])->name('register-gamer');
Route::post('register-gamer', [\App\Http\Controllers\Auth\RegisteredUserController::class, 'register_gamer'])->name('register-gamer-store');


Route::get('/all-registration', function () {
    return view('registrations/all_option');
})->name('all-registration');

Route::get('/academic-registration', function () {
    return view('registrations/academic_option');
})->name('academic-registration');

Route::get('/university-registration', function () {
    return view('registrations/university_options');
})->name('university-registration');

Route::get('/school-registration', function () {
    return view('registrations/school_options');
})->name('school-registration');


Route::middleware(['admin'])->get('/translations', [\App\Http\Controllers\TranslationController::class, 'list']);
Route::middleware(['admin'])->get('/translation/{action}', [\App\Http\Controllers\TranslationController::class, 'action']);
Route::middleware(['admin'])->post('/translation/{action}', [\App\Http\Controllers\TranslationController::class, 'postaction']);


Route::group(['middleware' => ['auth']], function () {

	Route::middleware(['auth'])->get('/notifications', [\App\Http\Controllers\NotificationsController::class, 'index']);
	Route::middleware(['auth'])->post('/notifications/delete', [\App\Http\Controllers\NotificationsController::class, 'delete'])->name('notifications.delete');

    Route::get('/getassignments', function (Request $request) {
        $cat = $request->get('category');
        return \App\Models\Assignment::where('category', '=', $cat)->get();
    })->name('getassignments');

   // Route::get('/getassignments', [\App\Http\Controllers\AssignmentController::class, 'getassignments'])->name('getassignments');

    Route::middleware(['school_staff'])->get('/assignment_subjects', [\App\Http\Controllers\AssignmentController::class, 'assignment_subjects'])->name('assignment_subjects');
	Route::middleware(['school_staff'])->get('assignment_subjects_for_lesson_plans', [App\Http\Controllers\AssignmentController::class, 'assignment_subjects'])
	->defaults('create','lesson_plan')->name('assignment_subjects_for_lesson_plans');

    Route::get('/playgame', [\App\Http\Controllers\HomeController::class, 'playgame'])->name('playgame');
    Route::get('/educationTool', [\App\Http\Controllers\HomeController::class, 'educational_tool'])->name('educationTool');
    Route::get('/sdgTool', [\App\Http\Controllers\HomeController::class, 'sdgTool'])->name('sdgTool');
    Route::get('/countryGroup', [\App\Http\Controllers\HomeController::class, 'countryGroupTool'])->name('sdgTool');
    Route::get('/countryDisaparity', [\App\Http\Controllers\HomeController::class, 'countryDisaparityTool'])->name('sdgTool');
    Route::get('/dataTools',function(){
        return view('dataTools.index');
    });

    Route::get('toggleAllocate', function (Request $request) {
        $userid = $request->get("userid");
        $schoolid = get_school_id();
        $user = \App\Models\User::find($userid);
        $school = \App\Models\School::find($schoolid);


        if ($user) {
            if ($user->school_schid == $schoolid) {       //Check user is from same organization

                // If user is allocated
                if ($user->deallocated == 0) { // deallocating
                    $user->deallocated = 1;
                    $user->purchaseflag = "No";
                    $user->save();

                    $license = License::find($user->license_licid);
                    if ($license)
                    {
                        // $license->available_lives = $license->available_lives + 1;
                        $license->user_id = 0;
                        $license->save();
                    }

                    $total = License::where('school_schid', '=', $schoolid)->sum('available_lives');

                    $school = \App\Models\School::find($schoolid);

                    $school->available_licenses = $total;
                    $school->save();

                    return ['status'=>'Success','message'=>__('students.Student deallocated')];
                    return true;

                } elseif ($user->deallocated == 1) {  // Reallocate
                    $licenses = License::where('school_schid','=', $schoolid)->where('user_id','=','0')->whereDate('expiry_date', '>=', Carbon::now())->first();

                    if ($licenses) {
                        $user->deallocated = 0;  // Allocate Student
                        $user->purchaseflag = "Yes";

                        $user->license_licid = $licenses->licid;
                        $user->account_expiry_date = $licenses->expiry_date;

                        $user->save();

                        // $licenses->available_lives = $licenses->available_lives - 1;
                         $licenses->user_id = $user->id;
                        $licenses->save();


                        $total = License::where('school_schid', '=', $schoolid)->sum('available_lives');


                        $school->available_licenses = $total;
                        $school->save();
                        return ['status'=>'Success','message'=>__('students.Student allocated')];
                    }
                    // Licenses are not available to attach
                    if($school->school_type == 'family'){
                        return ['status'=>'Error','message'=>__('students.Licenses are not available at family')];
                    }
                    else{
                        return ['status'=>'Error','message'=>__('students.Licenses are not available at school')];
                    }

                }
            }
            return ['status'=>'Error','message'=>__('students.Invalid school')];
        }
        return ['status'=>'Error','message'=>__('students.User not found')];


    });


    Route::get('toggleSchoolAdmin', function (Request $request) {
        $userid = $request->get("userid");
        $schoolid = get_school_id();
        $user = \App\Models\User::find($userid);

        if ($user) {
            if ($user->school_schid == $schoolid) {  // Check user is from same organization
                if ($user->drole == 'School Admin') {

                    if ($user->id != auth()->user()->id){
                        $user->drole = 'Teacher';
                        $user->save();
                        $user->syncRoles(['Teacher']);
                        return true;
                        return ['success'=>true, 'message'=>'Status changed'];
                    }
                    return ['success'=>false, 'message'=>'You Cant change status of yourself'];
                } elseif ($user->drole == 'Teacher') {
                    $user->drole = 'School Admin';
                    $user->save();
                    $role = \App\Models\Role::find(3);
                    $user->syncRoles(['School Admin']);


                    // Send Email

                    $school = School::find( $user->school_schid);

                    $details = array(
                        'fullname' => $user->name,
                        'username' => $user->username,
                        'view'=>'emails/School/afterAssigningSchoolAdmin',
                        'subject'=>'You are assigned as a RealLives school admin by your school',
                        'email'=> $user->email,
                        'to_fullname'=> $user->name,
                        'from'=> env('MAIL_FROM_ADDRESS','support@reallives.com'),
                        'from_name'=>env('MAIL_FROM_NAME',env('APP_NAME')),
                        'school_name'=> $school->sch_name,
                    );

                    // return view('emails/School/afterAssigningTeacher',$details);
                    SendEmail::dispatch($details);
                    return true;
                    return ['success'=>true, 'message'=>'Status changed'];
                }
            }
            return false;
        }
        return false;
        return ['success'=>false, 'message'=>'User not found'];

    });

    Route::get('setLang/{lang}', function ($lang) {
        if(in_array($lang,['en','hi','fa','ko'])){
            session(['locale'=> $lang]);
			auth()->user()->setPreferences(['locale'=> $lang]);
        }
        return back();
    });



    Route::get('gamedashboard', [\App\Http\Controllers\HomeController::class, 'index'])->name('gamedashboard');


    Route::middleware(['school_user'])->get('assignedClasses', [\App\Http\Controllers\ClassesController::class, 'assignedClasses'])->name('assignedClasses');
    Route::middleware(['school_user'])->get('myJoinedAssignmentClasses', [\App\Http\Controllers\ClassesController::class, 'myJoinedAssignmentClasses'])->name('myJoinedAssignmentClasses');


    Route::post('add-comment', [\App\Http\Controllers\LessonPlanController::class, 'saveComment']);
    Route::post('add-comment-assignment', [\App\Http\Controllers\ClassesController::class, 'saveAssignmentComment']);


    Route::get('/download-pdf/{file}', [\App\Http\Controllers\LessonPlanController::class, 'downloadPdf']);
//    Route::get('/download-pdf/{file}', 'LessonCommentController@downloadPdf');

    Route::get('/validate', function (Request $request) {  // Set School id in session
        $schoolid = $request->get("schoolid");
        $request->session()->put('school_schid', $schoolid);
        \Flash::success(__('School Selected successfully', ['model' => __('schools.singular')]));
        return redirect('/school_dashboard');
    });


    // This request comes from Organization Admin / Admin
    Route::get('/setschool', function (Request $request) {  // Set School id in session
        $schoolid = $request->get("schoolid");
        $request->session()->put('school_schid', $schoolid);
        \Flash::success(__('School Selected successfully', ['model' => __('schools.singular')]));

        if($request->has('back'))
            return redirect()->back();
        else
            return redirect('/school_dashboard');
    });

    // TODO::Gameplay Routes, Move to controller after testing on server
    Route::group(['prefix' => 'Gameplay'], function () {
        Route::get('/obituaries', [\App\Http\Controllers\GameplayDataController::class, 'obituaries']);
        Route::get('/letters', [\App\Http\Controllers\GameplayDataController::class, 'letters']);
        Route::get('/SdgComment', [\App\Http\Controllers\GameplayDataController::class, 'SdgComment']);
        Route::get('/BornReligion', [\App\Http\Controllers\GameplayDataController::class, 'BornReligion']);
        Route::get('/SdgData', [\App\Http\Controllers\GameplayDataController::class, 'SdgData']);
        Route::get('/BloodDonation', [\App\Http\Controllers\GameplayDataController::class, 'BloodDonation']);
        Route::get('/OrganDonation', [\App\Http\Controllers\GameplayDataController::class, 'OrganDonation']);
        Route::get('/FeedbackData', [\App\Http\Controllers\GameplayDataController::class, 'FeedbackData']);
        Route::get('/BugReportData', [\App\Http\Controllers\GameplayDataController::class, 'BugReportData']);
        Route::middleware(['org_staff'])->get('/SchoolGameplay', [\App\Http\Controllers\GameplayDataController::class, 'SchoolGameplay']);
    });


    Route::get('logout', [\App\Http\Controllers\Auth\AuthenticatedSessionController::class, 'destroy'])->name('logout');
    Route::get('admindashboard', [\App\Http\Controllers\AdminController::class, 'admindashboard'])->name('admindashboard');



    // License Routes

    Route::middleware(['non_students'])->get('purchase_license_list', [\App\Http\Controllers\PurchaseController::class, 'show_license_list'])->name('purchase_license_list');
    Route::post('createorder', [\App\Http\Controllers\PaypalController::class, 'createOrder'])->name('paypal.createorder');
    Route::get('captureorder/{orderId}/capture', [\App\Http\Controllers\PaypalController::class, 'capturePaymentWithpaypal'])->name('paypal.captureorder');
    Route::get('purchaseDetails', [\App\Http\Controllers\PurchaseController::class, 'purchase_details'])->name('purchaseDetails');
    Route::get('renewlicense', [\App\Http\Controllers\PurchaseController::class, 'renewlicense'])->name('renewlicense');

        Route::post('createorder_renew', [\App\Http\Controllers\PaypalController::class, 'createorder_renew'])->name('paypal.createorder_renew');

    Route::get('captureRenew/{orderId}/capture', [\App\Http\Controllers\PaypalController::class, 'captureRenewPaymentWithpaypal'])->name('paypal.captureRenew');







    Route::middleware(['non_students'])->resource('licenses', App\Http\Controllers\LicenseController::class,['only' => ['index', 'show']]);
    // Route::post('createorder', array('as' => 'paypal.createorder','uses' => 'App\Http\Controllers\PaypalController@createOrder'));
    // Route::get('captureorder/{orderId}/capture', array('as' => 'paypal.captureorder','uses' => 'App\Http\Controllers\PaypalController@capturePaymentWithpaypal'));



    Route::get('assign_pages_to_roles_form', [\App\Http\Controllers\GeneralController::class, 'assign_pages_to_roles_form'])->name('assign_pages_to_roles_form');

    Route::post('assign_pages_to_roles', [App\Http\Controllers\GeneralController::class, 'assign_pages_to_roles'])->name('assign_pages_to_roles');


    Route::resource('bugs', App\Http\Controllers\BugsController::class);


    Route::middleware(['teacher_and_above'])->resource('assignments', App\Http\Controllers\AssignmentController::class);

    Route::get('upload_photo_form', [\App\Http\Controllers\GeneralController::class, 'upload_photo_form'])->name('upload_photo_form');
    Route::post('upload_profile_photo', [\App\Http\Controllers\GeneralController::class, 'upload_profile_photo'])->name('upload_profile_photo');
    Route::post('upload_school_logo', [\App\Http\Controllers\GeneralController::class, 'upload_school_logo'])->name('upload_school_logo');


    // Required schoolid in sessions
    Route::group(['middleware' => ['hasSchoolId']], function () {


        Route::get('teacherdashboard/{teacherid}', [\App\Http\Controllers\TeacherController::class, 'teacherdashboard'])->name('teacherdashboard');
        Route::middleware(['schooladmin_and_above'])->get('school_dashboard', [\App\Http\Controllers\SchoolController::class, 'school_dashboard'])->name('school_dashboard');


        Route::get('classtudents', [\App\Http\Controllers\ClassesController::class, 'classtudents'])->name('classtudents');
        Route::get('assignmentstudents', [\App\Http\Controllers\ClassesController::class, 'assignmentstudents'])->name('assignmentstudents');



        Route::middleware(['teacher_and_above'])->resource('students', App\Http\Controllers\StudentController::class);
        Route::resource('classes', App\Http\Controllers\ClassesController::class);
        Route::resource('lessonPlans', App\Http\Controllers\LessonPlanController::class);
		Route::middleware(['org_staff'])->get('lessonPlansWithAssignment', [App\Http\Controllers\LessonPlanController::class, 'index'])
		->defaults('assignment','true')->name('lessonPlansWithAssignment');
		Route::middleware(['org_staff'])->get('myLessonPlansWithAssignment', [App\Http\Controllers\LessonPlanController::class, 'index'])
		->defaults('assignment','true')->defaults('self',true)->name('myLessonPlansWithAssignment');

        Route::middleware(['schooladmin_and_above'])->get('teachers/create', [\App\Http\Controllers\UserController::class, 'teachers_create_form'])->name('teachers_create_form');
        Route::middleware(['schooladmin_and_above'])->post('teachers/create', [\App\Http\Controllers\UserController::class, 'teachers_store'])->name('teachers_store');

        Route::middleware(['teacher_and_above'])->get('teachers_list', [\App\Http\Controllers\UserController::class, 'teachers_list'])->name('teachers_list');
        //Route::get('students_list', [\App\Http\Controllers\UserController::class, 'students_list'])->name('students_list');
        // Route::get('purchase_license', [\App\Http\Controllers\PurchaseController::class, 'purchase_license_form'])->name('purchase_license_form');


        // Import Routes
        Route::middleware(['schooladmin_and_above'])->get('importStudents', [App\Http\Controllers\ImportExportController::class, 'importExportView']);
        //  Route::get('export', [App\Http\Controllers\ImportExportController::class, 'export'])->name('export');
        Route::middleware(['schooladmin_and_above'])->post('importStudents', [App\Http\Controllers\ImportExportController::class, 'import'])->name('import');
        Route::middleware(['schooladmin_and_above'])->get('importTeachers', [App\Http\Controllers\ImportExportController::class, 'importTeachers']);
        //  Route::get('export', [App\Http\Controllers\ImportExportController::class, 'export'])->name('export');
        Route::middleware(['schooladmin_and_above'])->post('importTeachers', [App\Http\Controllers\ImportExportController::class, 'importTeachersStore'])->name('importTeachers');

    });


    // Organization Admin Routes
    Route::resource('users', App\Http\Controllers\UserController::class);
    Route::resource('people', App\Http\Controllers\PersonController::class);
    //  Route::post('createbuilding', [\App\Http\Controllers\HomeController::class, 'createbuilding'])->name('createbuilding');
    Route::middleware(['orgadmin_and_above'])->resource('schools', App\Http\Controllers\SchoolController::class);


    //Super Admin Routes
    Route::middleware(['admin'])->resource('organizations', App\Http\Controllers\OrganizationController::class);
    Route::middleware(['admin'])->resource('products', App\Http\Controllers\ProductController::class);
    Route::middleware(['non_students'])->resource('orders', App\Http\Controllers\OrderController::class);
    Route::middleware(['admin'])
    ->get('/toggleActivateUser', [App\Http\Controllers\UserController::class, 'toggleActivateUser']);
    Route::middleware(['admin'])
    ->get('/activateSchool', [App\Http\Controllers\LicenseController::class, 'freeLicense']);

    Route::resource('permissions', App\Http\Controllers\PermissionController::class);
    Route::resource('roles', App\Http\Controllers\RoleController::class);



    Route::get('profile', [\App\Http\Controllers\UserController::class, 'profile'])->name('profile');
    Route::post('updateProfile/{id}', [\App\Http\Controllers\UserController::class, 'updateProfile'])->name('updateProfile');
    Route::post('fetchCountry', [\App\Http\Controllers\UserController::class, 'fetchCountry'])->name('fetchCountry');
    Route::post('fetchState', [\App\Http\Controllers\UserController::class, 'fetchState'])->name('fetchState');
    Route::post('fetchCity', [\App\Http\Controllers\UserController::class, 'fetchCity'])->name('fetchCity');

    Route::get('/home', function () {

        return redirect('gamedashboard');
        return view('index');
    })->middleware('auth')->name('home');


});

Route::post('fetchCountry', [\App\Http\Controllers\UserController::class, 'fetchCountry'])->name('fetchCountry');
Route::post('fetchState', [\App\Http\Controllers\UserController::class, 'fetchState'])->name('fetchState');
Route::post('fetchCity', [\App\Http\Controllers\UserController::class, 'fetchCity'])->name('fetchCity');

require __DIR__ . '/auth.php';

Route::group(['prefix' => '/ubold/'], function () {
    Route::get('', [RoutingController::class, 'index'])->name('root');
    Route::get('{first}/{second}/{third}', [RoutingController::class, 'thirdLevel'])->name('third');
    Route::get('{first}/{second}', [RoutingController::class, 'secondLevel'])->name('second');
    Route::get('{any}', [RoutingController::class, 'root'])->name('any');
});


Route::post('/paypal/ipnhandler', [\App\Http\Controllers\PaypalipnController::class, 'index']);
Route::get('/paypal/ipnhandler', [\App\Http\Controllers\PaypalipnController::class, 'index']);





