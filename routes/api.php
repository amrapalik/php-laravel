<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});






















Route::resource('users', App\Http\Controllers\API\UserAPIController::class);


Route::resource('organizations', App\Http\Controllers\API\OrganizationAPIController::class);


Route::resource('schools', App\Http\Controllers\API\SchoolAPIController::class);


Route::resource('products', App\Http\Controllers\API\ProductAPIController::class);


Route::resource('orders', App\Http\Controllers\API\OrderAPIController::class);


Route::resource('permissions', App\Http\Controllers\API\PermissionAPIController::class);


Route::resource('roles', App\Http\Controllers\API\RoleAPIController::class);


Route::resource('students', App\Http\Controllers\API\StudentAPIController::class);










Route::resource('classes', App\Http\Controllers\API\ClassesAPIController::class);


Route::resource('lesson_plans', App\Http\Controllers\API\LessonPlanAPIController::class);




Route::resource('bugs', App\Http\Controllers\API\BugsAPIController::class);


Route::resource('assignments', App\Http\Controllers\API\AssignmentAPIController::class);


Route::resource('licenses', App\Http\Controllers\API\LicenseAPIController::class);
