<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\LessonPlan;

class LessonPlanApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_lesson_plan()
    {
        $lessonPlan = LessonPlan::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/lesson_plans', $lessonPlan
        );

        $this->assertApiResponse($lessonPlan);
    }

    /**
     * @test
     */
    public function test_read_lesson_plan()
    {
        $lessonPlan = LessonPlan::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/lesson_plans/'.$lessonPlan->lpid
        );

        $this->assertApiResponse($lessonPlan->toArray());
    }

    /**
     * @test
     */
    public function test_update_lesson_plan()
    {
        $lessonPlan = LessonPlan::factory()->create();
        $editedLessonPlan = LessonPlan::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/lesson_plans/'.$lessonPlan->lpid,
            $editedLessonPlan
        );

        $this->assertApiResponse($editedLessonPlan);
    }

    /**
     * @test
     */
    public function test_delete_lesson_plan()
    {
        $lessonPlan = LessonPlan::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/lesson_plans/'.$lessonPlan->lpid
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/lesson_plans/'.$lessonPlan->lpid
        );

        $this->response->assertStatus(404);
    }
}
