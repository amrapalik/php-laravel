<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\License;

class LicenseApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_license()
    {
        $license = License::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/licenses', $license
        );

        $this->assertApiResponse($license);
    }

    /**
     * @test
     */
    public function test_read_license()
    {
        $license = License::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/licenses/'.$license->licid
        );

        $this->assertApiResponse($license->toArray());
    }

    /**
     * @test
     */
    public function test_update_license()
    {
        $license = License::factory()->create();
        $editedLicense = License::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/licenses/'.$license->licid,
            $editedLicense
        );

        $this->assertApiResponse($editedLicense);
    }

    /**
     * @test
     */
    public function test_delete_license()
    {
        $license = License::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/licenses/'.$license->licid
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/licenses/'.$license->licid
        );

        $this->response->assertStatus(404);
    }
}
