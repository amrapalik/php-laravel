<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Bugs;

class BugsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_bugs()
    {
        $bugs = Bugs::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/bugs', $bugs
        );

        $this->assertApiResponse($bugs);
    }

    /**
     * @test
     */
    public function test_read_bugs()
    {
        $bugs = Bugs::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/bugs/'.$bugs->bugid
        );

        $this->assertApiResponse($bugs->toArray());
    }

    /**
     * @test
     */
    public function test_update_bugs()
    {
        $bugs = Bugs::factory()->create();
        $editedBugs = Bugs::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/bugs/'.$bugs->bugid,
            $editedBugs
        );

        $this->assertApiResponse($editedBugs);
    }

    /**
     * @test
     */
    public function test_delete_bugs()
    {
        $bugs = Bugs::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/bugs/'.$bugs->bugid
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/bugs/'.$bugs->bugid
        );

        $this->response->assertStatus(404);
    }
}
