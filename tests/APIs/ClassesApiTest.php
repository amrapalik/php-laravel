<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Classes;

class ClassesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_classes()
    {
        $classes = Classes::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/classes', $classes
        );

        $this->assertApiResponse($classes);
    }

    /**
     * @test
     */
    public function test_read_classes()
    {
        $classes = Classes::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/classes/'.$classes->classid
        );

        $this->assertApiResponse($classes->toArray());
    }

    /**
     * @test
     */
    public function test_update_classes()
    {
        $classes = Classes::factory()->create();
        $editedClasses = Classes::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/classes/'.$classes->classid,
            $editedClasses
        );

        $this->assertApiResponse($editedClasses);
    }

    /**
     * @test
     */
    public function test_delete_classes()
    {
        $classes = Classes::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/classes/'.$classes->classid
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/classes/'.$classes->classid
        );

        $this->response->assertStatus(404);
    }
}
