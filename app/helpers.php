<?php


use Illuminate\Http\Request;

if (!function_exists('fetch_game_token'))
{
    function set_game_token_in_session(Request $request, $user_id)
    {


        try {
           // dd('$json');
            //$client = new HttpClient();
            $endpoint = env('REALLIVES_API_URL') . "/endpoint/session/create";
            $client = new \GuzzleHttp\Client();
          //  $client->setDefaultOption('verify', false);

            $response = $client->post($endpoint, [
                \GuzzleHttp\RequestOptions::JSON => ['user_id' => $user_id],
                'verify' => false,
            ]);

            //$json = json_decode($response->getBody()->getContents());
            $json = json_decode($response->getBody(), true);
            //dd($json['data']['result']['token']);
            Log::debug(print_r($json, true));
            $request->session()->put('game_token', $json['data']['result']['token'] );
        } catch (Exception $e) {

            dd($e);
        }
    }
}


function abc($cc = 'Test'){


    return  '<li class="nav-item">
        <a href="/Gameplay/SchoolGameplay">
            <i data-feather="database"></i>
            <span>  '.$cc .' </span>
        </a>
    </li>';

     //<span> @lan?g('gamedata.SchoolGameplay') </span>
}

if(!function_exists('get_school_id')){
    function get_school_id(){
        $user = auth()->user();
        if ($user->drole == 'Admin' | $user->drole == 'Organization Admin'){
            $sessSchoolId = session('school_schid');

           if(session('school_schid') == null)
           {
               return redirect('schools');
           }

           return $sessSchoolId;

        }
        elseif($user->drole == 'Gamer'){
            return redirect('Login');
        }else{
          return $user->school_schid;
        }
    }
}


if(!function_exists('get_school_id_or_null')){
    function get_school_id_or_null(){
        $user = auth()->user();
        if ($user->drole == 'Admin' | $user->drole == 'Organization Admin'){
            $sessSchoolId = session('school_schid');
           return $sessSchoolId;
        }
        elseif($user->drole == 'Gamer'){
            return redirect('Login');
        }else{
          return $user->school_schid;
        }
    }
}

if(!function_exists('teacher_from_this_school')){
    function teacher_from_this_school($teacher_id){
       $schoolid =  get_school_id();

      $teacher = \App\Models\User::find($teacher_id);
      if ($teacher == null)
      {
          return   false;
      }


       if ($schoolid  == $teacher->school_schid){
          return  true;
       }

        return   false;

    }
}


