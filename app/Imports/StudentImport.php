<?php

namespace App\Imports;

use App\Models\Student;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class StudentImport implements ToModel, WithHeadingRow, SkipsOnError, WithValidation, SkipsOnFailure, WithBatchInserts
{

    use SkipsErrors, Importable, SkipsFailures;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $schoolid =   session('school_schid');
        //  (User::max('id') + random_int(99, 99999));
        //dd($schoolid);

        //TODO:: check session
        return new Student([
            'school_schid'  => $schoolid,
            'firstname'     => ucwords($row['firstname']),
            'lastname'      => ucwords($row['lastname']),
            'name'          => ucwords($row['firstname']).' '. ucwords($row['lastname']),
            //'username'    => "Uabc",//ucwords($row['username']),
            'username'      => preg_replace('/\s+/', '', strtolower($row['firstname']).strtolower($row['lastname']).(Student::max('id')+1).rand(10,99)),

           // 'username'      => preg_replace('/\s+/', '', strtolower($row['firstname']).strtolower($row['lastname']).(Student::max('id')+1).rand(10,99)),
            // 'username'   => preg_replace('/\s+/', '', strtolower($row['firstname']).strtolower($row['lastname'])),
            //'email'       => ucwords($row['lastname']),
            'password'      => \Hash::make('12345678'),
            'age'           => $row['age'],
            'grade'         => $row['grade'],
            'drole'         => 'Student',
            'account_status'=> 'Imported',
            'deallocated'   => 1
        ]);
    }

    public function rules(): array {
        return [
            '*.email'=>['email','unique:users,email'],
            '*.username'=>[ 'unique:users,username'],
            '*.gender'=>[ 'required'],
            '*.grade'=>[ 'required'],
        ];
    }
    public function batchSize(): int
    {
       return  1000;
    }

}
