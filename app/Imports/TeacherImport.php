<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class TeacherImport implements ToModel, WithHeadingRow, SkipsOnError, WithValidation, SkipsOnFailure, WithBatchInserts
{

    use SkipsErrors, Importable, SkipsFailures;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $schoolid =   session('school_schid');
        //  (User::max('id') + random_int(99, 99999));
        //dd($schoolid);


        return new User([
            'school_schid'  => $schoolid,
            'firstname'     => ucwords($row['firstname']),
            'lastname'      => ucwords($row['lastname']),
            'name'          => ucwords($row['firstname']).' '. ucwords($row['lastname']),
            //'username'      => "Uabc",//ucwords($row['username']),
            'username'      => preg_replace('/\s+/', '', strtolower($row['firstname']).strtolower($row['lastname']).(User::max('id')+1)),
            // 'username'      => preg_replace('/\s+/', '', strtolower($row['firstname']).strtolower($row['lastname'])),
            'email'         => ucwords($row['email']),
            'password'      => \Hash::make('password'),
            'age'           => $row['age'],
            'gender'        => $row['gender'],
            'designation'   => $row['designation'],
            'account_status'=> 'Imported',
            'drole'         => 'Teacher',
        ]);

        //TODO:: Assign Spatie role
    }

    public function rules(): array {
        return [
            '*.email'=>['nullable','email','unique:users,email'],
            '*.username'=>[ 'unique:users,username']
        ];
    }
    public function batchSize(): int
    {
        return  1000;
    }

}
