<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\License;
use App\Models\Product;
use App\Models\User;
use App\Models\School;
use App\Models\Order;
use Log;


class getFreeSchools extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RL:getFreeSchools';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //create entry for all the free schools into orders....
        $schools = License::where('paypal_captureid','=','some_id')->where('school_schid','!=',NULL)->get();
        foreach ($schools as $school) {
            // $date = \Carbon\Carbon::createFromFormat('m/d/Y', $school->created_at)->format('Y-m-d');
            $date = \Carbon\Carbon::now();
            // $date = date('d/M/Y H:i:s', $date);
            // echo $date;

            Order::create([
                'product_prodid'=>$school->product_prodid,
                'user_id'=> $school->user_id,
                'licences'=>$school->lives,
                'licences_available'=>$school->lives,
                'price'=>$school->price,
                'expiry_date'=>$school->expiry_date,
                'school_schid'=> $school->school_schid,
                'paypal_orderid'=>"free_order",
                'status'=>'COMPLETE',
                'paid'=>0,
            ]);
        }
        echo $schools;
        return 0;

    }
}
