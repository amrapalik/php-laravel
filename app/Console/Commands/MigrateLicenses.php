<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Order;
use App\Models\License;
use App\Models\Product;
use App\Models\User;
use App\Models\School;
use Log;
class MigrateLicenses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RL:MigrateLicenses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates current licenses to the new versions based on the updates to licensing related db updates.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
		echo "Updating licenses\n";
		echo "Getting orders ... \n";
    
        Log::channel('command')->info('Starting the License Migration!--START'.\Carbon\Carbon::now()); 
        $products = Product::all();
        foreach ($products as $product) {
            if($product->user_type == 'Gamer'){
                $product->expiry_type = 'max_lives';
            }
            else{
                $product->expiry_type = 'date' ;
            }
            $product->save();
        }
        $orders = Order::where('status','=','COMPLETED')->get();
        foreach ($orders as $order) { // for users who got license recently
            $license = License::where('user_id' ,'=',$order->user_id)->first();
            if($license){//check the product(if max_lives is the product type then give the max_lives to that license else make it null)
                $product = Product::where('prodid','=',$license->product_prodid)->first();
                if($product->expiry_type == 'max_lives'){
                    $dt = \Carbon\Carbon::now();
                    $license->available_lives = $product->max_lives;
                    $license->expiry_date = $dt->addYear(3)->toDateString();
                    $license->save();
                }
                else{
                    $license->available_lives = null;
                    $license->save();
                }
                Log::channel('command')->info('Updating Existing user...'.$order->user_id); 
            }
            else{// add the license..check the product type..if max_lives then add the max_lives to licenses else null
                $product = Product::where('prodid','=',$order->product_prodid)->first();
                $available_lives;
                if($product->expiry_type == 'max_lives'){
                    $available_lives = $product->max_lives;
                }
                else{
                    $available_lives = NULL;
                }
               
                $dt = \Carbon\Carbon::now();
                 License::create(     [
                    'order_ordid'=>$order->ordid,
                    'product_prodid'=>$order->product_prodid,
                    'user_id'=> $order->user_id,
                    'lives'=>0,
                    'available_lives'=>$available_lives,
                    'price'=>$order->price,
                    'school_schid'=> $order->school_schid,
                    'renew_price'=> $product->renew_price,
                    'expiry_date'=> $dt->addYear($product->validity)->toDateString(),
                    'paypal_orderid'=> $order->paypal_orderid,
                    'status'=>'Paid',
                    'paid'=>1,
                ]);


                Log::channel('command')->info('Adding new User...'); 
            }
            
        }
        $users = User::where('purchaseflag','=','Yes')->where('order_ordid','=',NULL)->get();
        foreach ($users as $user) {//for old users
            $dt = \Carbon\Carbon::now();
            $license = License::where('user_id','=',$user->id)->where('paypal_captureid','=','some_id')->first();
            if($license){//update lives and expiry
                echo "old License Updated : ".$license->licid."\n";
                Log::channel('command')->info('Old License Updated... Licid:'.$license->licid); 

                $license->available_lives = 10000;
                $license->expiry_date = $dt->addYear(3)->toDateString();
                $license->save();
            }
            else{
                  $licenses =  License::create([
                    'order_ordid'=>1,
                    'product_prodid'=>1,
                    'user_id'=> $user->id,
                    'lives'=>0,
                    'available_lives'=>10000,
                    'price'=>1,
                    'school_schid'=> null,
                    'renew_price'=> 1,
                    'expiry_date'=> $dt->addYear(5)->toDateString(),
                    'paypal_captureid'=>'some_id',
                    'paypal_orderid'=> 1,
                    'status'=>'Paid',
                    'paid'=>1,
                ]);
                echo "Created New : ".$licenses->licid."\n";
                Log::channel('command')->info('New License Created... Licid:'.$licenses->licid); 


            }
        }
        $schools = School::where('purchased_licenses','>',0)->get();
        foreach($schools as $school){// for school licenses
            $license = License::where('school_schid' ,'=',$school->schid)->first();
            if($license){
                $license->available_lives = NULL;
                $license->lives = 0;
                $license->save();
                for ($i=0; $i < $school->purchased_licenses-1; $i++) { 
                    $dt = \Carbon\Carbon::now();

                    $license =  License::create(     [
                        'order_ordid'=>$license->order_ordid,
                        'product_prodid'=>$license->product_prodid,
                        'user_id'=> 0,
                        'lives'=>0,
                        'available_lives'=>NULL,
                        'price'=>$license->price,
                        'school_schid'=> $license->school_schid,
                        'renew_price'=> $license->renew_price,
                        'expiry_date'=> $dt->addYear(1)->toDateString(),
                        'paypal_orderid'=> $license->paypal_orderid,
                        'status'=>'Paid',
                        'paid'=>1,
                    ]);

                    echo "Schools Updated :".$license->licid."\n";
                    Log::channel('command')->info('School License created... Licid:'.$license->licid); 


                }
            }
           
        }
        $students = User::where('drole','=','Student')->where('deallocated','=',0)->get();
        foreach($students as $student){// To assign the licenses to the students
            
            $license = License::where('school_schid' ,'=',$student->school_schid)->whereDate('expiry_date', '>=',  \Carbon\Carbon::now())
            ->where('user_id','==',0)->first();
            if($license){
                $license->user_id = $student->id;                       
                $student->account_expiry_date = $license->expiry_date;
                $student->license_licid = $license->licid;
                $student->save();
                $license->save();
                echo "Student added :".$license->licid."\n";
                Log::channel('command')->info('Students added... Licid:'.$license->licid); 

            }
        }
		// get list of orders 
		// Something like $orders = Order::all();
		// find users/licensees
		// Are the number of models equal to the licenses number ? 
		// if not, number of models need to be created
		// assignment will also have to be done in case of gamers
		
        return 0;
    }
}
