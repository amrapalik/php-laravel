<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
     public $details;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $details = $this->details;

        Mail::send($this->details['view'], $this->details , function ($message) use ($details)
        {
            if(!array_key_exists('raw',$details)){
                $message->to($details['email'], $details['to_fullname'])
            // $message->to('dhiraj@webosys.com', 'Dhiraj Sontakke')
            ->bcc('support@reallivesworld.com')
                    ->subject($details['subject']);
                $message->from($details['from'],$details['from_name']);
            }
            else{
                $message->to($details['email'], $details['to_fullname'])
                // $message->to('dhiraj@webosys.com', 'Dhiraj Sontakke')
                        ->subject($details['subject']);
                    $message->from($details['from'],$details['from_name']);
            }
        });

    }
}
