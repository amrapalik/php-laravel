<?php

namespace App\Http\Controllers;

use App\DataTables\StudentDataTable1111;
use App\DataTables\TeacherDataTable;
use App\DataTables\UserDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Jobs\SendEmail;
use App\Models\Role;
use App\Models\School;
use App\Models\User;
use App\Models\Countries;
use App\Models\State;
use App\Models\City;
use App\Models\License;

use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Response;
use Illuminate\Http\Request;

class UserController extends AppBaseController
{
    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {
        if (auth()->user()->drole == 'Admin'){
            return $userDataTable->render('users.index');
        }
    }

    public function students_list(StudentDataTable1111 $studentDataTable)
    {

        $schoolid = session('school_schid');

        if ($schoolid == null)
        {
            return redirect ('logout');
        }

        return $studentDataTable->render('students.index');
    }

    public function teachers_list(TeacherDataTable $teacherDataTable)
    {

        $schoolid = session('school_schid');

        if ($schoolid == null)
        {
            return redirect ('logout');
        }

        return $teacherDataTable->render('teachers.index');
    }





    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function teachers_create_form(Request $request)
    {

        return view('teachers.create');
    }





    public function create(Request $request)
    {
        $type =  $request->get('utype');

        if(!in_array($type, ['student','teacher']))
        {
            if (auth()->user()->drole != 'Admin')
            {
                return redirect()->back();
            }
        }
        return view('users.create');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();

        /** @var User $user */

        $schoolid = session('school_schid');

        if (auth()->user()->drole == 'School Admin' || auth()->user()->drole == 'Teacher' )
        {
            $schoolid = auth()->user()->school_schid;
        }

//dd(auth()->user()->school_schid);

        $input['school_schid'] = $schoolid;
        $input['firstname'] = ucwords( $input['firstname']);
        $input['lastname'] = ucwords( $input['lastname']);
        $input['name'] =  $input['firstname']  .' '.  $input['lastname'];
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);

        $role = Role::where('name','=', $input['drole'])->first();
        $user->assignRole($role);

        Flash::success(__('messages.saved', ['model' => __('users.singular')]));


        if($input['utype'] == 'teacher')
        {
            return redirect('/teachers_list');
        }elseif($input['utype'] == 'student')
        {
              return redirect('/students');
        }else{

            return redirect('/users');
        }

    }


    public function teachers_store(Requests\CreateTeacherRequest $request)
    {
        $input = $request->only('firstname','lastname','username','email','password','gender','designation');
        $input['school_schid'] =   get_school_id();
		//validation for family
		$school = School::find($input['school_schid']);
		$teacher_cnt = User::where('drole', 'Teacher')->where('school_schid', $input['school_schid'])->count();
		if($school->school_type == 'family' && $teacher_cnt > env('MAX_PARENTS', 2)){
            Flash::error(__('You can not add more parents.'));
            return redirect()->back();
		}

        $school =  \App\Models\School::find(get_school_id());
        $createFlag = true;
        $pass = $input['password'];
        $input['firstname'] = ucwords( $input['firstname']);
        $input['lastname'] = ucwords( $input['lastname']);
        $input['name'] =  $input['firstname']  .' '.  $input['lastname'];
        $input['password'] = Hash::make($input['password']);
        if($school->school_type == 'family'){
            $teacher_count =  User::where('school_schid' ,'=',$school->schid)->whereIn('drole', array('Teacher', 'School Admin'))->count();
            if($teacher_count >= 2){
                    $createFlag = false;
            }
            $input['drole'] = 'School Admin';
        }
        else{
            $input['drole'] = 'Teacher';
        }



        if($createFlag){
            $user = User::create($input);

            $role = Role::where('name','=', 'Teacher')->first();


            $user->assignRole($role);

            // Send Email
            $school = School::find( $input['school_schid']);
            if($school->school_type != 'family'){
                Flash::success( __('teachers.teacher Created successfully'));
            }









            $teacher_count =  User::where('school_schid' ,'=',$school->schid)->whereIn('drole', array('Teacher', 'School Admin'))->count();

            $school->no_of_teachers = $teacher_count;

            $school->save();

            $details = array(
                'fullname' => $user->name,
                'username' => $user->username,
                'view'=>'emails/School/afterAssigningTeacher',
                'subject'=>'You are assigned as a RealLives teacher by your school',
                'email'=> $user->email,
                'to_fullname'=> $user->name,
                'from'=> env('MAIL_FROM_ADDRESS','support@reallivesworld.com'),
                'from_name'=>env('MAIL_FROM_NAME',env('APP_NAME')),
                'school_name'=> $school->sch_name,
                'password'=>$pass
            );

            // return view('emails/School/afterAssigningTeacher',$details);
            SendEmail::dispatch($details);
        }
        else{
            Flash::error( __("Cannot create more than 2 parents"));

        }
            return redirect('/teachers_list');
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error(__('users.singular').' '.__('messages.not_found'));

            return redirect()->back();

        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error(__('messages.not_found', ['model' => __('users.singular')]));

            return redirect()->back();

        }

        return view('users.edit')->with('user', $user);
    }

    public function profile()
    {

        $id=auth()->user()->id;
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error(__('messages.not_found', ['model' => __('users.singular')]));

            return redirect()->back();

        }

        return view('users.profile')->with('user', $user);
    }

    public function updateProfile($id, UpdateUserRequest $request)
    {
        /** @var User $user */
        $user = User::find(auth()->user()->id);

        if (empty($user)) {
            Flash::error(__('messages.not_found', ['model' => __('users.singular')]));

            return redirect()->back();

        }

        $input = $request->except('drole');

        $input['firstname'] = ucwords( $input['firstname']);
        $input['lastname'] = ucwords( $input['lastname']);
        $input['name'] =  $input['firstname']  .' '.  $input['lastname'];

        //if password is blank remove
        if ($input['password'] != '' && $input['password'] != null)
        {
            $input['password']= \Hash::make($input['password']);

        }else
        {
            unset($input['password']);
        }

        //dd($input);

        $user->fill($input);
        $user->save();


//        $role = Role::where('name','=', $input['drole'])->first();
//        $user->assignRole($role);


        Flash::success(__('general.Profile Updated Successfully', ['model' => __('users.singular')]));

         return redirect(route('profile'));
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error(__('messages.not_found', ['model' => __('users.singular')]));

            return redirect()->back();

        }

        $input = $request->all();


        $input['firstname'] = ucwords( $input['firstname']);
        $input['lastname'] = ucwords( $input['lastname']);
        $input['name'] =  $input['firstname']  .' '.  $input['lastname'];
        if ($input['password'] != '' && $input['password'] != null)
        {
            $input['password']= \Hash::make($input['password']);

        }else
        {
            unset($input['password']);
        }
        
        


        $user->fill($input);
        $user->save();

        $role = Role::where('name','=', $input['drole'])->first();
        $user->assignRole($role);


        Flash::success(__('messages.updated', ['model' => __('users.singular')]));

        if($input['utype'] == 'teacher')
        {
            return redirect('/teachers_list');
        }elseif($input['utype'] == 'student')
        {
            return redirect('/students_list');
        }else{

            return redirect('/users');
        }

        return redirect()->back();

    }
    public function toggleActivateUser(Request $request){
        //check for update license updation of user for admin
        //purchaseflag
            $userid = $request->get("userid");
            $user = \App\Models\User::find($userid);

            $license = License::where('user_id','=',$user->id)->first();
            if($user->purchaseflag == 'No'){

                $user->purchaseflag = 'Yes';
                if($license){
                }
                else{
                    $licenses =  License::create([
                        'order_ordid'=>1,
                        'product_prodid'=>1,
                        'user_id'=> $user->id,
                        'lives'=> 0,
                        'available_lives'=>300,
                        'price'=>1,
                        'school_schid'=> null,
                        'renew_price'=> 1,
                        'expiry_date'=> \Carbon\Carbon::now()->addYear(3)->toDateString(),
                        'paypal_captureid'=>'some_id',
                        'paypal_orderid'=> 1,
                        'status'=>'Paid',
                        'paid'=>1,
                    ]);
                }
            }
            elseif($user->purchaseflag == 'Yes'){
                $user->purchaseflag = 'No';
            }
            $user->save();
            return ['success'=>true, 'message'=>'Status changed'];

        
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error(__('messages.not_found', ['model' => __('users.singular')]));

            return redirect()->back();

        }

        $user->delete();

        Flash::success(__('messages.deleted', ['model' => __('users.singular')]));

        return redirect()->back();

        return redirect()->back();

    }

    public function fetchCountry(Request $request)
    {

        $data['country'] = Countries::where("phonecode",'=',$request->country_code)->get(["name", "id"]);

      $data['states'] = State::where("country_id",'=',$data['country'][0]['id'])->get(["name", "id"]);
        return response()->json($data);
    }
    public function fetchPhoneCode(Request $request)
    {

        $data['country'] = Countries::where("id",'=',$request->country_id)->get(["name", "phonecode"]);

      $data['states'] = State::where("country_id",'=',$data['country'][0]['id'])->get(["name", "id"]);
        return response()->json($data);
    }
     public function fetchState(Request $request)
    {

        $data['country'] = Countries::where("id",'=',$request->country_id)->get(["name", "phonecode"]);

        $data['states'] = State::where("country_id",'=',$request->country_id)->get(["name", "id"]);


        return response()->json($data);
    }

    public function fetchCity(Request $request)
    {
        $data['cities'] = City::where("state_id",'=',$request->state_id)->get(["name", "id"]);
        return response()->json($data);
    }
}
