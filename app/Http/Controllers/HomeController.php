<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBuildingRequest;
use App\Models\Game;
use App\Models\Person;
use App\Models\Sdggoals;
use App\Models\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Mapper;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		// set language
		$user_preferences = auth()->user()->preferences;
		if($user_preferences){
			$p = json_decode($user_preferences->preferences);
			if($p->locale != $request->session()->get('locale')){
				return redirect('/setLang/'.$p->locale);
			}
		}
		
        $userid =  auth()->user()->id;
        $token = session('game_token');
        if (!$token)
        {
            return back();
        }
//              // Dummy Data
//            $response = \Illuminate\Support\Facades\Http::withOptions([
//                'verify' => false,
//            ])->withHeaders([
//                'Authorization' => '61e678d17ca3af16a33271cf'
//            ])->get(env('REALLIVES_API_URL') . '/dashboard/getSchoolGameplayData/19');


       // return $token;
//   $response = \Illuminate\Support\Facades\Http::withOptions([
//                'verify' => false,
//            ])->withHeaders([
//                'Authorization' => $token
//            ])->get(env('REALLIVES_API_URL') . '/dashboard/getSchoolGameplayData/'.$userid);


        $response = \Illuminate\Support\Facades\Http::withOptions([
            'verify' => false,
        ])->withHeaders([
            'Authorization' => $token
        ])->get(env('REALLIVES_API_URL') . '/dashboard/getUserGameplayData/'.$userid);





        $value = json_decode($response->body(),true);

            if ( array_key_exists("status",$value))
            {
                Auth::guard('web')->logout();

                $request->session()->invalidate();

                $request->session()->regenerateToken();

                return redirect('/login');

            }

            if(isset($value) && count($value['countriesCoveredLatLng']) > 0)
            {
                \Mapper::map($value['countriesCoveredLatLng'][0]['lat'],$value['countriesCoveredLatLng'][0]['lng'],['zoom' => 2]);
            }else{
                \Mapper::map(52.381128999999990000, 0.470085000000040000,['zoom' => 2]);

            }
            // $bbc->marker(17.060816, -61.796428);
            foreach ($value['countriesCoveredLatLng'] as $bb)
            {

                if (array_key_exists("lat",$bb))
                {
                    \Mapper::marker($bb['lat'], $bb['lng']);
                }


            }

             //return $value;
            return view('gamedata/UserGameplay',['value' => $value]);


    }



    public function playgame(Request $request)
    {
        if ($request->session()->has('game_token')) {

            $user = auth()->user();
            $user->gameplaycount ++;
            $user->save();

            return redirect()->away(env('REALLIVES_GAME_URL').'?token='.$request->session()->get('game_token'));
        }

       return  redirect()->back();
    }
    public function educational_tool(Request $request)
    {
        if ($request->session()->has('game_token')) {

            $user = auth()->user();

            return redirect()->away(env('REALLIVES_GAME_URL').'/learningToolPhp?token='.$request->session()->get('game_token'));
        }

       return  redirect()->back();
    }
    public function sdgTool(Request $request)
    {
        if ($request->session()->has('game_token')) {

            $user = auth()->user();
            return redirect()->away(env('REALLIVES_GAME_URL').'/sdgLearningToolPhp?token='.$request->session()->get('game_token'));
        }

       return  redirect()->back();
    }

    public function countryGroupTool(Request $request)
    {
        if ($request->session()->has('game_token')) {

            $user = auth()->user();
            return redirect()->away(env('REALLIVES_GAME_URL').'/countryGroupLearningToolPhp?token='.$request->session()->get('game_token'));
        }

       return  redirect()->back();
    }
    public function countryDisaparityTool(Request $request)
    {
        if ($request->session()->has('game_token')) {

            $user = auth()->user();
            return redirect()->away(env('REALLIVES_GAME_URL').'/countryDisaparityLearningToolPhp?token='.$request->session()->get('game_token'));
        }

       return  redirect()->back();
    }


    public function index2(Request $request)
    {
        $user = auth()->user();

//        if ($user->drole == 'spanishtranslator') {
//            return redirect('translations?lang=es&type=event');
//        }
//
//        if ($user->drole == 'koreantranslator') {
//            return redirect('translations?lang=ko&type=event');
//        }


        //Send to Purchase
//        if ($user->payment_status == NULL) {
//
//            if ($user->type == 'organisation') {
//                return redirect()->action(
//                    'PurchaseController@purchaseOrganisation'
//                );
//            }
//            if ($user->type == 'school') {
//                return redirect()->action(
//                    'PurchaseController@purchaseSchool'
//                );
//            }
//            if ($user->type == 'gamer') {
//                return redirect()->action(
//                    'PurchaseController@purchaseGamer'
//                );
//            }
//        }

        // if user is gamer / Student / Gameplay send game data
        if ($user->drole == 'Gamer' || $user->drole == 'Student'  || $request->segment(1) == 'gameplay') {
            $persons = (new GameController)->GetUserData($user)->toArray();
        } else {

            if ($user->drole == 'Teacher' || $user->drole == 'School Admin') {
                $schoolUsers = User::where(['school_schid' => auth()->user()->school_schid])->get(['id'])->toArray();
                $schoolUserIds = array_column($schoolUsers, 'id');
                $persons = Person::whereIn('registered_player_id', $schoolUserIds)->get(['game_id', 'full_name', 'age', 'sex', 'country', 'religion', 'blood_donation_count', 'dead', 'organ_donation'])->toArray();
            }

            if ($user->drole == 'Organization Admin') {
                $orgUsers = User::where(['organization_orgid' => auth()->user()->organization_orgid])->get(['id'])->toArray();
                $orgUserIds = array_column($orgUsers, 'id');
                $persons = Person::whereIn('registered_player_id', $orgUserIds)->get(['game_id', 'full_name', 'age', 'sex', 'country', 'religion', 'blood_donation_count', 'dead', 'organ_donation'])->toArray();
            }

        }
        //dd($persons);

        $livesMale = 0;
        $livesFemale = 0;
        $livesInProgress = 0;
        $livesCompleted = 0;
        $countriesCovered = array();
        $countries = array();
        $game_ids = array();
        $organDonated['Heart'] = 0;
        $organDonated['Liver'] = 0;
        $organDonated['Eyes'] = 0;
        $organDonated['Kidney'] = 0;
        $organDonated['Skin'] = 0;
        $organDonated['Lungs'] = 0;
        $organDonated['Blood'] = 0;
        $obituaryCount = 0;
        $letterCount = 0;
        $sdgCommentCount = 0;

        $markerStr = '';
        $livesCount = count($persons);
       // Mapper::map($persons[0]['country']['lat'], $persons[0]['country']['lng']);
        if ($livesCount > 0) {

            //$map = Mapper::map($persons[0]['country']['lat'], $persons[0]['country']['lng']);



           Mapper::map(52.381128999999990000, 0.470085000000040000)->marker(17.060816, -61.796428);
           //Mapper::map(52.381128999999990000, 0.470085000000040000)->marker(17.060816, -61.796428)->marker(15.454166, 18.732207)->marker(-1.831239, -78.183406);

            //dd($game_ids);

            // If there are multiple persons (Teacher / School Admin / Organization Admin) Increment organ donation count
            foreach ($persons as $person) {
               // $map->marker($person['country']['lat'], $person['country']['lng']);
               // Mapper::marker(53.381128999999990000, -1.470085000000040000);

                $organDonated['Blood'] += $person['blood_donation_count'];

                foreach ($person['organ_donation'] as $personOrgan) {

                    if ($personOrgan['organName'] == "Heart" && $personOrgan['organStatus']) {
                        $organDonated['Heart']++;
                    }
                    if ($personOrgan['organName'] == "Liver" && $personOrgan['organStatus']) {
                        $organDonated['Liver']++;
                    }
                    if ($personOrgan['organName'] == "Eyes" && $personOrgan['organStatus']) {
                        $organDonated['Eyes']++;
                    }
                    if ($personOrgan['organName'] == "Kidney" && $personOrgan['organStatus']) {
                        $organDonated['Kidney']++;
                    }
                    if ($personOrgan['organName'] == "Skin" && $personOrgan['organStatus']) {
                        $organDonated['Skin']++;
                    }
                    if ($personOrgan['organName'] == "Lungs" && $personOrgan['organStatus']) {
                        $organDonated['Lungs']++;
                    }
                }

                $game_ids[] = $person['game_id'];
                $countries[] = $person['country']['country'];

                if ($person['dead']) {
                    $livesCompleted++;
                } else {
                    $livesInProgress++;
                }

                if ($person['sex'] == 'M') {
                    $livesMale++;
                } else {
                    $livesFemale++;
                }
            }

        }else{
            Mapper::map(52.381128999999990000, 0.470085000000040000);
        }

        if (!empty($countries)) {
            $countriesCovered = count(array_count_values($countries));
        } else {
            $countriesCovered = 0;
        }
        //dd($countries);

      // Mapper::map(52.381128999999990000, 0.470085000000040000)->marker(17.060816, -61.796428)->marker(15.454166, 18.732207)->marker(-1.831239, -78.183406);

        $sdgIds = Game::whereIn('game_id', $game_ids)->where(['user_id' => auth()->user()->id])->get(['sdg_id', 'my_sdg_comments'])->toArray();
        //dd($sdgIds);

        $sdgsSelected = array_count_values(array_column($sdgIds, 'sdg_id'));
        //dd($sdgsSelected);

        $sdgs = Sdggoals::get(['SDG_Id', 'SDG_name'])->toArray();
        //dd($sdgs);

        //$obituaryCount = My_obituary::whereIn('game_id', $game_ids)->get()->count();
        //dd($obituaryCount);

        $user = User::find($user->id);
        $obituaries = (new GameController)->GetObituaries($user)->gatherData();
        foreach ($obituaries['obituaries'] as $obituary) {
            foreach ($obituary['my_obituary'] as $myObituary) {
                if (!is_array($myObituary)) {
                    $obituaryCount++;
                }
            }
        }
        //dd($obituaryCount);

        $letters = (new GameController)->GetLetters($user)->gatherData();
        foreach ($letters['letters'] as $letter) {
            if (!empty($letter['my_letter'])) {
                $letterCount++;
            }
        }
        //dd($letterCount);

        $sdgComments = (new GameController)->SdgComments($user)->gatherData();
        foreach ($sdgComments['sdgComments'] as $sdgComment) {
            if (!empty($sdgComment['my_sdg_comments'])) {
                $sdgCommentCount = $sdgCommentCount + count($sdgComment['my_sdg_comments']);
            }
            // echo '<pre>';
            // var_dump($sdgComment['my_sdg_comments']);
            // echo '</pre>';
        }
        //dd($sdgCommentCount);

        if ($user->drole == 'Gamer') {
            return view('dashboards/dashboard-gamer', compact('organDonated', 'sdgsSelected', 'sdgs', 'livesCompleted', 'livesInProgress', 'livesMale', 'livesFemale', 'countriesCovered', 'obituaryCount', 'letterCount', 'sdgCommentCount'));
        }
        if ($user->drole == 'Student') {
            return view('dashboards/dashboard-student', compact('organDonated', 'sdgsSelected', 'sdgs', 'livesCompleted', 'livesInProgress', 'livesMale', 'livesFemale', 'countriesCovered', 'obituaryCount', 'letterCount', 'sdgCommentCount'));
        }
        if ($user->drole == 'Teacher') {
            return view('dashboards/dashboard-teacher', compact('organDonated', 'sdgsSelected', 'sdgs', 'livesCompleted', 'livesInProgress', 'livesMale', 'livesFemale', 'countriesCovered', 'obituaryCount', 'letterCount', 'sdgCommentCount'));
        }
        if ($user->drole == 'School Admin') {
            return view('dashboards/dashboard-schooladmin', compact('organDonated', 'sdgsSelected', 'sdgs', 'livesCompleted', 'livesInProgress', 'livesMale', 'livesFemale', 'countriesCovered', 'obituaryCount', 'letterCount', 'sdgCommentCount'));
        }
        if ($user->drole == 'Organization Admin') {
            //dd($sdgCommentCount);
            return view('dashboards/dashboard-orgadmin', compact('organDonated', 'sdgsSelected', 'sdgs', 'livesCompleted', 'livesInProgress', 'livesMale', 'livesFemale', 'countriesCovered', 'obituaryCount', 'letterCount', 'sdgCommentCount'));

           // return view('gamedata/dashboard-orgadmin', compact('organDonated', 'sdgsSelected', 'sdgs', 'livesCompleted', 'livesInProgress', 'livesMale', 'livesFemale', 'countriesCovered', 'obituaryCount', 'letterCount', 'sdgCommentCount'));
        }

        //return view('home');
    }

    public function createbuilding(CreateBuildingRequest $request)
    {
        $input = $request->all();

        /** @var Building $building */
        $building = Building::create($input);

        Flash::success('Building saved successfully.');

        return redirect(route('company'));
    }
}
