<?php

namespace App\Http\Controllers;

use App\DataTables\ProductDataTable;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\License;
use App\Models\Product;
use App\Models\Order;
use Flash;
use Illuminate\Http\Request;


class PurchaseController extends AppBaseController
{
    /**
     * Display a listing of the Product.
     *
     * @param ProductDataTable $productDataTable
     * @return Response
     */
    
    public function index(ProductDataTable $productDataTable)
    {
        return $productDataTable->render('products.index');
    }
    
    public function show_license_list( )
    {
        
        $school= \App\Models\School::find(auth()->user()->school_schid);

        if(auth()->user()->drole == 'Gamer')
        {
            $products = Product::where('user_type','=','Gamer')->where('suspended','=','No')->get();
        }
        elseif(!empty($school) && $school->school_type == 'family'){
            $products = Product::where('user_type','=','Family')->where('suspended','=','No')->get();
        }
        elseif(!empty($school) && $school->school_type == 'university_dept'){
            $products = Product::where('user_type','=','University_dept')->where('suspended','=','No')->get();
        }
        else{

          $schoolid =  session('school_schid');

          if ($schoolid == null){
            $org= \App\Models\Organization::find(auth()->user()->organization_orgid);

            if($org->org_type == 'organization'){
                \Flash::error(__('orders.please select a school'),['model' => __('schools.singular')]);
            }
            else{
                \Flash::error(__('orders.please select a department'),['model' => __('schools.singular')]);
            }


              return redirect('/schools');
          }

            $products = Product::where('user_type','=','School')->where('suspended','=','No')->get();
        }


        return  view('licenses/purchase_license_list')->with('products',$products);
    }

    public function renewlicense(Request $request)
    {
        $lic = $request->get('lic');

        $license = Order::find($lic);
        // echo $license;
        if($license)
        {
            $product = Product::where( 'parent_id','=',$license->product_prodid)->first();

            return  view('licenses/renewlicense')->with('product',$product)->with('license',$license);

        }


        //return redirect()->back();
    }

    public function purchase_license_form(Request $request)
    {

        $bb = $request->get('productid');


        $products = Product::find($bb);
        return  view('licenses/purchase_license_list')->with('product',$products);

    }





    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $input = $request->all();

        /** @var Product $product */
        $product = Product::create($input);

        Flash::success(__('messages.saved', ['model' => __('products.singular')]));

        return redirect(route('products.index'));
    }

    /**
     * Display the specified Product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Product $product */
        $product = Product::find($id);

        if (empty($product)) {
            Flash::error(__('products.singular').' '.__('messages.not_found'));

            return redirect(route('products.index'));
        }

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Product $product */
        $product = Product::find($id);

        if (empty($product)) {
            Flash::error(__('messages.not_found', ['model' => __('products.singular')]));

            return redirect(route('products.index'));
        }

        return view('products.edit')->with('product', $product);
    }

    /**
     * Update the specified Product in storage.
     *
     * @param  int              $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductRequest $request)
    {
        /** @var Product $product */
        $product = Product::find($id);

        if (empty($product)) {
            Flash::error(__('messages.not_found', ['model' => __('products.singular')]));

            return redirect(route('products.index'));
        }

        $product->fill($request->all());
        $product->save();

        Flash::success(__('messages.updated', ['model' => __('products.singular')]));

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Product $product */
        $product = Product::find($id);

        if (empty($product)) {
            Flash::error(__('messages.not_found', ['model' => __('products.singular')]));

            return redirect(route('products.index'));
        }

        $product->delete();

        Flash::success(__('messages.deleted', ['model' => __('products.singular')]));

        return redirect(route('products.index'));
    }
    public function purchase_details(Request $request)
    {
       $id=$request->get('id');

     $order = Order::where('paypal_orderid',$id)->get();

     return  view('orders/purchase_details')->with('order',$order);
    }

}
