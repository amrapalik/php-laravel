<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Response;
use App\Models\Translation;
use Log;

class TranslationController extends RaviController
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->_tablename = 'Translations';
        $this->_viewname = 'translation';
    }

    public function list(Request $request)
    {   
        $langs = ['en' => 'English', 'ko' => 'Korean', 'es' => 'Spanish'];
        if( array_key_exists(Auth::user()->city, $langs) ) {
            $langs = [ Auth::user()->city => $langs[Auth::user()->city] ];
        }
        $type = $request->input('type', 'event');
        $lang = $request->input('lang', array_key_first($langs) );
        $events = DB::connection('mongodb')->collection('Translation')->distinct('type')->get();
       
        
        //echo '<pre>'; print_r($events); exit;
    	//$contacts = Contact::where('org_id', Auth::user()->org_id)->take(10)->get();
        return view($this->_viewname.'.list')->with(['type' => $type, 'lang' => $lang, 'types' => $events, 'langs' => $langs ]);
    }


    public function ajaxlist(Request $request)
    {
        $type = $request->input('type', 'event');
        $lang = $request->input('lang', 'en');
        $events = DB::connection('mongodb')->collection('Translation')->where('type', $type)->where('source_string', '<>', '')->where('lang_code', $lang)->select('source_string', 'translated_string', 'lang_code', 'identifier', 'is_approved')->get();
        return response()->json(['events' => $events]);
    }

    public function postsaverow(Request $request)
    {
        $post = $request->input();
        DB::connection('mongodb')->collection('Translation')->where('_id', $post['data']['_id']['$oid'])->update(['translated_string' => $post['data']['translated_string'], 'is_approved' => $post['data']['is_approved'] == 'true' ? true : false ]);
        if($post['data']['lang_code'] == 'en') {
            DB::connection('mongodb')->collection('Translation')->where('identifier', $post['data']['identifier'])->where('lang_code', 'ko')->update(['source_string' => $post['data']['translated_string'], 'to_translate' => true ]);
            DB::connection('mongodb')->collection('Translation')->where('identifier', $post['data']['identifier'])->where('lang_code', 'es')->update(['source_string' => $post['data']['translated_string'], 'to_translate' => true ]);
        }
        return response()->json(['status' => 'success']);
    }


    public function eventlist(Request $request)
    {   
        $type = $request->input('type', 'event');
        $lang = $request->input('lang', 'en');
        //$contacts = Contact::where('org_id', Auth::user()->org_id)->take(10)->get();
        return view($this->_viewname.'.eventlist')->with(['type' => $type, 'lang' => $lang]);
    }

    public function eventajaxlist(Request $request)
    {
        $type = $request->input('type', 'event');
        $lang = $request->input('lang', 'en');
        $translations = DB::connection('mongodb')->collection('Translation')
                    ->where('lang_code', 'en')
                    ->wherein('type', ['event', 'factroid', 'dairy_addline'])
                    ->select('type', 'identifier', 'translated_string')
                    ->get();
        $trans = [];
        foreach($translations as $translation){
            $trans[ $translation['identifier'] ] = $translation['translated_string'];
        }

        //echo '<pre>'; print_r($trans); exit;
        // [ $translation['type'] ] 

        $events = DB::connection('mongodb')->collection('Events')
                    ->select('id', 'name', 'lang_code', 'factroid', 'dairy_addline')
                    ->get();
        $evts = [];
        foreach($events as $event) {
            //echo '<pre>'; print_r($event['name']); exit;
            $evts[] = ['name' => array_key_exists('name', $event) ? $event['name']:'',
                'lang_code' => array_key_exists('lang_code', $event) ? $event['lang_code']:'',
                'event_str' => array_key_exists($event['lang_code'], $trans) ? $trans[ $event['lang_code'] ] : '',
                'factroid'  => $event['factroid'],
                'factroid_str' => array_key_exists($event['factroid'], $trans) ? $trans[ $event['factroid'] ] : '',
                'dairy_addline'  => $event['dairy_addline'],
                'dairy_addline_str' => array_key_exists($event['dairy_addline'], $trans) ? $trans[ $event['dairy_addline'] ] : ''
            ];
        }
        //echo '<pre>'; print_r($evts); exit;
        return response()->json(['events' => $evts]);
    }


    public function postsaveeventrow(Request $request)
    {
        $post = $request->input('data');
        $factoid = DB::connection('mongodb')->collection('Translation')
                    ->where('lang_code', 'en')
                    ->where('identifier', $post['factroid'])
                    ->select('identifier', 'translated_string')
                    ->first();
        if($factoid) {
            if($factoid['translated_string'] != $post['factroid_str']) {
                DB::connection('mongodb')->collection('Translation')
                    ->where('lang_code', 'en')
                    ->where('identifier', $post['factroid'])
                    ->update(['translated_string' => $post['factroid_str'], 'source_string' => $post['factroid_str'] ]);
                DB::connection('mongodb')->collection('Translation')
                        ->where('identifier', $post['factroid'])
                        ->where('lang_code', '!=', 'en')
                        ->update(['source_string' => $post['factroid_str'], 'to_translate' => true ]);
            }
        } else {
            DB::connection('mongodb')->collection('Translation')
                    ->updateOrInsert(['lang_code' => 'en', 'identifier' => $post['factroid']], ['translated_string' => $post['factroid_str'], 'source_string' => $post['factroid_str'] ]);

            DB::connection('mongodb')->collection('Translation')
                    ->updateOrInsert(['lang_code' => 'ko', 'identifier' => $post['factroid']], ['source_string' => $post['factroid_str'], 'to_translate' => true ]);
            DB::connection('mongodb')->collection('Translation')
                    ->updateOrInsert(['lang_code' => 'es', 'identifier' => $post['factroid']], ['source_string' => $post['factroid_str'], 'to_translate' => true ]);
        }


        $dairy_addline = DB::connection('mongodb')->collection('Translation')
                    ->where('lang_code', 'en')
                    ->where('identifier', $post['dairy_addline'])
                    ->select('identifier', 'translated_string')
                    ->first();
        if($dairy_addline && $dairy_addline['translated_string'] != $post['dairy_addline_str']) {
            DB::connection('mongodb')->collection('Translation')
                    ->where('lang_code', 'en')
                    ->where('identifier', $post['dairy_addline'])
                    ->update(['translated_string' => $post['dairy_addline_str'], 'source_string' => $post['dairy_addline_str'] ]);
            DB::connection('mongodb')->collection('Translation')
                    ->where('identifier', $post['dairy_addline'])
                    ->where('lang_code', '!=', 'en')
                    ->update(['source_string' => $post['dairy_addline_str'], 'to_translate' => true ]);
        }

        return response()->json(['status' => 'success']);
    }
    function postgetexample(Request $request){
        $post = $request->input('identifier');
       
        $examples = DB::connection('mongodb')->collection('Game_Events')
        ->where('lang_code',$post)
        ->distinct('text')
        ->get();
        $response="";
        $count=0;
        foreach ($examples as $key => $value) {
            if (!preg_match('%^[ -~0-9]+$%', $value)){
            }else{
                $count++;
                $response.= $value."<br>----<br>";
            }
            if ($count == 5){
                break;
            }
        }
        if (empty($response)){
            $response = "No examples available";
        }
        return response()->json(['examples' => $response]);
    }
}