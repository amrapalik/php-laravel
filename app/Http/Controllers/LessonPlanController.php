<?php

namespace App\Http\Controllers;

use App\DataTables\LessonPlanDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateLessonPlanRequest;
use App\Http\Requests\UpdateLessonPlanRequest;
use App\Models\Classes;
use App\Models\LessonComment;
use App\Models\LessonPlan;
use App\Models\Assignment;
use App\Models\School;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Image;
use PDF;
class LessonPlanController extends AppBaseController
{
	public function __construct(){
		$this->middleware('school_user', ['only'=>['show']]);
		$this->middleware('school_staff', ['only'=>['create']]);
	}
    /**
     * Display a listing of the LessonPlan.
     *
     * @param LessonPlanDataTable $lessonPlanDataTable
     * @return Response
     */
    public function index(LessonPlanDataTable $lessonPlanDataTable, Request $request)
    {
            return $lessonPlanDataTable->render('lesson_plans.index');
    }

    /**
     * Show the form for creating a new LessonPlan.
     *
     * @return Response
     */
    public function create(Request $request)
    {
			$assignment = Assignment::find($request->asid);
            return view('lesson_plans.create',['assignment'=>$assignment]);

    }

    /**
     * Store a newly created LessonPlan in storage.
     *
     * @param CreateLessonPlanRequest $request
     *
     * @return Response
     */
    public function store(CreateLessonPlanRequest $request)
    {

       // get_school_id();

        $input = $request->all();

        /** @var LessonPlan $lessonPlan */

        $input['image'] = ($request->hasFile("image") ? $request->image->getClientOriginalName() : NULL);
        $input['created_by'] = auth()->user()->id;
        $input['school_schid'] = auth()->user()->school_schid;

        $lessonPlan = LessonPlan::create($input);

        if ($request->hasFile("image")) {
            request()->validate([
                'image' => 'mimes:jpeg,jpg,png,gif,svg|max:2048',
            ]);

            $this->uploadImage($request->image);
        }

        $this->createPDF($lessonPlan);
        Flash::success(__('messages.saved', ['model' => __('lessonPlans.singular')]));

		if(!empty($input['assignment_id'])){
        	return redirect(route('lessonPlansWithAssignment'));
		}
		else{
        	return redirect(route('lessonPlans.index'));
		}
    }

    public function downloadPdf($pdfName)
    {
        $file_path = public_path('/storage/lesson_plans/pdfs/' . $pdfName);
        return response()->download($file_path);
    }

    public function saveComment(Request $request)
    {
        $input = $request->all();



        LessonComment::create([
            'lesson_plan_id' => $input['lesson_plan_id'],
            'comment' => $input['comment'],
            'added_by' => auth()->user()->id,
           // 'replied_to' => $input['replied_to'],
        ]);


        Flash::success(__('messages.saved', ['model' => __('lessonPlans.singular')]));
        return redirect()->back()->with('success', 'Comment added successfully.');
        return redirect(route('lessonPlans.index'));
    }

    public function uploadImage($image)
    {
        $filename = $image->getClientOriginalName();
        $destinationPath = public_path('/storage/lesson_plans');
        $thumb_img = Image::make($image->getRealPath())->widen(500);
        $thumb_img->save($destinationPath . '/' . $filename, 100);
    }

    public function deleteOldImage($id)
    {
        $delImage = LessonPlan::where(['id' => $id])->get('image')->first();
        if ($delImage['image'] != '') {
            Storage::delete('/public/lesson_plans/' . $delImage['image']);
        }
    }



    public function createPDF($lessonPlan)
    {
        //dd($lessonPlan);


      //  $school = School::where(['schid' => get_school_id()])->get('name')->first();

        $school = School::find(get_school_id());

      //  dd($school);
        //TODO:: Check solution



        $data = [
            'schoolName' => $school->sch_name,
            'teacherName' => AUTH()->user()->name,
            'teacherEmail' => AUTH()->user()->email,
            'lessonPlanName' => $lessonPlan['name'],
            'grades' => $lessonPlan['grades'],
            'standards' => $lessonPlan['standards'],
            'overview' => $lessonPlan['overview'],
            'objectives' => $lessonPlan['objectives'],
            'procedures' => $lessonPlan['procedures'],
            'assessment' => $lessonPlan['assessment'],
            'extension_activities' => $lessonPlan['extension_activities'],
            'youtube_link' => $lessonPlan['youtube_link'],
            'image' => (isset($lessonPlan['image']) ? $lessonPlan['image'] : $lessonPlan['oldImage']),
        ];


        $pdf = PDF::loadView('lesson-plan-pdf', $data);

        //return $pdf->download('lessonPlan.pdf');
        $savePath = public_path('/storage/lesson_plans/pdfs');
        $pdfname = str_replace(' ', '_', $data['lessonPlanName']) . '_' . $lessonPlan['id'] . '.pdf';
        $pdf->save($savePath . '/' . $pdfname);
        //$planpdf = $pdf->stream('lessonPlan.pdf');

        LessonPlan::where(['lpid' => $lessonPlan['lpid']])->update(['pdf' => $pdfname]);
    }



    /**
     * Display the specified LessonPlan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

        /** @var LessonPlan $lessonPlan */
        //$lessonPlan = LessonPlan::find($id); 

        //$comments = LessonComment::find($lessonPlanId);
        if ($id) {


            $comments = DB::table('lesson_comments as lc')
                ->leftjoin('users as u', 'lc.added_by', '=', 'u.id')
                ->where('lc.lesson_plan_id', '=', $id)
                ->orderBy('lc.created_at',  'DESC')
                ->get(['lc.*', 'u.name', 'u.firstname','u.gender', 'u.lastname', 'u.email', 'u.avatar', 'u.drole']);

            $lessonPlan = DB::table('lesson_plans as lp')
                ->leftjoin('users as u', 'lp.created_by', '=', 'u.id')
                ->leftjoin('schools as s', 's.schid', '=', 'u.school_schid')
                ->where('lp.lpid', '=', $id)
                ->get(['lp.*', 'u.name as teacherName', 'u.gender', 'u.email as teacherEmail', 's.sch_name as schoolName'])->first();

			$lesson_plan = LessonPlan::find($id);
            return view('lesson_plans.show', compact('comments', 'lessonPlan', 'lesson_plan'));
        }

        if (empty($lessonPlan)) {
            Flash::error(__('lessonPlans.singular').' '.__('messages.not_found'));

            return redirect(route('lessonPlans.index'));
        }

        return view('lesson_plans.show')->with('lessonPlan', $lessonPlan);
    }

    /**
     * Show the form for editing the specified LessonPlan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var LessonPlan $lessonPlan */
        $lessonPlan = LessonPlan::find($id);

        if (empty($lessonPlan)) {
            Flash::error(__('messages.not_found', ['model' => __('lessonPlans.singular')]));

            return redirect(route('lessonPlans.index'));
        }

        return view('lesson_plans.edit')->with('lessonPlan', $lessonPlan);
    }

    /**
     * Update the specified LessonPlan in storage.
     *
     * @param  int              $id
     * @param UpdateLessonPlanRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLessonPlanRequest $request)
    {
        /** @var LessonPlan $lessonPlan */
        $lessonPlan = LessonPlan::find($id);

        if (empty($lessonPlan)) {
            Flash::error(__('messages.not_found', ['model' => __('lessonPlans.singular')]));

            return redirect(route('lessonPlans.index'));
        }

        $lessonPlan->fill($request->all());
        $lessonPlan->save();

        Flash::success(__('messages.updated', ['model' => __('lessonPlans.singular')]));

        return redirect(route('lessonPlans.index'));
    }

    /**
     * Remove the specified LessonPlan from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var LessonPlan $lessonPlan */
        $lessonPlan = LessonPlan::find($id);

        if (empty($lessonPlan)) {
            Flash::error(__('messages.not_found', ['model' => __('lessonPlans.singular')]));

            return redirect(route('lessonPlans.index'));
        }

        $lessonPlan->delete();

        Flash::success(__('messages.deleted', ['model' => __('lessonPlans.singular')]));

        return redirect(route('lessonPlans.index'));
    }
}
