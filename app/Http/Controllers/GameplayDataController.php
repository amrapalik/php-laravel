<?php

namespace App\Http\Controllers;

use App\FeedbackBugReport;
use App\Person;
use App\Models\User;
use App\My_obituary;
use App\Game;
use App\Sdggoals;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GameplayDataController extends Controller
{



    public function index()
    {
        //
    }

    public function obituaries()
    {
        $value = $this->getGamedata(env('REALLIVES_API_URL') . '/dashboard/getObituaryData/');
        return view('gamedata/obituarydata', ['value' => $value]);
    }

    public function letters()
    {
        $value = $this->getGamedata(env('REALLIVES_API_URL') . '/dashboard/getLetterData/');
        return view('gamedata/letterdata', ['value' => $value]);
    }

    public function SdgComment()
    {
        $value = $this->getGamedata(env('REALLIVES_API_URL') . '/dashboard/getSdgCommentData/');
        return view('gamedata/SdgCommentData', ['value' => $value]);
    }
//
    public function BornReligion()
    {
        $value = $this->getGamedata(env('REALLIVES_API_URL') . '/dashboard/getBornReligion/');
           return view('gamedata/BornReligion', ['value' => $value]);
    }

    public function SdgData()
    {
        $value = $this->getGamedata(env('REALLIVES_API_URL') . '/dashboard/getSdgData/');
         return view('gamedata/SdgData', ['value' => $value]);
    }

    public function BloodDonation()
    {
        $value = $this->getGamedata(env('REALLIVES_API_URL') . '/dashboard/getBloodDonationData/');
        return view('gamedata/BloodDonation', ['value' => $value]);
    }

    public function OrganDonation()
    {
        $value = $this->getGamedata(env('REALLIVES_API_URL') . '/dashboard/getOrganDonationData/');
        return view('gamedata/OrganDonation', ['value' => $value]);
    }

    public function FeedbackData()
    {
        $value = $this->getGamedata(env('REALLIVES_API_URL') . '/dashboard/getFeedbackData/');
        return view('gamedata/Feedback', ['value' => $value]);
    }

    public function BugReportData()
    {
        $value = $this->getGamedata(env('REALLIVES_API_URL') . '/dashboard/getbugReportData/');
        return view('gamedata/BugReport', ['value' => $value]);
    }

    public function SchoolGameplay()
    {

        $schoolid = get_school_id();

        $token = session('game_token');
        if (!$token)
        {
            return back();
        }

//        $response = \Http::withOptions([
//            'verify' => false,
//        ])->withHeaders([
//            'Authorization' => '61e678d17ca3af16a33271cf'
//        ])->get(env('REALLIVES_API_URL') . '/dashboard/getSchoolGameplayData/19');

        $response = \Http::withOptions([
            'verify' => false,
        ])->withHeaders([
            'Authorization' => $token
        ])->get(env('REALLIVES_API_URL') . '/dashboard/getSchoolGameplayData/'.$schoolid);


        $value = json_decode($response->body(), true);

        if (count($value['countriesCoveredLatLng']) > 0) {
            \Mapper::map($value['countriesCoveredLatLng'][0]['lat'], $value['countriesCoveredLatLng'][0]['lng'],['zoom' => 2]);
        } else {
            \Mapper::map(52.381128999999990000, 0.470085000000040000,['zoom' => 2]);
        }
        // $bbc->marker(17.060816, -61.796428);
        foreach ($value['countriesCoveredLatLng'] as $bb) {

            if (array_key_exists("lat", $bb)) {
                \Mapper::marker($bb['lat'], $bb['lng']);
            }
        }
        //return $value;
        return view('gamedata/SchoolGameplay', ['value' => $value]);
    }

    private function getGamedata($url)
    {   $userid = 0;// auth()->user()->id;
        $userid =  auth()->user()->id;
        $token = session('game_token');
        if (!$token)
        {
            return back();
        }
        $response =  \Http::withOptions([
            'verify' => false,
        ])->withHeaders([
            'Authorization' => $token
            //'Authorization' => '61e678d17ca3af16a33271cf'
        ])->get($url.$userid);
        return json_decode($response->body(), true);
    }


}
