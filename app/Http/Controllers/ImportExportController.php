<?php

namespace App\Http\Controllers;

use App\Imports\TeacherImport;
use App\Models\License;
use App\Models\School;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Imports\StudentImport;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;
use Maatwebsite\Excel\Facades\Excel;

class ImportExportController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function importExportView()
    {
        return view('students/import');
    }


    public function importTeachers()
    {
        return view('teachers/import');
    }



    /**
     * @return \Illuminate\Support\Collection
     */
    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */

    public function remove(Request $request)
    {
        User::where(['school_schid'=>14, 'drole'=>'Student']) ->update(['account_status'=>'Imported','deallocated'=>1]);


    }

    public function import2(Request $request)
    {

        // Student Activation process
        $schoolid = get_school_id();

        $school = \App\Models\School::find($schoolid);
        // Get all Imported students
        //$available_licenses = $school->available_licenses;



        // $licenses = License::where('school_schid' ,'=',$schoolid)->where('user_id','==',0)->whereDate('expiry_date', '>=', Carbon::now())->get();
        $licenses = License::where('school_schid' ,'=',$schoolid)->whereDate('expiry_date', '>=', Carbon::now())
                ->where('available_lives','>=',1)->where('user_id','==',0)->get();

        echo License::where('school_schid' ,'=',$schoolid)->whereDate('expiry_date', '>=', Carbon::now())
        ->where('available_lives','>=',1)->where('user_id','==',0)->get()->count();        

        $lic= [];


        $l_licences_available = 0;
        foreach ($licenses as $lic ) //CHANGE_MODULE : change required for new Licensing module
        {
            // $lic_count = $lic->available_lives;
            // $l_licences_available= $l_licences_available+ $lic->available_lives;
            $students = User::where(['school_schid'=>$schoolid, 'account_status'=>'Imported'])->get();

            foreach ($students as $student)
            {
    
                
                    $student->assignRole('Student');
                    $student->account_status = 'Active';
                    $student->deallocated = 0;  // Allocate Student
                    $l_licences_available --;
                    $student->purchaseflag = "Yes";
                    //TODO:: Add order id to user
                    $student->license_licid = $lic->licid;
                    $student->account_expiry_date= $lic->expiry_date;
                    $student->save();
                    // $lic_count --;
                    $lic->user_id = $student->id;
                    $lic->save();
                
               
            }

            // $lic->available_lives = $lic_count;
        }

        $students = User::where(['school_schid'=>$schoolid, 'account_status'=>'Imported'])->get();
        foreach ($students as $student)
        {
            $student->assignRole('Student');
            $student->account_status = 'Active';
            //TODO:: Add order id to user
            $student->save();
        }



        $licences_available = DB::table('licenses')->where('school_schid' ,'=',$schoolid)
            ->whereDate('expiry_date', '>=', Carbon::now())->sum('available_lives');


       $sch =  School::find($schoolid);



        $sch->available_licenses =$licences_available;
        $sch->save();

//        $available_licenses = 0;
//
//        echo $available_licenses . '<br>';
//        foreach ($students as $student)
//        {
//            $student->assignRole('Student');
//            $student->account_status = 'Active';
//
//            if($available_licenses > 0)
//            {
//                $student->account_status = 'Active';
//                $student->deallocated = 0;  // Allocate Student
//                $available_licenses --;
//                $student->purchaseflag = "Yes";
//                //TODO:: Add order id to user
//               // $student->order_ordid = $order->ordid;
//            }
//            $student->save();
//
//        }
        echo $l_licences_available;

    }





    public function import(Request $request)
    {
        //Excel::import(new UsersImport,request()->file('file'));
       $schoolid = get_school_id();

        $file = $request->file('file');
        $import = new StudentImport;
        $import->import($file);

        if ($import->failures()->isNotEmpty()){
           // dd('sdsf');
            return  back()->withFailures($import->failures());
        }

       // dd($import->errors());
        if ($import->errors()->isNotEmpty()){

        //   dd('errors');
        //    return  back()->withFailures($import->errors()->collect());
            return  back()->withErrors('Error');
        }

        // Student import process completes



    // Student Activation process started


        $school = \App\Models\School::find($schoolid);
        // Get all Imported students
      //  $licenses = License::where('school_schid' ,'=',$schoolid)->whereDate('expiry_date', '>=', Carbon::now())->get();


      

    echo User::where('school_schid','==',$schoolid)->where('account_status','==','Imported')->first();



        // $licences_available = License::where('school_schid', '=', $schoolid)->whereDate('expiry_date', '>=', Carbon::now())->sum('available_lives');


        // If licenses are available then apply licenses
        // if($licences_available > 0)
        // {
            // $licenses = License::where('school_schid','=', $schoolid)->where('available_lives', '>', 0)->whereDate('expiry_date', '>=', Carbon::now())->get();
            // $lic_count = $lic->available_lives;
            
            // Load all imported students
            // $students = User::where('school_schid','==',$schoolid)->where('account_status','==','Imported')->get();
            $students = User::where(['school_schid'=>$schoolid, 'drole'=>'Student','account_status'=>'Imported'])->get();

            foreach ($students as $student )
            {
                $licenses = License::where('school_schid','=',$schoolid)->whereDate('expiry_date', '>=', Carbon::now())
                ->where('user_id','==',0)->first();
                
            
                    if($licenses)
                    {
                        $student->assignRole('Student');
                        $student->account_status = 'Active';
                        $student->deallocated = 0;  // Allocate Student
                        $student->purchaseflag = "Yes";
                        $student->account_expiry_date = $licenses->expiry_date;
                        $student->license_licid = $licenses->licid;
                        $licenses->user_id = $student->id;
                        $licenses->save();
                        $student->save();
                    }
                    else{
                        $student->assignRole('Student');
                        $student->account_status = 'Active';
                        $student->save();
                    }
            

            }
        

        // if license is not present then deallocate those students
        // $students = User::where(['school_schid'=>$schoolid, 'drole'=>'Student','account_status'=>'Imported'])->get();
        // foreach ($students as $student)
        // {
        //     $student->assignRole('Student');
        //     $student->account_status = 'Active';
        //     $student->save();
        // }


        $total = License::where('school_schid', '=', $schoolid)->sum('available_lives');
        $school->available_licenses = $total;
        $school->save();

    // End Student activation process

       // dd($import->errors());
        Flash::success(__('Student Imported Successfully'));

        return back();
    }



    public function importTeachersStore(Request $request)
    {
        //Excel::import(new UsersImport,request()->file('file'));

        $file = $request->file('file');
        $import = new TeacherImport;
        $import->import($file);

        if ($import->failures()->isNotEmpty()){
            // dd('sdsf');
            return  back()->withFailures($import->failures());
        }

        // dd($import->errors());
        if ($import->errors()->isNotEmpty()){

            //   dd('errors');
            //    return  back()->withFailures($import->errors()->collect());
            return  back()->withErrors('Error');
        }


        // Assign Roles

        $schoolid = get_school_id();
        $teachers = User::where(['school_schid'=>$schoolid,'drole'=>'Teacher', 'account_status'=>'Imported'])->get();
        foreach ($teachers as $teacher)
        {
            $teacher->assignRole('Teacher');
            $teacher->account_status = 'Active';
            $teacher->save();
        }
        // dd($import->errors());
        Flash::success(__('Teacher Imported Successfully'));

        return back();
    }
}
