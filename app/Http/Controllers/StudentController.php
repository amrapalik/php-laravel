<?php

namespace App\Http\Controllers;

use App\DataTables\StudentDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Models\License;
use App\Models\Role;
use App\Models\School;
use App\Models\Student;
use App\Models\User;
use Carbon\Carbon;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use Response;

class StudentController extends AppBaseController
{
    /**
     * Display a listing of the Student.
     *
     * @param StudentDataTable $studentDataTable
     * @return Response
     */
    public function index(StudentDataTable $studentDataTable)
    {
        return $studentDataTable->render('students.index');
    }

    /**
     * Show the form for creating a new Student.
     *
     * @return Response
     */
    public function create()
    {
        return view('students.create');
    }

    /**
     * Store a newly created Student in storage.
     *
     * @param CreateStudentRequest $request
     *
     * @return Response
     */
    public function store(CreateStudentRequest $request) //CHANGE_MODULE : changes required for new licensing module
    {
        $input = $request->all();
         $input['school_schid']=session('school_schid');
         $input['username']      = preg_replace('/\s+/', '', strtolower($input['firstname']).strtolower($input['lastname']).(Student::max('id')+1));
         $input['account_status']= 'Active';
         $input['deallocated']= 1;
         $input['name']= ucwords($input['firstname']).' '. ucwords($input['lastname']);
         $input['password']= \Hash::make($input['password']);
        /** @var Student $student */


        $student = User::create($input);

        $schoolid = get_school_id();


        $licenses = License::where('school_schid' ,'=',$schoolid)->whereDate('expiry_date', '>=', Carbon::now())
            ->where('user_id','==',0)->first();

        if($licenses)
        {

            $student->account_status = 'Active';
            $student->deallocated = 0;  // Allocate Student
            $student->purchaseflag = "Yes";

            //TODO:: Add order id to user
            $student->license_licid = $licenses->licid;
            $student->account_expiry_date= $licenses->expiry_date;
            $licenses->user_id=$student->id;
            $student->save();

            // $licenses->available_lives = $licenses->available_lives - 1;

            $licenses->save();

            $licences_available = DB::table('licenses')->where('school_schid' ,'=',$schoolid)
                ->whereDate('expiry_date', '>=', Carbon::now())->sum('available_lives');

            $sch =  School::find($schoolid);
            $sch->available_licenses =$licences_available;
            $sch->save();


        }
       



       $user = User::find($student->id);

        $role =  \Spatie\Permission\Models\Role::where('name','=', 'Student')->first();


        $user->assignRole($role);

        Flash::success(__('messages.saved', ['model' => __('students.singular')]));

        return redirect(route('students.index'));
    }

    /**
     * Display the specified Student.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Student $student */
        $student = Student::find($id);

        if (empty($student)) {
            Flash::error(__('students.singular').' '.__('messages.not_found'));

            return redirect(route('students.index'));
        }

        return view('students.show')->with('student', $student);
    }

    /**
     * Show the form for editing the specified Student.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Student $student */
        $student = Student::find($id);

        if (empty($student)) {
            Flash::error(__('messages.not_found', ['model' => __('students.singular')]));

            return redirect(route('students.index'));
        }

        return view('students.edit')->with('student', $student);
    }

    /**
     * Update the specified Student in storage.
     *
     * @param  int              $id
     * @param UpdateStudentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStudentRequest $request)
    {
        /** @var Student $student */
        $student = Student::find($id);

        if (empty($student)) {
            Flash::error(__('messages.not_found', ['model' => __('students.singular')]));

            return redirect(route('students.index'));
        }
        $input = $request->except('drole');
        $input['name']= ucwords($input['firstname']).' '. ucwords($input['lastname']);

        //if password is blank remove
        if ($input['password'] != '' && $input['password'] != null)
        {
            $input['password']= \Hash::make('password');

        }else
        {
            unset($input['password']);
        }

        $student->fill($input);
        $student->save();


        Flash::success(__('messages.updated', ['model' => __('students.singular')]));

        return redirect(route('students.index'));
    }

    /**
     * Remove the specified Student from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Student $student */


        $user = User::find($id);

        if (empty($user)) {
            Flash::error(__('messages.not_found', ['model' => __('students.singular')]));

            return redirect(route('students.index'));
        }

        $user->deallocated = 1;
        $user->purchaseflag = "No";
        $user->save();


        $license = License::find($user->license_licid);
        $license->available_lives = $license->available_lives + 1;
        $license->save();

        $total = License::where('school_schid', '=', $user->school_schid)->sum('available_lives');

        $school = \App\Models\School::find($user->school_schid);

        $school->available_licenses = $total;
        $school->save();


        $user->delete();

        Flash::success(__('messages.deleted', ['model' => __('students.singular')]));

        return redirect(route('students.index'));
    }
}
