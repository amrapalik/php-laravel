<?php

namespace App\Http\Controllers;

use App\FeedbackBugReport;
use App\Person;
use App\Models\User;
use App\My_obituary;
use App\Game;
use App\Sdggoals;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function GetUserData(User $user)
    {
        // //$games = \App\Models\Game::where(['user_id' => Auth::user()->id])->get(['game_id']);


        // //dd($games);
        // $game_ids = array();
        // foreach ($games as $game) {
        //     $game_ids[] = $game['game_id'];
        // }
        // //dd($game_ids);
        // $obituaries = My_obituary::whereIn('game_id', $game_ids)->get();
        // //dd($obituaries);
        // return view('obituaries', compact('obituaries'));

        $persons = \App\Models\Person::where(['registered_player_id' => Auth::user()->id])->get(['game_id', 'full_name', 'age', 'sex', 'country', 'religion', 'blood_donation_count', 'dead', 'organ_donation']);
        //dd($persons);
        return $persons;
    }

    public function GetObituaries(User $user)
    {
        $persons = $this->GetUserData($user);

        $gameIds = array();
        foreach ($persons as $person) {
            $gameIds[] = $person['game_id'];
        }
        //dd($gameIds);
        $obituaries = \App\Models\Game::whereIn('game_id', $gameIds)->where(['user_id' => auth()->user()->id])->get(['game_id', 'my_obituary'])->toArray();

        //dd($obituaries);

        return view('obituaries', compact('persons', 'obituaries', 'gameIds'));
    }

    public function GetLetters(User $user)
    {
        $persons = $this->GetUserData($user);

        $game_ids = array();
        foreach ($persons as $person) {
            $game_ids[] = $person['game_id'];
        }

        $letters = \App\Models\Game::whereIn('game_id', $game_ids)->where(['user_id' => Auth::user()->id])->get(['game_id', 'my_letter'])->toArray();
        //dd($letters);

        return view('letters', compact('persons', 'letters', 'game_ids'));
    }

    public function SdgComments(User $user)
    {
        $persons = $this->GetUserData($user);

        $game_ids = array();
        foreach ($persons as $person) {
            $game_ids[] = $person['game_id'];
        }


        $sdgComments = \App\Models\Game::whereIn('game_id', $game_ids)->where(['user_id' => Auth::user()->id])->get(['game_id', 'my_sdg_comments']);
        //dd($sdgComments);
        return view('sdg-comments', compact('persons', 'sdgComments'));
    }

    public function ReligionsBornIn(User $user)
    {
        $persons = $this->GetUserData($user);

        return view('religions', compact('persons'));
    }

    public function SdgSelected(User $user)
    {
        $persons = $this->GetUserData($user)->toArray();
        //dd($persons);
        $game_ids = array();
        foreach ($persons as $person) {
            $game_ids[] = $person['game_id'];
        }

        $sdgIds = \App\Models\Game::whereIn('game_id', $game_ids)->where(['user_id' => Auth::user()->id])->get(['sdg_id', 'game_id'])->toArray();
        //dd($sdgIds);

        $sdgsSelected = array_column($sdgIds, 'sdg_id');

        $gameIds = array_column($sdgIds, 'game_id');
        //dd($gameIds);

        $finalArray = array();
        foreach ($persons as $arr) {
            $key = array_search($arr['game_id'], $gameIds);
            if ($key === false) {
                $key = array_search(0, $gameIds);
            }
            unset($sdgIds[$key]['game_id']);
            $finalArray[] = array_merge($arr, $sdgIds[$key]);
        }
        //dd($finalArray);

        $sdgs = Sdggoals::whereIn('SDG_Id', $sdgsSelected)->get(['SDG_Id', 'SDG_title'])->toArray();

        $newSdgsIds = array_column($sdgs, 'SDG_Id');

        $selectedSdgs = array();
        foreach ($finalArray as $arr) {
            $key = array_search($arr['sdg_id'], $newSdgsIds);
            if ($key === false) {
                $key = array_search(0, $newSdgsIds);
            }
            unset($sdgs[$key]['game_id']);
            $selectedSdgs[] = array_merge($arr, $sdgs[$key]);
        }
        //dd($selectedSdgs);

        return view('sdg-selected', compact('selectedSdgs'));
    }

    public function BloodDonated(User $user)
    {
        $persons = $this->GetUserData($user);

        return view('blood-donated', compact('persons'));
    }

    public function OrganDonated(User $user)
    {
        $persons = $this->GetUserData($user);

        return view('organ-donated', compact('persons'));
    }

    public function MyFeedback(User $user)
    {
        $persons = $this->GetUserData($user);
        //dd($persons);

        $game_ids = array();
        foreach ($persons as $person) {
            $game_ids[] = $person['game_id'];
        }

        $feedbacks = FeedbackBugReport::whereIn('game_id', $game_ids)->get(['game_id', 'feedback'])->toArray();
        //dd($feedbacks);

        return view('feedback', compact('feedbacks', 'persons', 'game_ids'));
    }

    public function BugReport(User $user)
    {
        $persons = $this->GetUserData($user);

        $game_ids = array();
        foreach ($persons as $person) {
            $game_ids[] = $person['game_id'];
        }

        $bugReports = FeedbackBugReport::whereIn('game_id', $game_ids)->get(['game_id', 'bugReport'])->toArray();
        //dd($bugReports);

        return view('bug-report', compact('bugReports', 'persons', 'game_ids'));
    }
}
