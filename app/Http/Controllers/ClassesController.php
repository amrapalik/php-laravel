<?php

namespace App\Http\Controllers;


use App\DataTables\AssignmentStudentDatatable;
use App\DataTables\ClassesAssignedDataTable;
use App\DataTables\ClassesDataTable;
use App\DataTables\ClassStudentDatatable;
use App\DataTables\JoinedAssignmentDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateClassesRequest;
use App\Http\Requests\UpdateClassesRequest;
use App\Models\AssignmentComment;
use App\Models\Classes;
use App\Models\ClassUser;
use App\Models\Student;
use App\Models\StudentAssignment;
use App\Models\User;
use App\Models\Notification;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class ClassesController extends AppBaseController
{

    public function __construct() {
        // permission:publish articles'
        //   $this->middleware('permission', ['only' => ['create']]);
        //  $this->middleware(['permission:Create bugs'], ['only' => ['create']]);
        $this->middleware(['permission:Create classes'], ['only' => ['create']]);

        // $this->middleware('post-edit', ['only' => ['edit']]);
		$this->middleware('school_user', ['only'=>['show']]);
		$this->middleware('org_staff', ['only'=>['index']]);
    }

    /**
     * Display a listing of the Classes.
     *
     * @param ClassesDataTable $classesDataTable
     * @return Response
     */
    public function index(ClassesDataTable $classesDataTable)
    {

        //dd(\Request::has('other'));


        return $classesDataTable->render('classes.index');
    }

    public function assignedClasses(ClassesAssignedDataTable $classesDataTable)
    {
        return $classesDataTable->render('classes.assigned_classes');
    }




    public function assignmentstudents(AssignmentStudentDatatable $assignmentStudentDatatable,Request $request)
    {

        $classid =  $request->get('class');
        if ($classid != null && $classid != '')
        {
            return $assignmentStudentDatatable->render('classes.assignment_student');
        }else{
            return redirect()->back();
        }
    }

    public function classtudents(ClassStudentDatatable $classStudentDatatable,Request $request)
    {

       $classid =  $request->get('class');
        if ($classid != null && $classid != '')
        {
            return $classStudentDatatable->render('classes.class_student');
        }else{
            return redirect()->back();
        }
    }

    public function myJoinedAssignmentClasses(JoinedAssignmentDataTable $assignmentDataTable)
    {
        return $assignmentDataTable->render('assignments.myassignments_index');
    }

    /**
     * Show the form for creating a new Classes.
     *
     * @return Response
     */
    public function create1()
    {
        return view('classes.create');
    }

    public function saveAssignmentComment(Request $request)
     {
        $input = $request->all();

         AssignmentComment::create([
            'student_assignment_id' => $input['student_assignment_id'],
             'assignment_asid' => $input['assignment_asid'],
            'comment' => $input['comment'],
            'added_by' => auth()->user()->id,
            // 'replied_to' => $input['replied_to'],
        ]);


        Flash::success(__('messages.saved', ['model' => __('lessonPlans.singular')]));
        return redirect()->back()->with('success', 'Comment added successfully.');
        return redirect(route('lessonPlans.index'));
    }


    public function create(Request $request)
    {

        if (is_numeric($request->segment(2))) {

            $classData = $this->getClass($request->segment(2));

            $class = $classData['class'];
            $students = $classData['students'];
            $teachers = $classData['teachers'];
            $classUserStudents = $classData['classUserStudents'];
            $classUserTeachers = $classData['classUserTeachers'];

            return view('create-class', compact('class', 'students', 'teachers', 'classUserStudents', 'classUserTeachers'));
        }

       $schid =  session('school_schid');

        $students  = User::where(['drole' => 'student', 'school_schid' => $schid,'deallocated'=>0])->orderBy('grade', 'ASC')->get(['id', 'username', 'name', 'grade', 'organization_orgid', 'purchaseflag'])->toArray();

        $students2 = User::where(['drole' => 'student', 'school_schid' => $schid,'deallocated'=>0])->orderBy('grade', 'ASC')->get(['id', 'name', 'grade', ])->groupBy('grade') ->toArray();
//dd($students2['4TH']);

        // $teachers = User::where(['drole' => 'teacher', 'school_schid' => $schid])->orderBy('grade', 'ASC')->get(['id', 'username', 'name', 'grade', 'organization_orgid', 'purchaseflag']);
        $teachers2 = User::where('school_schid' ,'=',$schid)->whereIn('drole', array('Teacher', 'School Admin'))->get(['id', 'username', 'name', 'grade', 'organization_orgid', 'purchaseflag']);



      // dd($teachers->toArray());
        $class = array();
        $classUserStudents = array();
        $classUserTeachers = array();

        //return view('create-class', compact('students', 'teachers'));
        //return view('classes/createclasses', compact('class', 'students', 'teachers', 'classUserStudents', 'classUserTeachers'));

        return view('classes/create', compact('class', 'students', 'students2', 'teachers2','classUserStudents', 'classUserTeachers'));
    }

    /**
     * Store a newly created Classes in storage.
     *
     * @param CreateClassesRequest $request
     *
     * @return Response
     */
    public function store(CreateClassesRequest $request)
    {
        $input = $request->all();


       // return $input;

        $schoolid = session('school_schid');

       // dd($schoolid);
        $input['school_schid'] = $schoolid;
        $input['created_by'] =  auth()->user()->id;
        /** @var Classes $classes */


        DB::beginTransaction();
        try {
            $classes = Classes::create($input);
            //if ($input['type'] == 'Assignment'){
                foreach ($request['students'] as $student) {
                   // dd($student);
                    $assignmentStudents[] = [
                        'assignment_asid' => $input['assignment_asid'],
                        'teacher_id' => auth()->user()->id,
                        'school_schid' => get_school_id(),
                        'utype' => 'Student',
                        'game_status'   => 'Pending',
                        'obituary_status'   => 'Pending',
                        'letter_status' => 'Pending',
                        'class_classid' =>  $classes->classid,
                        'student_id' => $student,
//                        'game_id' => 1,
//                        'invitation' => 1,
//                        'invitation' => 1,
//                        'invitation' => 1,
                    ];
                }
                StudentAssignment::insert($assignmentStudents);
            //}else{
                foreach ($request['students'] as $student) {
                    $classStudents[] = [
                        'class_classid' => $classes->classid,
                        'user_id' => $student,
                        'user_type' => 'student',
                        'invitation' => 1,
                    ];
                }
                ClassUser::insert($classStudents);
            //}
			// generate notifications
			foreach($request['students'] as $student){
				$notification = new Notification;
				$notification->message = __('classes.You have been added to class -').$input['class_name'];
				$notification->from = auth()->user()->id;
				$notification->to = $student;
				$notification->link = '/classes/'.$classes->classid;
				$notification->save();
			}

        } catch (Exception $e) {
            DB::rollBack();
            session()->flash('error', $e->getMessage());

            if($input['type'] == 'Assignment'){
                return redirect('/classes/create?assignment=true');
            }else{
                return redirect('/classes/create');
            }
        }

//        $grades = [];
//        foreach ($request['students'] as $studid) {
//            $grades[]=$studid;
//        }
        if ($request['teachers'] == null){
            $request['teachers'] = [];
        }

        $classTeachers = [];

        //if ($input['type'] == 'Assignment') {


            foreach ($request['teachers'] as $user_id) {

                $classTeachers[] = [
                    'assignment_asid' => $input['assignment_asid'],
                    'teacher_id' => auth()->user()->id,
                    'school_schid' => get_school_id(),
                    'utype' => 'Teacher',
                    'game_status' => 'Pending',
                    'letter_status' => 'Pending',
                    'obituary_status'   => 'Pending',
                    'class_classid' =>  $classes->classid,
                    'student_id' => $user_id,
//                        'game_id' => 1,
//                        'invitation' => 1,
//                        'invitation' => 1,
//                        'invitation' => 1,
                ];

//                $classTeachers[] = [
//                    'class_classid' => $classes->classid,
//                    'user_id' => $user_id[0],
//                    'user_type' => 'teacher',
//                    'invitation' => 1,
//                ];
            }

            try {
                StudentAssignment::insert($classTeachers);
				// generate notifications
				foreach($request['teachers'] as $teacher){
				$notification = new Notification;
				$notification->message = __('classes.You have been added to class -').$input['class_name'];
				$notification->from = auth()->user()->id;
				$notification->to = $teacher;
				$notification->link = '/classes/'.$classes->classid;
				$notification->save();
				}
            } catch (Exception $e) {
                DB::rollBack();
                session()->flash('error', $e->getMessage());
                return redirect('/classes/create?assignment=true');
            }




        //}else{

            foreach ($request['teachers'] as $user_id) {
                $classUserTeachers[] = [
                    'class_classid' => $classes->classid,
                    'user_id' => $user_id,
                    'user_type' => 'teacher',
                    'invitation' => 1,
                ];
            }

            // try {
            //     ClassUser::insert($classUserTeachers);
            // } catch (Exception $e) {
            //     DB::rollBack();
            //     session()->flash('error', $e->getMessage());
            //     return redirect('/classes/create');
            // }

        //}

     //   dd($classTeachers);
//


        DB::commit();

//        if ($request['submit'] == 'save') {
//            session()->flash('success', 'Class created successfully.');
//            return redirect('/classes');
//        } else {
//            return redirect('/create-lesson-plan/' . $classes->classid . '/new');
//        }


        Flash::success(__('messages.saved', ['model' => __('classes.singular')]));

        if($input['type'] == 'Assignment'){
            return redirect('/classes?assignment=true');
        }else{
            return redirect('/classes');
        }

       // return redirect(route('classes.index'));
    }

    /**
     * Display the specified Classes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Classes $classes */
        $classes = Classes::find($id);

        if (empty($classes)) {
            Flash::error(__('classes.singular').' '.__('messages.not_found'));
            return redirect(route('classes.index'));
        }
		if($classes->type == 'Assignment'){
        	return view('classes.show_assignment')->with('classes', $classes);
		}
		else{
        	//return view('classes.show_lessonplan')->with('classes', $classes);
        	return view('classes.show_assignment')->with('classes', $classes);
		}
    }

    /**
     * Show the form for editing the specified Classes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Classes $classes */
        $classes = Classes::find($id);

        if (empty($classes)) {
            Flash::error(__('messages.not_found', ['model' => __('classes.singular')]));

            return redirect(route('classes.index'));
        }

        return view('classes.edit')->with('classes', $classes);
    }

    /**
     * Update the specified Classes in storage.
     *
     * @param  int              $id
     * @param UpdateClassesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClassesRequest $request)
    {
        /** @var Classes $classes */
        $classes = Classes::find($id);

        if (empty($classes)) {
            Flash::error(__('messages.not_found', ['model' => __('classes.singular')]));

            return redirect(route('classes.index'));
        }

        $classes->fill($request->all());
        $classes->save();

        Flash::success(__('messages.updated', ['model' => __('classes.singular')]));

        return redirect(route('classes.index'));
    }

    /**
     * Remove the specified Classes from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Classes $classes */
        $classes = Classes::find($id);

        if (empty($classes)) {
            Flash::error(__('messages.not_found', ['model' => __('classes.singular')]));

            return redirect(route('classes.index'));
        }

        $classes->delete();

        Flash::success(__('messages.deleted', ['model' => __('classes.singular')]));

        return redirect()->back();
    }
}
