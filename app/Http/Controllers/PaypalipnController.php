<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Log;
use App\Models\Order;
use App\Models\License;
use App\Models\User;

/*

{
    "id": "WH-96997974BS327361V-6FU41104DV417332N",
    "event_version": "1.0",
    "create_time": "2022-05-08T11:40:13.560Z",
    "resource_type": "checkout-order",
    "resource_version": "2.0",
    "event_type": "CHECKOUT.ORDER.APPROVED",
    "summary": "An order has been approved by buyer",
    "resource": {
        "update_time": "2022-05-08T11:40:04Z",
        "create_time": "2022-05-08T11:39:43Z",
        "purchase_units": [{
            "reference_id": "default",
            "amount": {
                "currency_code": "USD",
                "value": "1.00"
            },
            "payee": {
                "email_address": "parag-facilitator@reallivesworld.com",
                "merchant_id": "SBNQFE7RB4BZJ"
            },
            "soft_descriptor": "PAYPAL *TESTFACILIT",
            "shipping": {
                "name": {
                    "full_name": "John Doe"
                },
                "address": {
                    "address_line_1": "Flat no. 507 Wing A Raheja Residency",
                    "address_line_2": "Film City Road",
                    "admin_area_2": "Mumbai",
                    "admin_area_1": "Maharashtra",
                    "postal_code": "400097",
                    "country_code": "IN"
                }
            },
            "payments": {
                "captures": [{
                    "id": "2C421526UV6193125",
                    "status": "COMPLETED",
                    "amount": {
                        "currency_code": "USD",
                        "value": "1.00"
                    },
                    "final_capture": true,
                    "seller_protection": {
                        "status": "ELIGIBLE",
                        "dispute_categories": ["ITEM_NOT_RECEIVED", "UNAUTHORIZED_TRANSACTION"]
                    },
                    "seller_receivable_breakdown": {
                        "gross_amount": {
                            "currency_code": "USD",
                            "value": "1.00"
                        },
                        "paypal_fee": {
                            "currency_code": "USD",
                            "value": "0.54"
                        },
                        "net_amount": {
                            "currency_code": "USD",
                            "value": "0.46"
                        }
                    },
                    "links": [{
                        "href": "https://api.sandbox.paypal.com/v2/payments/captures/2C421526UV6193125",
                        "rel": "self",
                        "method": "GET"
                    }, {
                        "href": "https://api.sandbox.paypal.com/v2/payments/captures/2C421526UV6193125/refund",
                        "rel": "refund",
                        "method": "POST"
                    }, {
                        "href": "https://api.sandbox.paypal.com/v2/checkout/orders/17G309691N4225345",
                        "rel": "up",
                        "method": "GET"
                    }],
                    "create_time": "2022-05-08T11:40:04Z",
                    "update_time": "2022-05-08T11:40:04Z"
                }]
            }
        }],
        "links": [{
            "href": "https://api.sandbox.paypal.com/v2/checkout/orders/17G309691N4225345",
            "rel": "self",
            "method": "GET"
        }],
        "id": "17G309691N4225345",
        "intent": "CAPTURE",
        "payer": {
            "name": {
                "given_name": "John",
                "surname": "Doe"
            },
            "email_address": "sb-hgs8v8602236@personal.example.com",
            "payer_id": "H7QZQ76WHHNVU",
            "address": {
                "country_code": "IN"
            }
        },
        "status": "COMPLETED"
    },
    "links": [{
        "href": "https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-96997974BS327361V-6FU41104DV417332N",
        "rel": "self",
        "method": "GET"
    }, {
        "href": "https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-96997974BS327361V-6FU41104DV417332N/resend",
        "rel": "resend",
        "method": "POST"
    }]
}

{
    "id": "WH-2DT25098V7976763S-0HN17203BV3672414",
    "event_version": "1.0",
    "create_time": "2022-05-08T11:40:08.656Z",
    "resource_type": "capture",
    "resource_version": "2.0",
    "event_type": "PAYMENT.CAPTURE.COMPLETED",
    "summary": "Payment completed for $ 1.0 USD",
    "resource": {
        "amount": {
            "value": "1.00",
            "currency_code": "USD"
        },
        "seller_protection": {
            "dispute_categories": ["ITEM_NOT_RECEIVED", "UNAUTHORIZED_TRANSACTION"],
            "status": "ELIGIBLE"
        },
        "supplementary_data": {
            "related_ids": {
                "order_id": "17G309691N4225345"
            }
        },
        "update_time": "2022-05-08T11:40:04Z",
        "create_time": "2022-05-08T11:40:04Z",
        "final_capture": true,
        "seller_receivable_breakdown": {
            "paypal_fee": {
                "value": "0.54",
                "currency_code": "USD"
            },
            "gross_amount": {
                "value": "1.00",
                "currency_code": "USD"
            },
            "net_amount": {
                "value": "0.46",
                "currency_code": "USD"
            }
        },
        "links": [{
            "method": "GET",
            "rel": "self",
            "href": "https://api.sandbox.paypal.com/v2/payments/captures/2C421526UV6193125"
        }, {
            "method": "POST",
            "rel": "refund",
            "href": "https://api.sandbox.paypal.com/v2/payments/captures/2C421526UV6193125/refund"
        }, {
            "method": "GET",
            "rel": "up",
            "href": "https://api.sandbox.paypal.com/v2/checkout/orders/17G309691N4225345"
        }],
        "id": "2C421526UV6193125",
        "status": "COMPLETED"
    },
    "links": [{
        "href": "https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-2DT25098V7976763S-0HN17203BV3672414",
        "rel": "self",
        "method": "GET"
    }, {
        "href": "https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-2DT25098V7976763S-0HN17203BV3672414/resend",
        "rel": "resend",
        "method": "POST"
    }]
}


{
    "id": "WH-22B8041083679314A-6L4079850N059482M",
    "event_version": "1.0",
    "create_time": "2022-05-08T16:51:24.798Z",
    "resource_type": "dispute",
    "event_type": "RISK.DISPUTE.CREATED",
    "summary": "A new dispute opened with Case # PP-D-64397",
    "resource": {
        "disputed_transactions": [{
            "buyer_transaction_id": "0CF12657W5304290E",
            "seller": {
                "name": "test facilitator's Test Store",
                "merchant_id": "SBNQFE7RB4BZJ"
            },
            "seller_transaction_id": "2C421526UV6193125",
            "items": [{
                "reason": "MERCHANDISE_OR_SERVICE_NOT_AS_DESCRIBED",
                "notes": "sdasdasdsd sdfsd fades dfsdfsd fsd fsd fsd fsd fs df sof sd fsd f s df s df sd f sdf sdf sdf sd f sdfsd f sd f sdf sd f sdfs df sd f sdf s df sdfs df s df sd f sd f s dfs df sd f sdf s df sdf",
                "item_type": "SERVICE",
                "item_name": "Test",
                "item_description": "Test",
                "item_quantity": "1"
            }],
            "seller_protection_eligible": false
        }],
        "reason": "MERCHANDISE_OR_SERVICE_NOT_AS_DESCRIBED",
        "dispute_channel": "INTERNAL",
        "update_time": "2022-05-08T16:50:41.000Z",
        "create_time": "2022-05-08T16:50:41.000Z",
        "messages": [{
            "posted_by": "BUYER",
            "time_posted": "2022-05-08T16:50:51.000Z",
            "content": "Details or photos of the service you were promised Details or photos of the service that was provided highlighting why it wasn\u2019t satisfactory Conversation or chats with the seller on any platform The seller\u2019s return policy related to the issue you\u2019re facing "
        }],
        "links ": [{
            "method ": "GET ",
            "rel ": "self ",
            "href ": "https: //api.sandbox.paypal.com/v1/customer/disputes/PP-D-64397"
        }],
        "dispute_amount": {
            "value": "1.00",
            "currency_code": "USD"
        },
        "dispute_id": "PP-D-64397",
        "dispute_life_cycle_stage": "INQUIRY",
        "status": "OPEN"
    },
    "links": [{
        "href": "https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-22B8041083679314A-6L4079850N059482M",
        "rel": "self",
        "method": "GET"
    }, {
        "href": "https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-22B8041083679314A-6L4079850N059482M/resend",
        "rel": "resend",
        "method": "POST"
    }]
}

{
  "id": "WH-1RF922882E7009354-6M5968312B7583318",
  "event_version": "1.0",
  "create_time": "2022-05-09T04:56:33.546Z",
  "resource_type": "refund",
  "resource_version": "2.0",
  "event_type": "PAYMENT.CAPTURE.REFUNDED",
  "summary": "A $ 1.0 USD capture payment was refunded",
  "resource": {
    "seller_payable_breakdown": {
      "total_refunded_amount": {
        "value": "1.00",
        "currency_code": "USD"
      },
      "paypal_fee": {
        "value": "0.05",
        "currency_code": "USD"
      },
      "gross_amount": {
        "value": "1.00",
        "currency_code": "USD"
      },
      "net_amount": {
        "value": "0.95",
        "currency_code": "USD"
      }
    },
    "amount": {
      "value": "1.00",
      "currency_code": "USD"
    },
    "update_time": "2022-05-08T21:56:29-07:00",
    "create_time": "2022-05-08T21:56:29-07:00",
    "links": [
      {
        "method": "GET",
        "rel": "self",
        "href": "https://api.sandbox.paypal.com/v2/payments/refunds/5GR34051F5267223C"
      },
      {
        "method": "GET",
        "rel": "up",
        "href": "https://api.sandbox.paypal.com/v2/payments/captures/3RE33193VE900101Y"
      }
    ],
    "id": "5GR34051F5267223C",
    "status": "COMPLETED"
  },
  "links": [
    {
      "href": "https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-1RF922882E7009354-6M5968312B7583318",
      "rel": "self",
      "method": "GET"
    },
    {
      "href": "https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-1RF922882E7009354-6M5968312B7583318/resend",
      "rel": "resend",
      "method": "POST"
    }
  ]
}

{
  "id": "WH-3EC545679X386831C-3D038940937933201",
  "event_version": "1.0",
  "create_time": "2014-10-23T00:19:27Z",
  "resource_type": "sale",
  "event_type": "PAYMENT.SALE.REVERSED",
  "summary": "A $ 0.49 USD sale payment was reversed",
  "resource": {
    "id": "77689802DL785834G",
    "create_time": "2014-10-23T00:19:12Z",
    "state": "completed",
    "amount": {
      "total": "-0.49",
      "currency": "USD",
      "details": {
        "subtotal": "-0.64",
        "tax": "0.08",
        "shipping": "0.07"
      }
    },
    "links": [
      {
        "href": "https://api.paypal.com/v1/payments/refund/77689802DL785834G",
        "rel": "self",
        "method": "GET"
      }
    ]
  },
  "links": [
    {
      "href": "https://api.paypal.com/v1/notifications/webhooks-events/WH-3EC545679X386831C-3D038940937933201",
      "rel": "self",
      "method": "GET"
    },
    {
      "href": "https://api.paypal.com/v1/notifications/webhooks-events/WH-3EC545679X386831C-3D038940937933201/resend",
      "rel": "resend",
      "method": "POST"
    }
  ]
}


{"id":"WH-674550604W984070M-37T36862DA1895607","event_version":"1.0","create_time":"2022-05-11T06:17:37.542Z","resource_type":"capture","resource_version":"2.0","event_type":"PAYMENT.CAPTURE.COMPLETED","summary":"Payment completed for $ 1.0 USD","resource":{"amount":{"value":"1.00","currency_code":"USD"},"seller_protection":{"dispute_categories":["ITEM_NOT_RECEIVED","UNAUTHORIZED_TRANSACTION"],"status":"ELIGIBLE"},"supplementary_data":{"related_ids":{"order_id":"5HR400476C7759707"}},"update_time":"2022-05-11T06:17:33Z","create_time":"2022-05-11T06:17:33Z","final_capture":true,"seller_receivable_breakdown":{"paypal_fee":{"value":"0.54","currency_code":"USD"},"gross_amount":{"value":"1.00","currency_code":"USD"},"net_amount":{"value":"0.46","currency_code":"USD"}},"links":[{"method":"GET","rel":"self","href":"https://api.sandbox.paypal.com/v2/payments/captures/6NM578892H987562A"},{"method":"POST","rel":"refund","href":"https://api.sandbox.paypal.com/v2/payments/captures/6NM578892H987562A/refund"},{"method":"GET","rel":"up","href":"https://api.sandbox.paypal.com/v2/checkout/orders/5HR400476C7759707"}],"id":"6NM578892H987562A","status":"COMPLETED"},"links":[{"href":"https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-674550604W984070M-37T36862DA1895607","rel":"self","method":"GET"},{"href":"https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-674550604W984070M-37T36862DA1895607/resend","rel":"resend","method":"POST"}]}



*/
class PaypalipnController extends Controller
{
    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    protected function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public function index(Request $request)
    {
        $raw_post_data = file_get_contents('php://input');
        $event = json_decode($raw_post_data);
        $post = $request->all();
        
        Log::debug(print_r($post, true));
        Log::debug(print_r($raw_post_data, true));
        Log::debug(print_r($event, true));

        switch($event->event_type) {
            case 'CHECKOUT.ORDER.APPROVED':
                // Order::where('paypal_orderid', $event->resource->id)->update(['paypal_captureid' => $event->resource->purchase_units[0]->payments->captures[0]->id]);
                // License::where('paypal_orderid', $event->resource->id)->update(['paypal_captureid' => $event->resource->purchase_units[0]->payments->captures[0]->id]);
                break;
            case 'PAYMENT.CAPTURE.COMPLETED':
                $order_id = $event->resource->supplementary_data->related_ids->order_id;
                Order::where('paypal_orderid', $order_id)->update(['paypal_captureid' => $event->resource->id, 'paid' => 1]);
                License::where('paypal_orderid', $order_id)->update(['paypal_captureid' => $event->resource->id, 'paid' => 1]);

                $order = Order::where('paypal_orderid', $order_id)->first();
                User::where('id', $order->user_id)->update(['purchaseflag' => 'Yes']);

                // Order::where('paypal_captureid', $event->resource->id)->update(['paid' => 1]);
                // License::where('paypal_captureid', $event->resource->id)->update(['paid' => 1]);
                break;
            case 'PAYMENT.CAPTURE.REFUNDED':
                $capture_id = $this->get_string_between($raw_post_data, 'payments/captures/', '"');
                Log::debug('$capture_id is: '.$capture_id);
                Order::where('paypal_captureid', $capture_id)->update(['paid' => 0, 'status' => 'REFUNDED']);
                License::where('paypal_captureid', $capture_id)->update(['paid' => 0, 'status' => 'REFUNDED']);
                $order = Order::where('paypal_captureid', $capture_id)->first();
                User::where('id', $order->user_id)->update(['purchaseflag' => 'No']);
                break;

        }


        exit;

        

        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode ('=', $keyval);
            if (count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);
        }
        // read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
        $req = 'cmd=_notify-validate';
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }

        $ch = curl_init('https://ipnpb.paypal.com/cgi-bin/webscr');

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        // In wamp-like environments that do not come bundled with root authority certificates,
        // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set
        // the directory path of the certificate as shown below:
        // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
        if ( !($res = curl_exec($ch)) ) {
            // error_log("Got " . curl_error($ch) . " when processing IPN data");
            curl_close($ch);
            exit;
        }

        Log::debug(print_r($res, true));

        $servername = "reallives.ckjhqwvewnox.us-east-1.rds.amazonaws.com";
        $username = "reallives";
        $password = "r3TPXIwJ4HHacWb4o8dE";
        $dbname = "reallive_world";

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);

        if (strcmp ($res, "VERIFIED") == 0) {

            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            } 

            $username=$_POST['custom'];
            $status=$_POST['payment_status'];
                $txn_id = $_POST['txn_id'];
               
            $sql = "UPDATE  `registered_player` SET  `paid` =  '1' WHERE  `registered_player`.`username` ='".$username."'";
              
            $result = $conn->query($sql);


            error_log($username);

            $sql = "SELECT `registered_player_id` FROM `registered_player` WHERE  `registered_player`.`username` ='".$username."'";
            $result=$conn->query($sql);
            $row = mysqli_fetch_assoc($result);
            $registered_player_id = $row['registered_player_id'];

            $sql = "SELECT `subRole` FROM `registered_player_role` WHERE `registered_player_role`.`registered_player_id` ='".$registered_player_id."'";
            $res=$conn->query($sql);
            $row = mysqli_fetch_assoc($res);
            $subRole = $row['subRole'];

            $roleGameUser='ROLE_GAMEUSER';
            $roleSchoolAdmin='SCHOOLADMIN';
            $roleOrganizationAdmin='ORGANIZATIONADMIN';

                $sql="UPDATE  `registered_player` SET  `payment_status` =  '".$status."' WHERE  `registered_player`.`username` ='".$username."'";
                $result=$conn->query($sql);

            //$result=$conn->exec($sql);

                $sql="UPDATE  `registered_player` SET  `txn_id` = '".$txn_id."' WHERE  `registered_player`.`username` ='".$username."'";
                $result=$conn->query($sql);

            //$result=$conn->exec($sql);



        } else if (strcmp ($res, "INVALID") == 0) {
            // IPN invalid, log for manual investigation
        }

        $sql="SELECT `payment_status` FROM `registered_player` WHERE `username`='".$username."'";
        $result=$conn->query($sql);
        $row = mysqli_fetch_assoc($result);
        $payment_status = $row['payment_status'];
        //error_log("your status".$payment_status  );

        if($payment_status == "Completed"){

        $sql="SELECT `email_id` FROM `registered_player` WHERE `username`='".$username."'";
        $result=$conn->query($sql);
        $row = mysqli_fetch_assoc($result);
        $to = $row['email_id'];

        $sql="SELECT `fullname` FROM `registered_player` WHERE `username`='".$username."'";
        $result=$conn->query($sql);
        $row = mysqli_fetch_assoc($result);
        $fullname = $row['fullname'];


        //error_log("your messageeeeeeeeee".$to );

        //$subject = "payment is completed";

        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->Host = "smtp.gmail.com";  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = "support@reallivesworld.com";                 // SMTP username
        $mail->Password = "neeti@2017";                           // SMTP password
        $mail->SMTPSecure = "tls";                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom("support@reallivesworld.com", "Support");
        $mail->addAddress("$to", "$username");     // Add a recipient
        $mail->addAddress("support@reallivesworld.com");
        $mail->isHTML(true);                                  // Set email format to HTML


        $search=array('%username%','%fullname%','%email%');
        $replace=array($username,$fullname,$to);
        if($subRole == $roleGameUser ){
                $mail->Subject = "Payment Successfull";
            //for gamer mail
        $content = str_replace($search, $replace, file_get_contents('afterpurchasing.html'));
        $mail->msgHTML($content, dirname("afterpurchasing.html"));
        }

        if($subRole == $roleOrganizationAdmin){
            $mail->Subject = "Payment Successfull for organization";
            //for school mail
        $content = str_replace($search, $replace, file_get_contents('afterpurchasingSchool.html'));
        $mail->msgHTML($content, dirname("afterpurchasingSchool.html"));
            
        }
        if($subRole == $roleSchoolAdmin){
            //for school mail
            $mail->Subject = "Payment Successfull for school";
        $content = str_replace($search, $replace, file_get_contents('afterpurchasingSchool.html'));
        $mail->msgHTML($content, dirname("afterpurchasingSchool.html"));
            
        }
        // $content = str_replace($search, $replace, file_get_contents('afterpurchasing.html'));
        //$content = str_replace('%fullname%', $fullname, file_get_contents('afterpurchasing.html'));
        //$mail->msgHTML($content, dirname("afterpurchasing.html"));
        //$mail->MsgHTML(file_get_contents('afterpurchasing.html'));
        //$message = str_replace("username", $username, "username"); 
        //$mail->Body    = "This is the HTML message body in bold!";
        $mail->AltBody = " Thank you!!!!
        payment is successfull";

        if(!$mail->send()) {
            echo "Message could not be sent.";
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message has been sent";
        error_log("message sent");
        }

        }

        if($payment_status == "Failed"){
        //send mail to payment status Failed

        $sql="SELECT `email_id` FROM `registered_player` WHERE `username`='".$username."'";
        $result=$conn->query($sql);
        $row = mysqli_fetch_assoc($result);
        $to = $row['email_id'];

        $sql="SELECT `fullname` FROM `registered_player` WHERE `username`='".$username."'";
        $result=$conn->query($sql);
        $row = mysqli_fetch_assoc($result);
        $fullname = $row['fullname'];


        //error_log("your messageeeeeeeeee".$to );

        //$subject = "payment is Failed";

        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->Host = "smtp.gmail.com";  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = "support@reallivesworld.com";                 // SMTP username
        $mail->Password = "neeti@2017";                           // SMTP password
        $mail->SMTPSecure = "tls";                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom("support@reallivesworld.com", "Support");
        $mail->addAddress("$to", "$username");     // Add a recipient
        $mail->addAddress("support@reallivesworld.com");
        $mail->isHTML(true);                                  // Set email format to HTML

        $search=array('%username%','%fullname%','%email%');
        $replace=array($username,$fullname,$to);
        if($subRole == $roleGameUser ){
            //for gamer mail
            
        $mail->Subject = "Payment Failed";
        $content = str_replace($search, $replace, file_get_contents('afterpaymentfailed.html'));
        $mail->msgHTML($content, dirname("afterpaymentfailed.html"));
        }

        if($subRole == $roleSchoolAdmin){
                
        $mail->Subject = "Payment Failed For School";
        $content = str_replace($search, $replace, file_get_contents('afterpaymentfailed.html'));
        $mail->msgHTML($content, dirname("afterpaymentfailed.html"));
        }

        if($subRole == $roleOrganizationAdmin){
            //for school mail
            
        $mail->Subject = "Payment Failed For Organization";
        $content = str_replace($search, $replace, file_get_contents('afterpaymentfailed.html'));
        $mail->msgHTML($content, dirname("afterpaymentfailed.html"));
            
        }
        // $content = str_replace($search, $replace, file_get_contents('afterpaymentfailed.html'));
        //$content = str_replace('%fullname%', $fullname, file_get_contents('afterpaymentfailed.html'));
        // $mail->msgHTML($content, dirname("afterpaymentfailed.html"));
        //$mail->MsgHTML(file_get_contents('afterpaymentfailed.html'));
        //$message = str_replace("username", $username, "username"); 
        //$mail->Body    = "This is the HTML message body in bold!";
        $mail->AltBody = "Your payment is Failed";

        if(!$mail->send()) {
            echo "Message could not be sent.";
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message has been sent";
        error_log("message sent");
        }


        }


        //payment status is pending

        if($payment_status == "Pending"){
        //send mail to payment status Pending

        $sql="SELECT `email_id` FROM `registered_player` WHERE `username`='".$username."'";
        $result=$conn->query($sql);
        $row = mysqli_fetch_assoc($result);
        $to = $row['email_id'];

        $sql="SELECT `fullname` FROM `registered_player` WHERE `username`='".$username."'";
        $result=$conn->query($sql);
        $row = mysqli_fetch_assoc($result);
        $fullname = $row['fullname'];


        //error_log("your messageeeeeeeeee".$to );

        //$subject = "payment is Pending";

        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->Host = "smtp.gmail.com";  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = "support@reallivesworld.com";                 // SMTP username
        $mail->Password = "neeti@2017";                           // SMTP password
        $mail->SMTPSecure = "tls";                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom("support@reallivesworld.com", "Support");
        $mail->addAddress("$to", "$username");     // Add a recipient
        $mail->addAddress("support@reallivesworld.com");
        $mail->isHTML(true);                                  // Set email format to HTML


        $search=array('%username%','%fullname%','%email%');
        $replace=array($username,$fullname,$to);
        if($subRole == $roleGameUser ){
            //for gamer mail
            $mail->Subject = "Payment Pending";
        $content = str_replace($search, $replace, file_get_contents('afterpaymentpending.html'));
        $mail->msgHTML($content, dirname("afterpaymentpending.html"));
        }
        if($subRole == $roleOrganizationAdmin)
        {
                $mail->Subject = "Payment Pending For Organization";
        $content = str_replace($search, $replace, file_get_contents('afterpaymentpending.html'));
        $mail->msgHTML($content, dirname("afterpaymentpending.html"));
        }
        if($subRole == $roleSchoolAdmin){
            //for school mail
            $mail->Subject = "Payment Pending For School";
        $content = str_replace($search, $replace, file_get_contents('afterpaymentpending.html'));
        $mail->msgHTML($content, dirname("afterpaymentpending.html"));
            
        }
        //$content = str_replace($search, $replace, file_get_contents('afterpaymentpending.html'));
        //$content = str_replace('%fullname%', $fullname, file_get_contents('afterpaymentpending.html'));
        //$mail->msgHTML($content, dirname("afterpaymentpending.html"));
        //$mail->MsgHTML(file_get_contents('afterpaymentpending.html'));
        //$message = str_replace("username", $username, "username"); 
        //$mail->Body    = "This is the HTML message body in bold!";
        $mail->AltBody = " Your payment is Pending";

        if(!$mail->send()) {
            echo "Message could not be sent.";
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message has been sent";
        error_log("message sent");
        }


        }

        //payment status is Declined

        if($payment_status == "Declined"){
        //send mail to payment status Pending

        $sql="SELECT `email_id` FROM `registered_player` WHERE `username`='".$username."'";
        $result=$conn->query($sql);
        $row = mysqli_fetch_assoc($result);
        $to = $row['email_id'];

        $sql="SELECT `fullname` FROM `registered_player` WHERE `username`='".$username."'";
        $result=$conn->query($sql);
        $row = mysqli_fetch_assoc($result);
        $fullname = $row['fullname'];


        //error_log("your messageeeeeeeeee".$to );

        //$subject = "payment is Declined";

        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->Host = "smtp.gmail.com";  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = "support@reallivesworld.com";                 // SMTP username
        $mail->Password = "neeti@2017";                           // SMTP password
        $mail->SMTPSecure = "tls";                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom("support@reallivesworld.com", "Support");
        $mail->addAddress("$to", "$username");     // Add a recipient
        $mail->addAddress("support@reallivesworld.com");
        $mail->isHTML(true);                                  // Set email format to HTML


        $search=array('%username%','%fullname%','%email%');
        $replace=array($username,$fullname,$to);
        if($subRole == $roleGameUser ){
            //for gamer mail
            $mail->Subject = "Payment Declined";
        $content = str_replace($search, $replace, file_get_contents('afterpaymentdeclined.html'));
        $mail->msgHTML($content, dirname("afterpaymentdeclined.html"));
        }
        if($subRole == $roleOrganizationAdmin){
                //for organzation mail
            $mail->Subject = "Payment Declined For Organization";
        $content = str_replace($search, $replace, file_get_contents('afterpaymentdeclined.html'));
        $mail->msgHTML($content, dirname("afterpaymentdeclined.html"));
        }

        if($subRole == $roleSchoolAdmin){
            //for school mail
            $mail->Subject = "Payment Declined For School";
        $content = str_replace($search, $replace, file_get_contents('afterpaymentdeclined.html'));
        $mail->msgHTML($content, dirname("afterpaymentdeclined.html"));
            
        }
        // $content = str_replace($search, $replace, file_get_contents('afterpaymentdeclined.html'));
        //$content = str_replace('%fullname%', $fullname, file_get_contents('afterpaymentdeclined.html'));
        // $mail->msgHTML($content, dirname("afterpaymentdeclined.html"));
        //$mail->MsgHTML(file_get_contents('afterpaymentdeclined.html'));
        //$message = str_replace("username", $username, "username"); 
        //$mail->Body    = "This is the HTML message body in bold!";
        $mail->AltBody = " Your payment is Declined";

        if(!$mail->send()) {
            echo "Message could not be sent.";
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message has been sent";
        error_log("message sent");
        }


        }

        //payment status is Reversed

        if($payment_status == "Reversed"){
            //send mail to payment status Reversed

            $sql="SELECT `email_id` FROM `registered_player` WHERE `username`='".$username."'";
            $result=$conn->query($sql);
            $row = mysqli_fetch_assoc($result);
            $to = $row['email_id'];

            $sql="SELECT `fullname` FROM `registered_player` WHERE `username`='".$username."'";
            $result=$conn->query($sql);
            $row = mysqli_fetch_assoc($result);
            $fullname = $row['fullname'];


            //error_log("your messageeeeeeeeee".$to );

            //$subject = "payment is Reversed";

            $mail = new PHPMailer;

            //$mail->SMTPDebug = 3;                               // Enable verbose debug output

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->Host = "smtp.gmail.com";  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = "support@reallivesworld.com";                 // SMTP username
            $mail->Password = "neeti@2017";                           // SMTP password
            $mail->SMTPSecure = "tls";                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            $mail->setFrom("support@reallivesworld.com", "Support");
            $mail->addAddress("$to", "$username");     // Add a recipient
            $mail->addAddress("support@reallivesworld.com");
            $mail->isHTML(true);                                  // Set email format to HTML


            $search=array('%username%','%fullname%','%email%');
            $replace=array($username,$fullname,$to);
            if($subRole == $roleGameUser ){
                //for gamer mail
                $mail->Subject = "Payment Reversed";
            $content = str_replace($search, $replace, file_get_contents('afterpaymentreversed.html'));
            $mail->msgHTML($content, dirname("afterpaymentreversed.html"));
            }
            if($subRole == $roleSchoolAdmin)
            {
                    //for school mail
                $mail->Subject = "Payment Reversed For School";
            $content = str_replace($search, $replace, file_get_contents('afterpaymentreversed.html'));
            $mail->msgHTML($content, dirname("afterpaymentreversed.html"));
            }

            if($subRole == $roleOrganizationAdmin){
                //for organization mail
                $mail->Subject = "Payment Reversed For Organization";
            $content = str_replace($search, $replace, file_get_contents('afterpaymentreversed.html'));
            $mail->msgHTML($content, dirname("afterpaymentreversed.html"));
                
            }
            // $content = str_replace($search, $replace, file_get_contents('afterpaymentreversed.html'));
            //$content = str_replace('%fullname%', $fullname, file_get_contents('afterpaymentreversed.html'));
            // $mail->msgHTML($content, dirname("afterpaymentreversed.html"));
            //$mail->MsgHTML(file_get_contents('afterpaymentreversed.html'));
            //$message = str_replace("username", $username, "username"); 
            //$mail->Body    = "This is the HTML message body in bold!";
            $mail->AltBody = " Your payment is Reversed";

            if(!$mail->send()) {
                echo "Message could not be sent.";
                echo "Mailer Error: " . $mail->ErrorInfo;
            } else {
                echo "Message has been sent";
            error_log("message sent");
            }


        }


        //payment status is Refunded

        if($payment_status == "Refunded"){
        //send mail to payment status Refunded

        $sql="SELECT `email_id` FROM `registered_player` WHERE `username`='".$username."'";
        $result=$conn->query($sql);
        $row = mysqli_fetch_assoc($result);
        $to = $row['email_id'];

        $sql="SELECT `fullname` FROM `registered_player` WHERE `username`='".$username."'";
        $result=$conn->query($sql);
        $row = mysqli_fetch_assoc($result);
        $fullname = $row['fullname'];


        //error_log("your messageeeeeeeeee".$to );

        //$subject = "payment is Refunded";

        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->Host = "smtp.gmail.com";  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = "support@reallivesworld.com";                 // SMTP username
        $mail->Password = "neeti@2017";                           // SMTP password
        $mail->SMTPSecure = "tls";                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom("support@reallivesworld.com", "Support");
        $mail->addAddress("$to", "$username");     // Add a recipient
        $mail->addAddress("support@reallivesworld.com");
        $mail->isHTML(true);                                  // Set email format to HTML


        $search=array('%username%','%fullname%','%email%');
        $replace=array($username,$fullname,$to);
        if($subRole == $roleGameUser ){
            //for gamer mail
            $mail->Subject = "Payment Refunded";
        $content = str_replace($search, $replace, file_get_contents('afterpaymentrefunded.html'));
        $mail->msgHTML($content, dirname("afterpaymentrefunded.html"));
        }
        if( $subRole == $roleOrganizationAdmin){
            $mail->Subject = "Payment Refunded For Organization";
        $content = str_replace($search, $replace, file_get_contents('afterpaymentrefunded.html'));
        $mail->msgHTML($content, dirname("afterpaymentrefunded.html"));
        }
        if($subRole == $roleSchoolAdmin){
            //for school mail
        $mail->Subject = "Payment Refunded For School";
        $content = str_replace($search, $replace, file_get_contents('afterpaymentrefunded.html'));
        $mail->msgHTML($content, dirname("afterpaymentrefunded.html"));
            
        }
        // $content = str_replace($search, $replace, file_get_contents('afterpaymentrefunded.html'));
        //$content = str_replace('%fullname%', $fullname, file_get_contents('afterpaymentrefunded.html'));
        // $mail->msgHTML($content, dirname("afterpaymentrefunded.html"));
        //$mail->MsgHTML(file_get_contents('afterpaymentrefunded.html'));
        //$message = str_replace("username", $username, "username"); 
        //$mail->Body    = "This is the HTML message body in bold!";
        $mail->AltBody = " Your payment is Refunded";

        if(!$mail->send()) {
            echo "Message could not be sent.";
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message has been sent";
        error_log("message sent");
        }


        }  
    }
}