<?php

namespace App\Http\Controllers;

use App\DataTables\AssignmentDataTable;
use App\DataTables\JoinedAssignmentDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAssignmentRequest;
use App\Http\Requests\UpdateAssignmentRequest;
use App\Models\Assignment;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class AssignmentController extends AppBaseController
{

	public function __construct(){
		$this->middleware('admin', ['only'=>['create']]);
	}
    /**
     * Display a listing of the Assignment.
     *
     * @param AssignmentDataTable $assignmentDataTable
     * @return Response
     */
    public function index(AssignmentDataTable $assignmentDataTable,Request $request)
    {
        $cat = $request->get('cat');
        if ($cat)
        {
            $locale =  session('locale');
            $url =  env('REALLIVES_API_URL') . '/game/getAllStudentAssignmentsBySubject/false/'.$cat.'/'.$locale;
            $token = session('game_token');

            // dd($token);
            if (!$token)
            {
                return back();
            }
            $response =  \Http::withOptions([
                'verify' => false,
            ])->withHeaders([
                'Authorization' => $token
                //'Authorization' => '61e678d17ca3af16a33271cf'
            ])->get($url);

    //        dd($response->body());
 //       return json_decode($response->body(), true);
                $value =  json_decode($response->body(), true);

    //        $value = collect($value);
    //        dd($value);
            return view('assignments.assignmentlist', ['value' => $value]);

        }


        return $assignmentDataTable->render('assignments.index');
    }

    public function assignment_subjects(Request $req)
    {

	   $create = empty($req->get('create'))?'assignment':$req->get('create');
       $locale =  session('locale');

        $url =  env('REALLIVES_API_URL') . '/game/getAllSubjects/false/'.$locale;
//echo $url.'<br><br>';
//dd($url);
        $userid = 0;// auth()->user()->id;
        $token = session('game_token');

       // dd($token);
        if (!$token)
        {
            return back();
        }
        $response =  \Http::withOptions([
            'verify' => false,
        ])->withHeaders([
            'Authorization' => $token
            //'Authorization' => '61e678d17ca3af16a33271cf'
        ])->get($url);

//        dd($response->body());
//        return json_decode($response->body(), true);
        $value =  json_decode($response->body(), true);

//        $value = collect($value);
//        dd($value);
        return view('assignments.categories', ['categories' => $value, 'create'=>$create]);

    }


    public function getassignments(Request $request)
    {
        $cat = $request->get('category');
        return \App\Models\Assignment::where('category', '=', $cat)->get();
    }



    /**
     * Show the form for creating a new Assignment.
     *
     * @return Response
     */
    public function create()
    {
        return view('assignments.create');
    }

    /**
     * Store a newly created Assignment in storage.
     *
     * @param CreateAssignmentRequest $request
     *
     * @return Response
     */
    public function store(CreateAssignmentRequest $request)
    {
        $input = $request->all();

        /** @var Assignment $assignment */
        $assignment = Assignment::create($input);

        Flash::success(__('messages.saved', ['model' => __('assignments.singular')]));

        return redirect(route('assignments.index'));
    }

    /**
     * Display the specified Assignment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Assignment $assignment */
        $assignment = Assignment::find($id);

        if (empty($assignment)) {
            Flash::error(__('assignments.singular').' '.__('messages.not_found'));

            return redirect(route('assignments.index'));
        }

        return view('assignments.show')->with('assignment', $assignment);
    }

    /**
     * Show the form for editing the specified Assignment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Assignment $assignment */
        $assignment = Assignment::find($id);

        if (empty($assignment)) {
            Flash::error(__('messages.not_found', ['model' => __('assignments.singular')]));

            return redirect(route('assignments.index'));
        }

        return view('assignments.edit')->with('assignment', $assignment);
    }

    /**
     * Update the specified Assignment in storage.
     *
     * @param  int              $id
     * @param UpdateAssignmentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAssignmentRequest $request)
    {
        /** @var Assignment $assignment */
        $assignment = Assignment::find($id);

        if (empty($assignment)) {
            Flash::error(__('messages.not_found', ['model' => __('assignments.singular')]));

            return redirect(route('assignments.index'));
        }

        $assignment->fill($request->all());
        $assignment->save();

        Flash::success(__('messages.updated', ['model' => __('assignments.singular')]));

        return redirect(route('assignments.index'));
    }

    /**
     * Remove the specified Assignment from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Assignment $assignment */
        $assignment = Assignment::find($id);

        if (empty($assignment)) {
            Flash::error(__('messages.not_found', ['model' => __('assignments.singular')]));

            return redirect(route('assignments.index'));
        }

        $assignment->delete();

        Flash::success(__('messages.deleted', ['model' => __('assignments.singular')]));

        return redirect(route('assignments.index'));
    }
}
