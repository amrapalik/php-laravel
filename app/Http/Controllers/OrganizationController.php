<?php

namespace App\Http\Controllers;

use App\DataTables\OrganizationDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrganizationRequest;
use App\Http\Requests\UpdateOrganizationRequest;
use App\Http\Requests\RegisterOrganizationRequest;
use App\Jobs\SendEmail;
use App\Models\Organization;
use App\Models\Role;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Response;

class OrganizationController extends AppBaseController
{
    /**
     * Display a listing of the Organization.
     *
     * @param OrganizationDataTable $organizationDataTable
     * @return Response
     */
    public function index(OrganizationDataTable $organizationDataTable)
    {
        return $organizationDataTable->render('organizations.index');
    }

    /**
     * Show the form for creating a new Organization.
     *
     * @return Response
     */
    public function create()
    {
        return view('organizations.create');
    }

    public function showRegistrationForm()
    {
        return view('registrations.organization_registration');
    }


    public function register(RegisterOrganizationRequest $request)
    {
       // dd("sddd");
        $organization = $request->except('firstname','lastname','username','password','email','user_mobile','gender','dob','designation');

        $organization = Organization::create($organization);


        $user = $request->only('firstname','lastname','username','password','email','country_code','user_mobile','gender','dob','designation','address','country','state','city','zipcode');

        $user['password'] = Hash::make($user['password']);
        $user['drole'] = "Organization Admin";
        $user['organization_orgid'] = $organization->orgid;

        $user['firstname'] = ucwords( $user['firstname']);
        $user['lastname'] = ucwords( $user['lastname']);

        $user['name'] = ucwords( $user['firstname']) .' '. ucwords( $user['lastname']);

        $user = User::create($user);

        set_game_token_in_session($request , $user->id);
        // Assign Role
        $role = Role::where('name','=','Organization Admin')->first();
        $user->assignRole($role);


        // Set Organization and school id in session
        // we use this to show info related to this school & organization
        session(['organization_orgid' => $organization->orgid]);
        session(['school_schid' => 0]);


        Flash::success(__('messages.saved', ['model' => __('organizations.singular')]));

        Auth::login($user);


        // Send Email


        $orgType = "";
        if($organization->org_type == 'organization'){
            $orgType = "school organisation";
        }
        else{
            $orgType = "university";
        }
        $details = array(
            'fullname' => $user->name,
            'username' => $user->username,
            'view'=>'emails/Organization/afterRegistrationOrganisation',
            'subject'=>'Registration Successful',
            'email'=> $user->email,
            'to_fullname'=> $user->name,
            'from'=> env('MAIL_FROM_ADDRESS','support@reallivesworld.com'),
            'from_name'=>env('MAIL_FROM_NAME',env('APP_NAME')),
            'organ_name'=> $organization->org_name,
            'org_type'=> $orgType
        );

        // return view('emails/Gamer/afterRegistrationOrganisation',$details  );
		try{
        	SendEmail::dispatch($details);
		}
		catch(\Exception $e){
			// do nothing for now
		}

        return redirect(RouteServiceProvider::School);

        return redirect(route('organizations.index'));
    }

    /**
     * Store a newly created Organization in storage.
     *
     * @param CreateOrganizationRequest $request
     *
     * @return Response
     */
    public function store(CreateOrganizationRequest $request)
    {
        $input = $request->all();

        /** @var Organization $organization */
        $organization = Organization::create($input);

        Flash::success(__('messages.saved', ['model' => __('organizations.singular')]));

        return redirect(route('organizations.index'));
    }

    /**
     * Display the specified Organization.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Organization $organization */
        $organization = Organization::find($id);

        if (empty($organization)) {
            Flash::error(__('organizations.singular').' '.__('messages.not_found'));

            return redirect(route('organizations.index'));
        }

        return view('organizations.show')->with('organization', $organization);
    }

    /**
     * Show the form for editing the specified Organization.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Organization $organization */
        $organization = Organization::find($id);

        if (empty($organization)) {
            Flash::error(__('messages.not_found', ['model' => __('organizations.singular')]));

            return redirect(route('organizations.index'));
        }

        return view('organizations.edit')->with('organization', $organization);
    }

    /**
     * Update the specified Organization in storage.
     *
     * @param  int              $id
     * @param UpdateOrganizationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrganizationRequest $request)
    {
        /** @var Organization $organization */
        $organization = Organization::find($id);

        if (empty($organization)) {
            Flash::error(__('messages.not_found', ['model' => __('organizations.singular')]));

            return redirect(route('organizations.index'));
        }

        $organization->fill($request->all());
        $organization->save();

        Flash::success(__('messages.updated', ['model' => __('organizations.singular')]));

        return redirect(route('organizations.index'));
    }

    /**
     * Remove the specified Organization from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Organization $organization */
        $organization = Organization::find($id);

        if (empty($organization)) {
            Flash::error(__('messages.not_found', ['model' => __('organizations.singular')]));

            return redirect(route('organizations.index'));
        }

        $organization->delete();

        Flash::success(__('messages.deleted', ['model' => __('organizations.singular')]));

        return redirect(route('organizations.index'));
    }
}
