<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\NotificationsDataTable;
use App\Models\Notification;

class NotificationsController extends Controller
{
	public function index(NotificationsDataTable $notificationsDataTable){
		return $notificationsDataTable->render('notifications.index');
	}

	public function delete(Request $request){
		Notification::where('to', auth()->user()->id)->where('id', $request->id)->delete();
		return redirect('/notifications'); 
	}
}
