<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests;
use App\Jobs\SendEmail;
use App\Models\Permission;
use App\Models\Role;
use App\Models\School;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

use Image;

class GeneralController extends AppBaseController
{






    public function sendmail()
    {
        // Test Email

        $views = [
            0=>'emails/Organization/afterPaymentCompletion',
            1=> 'emails/Organization/afterRegistrationOrganisation',
            2=> 'emails/Organization/afterAssigningTeacher',

            3=> 'emails/School/afterAssigningTeacher',
            4=> 'emails/School/afterAssigningSchoolAdmin',
            5=> 'emails/School/afterRegistrationSchool',
            6=> 'emails/password-reset',

            ];

        $viewname = 6;

        $details = array(
            'fullname' => "Parag Mankekar",
            'username' => "parag123",
            'organ_name'=>'Nisarg Bahuuddeshiya Sanstha',
            'school_name'=> 'MLZS Latur',
            'view'=>$views[$viewname],
            'subject'=>'Abc Subject',
            'email'=>'dhsont@gmail.com',
            'password'=>'password',
            'to_fullname'=>'Dhiraj Sontakke',
            'from'=>'support@reallivesworld.com',
            'from_name'=>'RealLives',
            'schoolName'=>'XYZ School',
            'token'=>'asdsadasdadadasdasdasdasdasdasdasdadasdasd',
            'Package'=>'XYZ',
            'Purchase_date'=>'1234',
            'Licence_quantity'=>'11',
            'price'=>'1111',
            'renewal_date'=>'12-3-2022'
        );


          return view($views[$viewname],$details  );


        SendEmail::dispatch($details);


        echo "Successfully sent the email";
    }



    public function assign_pages_to_roles_form()
    {
        return view('roles/attach_pages_to _roles');
    }

    public function upload_photo_form()
    {
        return view('general/upload_photo');
    }


    public function upload_school_logo(Request $request)
    {
        $request->validate([
            'image' => 'required|mimes:jpg,jpeg,png|max:2048',
        ]);

        $schoolid =  get_school_id();

        // $filename = $schoolid.'.jpg';//. $request->image->getClientOriginalExtension();
        // $thumb_img = Image::make($request->image->getRealPath())->widen(100);
        // $destinationPath = '/storage/public/images/'; // public_path('/storage/schools');
        // $thumb_img->save($destinationPath . '/'. $filename, 100);
        // $input['image'] = $filename;

        if( $request->file('image') ) {
            $input['image'] = $this->_uploadMedia($request, 'image', 'images');
            $filename = $input['image'];
        }

        $school = School::find($schoolid);
        $school->logo = $filename;
        $school->save();

        return back()
            ->with('success',__('general.You have successfully uploaded your school logo'))
            ->with('file',$filename);
        return view('general/upload_photo');
    }


    public function upload_profile_photo(Request $request)
    {
        $request->validate([
            'image' => 'required|mimes:jpg,jpeg,png|max:2048',
        ]);


        // $filename = auth()->user()->id.'.'. $request->image->getClientOriginalExtension();
        // $thumb_img = Image::make($request->image->getRealPath())->widen(100);
        // $destinationPath = 'public/users'; // public_path('/storage/users');
        // $thumb_img->save($destinationPath . '/'. $filename, 100);
        // $input['image'] = $filename;
        if( $request->file('image') ) {
            $input['image'] = $this->_uploadMedia($request, 'image', 'users');
            $filename = $input['image'];
        }

        $user = User::find(auth()->user()->id);

        $user->avatar= $filename;
        $user->save();

        return back()
            ->with('success',__('general.You have successfully uploaded your profile image'))
            ->with('file',$filename);
        return view('general/upload_photo');
    }



    public function assign_pages_to_roles(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'role_id' => 'required|integer',
            'page_id' => 'required|integer',
        ]);
        $role_id = $request->get('role_id');
        $page_id = $request->get('page_id');
        $role = \Spatie\Permission\Models\Role::find($role_id);
        $permissions = \Spatie\Permission\Models\Permission::where('page_pageid','=',$page_id)->get();
        $role->givePermissionTo($permissions);

        return $this->sendResponse([], 'Page assigned to role successfully');
    }



    public function get_roles_pages_list(Request $request)
    {
        $role_id =  $request->get('role_id');
        $role = Role::find($role_id);
        $permission = Permission::where('name','=','edit articles')->first();

        // $role = \Spatie\Permission\Models\Role::find($role_id);
        $permissions = $role->permissions()->get();

        $permissions =  $permissions->groupBy('frompage');

        return $this->sendResponse($permissions->toArray(), 'data retrieved successfully');

    }


    public function unlink_page_from_role(Request $request)
    {
        $role_id =  $request->get('role_id');
        $page_id =  $request->get('page_id');

//
//        $role = Role::where('name','=','writer')->first();
//        $permission = Permission::where('name','=','edit articles')->first();

        $role = \Spatie\Permission\Models\Role::find($role_id);
        $permission = \Spatie\Permission\Models\Permission::find($page_id);

        $role->revokePermissionTo($permission);

        return $this->sendResponse([], 'Page access  removed successfully');

    }

}
