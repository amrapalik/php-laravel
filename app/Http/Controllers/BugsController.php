<?php

namespace App\Http\Controllers;

use App\DataTables\BugsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateBugsRequest;
use App\Http\Requests\UpdateBugsRequest;
use App\Jobs\SendEmail;
use App\Models\Bugs;
use Flash;
use App\Http\Controllers\AppBaseController;
use Image;
use Response;

class BugsController extends AppBaseController
{
    public function __construct() {
		$this->middleware('admin', ['only'=>['index', 'show','edit']]);
    }
    /**
     * Display a listing of the Bugs.
     *
     * @param BugsDataTable $bugsDataTable
     * @return Response
     */
    public function index(BugsDataTable $bugsDataTable)
    {
        return $bugsDataTable->render('bugs.index');
    }

    /**
     * Show the form for creating a new Bugs.
     *
     * @return Response
     */
    public function create()
    {
        return view('bugs.create');
    }

    /**
     * Store a newly created Bugs in storage.
     *
     * @param CreateBugsRequest $request
     *
     * @return Response
     */
    public function store(CreateBugsRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = auth()->user()->id;

        if ($request->hasFile('image'))
        {
            $filename = time() . $request->image->getClientOriginalName();
            $destinationPath = public_path('/storage/bugs');
            $thumb_img = Image::make($request->image->getRealPath())->widen(500);
            $thumb_img->save($destinationPath . '/' . $filename, 100);
            $input['image'] = $filename;
        }
        //dd($input);
        /** @var Bugs $bugs */
        $bugs = Bugs::create($input);

        Flash::success(__('bugs.Bug Submitted', ['model' => __('bugs.Bug Submitted')]));


        $user = auth()->user();

        if ($input['type'] == 'Feedback'){
            $view = ' emails/Feedback/feedback';
            $sub = 'Feedback';
        }
        else
        {
            $view = ' emails/Feedback/bugReport';
            $sub = 'Bug Report';
        }

        echo $input['title'];
        $details = array(
            'fullname' => $user->name,
            'username' => $user->username,
            'view'=>$view,
            'subject'=>$sub,
            'raw' =>$input['description'],
            'email'=> $user->email,
            'to_fullname'=> $user->name,
            'from'=> env('MAIL_FROM_ADDRESS','support@reallivesworld.com'),
            'from_name'=>env('MAIL_FROM_NAME',env('APP_NAME')),

        );

        // return view('emails/School/afterAssigningTeacher',$details);
        SendEmail::dispatch($details);
        $details1 = array(
            'fullname' => $user->name,
            'username' => $user->username,
            'view'=>'emails/feedback-bug',
            'subject'=>$sub,
            'title'=>$input['title'],
            'type'=>$input['type'],
            'raw'=>$input['description'],
            'email'=> env('MAIL_FROM_ADDRESS','support@reallivesworld.com'),
            'user_email'=> $user->email,
            'to_fullname'=> $user->name,
            'from'=> env('MAIL_FROM_ADDRESS','support@reallivesworld.com'),
            'from_name'=>env('MAIL_FROM_NAME',env('APP_NAME')),
        );
        SendEmail::dispatch($details1);

        return redirect()->back();
    }

    /**
     * Display the specified Bugs.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

        /** @var Bugs $bugs */
        $bugs = Bugs::find($id);

        if (empty($bugs)) {
            Flash::error(__('bugs.singular').' '.__('messages.not_found'));

            return redirect(route('bugs.index'));
        }

        return view('bugs.show')->with('bugs', $bugs);
    }

    /**
     * Show the form for editing the specified Bugs.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        /** @var Bugs $bugs */
        $bugs = Bugs::find($id);

        if (empty($bugs)) {
            Flash::error(__('messages.not_found', ['model' => __('bugs.singular')]));

            return redirect(route('bugs.index'));
        }

        return view('bugs.edit')->with('bugs', $bugs);
    }

    /**
     * Update the specified Bugs in storage.
     *
     * @param  int              $id
     * @param UpdateBugsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBugsRequest $request)
    {
        /** @var Bugs $bugs */
        $bugs = Bugs::find($id);

        if (empty($bugs)) {
            Flash::error(__('messages.not_found', ['model' => __('bugs.singular')]));

            return redirect(route('bugs.index'));
        }

        $bugs->fill($request->all());
        $bugs->save();

        Flash::success(__('messages.updated', ['model' => __('bugs.singular')]));

        return redirect(route('bugs.index'));
    }

    /**
     * Remove the specified Bugs from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Bugs $bugs */
        $bugs = Bugs::find($id);

        if (empty($bugs)) {
            Flash::error(__('messages.not_found', ['model' => __('bugs.singular')]));

            return redirect(route('bugs.index'));
        }

        $bugs->delete();

        Flash::success(__('messages.deleted', ['model' => __('bugs.singular')]));

        return redirect(route('bugs.index'));
    }
}
