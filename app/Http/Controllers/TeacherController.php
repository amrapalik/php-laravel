<?php

namespace App\Http\Controllers;

use App\DataTables\StudentDataTable;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests;
use App\Http\Requests\CreateStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Models\Role;
use App\Models\Student;
use Flash;

use Illuminate\Http\Request;
use Response;

class TeacherController extends AppBaseController
{

    public function teacherdashboard( $teacherid ,Request $request )
    {

        if(teacher_from_this_school($teacherid))
        {

            return view('user_dashboards/teacher')->with('teacherid',$teacherid);


        }else{
            return 'no';
        }

        dd($teacherid);

        return $studentDataTable->render('students.index');
    }

}
