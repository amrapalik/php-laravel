<?php

namespace App\Http\Controllers;

use App\DataTables\SchoolDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSchoolRequest;
use App\Http\Requests\RegisterSchoolRequest;
use App\Http\Requests\UpdateSchoolRequest;
use App\Jobs\SendEmail;
use App\Models\Organization;
use App\Models\Role;
use App\Models\School;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Response;

class SchoolController extends AppBaseController
{
    /**
     * Display a listing of the School.
     *
     * @param SchoolDataTable $schoolDataTable
     * @return Response
     */
    public function index(SchoolDataTable $schoolDataTable)
    {
      //  dd(auth()->user()->drole);
        if (auth()->user()->drole == 'Organization Admin' || auth()->user()->drole == 'Admin')
        return $schoolDataTable->render('schools.index');

        return  redirect()->back();
    }

//schoolDashboard

    public function school_dashboard()
    {
        return view('schools.dashboard');

        return $schoolDataTable->render('schools.index');
    }

    public function showRegistrationForm()
    {
        //return "dfdf";
        return view('registrations.school_registration');
    }

    public function register(RegisterSchoolRequest   $request)
    {
        $school = $request->except('firstname','lastname','username','password','email','gender','dob','designation');
        $school['sch_name'] = ucwords($school['sch_name']);
        $school = School::create($school);


        $user = $request->only('firstname','lastname','username','password','email','country_code','user_mobile','gender','dob','designation','address1','country','state','city','zipcode');


        $user['password'] = Hash::make($user['password']);
        $user['drole'] = "School Admin";

        $user['school_schid'] = $school->schid;  // Assign School id
        $user['firstname'] = ucwords( $user['firstname']);
        $user['lastname'] = ucwords( $user['lastname']);
        $user['name'] = ucwords( $user['firstname']) .' '. ucwords( $user['lastname']);
        $user['address']=$user['address1'];
        $user = User::create($user);

        $role = Role::where('name','=','School Admin')->first();
        $user->assignRole($role);

        // Set Organization and school id in session
        // we use this to show info related to this schools
        session(['organization_orgid' => 0]);
        session(['school_schid' => $school->schid]);


        Flash::success(__('registrations.school.register_success'));

        Auth::login($user);
        set_game_token_in_session($request ,$user->id);

        $schoolType="";
        if($school->school_type=='school'){
            $schoolType = "school";
        }
        elseif($school->school_type=='university_dept'){
            $schoolType = "department";
        }
        else{
            $schoolType = "family";
        }
        // Send Registration Email
        $details = array(
            'fullname' => $user->name,
            'username' => $user->username,
            'view'=>'emails/School/afterRegistrationSchool',
            'subject'=>'Registration Successful',
            'email'=> $user->email,
            'to_fullname'=> $user->name,
            'from'=> env('MAIL_FROM_ADDRESS','support@reallivesworld.com'),
            'from_name'=>env('MAIL_FROM_NAME',env('APP_NAME')),
            'organ_name'=> 'Na',
            'school_name'=> $school->sch_name,
            'school_type'=> $schoolType
        );

        // return view('emails/Gamer/afterRegistrationOrganisation',$details  );
		try{
        	SendEmail::dispatch($details);
		}
		catch(\Exception $e){
			// do nothing
		}

        return redirect('/purchase_license_list');
        return redirect(RouteServiceProvider::School);

        return redirect(route('organizations.index'));
    }

    /**
     * Show the form for creating a new School.
     *
     * @return Response
     */
    public function create()
    {

      //  dd( session('organization_orgid'));
        return view('schools.create');
    }

    /**
     * Store a newly created School in storage.
     *
     * @param CreateSchoolRequest $request
     *
     * @return Response
     */
    public function store(CreateSchoolRequest $request)
    {
        $input = $request->all();

        $input['organization_orgid'] = session('organization_orgid');
        $org = Organization::find(session('organization_orgid'));
        if($org->org_type=='organization'){
            $input['school_type'] = 'school';
        }
        else{
            $input['school_type'] = 'university_dept';
        }
        $school = School::create($input);
		// create school admin
		$input['school_schid'] = $school->schid;
		$input['password'] = Hash::make($input['password']);
		$input['drole'] = 'School Admin';
		$input['name'] = $input['firstname'].' '.$input['lastname'];
		$school_admin = User::create($input);

        Flash::success(__('messages.saved', ['model' => __('schools.singular')]));
        return redirect(route('schools.index'));
    }

    /**
     * Display the specified School.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var School $school */
        $school = School::find($id);

        if (empty($school)) {
            Flash::error(__('schools.singular').' '.__('messages.not_found'));

            return redirect(route('schools.index'));
        }

        return view('schools.show')->with('school', $school);
    }

    /**
     * Show the form for editing the specified School.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var School $school */
        $school = School::find($id);

        if (empty($school)) {
            Flash::error(__('messages.not_found', ['model' => __('schools.singular')]));

            return redirect(route('schools.index'));
        }

        return view('schools.edit')->with('school', $school);
    }

    /**
     * Update the specified School in storage.
     *
     * @param  int              $id
     * @param UpdateSchoolRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSchoolRequest $request)
    {
        /** @var School $school */
        $school = School::find($id);

        if (empty($school)) {
            Flash::error(__('messages.not_found', ['model' => __('schools.singular')]));

            return redirect(route('schools.index'));
        }

        $school->fill($request->all());
        $school->save();

        Flash::success(__('messages.updated', ['model' => __('schools.singular')]));

        return redirect(route('schools.index'));
    }

    /**
     * Remove the specified School from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var School $school */
        $school = School::find($id);

        if (empty($school)) {
            Flash::error(__('messages.not_found', ['model' => __('schools.singular')]));

            return redirect(route('schools.index'));
        }

        $school->delete();

        Flash::success(__('messages.deleted', ['model' => __('schools.singular')]));

        return redirect(route('schools.index'));
    }
}
