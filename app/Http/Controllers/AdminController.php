<?php

namespace App\Http\Controllers;


use App\DataTables\AssignmentStudentDatatable;
use App\DataTables\ClassesAssignedDataTable;
use App\DataTables\ClassesDataTable;
use App\DataTables\ClassStudentDatatable;
use App\DataTables\JoinedAssignmentDataTable;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests;
use App\Http\Requests\CreateClassesRequest;
use App\Http\Requests\UpdateClassesRequest;
use App\Models\AssignmentComment;
use App\Models\Classes;
use App\Models\ClassUser;
use App\Models\Student;
use App\Models\StudentAssignment;
use App\Models\User;
use Flash;
 use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class AdminController extends AppBaseController
{

    public function __construct() {
        // permission:publish articles'
        //   $this->middleware('permission', ['only' => ['create']]);
        //  $this->middleware(['permission:Create bugs'], ['only' => ['create']]);
       // $this->middleware(['permission:Create classes'], ['only' => ['create']]);

        // $this->middleware('post-edit', ['only' => ['edit']]);
    }


    public function admindashboard()
    {
        return view('admin/dashboard');
    }

    public function assignedClasses(ClassesAssignedDataTable $classesDataTable)
    {
        return $classesDataTable->render('classes.assigned_classes');
    }






}
