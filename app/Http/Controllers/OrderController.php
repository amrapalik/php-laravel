<?php

namespace App\Http\Controllers;

use App\DataTables\OrderDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Order;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OrderController extends AppBaseController
{
    /**
     * Display a listing of the Order.
     *
     * @param OrderDataTable $orderDataTable
     * @return Response
     */
    public function index(OrderDataTable $orderDataTable)
    {
        if(auth()->user()->drole == 'Gamer')
        {
            return $orderDataTable->render('orders.index');
        }
        else{
            $schoolid =  session('school_schid');
            
            if ($schoolid == null) {
                $org= \App\Models\Organization::find(auth()->user()->organization_orgid);

                if($org->org_type == 'organization'){
                    \Flash::error(__('orders.please select a school'),['model' => __('schools.singular')]);
                }
                else{
                    \Flash::error(__('orders.please select a department'),['model' => __('schools.singular')]);
                }
                // Flash::error(__('orders.please select a school').' '.__(''));
                
                return redirect('/schools');            }

            return $orderDataTable->render('orders.index');
        }
    }

    /**
     * Show the form for creating a new Order.
     *
     * @return Response
     */
    public function create()
    {
        return view('orders.create');
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param CreateOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderRequest $request)
    {
        $input = $request->all();

        /** @var Order $order */
        $order = Order::create($input);

        Flash::success(__('messages.saved', ['model' => __('orders.singular')]));

        return redirect(route('orders.index'));
    }

    /**
     * Display the specified Order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Order $order */
        $order = Order::find($id);
        if(auth()->user()->drole == 'Gamer')
        {
            return view('orders.show')->with('order', $order);
        }
        else{
            $schoolid =  session('school_schid');
            
            if ($schoolid == null) {
                Flash::error(__('orders.singular').' '.__('messages.not_found'));
                
                return redirect(route('/schools'));
            }

            return view('orders.show')->with('order', $order);
        }
    }

    /**
     * Show the form for editing the specified Order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Order $order */
        $order = Order::find($id);

        if (empty($order)) {
            Flash::error(__('messages.not_found', ['model' => __('orders.singular')]));

            return redirect(route('orders.index'));
        }

        return view('orders.edit')->with('order', $order);
    }

    /**
     * Update the specified Order in storage.
     *
     * @param  int              $id
     * @param UpdateOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderRequest $request)
    {
        /** @var Order $order */
        $order = Order::find($id);

        if (empty($order)) {
            Flash::error(__('messages.not_found', ['model' => __('orders.singular')]));

            return redirect(route('orders.index'));
        }

        $order->fill($request->all());
        $order->save();

        Flash::success(__('messages.updated', ['model' => __('orders.singular')]));

        return redirect(route('orders.index'));
    }

    /**
     * Remove the specified Order from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Order $order */
        $order = Order::find($id);

        if (empty($order)) {
            Flash::error(__('messages.not_found', ['model' => __('orders.singular')]));

            return redirect(route('orders.index'));
        }

        $order->delete();

        Flash::success(__('messages.deleted', ['model' => __('orders.singular')]));

        return redirect(route('orders.index'));
    }
}
