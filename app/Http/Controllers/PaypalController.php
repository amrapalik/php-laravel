<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Jobs\SendEmail;
use App\Models\License;
use App\Models\Order;
use App\Models\Product;
use App\Models\School;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use Log;

/** All Paypal Details class **/

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\ProductionEnvironment;
use PayPalCheckoutSdk\Core\SandboxEnvironment;

use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;

class PaypalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /** setup PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $clientId = $paypal_conf['client_id'];
        $clientSecret = $paypal_conf['secret'];
    }

    /**
     * This is the page where you have to display your payment page
     *
     * @return \Illuminate\Http\Response
     */
    public function payWithPaypal()
    {
        return view('licenses/paypal2');
// Paste smart button code in this view
    }

    /**
     * Your business logic will come here
     * Order will be created in this function and then paypal window will open for authentication
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function createOrder(Request $request) // CHANGE_MODULE : changes required for new licensing module
    {
       $order =  $request->get('product');

       $product = Product::find($order);

       if ($product == null)
       {
           return ;
       }

        $school_id = session('school_schid');

        if (($school_id == null || $school_id < 0) && auth()->user()->drole != "Gamer")
        {
            return ;
        }


        $paypal_conf = \Config::get('paypal');

        $clientId = $paypal_conf[$paypal_conf['mode']]['client_id'];
        $clientSecret = $paypal_conf[$paypal_conf['mode']]['client_secret'];
        
        //TODO:: Change this to production
        if(env('PAYPAL_MODE') == 'sandbox'){
            $environment = new SandboxEnvironment($clientId, $clientSecret);
        }
        else{
            $environment = new ProductionEnvironment($clientId, $clientSecret);    
        }
        $client = new PayPalHttpClient($environment);
        $request = new OrdersCreateRequest();
        $request->prefer('return=representation');
        $request->body = [
            "intent" => "CAPTURE",
            "purchase_units" => [[
                "reference_id" => $school_id,
                "amount" => [
                    "value" => $product->price,
                    "currency_code" => "USD"
                    //put other details here as well related to your order
                ]
            ]],
            "application_context" => [
                "cancel_url" => "http://yourproject.com/paypalreturn",
                "return_url" => "http://yourproject.com/paypalcancel"
            ]
        ];
        try {
            // Call API with your client and get a response for your call
            $response = $client->execute($request);

            Order::create(
                [
                    'product_prodid'=>$product->prodid,
                    'user_id'=> auth()->user()->id,
                    'licences'=>$product->no_of_licenses,
                    'licences_available'=>$product->no_of_licenses,
                    'price'=>$product->price,
                    'school_schid'=> $school_id,
                   // 'expiry_date'=>,
                    'paypal_orderid'=> $response->result->id,
                    'status'=>'Pending',
                    'paid'=>0,
                ]
            );

            // If call returns body in response, you can get the deserialized version from the result attribute of the response
            echo json_encode($response);
            die;
        } catch (HttpException $ex) {
            echo $ex->statusCode;
            //echo json_encode($ex->getMessage());
        }
    }



    public function createorder_renew(Request $request)
    {

        $lic =  $request->get('lic');
        $license = Order::find($lic);
        $product = Product::find($license->product_prodid);
        

        if ($license == null)
        {
           return ;
        }
        $dt = \Carbon\Carbon::now();
        if ($license->expiry_date->isPast())
        {
            $license->expiry_date = $dt->addYear()->toDateString();
        }else{
            // If license is not expired add days from expiry day
            
            $license->expiry_date = $license->expiry_date->addYear($product->validity)->toDateString();
        }
        
        $school_id = $license->school_schid;
        //        if (($school_id == null || $school_id < 0) && auth()->user()->drole != "Gamer")
        //        {
            //            return ;
            //        }
            
            $paypal_conf = \Config::get('paypal');
            $clientId = $paypal_conf[$paypal_conf['mode']]['client_id'];
            $clientSecret = $paypal_conf[$paypal_conf['mode']]['client_secret'];
            
            //TODO:: Change this to production
            if(env('PAYPAL_MODE') == 'sandbox'){
                $environment = new SandboxEnvironment($clientId, $clientSecret);
            }
            else{
                $environment = new ProductionEnvironment($clientId, $clientSecret);    
            }
            $client = new PayPalHttpClient($environment);
            $request = new OrdersCreateRequest();
        $request->prefer('return=representation');
        $request->body = [
            "intent" => "CAPTURE",
            "purchase_units" => [[
                "reference_id" => $license->ordid,
                "amount" => [
                    "value" => $license->price,
                    "currency_code" => "USD"
                    //put other details here as well related to your order
                ]
            ]],
            "application_context" => [
                "cancel_url" => "http://yourproject.com/paypalreturn",
                "return_url" => "http://yourproject.com/paypalcancel"
            ]
        ];


        try {
            // Call API with your client and get a response for your call
            $response = $client->execute($request);

            Order::create([
                    'product_prodid'=>$license->product_prodid,
                    'user_id'=> auth()->user()->id,
                    'licences'=>0,
                    'licences_available'=>0,
                    'expiry_date'=>$license->expiry_date,
                    'price'=>$license->price,
                    'school_schid'=> $school_id,
                    'paypal_orderid'=> $response->result->id,
                    'status'=>'Pending',
                    'paid'=>0,
                ]);

            // If call returns body in response, you can get the deserialized version from the result attribute of the response
            echo json_encode($response);
            die;
        } catch (HttpException $ex) {
            echo $ex->statusCode;
            //echo json_encode($ex->getMessage());
        }
    }






    /**
     * After successful paypal authentication from paypal window, paypal will call this function to capture the order
     *
     * @param   $orderId returned from papal and also defined in routes
     * @return \Illuminate\Http\Response
     */
    public function captureRenewPaymentWithpaypal(Request $request,$orderId)
    {
        $paypal_conf = \Config::get('paypal');
        $licid =  $request->get('lic');
        $clientId = $paypal_conf[$paypal_conf['mode']]['client_id'];
        $clientSecret = $paypal_conf[$paypal_conf['mode']]['client_secret'];
        echo $licid;

        if(env('PAYPAL_MODE') == 'sandbox'){
            $environment = new SandboxEnvironment($clientId, $clientSecret);
        }
        else{
            $environment = new ProductionEnvironment($clientId, $clientSecret);    
        }
        $client = new PayPalHttpClient($environment);
// Here, OrdersCaptureRequest() creates a POST request to /v2/checkout/orders
// $response->result->id gives the orderId of the order created above
        $request = new OrdersCaptureRequest($orderId);
        $request->prefer('return=representation');
        try {
// Call API with your client and get a response for your call
            $response = $client->execute($request);

           $order = Order::where('paypal_orderid','=',$orderId)->first();

           if ($response->result->status == 'COMPLETED')
           {

               $school_id = session('school_schid');

               if (($school_id == null || $school_id < 0) && auth()->user()->drole != "Gamer")
               {
                   return ;
               }

               $order->status = $response->result->status;
               $order->paid = 1;

               $product = Product::find($order->product_prodid);

               $Oldlicense =  License::where('licid','=',$licid)->first();

               $licenses =  License::where('order_ordid','=',$Oldlicense->order_ordid)->get();
               foreach ($licenses as $license) {
                $dt = \Carbon\Carbon::now();
                $license->expiry_date = $order->expiry_date;
               // If license already expired add days from today

            //    if ($license->expiry_date->isPast())
            //    {
            //        $license->expiry_date = $dt->addYear()->toDateString();
            //    }else{
            //        // If license is not expired add days from expiry day

            //        $license->expiry_date = $license->expiry_date->addYear($product->validity)->toDateString();
            //    }
               $order->expiry_date = $license->expiry_date;
               $license->paypal_orderid= $order->paypal_orderid;
               $license->status = 'Paid';
               $license->paid = 1;
               $license->save();
            }
            $order->save();



                // It is school account
                if( auth()->user()->drole != "Gamer")
                {
                   $school = School::find($school_id);

                  // $school->purchased_licenses = $school->purchased_licenses + $order -> licences;

                    //TODO:: count and update license count
                   $school->available_licenses = $school->available_licenses + $order -> licences;

                   $school->save();

                    $student_type ="students";
                    if($school->school_type == 'family'){
                        $student_type ="children";
                    }

                   $details = array(
                       'fullname' => auth()->user()->name,//"Parag Mankekar",
                       'username' => auth()->user()->username,//"parag123",
                       'view'=>'emails/School/SchoolafterRenewalCompletion',
                       'subject'=>'License renewed',
                       'from'=> env('MAIL_FROM_ADDRESS','support@reallivesworld.com'),
                       'from_name'=>env('MAIL_FROM_NAME',env('APP_NAME')),
                       'organ_name'=>'XYZ Organization',
                       'email'=> auth()->user()->email,//
                       'to_fullname'=> auth()->user()->name,//'Dhiraj Sontakke',
                       'schoolName'=> $school->sch_name, //'XYZ School',
                       'Package'=>$product->prod_name,
                       'Purchase_date'=>$order->created_at->format('m/d/Y'),//'1234',
                       'Licence_quantity'=>10000, // 11
                       'price'=>$order->price,//'1111',
                    //    'renewal_date'=>$order->expiry_date->format('m/d/Y'),
                       'student_type'=>$student_type

                   );

                    // SendEmail::dispatch($details);

                  // return view('emails/Organization/afterPaymentCompletion',$details  );

                }else{   // User is gamer
                  $user =  User::find(auth()->user()->id);

                   $dt = \Carbon\Carbon::now();


                   $user->account_expiry_date  =  $license->expiry_date;
                   $user->purchaseflag = "Yes";
                   $user->order_ordid = $order->ordid;

                  // $user->account_expiry_date
                   $user->save();

                   $details = array(
                       'fullname' => $user->name,//"Parag Mankekar",
                       'username' => $user->username,//"parag123",
                       'organ_name'=>'XYZ Organization',
                       'view'=>'emails/Gamer/GamerAfterRenewal',
                       'subject'=>'Payment Success',
                       'email'=>  $user->email,
                       'to_fullname'=> $user->name,//'Dhiraj Sontakke',
                       'from'=> env('MAIL_FROM_ADDRESS','support@reallivesworld.com'),
                       'from_name'=>env('MAIL_FROM_NAME',env('APP_NAME'))

                   );
                    SendEmail::dispatch($details);

                  // return view('emails/Gamer/afterSuccessfullPaymentGamer',$details  );
                }


           }


// If call returns body in response, you can get the deserialized version from the result attribute of the response
            echo json_encode($response);
            die;
        } catch (HttpException $ex) {
            echo $ex->statusCode;
            die;
//print_r($ex->getMessage());
        }
    }


    /**
     * After successful paypal authentication from paypal window, paypal will call this function to capture the order
     *
     * @param   $orderId returned from papal and also defined in routes
     * @return \Illuminate\Http\Response
     */
    public function capturePaymentWithpaypal($orderId)
    {
        $paypal_conf = \Config::get('paypal');
        $clientId = $paypal_conf[$paypal_conf['mode']]['client_id'];
        $clientSecret = $paypal_conf[$paypal_conf['mode']]['client_secret'];
        if(env('PAYPAL_MODE') == 'sandbox'){
            $environment = new SandboxEnvironment($clientId, $clientSecret);
        }
        else{
            $environment = new ProductionEnvironment($clientId, $clientSecret);    
        }
        $client = new PayPalHttpClient($environment);
            // Here, OrdersCaptureRequest() creates a POST request to /v2/checkout/orders
            // $response->result->id gives the orderId of the order created above
        $request = new OrdersCaptureRequest($orderId);
        $request->prefer('return=representation');
        try {
            // Call API with your client and get a response for your call
            $response = $client->execute($request);
            $order = Order::where('paypal_orderid','=',$orderId)->first();
            $product = Product::find($order->product_prodid);
            if ($response->result->status == 'COMPLETED') {
                $school_id = session('school_schid');
                if (($school_id == null || $school_id < 0) && auth()->user()->drole != "Gamer")
                {
                    return ;
                }
                $order->status = $response->result->status;
                $order->expiry_date = \Carbon\Carbon::now()->addYear($product->validity);
                $order->paid = 1;
                $order->save();
                $userId = 0;
                if(auth()->user()->drole == 'Gamer'){
                    $userId = auth()->user()->id;
                }
                
                $available_lives = null;
                if($product->expiry_type == 'max_lives'){
                    $available_lives = $product->max_lives;
                }
                for ($i=0; $i < $product->no_of_licenses; $i++) { 
                    $dt = \Carbon\Carbon::now();
                    # code...
                    $license =  License::create(     [
                        'order_ordid'=>$order->ordid,
                        'product_prodid'=>$order->product_prodid,
                        'user_id'=> $userId,
                        'lives'=>0,
                        'available_lives'=>$available_lives,
                        'price'=>$order->price,
                        'school_schid'=> $school_id,
                        'renew_price'=> $product->renew_price,
                        'expiry_date'=> $dt->addYear($product->validity)->toDateString(),
                        'paypal_orderid'=> $order->paypal_orderid,
                        'status'=>'Paid',
                        'paid'=>1,
                    ]);
                }

                // It is school account
                if( auth()->user()->drole != "Gamer")
                {
                    $school = School::find($school_id);
                    $school->purchased_licenses = $school->purchased_licenses + $order -> licences;
                    $school->available_licenses = $school->available_licenses + $order -> licences;
                    $school->save();
                    $details = array(
                        'fullname' => auth()->user()->name,//"Parag Mankekar",
                        'username' => auth()->user()->username,//"parag123",
                        'view'=>'emails/School/afterPaymentCompletion',
                        'subject'=>'Payment Success',
                        'from'=> env('MAIL_FROM_ADDRESS','support@reallivesworld.com'),
                        'from_name'=>env('MAIL_FROM_NAME',env('APP_NAME')),
                        'organ_name'=>'XYZ Organization',
                        'email'=> auth()->user()->email,//
                        'to_fullname'=> auth()->user()->name,//'Dhiraj Sontakke',
                        'schoolName'=> $school->sch_name, //'XYZ School',
                        'Package'=>'XYZ',
                        'Purchase_date'=>$license->created_at->format('m/d/Y'),//'1234',
                        'Licence_quantity'=>$license->lives, // 11
                        'price'=>$license->price,//'1111',
                        'renewal_date'=>$license->expiry_date->format('m/d/Y')
                    );

                    SendEmail::dispatch($details);
                    // return view('emails/Organization/afterPaymentCompletion',$details  );
                } else {   // User is gamer
                    $user =  User::find(auth()->user()->id);
                    $dt = \Carbon\Carbon::now();
                    $user->account_expiry_date  = $dt->addYear($product->validity)->toDateString();
                    $user->purchaseflag = "Yes";
                    $user->order_ordid = $order->ordid;
                    
                    // $user->account_expiry_date
                    $user->save();
                    $details = array(
                        'fullname' => auth()->user()->name,//"Parag Mankekar",
                        'username' => auth()->user()->username,//"parag123",
                        'organ_name'=>'XYZ Organization',
                        'view'=>'emails/Gamer/afterSuccessfullPaymentGamer',
                        'subject'=>'Payment Success',
                        'email'=> $user->email, //$user->email;//
                        'to_fullname'=> auth()->user()->name,//'Dhiraj Sontakke',
                        'from'=>'support@reallivesworld.com',
                        'from_name'=>'RealLives'
                    );
                    if(env('PAYPAL_MODE') != 'sandbox'){
                        SendEmail::dispatch($details);

                    }

                    // return view('emails/Gamer/afterSuccessfullPaymentGamer',$details  );
                }
            }
            // If call returns body in response, you can get the deserialized version from the result attribute of the response
            echo json_encode($response);
            die;
        } catch (HttpException $ex) {
            echo $ex->statusCode;
            die;
            //print_r($ex->getMessage());
        }
    }
}
// You can also define return and cancel urls here.