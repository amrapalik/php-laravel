<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use DB;
use Auth;

class RaviController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $_tablename;
    protected $_viewname;
    protected $_id;
    protected $_request;

    protected $newvalues = array();
    protected $editvalues = array();

    public function list(Request $request)
    {
        $items = DB::table($this->_tablename)
                	->get();
        //print_r($items); exit;
        return view($this->_viewname.'.list')->with('items', $items);
    }

    public function action($action, Request $request) {
        $this->_request = $request;
        switch ($action) {
            case 'edit':
                return $this->showedit($request->input('id'));
                break;
            case 'addnew':
                return $this->shownewform();
                break;
            case 'delete':
                return $this->delete($request->input('id'));
                break;

            default:
                return $this->{$action}($request);
                break;
        }
    }

    public function postaction($action, Request $request) {
        $this->_request = $request;
        switch ($action) {
            case 'edit':
                return $this->postedit($request);
                break;
            case 'addnew':
                return $this->postnew($request);
                break;
            default:
                return $this->{'post'.$action}($request);
                break;
        }
    }


    protected function getItemData() {
        return DB::table($this->_tablename)
                ->where('id', $this->_id)
                ->first();
    }

    public function showedit($id)
    {
        $this->_id = $id;
        $item = $this->getItemData();
        $this->_prepareForm('edit');
        //echo '<pre>'; print_r($item); exit; 
        $view =  view($this->_viewname.'.form')
        					->with('edit', true)
                            ->with('item', $item);
        foreach ($this->editvalues as $key => $value) {
            $view->with($key, $value);
        }
        return $view;
    }

    public function shownewform()
    {
        $this->_prepareForm('new');
        $view = view($this->_viewname.'.form')
                ->with('edit', false);
        foreach ($this->newvalues as $key => $value) {
            $view->with($key, $value);
        }
        return $view;
    }

    public function delete($id)
    {
        DB::table($this->_tablename)
                ->where('id', $id)
                ->delete();
        $message = 'Deleted Successfully!!';
        return redirect()->route($this->_viewname.'-list')->with('message', $message);
    }

    protected function _prepareFormdata($request) {
        $post = $request->input();
    	unset($post['_token']);
    	$post['updated_at'] = date("Y-m-d H:i:s", strtotime('now'));
    	return $post;
    }


    public function postedit($request)
    {
        $id = $request->input('id');
        $this->_id = $id;
        $post = $request->input();
        $update = $this->_prepareFormdata($request);
        DB::table($this->_tablename)
                ->where('id', $id)
                ->update($update);
        $this->_postsave('edit', $id);
        $message = 'Saved successfully!';
        return redirect()->route($this->_viewname.'-list')->with('message', $message);
    }

    public function postnew($request)
    {
        $post = $request->input();
        //print_r($post); exit;
        $insert = $this->_prepareFormdata($request);
        $insert['created_at'] = date("Y-m-d H:i:s", strtotime('now'));
        $id = DB::table($this->_tablename)
                	->insertGetId($insert);
        $this->_postsave('new', $id);
        $message = 'Saved successfully!';
        return redirect()->route($this->_viewname.'-list')->with('message', $message);
    }

    protected function _prepareForm($type) {
        /// do nothing
    }

    protected function _postsave($type, $id) {
        /// do nothing
    }

    protected function _uploadMedia($request, $name = 'image', $folder = 'image', $disk = 'public') {
        $path = '';
        if( $request->file($name) ) {
            $folder = 'public/'.$folder;
            $path = $request->file($name)->store($folder, $disk);
            $path = str_replace($folder.'/', '', $path);
        }
        return $path;
    }
}
