<?php

namespace App\Http\Controllers;

use App\DataTables\LicenseDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateLicenseRequest;
use App\Http\Requests\UpdateLicenseRequest;
use App\Models\License;
use App\Models\School;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class LicenseController extends AppBaseController
{
    /**
     * Display a listing of the License.
     *
     * @param LicenseDataTable $licenseDataTable
     * @return Response
     */
    public function index(LicenseDataTable $licenseDataTable)
    {
        if(auth()->user()->drole == 'Gamer')
        {
            return $licenseDataTable->render('licenses.index');
        }
        else{
            $schoolid =  session('school_schid');
            
            if ($schoolid == null) {
                $org= \App\Models\Organization::find(auth()->user()->organization_orgid);

                if(!empty($org->org_type) && $org->org_type == 'organization'){
                    \Flash::error(__('orders.please select a school'),['model' => __('schools.singular')]);
                }
                else{
                    \Flash::error(__('orders.please select a department'),['model' => __('schools.singular')]);
                }
                // Flash::error(__('orders.please select a school').' '.__(''));
                
                return redirect('/schools');            }

                return $licenseDataTable->render('licenses.index');
            }
    }

    /**
     * Show the form for creating a new License.
     *
     * @return Response
     */
    public function create()
    {
        return view('licenses.create');
    }

    /**
     * Store a newly created License in storage.
     *
     * @param CreateLicenseRequest $request
     *
     * @return Response
     */
    public function store(CreateLicenseRequest $request)
    {
        $input = $request->all();

        /** @var License $license */
        $license = License::create($input);

        Flash::success(__('messages.saved', ['model' => __('licenses.singular')]));

        return redirect(route('licenses.index'));
    }



    public function freeLicense(Request $id)
    {
        $product = Product::find(16);
        $school = School::find($id->schoolid);
        $user = User::where('school_schid','=',$id->schoolid)->where('drole','=','School Admin')->first();
        //create order
        //create licenses
        $orders = Order::create(
            [
                'product_prodid'=>$product->prodid,
                'user_id'=> $user->id,
                'licences'=>$product->no_of_licenses,
                'licences_available'=>$product->no_of_licenses,
                'price'=>$product->price,
                'school_schid'=> $id->schoolid,
                // 'expiry_date'=>,
                'paypal_orderid'=> "free_order",
                'status'=>'Paid',
                'paid'=>0,
                ]
            );
           
            $user->order_ordid = $orders->ordid;
            $user->save();
            $dt = \Carbon\Carbon::now();
            for ($i=0; $i < $product->no_of_licenses; $i++){
                   # code...
                    $license =  License::create([
                        'order_ordid'=>$orders->ordid,
                        'product_prodid'=>$product->prodid,
                        'user_id'=> 0,
                        'lives'=>0,
                        'available_lives'=>NULL,
                        'price'=>$product->price,
                        'school_schid'=> $id->schoolid,
                        'renew_price'=> $product->renew_price,
                        'expiry_date'=> $dt->addYear($product->validity)->toDateString(),
                        'paypal_orderid'=> 'some_id',
                        'status'=>'Paid',
                        'paid'=>1,
                    ]);
                    echo "added";
                }
        $school->available_licenses = $product->no_of_licenses;
        $school->save();        
        Flash::success('Given License Successfully');
        echo $id->schoolid;
        return redirect(route('schools.index'));

    }

    /**
     * Display the specified License.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var License $license */
        $license = License::find($id);

        if (empty($license)) {
            Flash::error(__('licenses.singular').' '.__('messages.not_found'));

            return redirect(route('licenses.index'));
        }

        return view('licenses.show')->with('license', $license);
    }

    /**
     * Show the form for editing the specified License.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var License $license */
        $license = License::find($id);

        if (empty($license)) {
            Flash::error(__('messages.not_found', ['model' => __('licenses.singular')]));

            return redirect(route('licenses.index'));
        }

        return view('licenses.edit')->with('license', $license);
    }

    /**
     * Update the specified License in storage.
     *
     * @param  int              $id
     * @param UpdateLicenseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLicenseRequest $request)
    {
        /** @var License $license */
        $license = License::find($id);

        if (empty($license)) {
            Flash::error(__('messages.not_found', ['model' => __('licenses.singular')]));

            return redirect(route('licenses.index'));
        }

        $license->fill($request->all());
        $license->save();

        Flash::success(__('messages.updated', ['model' => __('licenses.singular')]));

        return redirect(route('licenses.index'));
    }

    /**
     * Remove the specified License from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var License $license */
        $license = License::find($id);

        if (empty($license)) {
            Flash::error(__('messages.not_found', ['model' => __('licenses.singular')]));

            return redirect(route('licenses.index'));
        }

        $license->delete();

        Flash::success(__('messages.deleted', ['model' => __('licenses.singular')]));

        return redirect(route('licenses.index'));
    }
}
