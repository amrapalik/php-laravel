<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStudentAPIRequest;
use App\Http\Requests\API\UpdateStudentAPIRequest;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class StudentController
 * @package App\Http\Controllers\API
 */

class StudentAPIController extends AppBaseController
{
    /**
     * Display a listing of the Student.
     * GET|HEAD /students
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Student::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $students = $query->get();

         return $this->sendResponse(
             $students->toArray(),
             __('messages.retrieved', ['model' => __('students.plural')])
         );
    }

    /**
     * Store a newly created Student in storage.
     * POST /students
     *
     * @param CreateStudentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStudentAPIRequest $request)
    {
        $input = $request->all();

        /** @var Student $student */
        $student = Student::create($input);

        return $this->sendResponse(
             $student->toArray(),
             __('messages.saved', ['model' => __('students.singular')])
        );
    }

    /**
     * Display the specified Student.
     * GET|HEAD /students/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Student $student */
        $student = Student::find($id);

        if (empty($student)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('students.singular')])
            );
        }

        return $this->sendResponse(
            $student->toArray(),
            __('messages.retrieved', ['model' => __('students.singular')])
        );
    }

    /**
     * Update the specified Student in storage.
     * PUT/PATCH /students/{id}
     *
     * @param int $id
     * @param UpdateStudentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStudentAPIRequest $request)
    {
        /** @var Student $student */
        $student = Student::find($id);

        if (empty($student)) {
           return $this->sendError(
               __('messages.not_found', ['model' => __('students.singular')])
           );
        }

        $student->fill($request->all());
        $student->save();

        return $this->sendResponse(
             $student->toArray(),
             __('messages.updated', ['model' => __('students.singular')])
        );
    }

    /**
     * Remove the specified Student from storage.
     * DELETE /students/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Student $student */
        $student = Student::find($id);

        if (empty($student)) {
           return $this->sendError(
                 __('messages.not_found', ['model' => __('students.singular')])
           );
        }

        $student->delete();

         return $this->sendResponse(
             $id,
             __('messages.deleted', ['model' => __('students.singular')])
         );
    }
}
