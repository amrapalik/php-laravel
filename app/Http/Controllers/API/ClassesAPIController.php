<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateClassesAPIRequest;
use App\Http\Requests\API\UpdateClassesAPIRequest;
use App\Models\Classes;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ClassesController
 * @package App\Http\Controllers\API
 */

class ClassesAPIController extends AppBaseController
{
    /**
     * Display a listing of the Classes.
     * GET|HEAD /classes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Classes::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $classes = $query->get();

         return $this->sendResponse(
             $classes->toArray(),
             __('messages.retrieved', ['model' => __('classes.plural')])
         );
    }

    /**
     * Store a newly created Classes in storage.
     * POST /classes
     *
     * @param CreateClassesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateClassesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Classes $classes */
        $classes = Classes::create($input);

        return $this->sendResponse(
             $classes->toArray(),
             __('messages.saved', ['model' => __('classes.singular')])
        );
    }

    /**
     * Display the specified Classes.
     * GET|HEAD /classes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Classes $classes */
        $classes = Classes::find($id);

        if (empty($classes)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('classes.singular')])
            );
        }

        return $this->sendResponse(
            $classes->toArray(),
            __('messages.retrieved', ['model' => __('classes.singular')])
        );
    }

    /**
     * Update the specified Classes in storage.
     * PUT/PATCH /classes/{id}
     *
     * @param int $id
     * @param UpdateClassesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClassesAPIRequest $request)
    {
        /** @var Classes $classes */
        $classes = Classes::find($id);

        if (empty($classes)) {
           return $this->sendError(
               __('messages.not_found', ['model' => __('classes.singular')])
           );
        }

        $classes->fill($request->all());
        $classes->save();

        return $this->sendResponse(
             $classes->toArray(),
             __('messages.updated', ['model' => __('classes.singular')])
        );
    }

    /**
     * Remove the specified Classes from storage.
     * DELETE /classes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Classes $classes */
        $classes = Classes::find($id);

        if (empty($classes)) {
           return $this->sendError(
                 __('messages.not_found', ['model' => __('classes.singular')])
           );
        }

        $classes->delete();

         return $this->sendResponse(
             $id,
             __('messages.deleted', ['model' => __('classes.singular')])
         );
    }
}
