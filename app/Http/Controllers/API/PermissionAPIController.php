<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePermissionAPIRequest;
use App\Http\Requests\API\UpdatePermissionAPIRequest;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PermissionController
 * @package App\Http\Controllers\API
 */

class PermissionAPIController extends AppBaseController
{
    /**
     * Display a listing of the Permission.
     * GET|HEAD /permissions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Permission::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $permissions = $query->get();

         return $this->sendResponse(
             $permissions->toArray(),
             __('messages.retrieved', ['model' => __('permissions.plural')])
         );
    }

    /**
     * Store a newly created Permission in storage.
     * POST /permissions
     *
     * @param CreatePermissionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePermissionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Permission $permission */
        $permission = Permission::create($input);

        return $this->sendResponse(
             $permission->toArray(),
             __('messages.saved', ['model' => __('permissions.singular')])
        );
    }

    /**
     * Display the specified Permission.
     * GET|HEAD /permissions/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Permission $permission */
        $permission = Permission::find($id);

        if (empty($permission)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('permissions.singular')])
            );
        }

        return $this->sendResponse(
            $permission->toArray(),
            __('messages.retrieved', ['model' => __('permissions.singular')])
        );
    }

    /**
     * Update the specified Permission in storage.
     * PUT/PATCH /permissions/{id}
     *
     * @param int $id
     * @param UpdatePermissionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePermissionAPIRequest $request)
    {
        /** @var Permission $permission */
        $permission = Permission::find($id);

        if (empty($permission)) {
           return $this->sendError(
               __('messages.not_found', ['model' => __('permissions.singular')])
           );
        }

        $permission->fill($request->all());
        $permission->save();

        return $this->sendResponse(
             $permission->toArray(),
             __('messages.updated', ['model' => __('permissions.singular')])
        );
    }

    /**
     * Remove the specified Permission from storage.
     * DELETE /permissions/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Permission $permission */
        $permission = Permission::find($id);

        if (empty($permission)) {
           return $this->sendError(
                 __('messages.not_found', ['model' => __('permissions.singular')])
           );
        }

        $permission->delete();

         return $this->sendResponse(
             $id,
             __('messages.deleted', ['model' => __('permissions.singular')])
         );
    }
}
