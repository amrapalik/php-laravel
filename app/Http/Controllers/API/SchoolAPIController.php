<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSchoolAPIRequest;
use App\Http\Requests\API\UpdateSchoolAPIRequest;
use App\Models\School;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SchoolController
 * @package App\Http\Controllers\API
 */

class SchoolAPIController extends AppBaseController
{
    /**
     * Display a listing of the School.
     * GET|HEAD /schools
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = School::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $schools = $query->get();

         return $this->sendResponse(
             $schools->toArray(),
             __('messages.retrieved', ['model' => __('schools.plural')])
         );
    }

    /**
     * Store a newly created School in storage.
     * POST /schools
     *
     * @param CreateSchoolAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSchoolAPIRequest $request)
    {
        $input = $request->all();

        /** @var School $school */
        $school = School::create($input);

        return $this->sendResponse(
             $school->toArray(),
             __('messages.saved', ['model' => __('schools.singular')])
        );
    }

    /**
     * Display the specified School.
     * GET|HEAD /schools/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var School $school */
        $school = School::find($id);

        if (empty($school)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('schools.singular')])
            );
        }

        return $this->sendResponse(
            $school->toArray(),
            __('messages.retrieved', ['model' => __('schools.singular')])
        );
    }

    /**
     * Update the specified School in storage.
     * PUT/PATCH /schools/{id}
     *
     * @param int $id
     * @param UpdateSchoolAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSchoolAPIRequest $request)
    {
        /** @var School $school */
        $school = School::find($id);

        if (empty($school)) {
           return $this->sendError(
               __('messages.not_found', ['model' => __('schools.singular')])
           );
        }

        $school->fill($request->all());
        $school->save();

        return $this->sendResponse(
             $school->toArray(),
             __('messages.updated', ['model' => __('schools.singular')])
        );
    }

    /**
     * Remove the specified School from storage.
     * DELETE /schools/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var School $school */
        $school = School::find($id);

        if (empty($school)) {
           return $this->sendError(
                 __('messages.not_found', ['model' => __('schools.singular')])
           );
        }

        $school->delete();

         return $this->sendResponse(
             $id,
             __('messages.deleted', ['model' => __('schools.singular')])
         );
    }
}
