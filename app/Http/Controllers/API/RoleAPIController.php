<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRoleAPIRequest;
use App\Http\Requests\API\UpdateRoleAPIRequest;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class RoleController
 * @package App\Http\Controllers\API
 */

class RoleAPIController extends AppBaseController
{
    /**
     * Display a listing of the Role.
     * GET|HEAD /roles
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Role::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $roles = $query->get();

         return $this->sendResponse(
             $roles->toArray(),
             __('messages.retrieved', ['model' => __('roles.plural')])
         );
    }

    /**
     * Store a newly created Role in storage.
     * POST /roles
     *
     * @param CreateRoleAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRoleAPIRequest $request)
    {
        $input = $request->all();

        /** @var Role $role */
        $role = Role::create($input);

        return $this->sendResponse(
             $role->toArray(),
             __('messages.saved', ['model' => __('roles.singular')])
        );
    }

    /**
     * Display the specified Role.
     * GET|HEAD /roles/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Role $role */
        $role = Role::find($id);

        if (empty($role)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('roles.singular')])
            );
        }

        return $this->sendResponse(
            $role->toArray(),
            __('messages.retrieved', ['model' => __('roles.singular')])
        );
    }

    /**
     * Update the specified Role in storage.
     * PUT/PATCH /roles/{id}
     *
     * @param int $id
     * @param UpdateRoleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoleAPIRequest $request)
    {
        /** @var Role $role */
        $role = Role::find($id);

        if (empty($role)) {
           return $this->sendError(
               __('messages.not_found', ['model' => __('roles.singular')])
           );
        }

        $role->fill($request->all());
        $role->save();

        return $this->sendResponse(
             $role->toArray(),
             __('messages.updated', ['model' => __('roles.singular')])
        );
    }

    /**
     * Remove the specified Role from storage.
     * DELETE /roles/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Role $role */
        $role = Role::find($id);

        if (empty($role)) {
           return $this->sendError(
                 __('messages.not_found', ['model' => __('roles.singular')])
           );
        }

        $role->delete();

         return $this->sendResponse(
             $id,
             __('messages.deleted', ['model' => __('roles.singular')])
         );
    }
}
