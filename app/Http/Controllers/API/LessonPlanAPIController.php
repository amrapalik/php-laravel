<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLessonPlanAPIRequest;
use App\Http\Requests\API\UpdateLessonPlanAPIRequest;
use App\Models\LessonPlan;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class LessonPlanController
 * @package App\Http\Controllers\API
 */

class LessonPlanAPIController extends AppBaseController
{
    /**
     * Display a listing of the LessonPlan.
     * GET|HEAD /lessonPlans
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = LessonPlan::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $lessonPlans = $query->get();

         return $this->sendResponse(
             $lessonPlans->toArray(),
             __('messages.retrieved', ['model' => __('lessonPlans.plural')])
         );
    }

    /**
     * Store a newly created LessonPlan in storage.
     * POST /lessonPlans
     *
     * @param CreateLessonPlanAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLessonPlanAPIRequest $request)
    {
        $input = $request->all();

        /** @var LessonPlan $lessonPlan */
        $lessonPlan = LessonPlan::create($input);

        return $this->sendResponse(
             $lessonPlan->toArray(),
             __('messages.saved', ['model' => __('lessonPlans.singular')])
        );
    }

    /**
     * Display the specified LessonPlan.
     * GET|HEAD /lessonPlans/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var LessonPlan $lessonPlan */
        $lessonPlan = LessonPlan::find($id);

        if (empty($lessonPlan)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('lessonPlans.singular')])
            );
        }

        return $this->sendResponse(
            $lessonPlan->toArray(),
            __('messages.retrieved', ['model' => __('lessonPlans.singular')])
        );
    }

    /**
     * Update the specified LessonPlan in storage.
     * PUT/PATCH /lessonPlans/{id}
     *
     * @param int $id
     * @param UpdateLessonPlanAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLessonPlanAPIRequest $request)
    {
        /** @var LessonPlan $lessonPlan */
        $lessonPlan = LessonPlan::find($id);

        if (empty($lessonPlan)) {
           return $this->sendError(
               __('messages.not_found', ['model' => __('lessonPlans.singular')])
           );
        }

        $lessonPlan->fill($request->all());
        $lessonPlan->save();

        return $this->sendResponse(
             $lessonPlan->toArray(),
             __('messages.updated', ['model' => __('lessonPlans.singular')])
        );
    }

    /**
     * Remove the specified LessonPlan from storage.
     * DELETE /lessonPlans/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var LessonPlan $lessonPlan */
        $lessonPlan = LessonPlan::find($id);

        if (empty($lessonPlan)) {
           return $this->sendError(
                 __('messages.not_found', ['model' => __('lessonPlans.singular')])
           );
        }

        $lessonPlan->delete();

         return $this->sendResponse(
             $id,
             __('messages.deleted', ['model' => __('lessonPlans.singular')])
         );
    }
}
