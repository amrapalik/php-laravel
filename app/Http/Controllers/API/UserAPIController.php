<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends AppBaseController
{
    /**
     * Display a listing of the User.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = User::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $users = $query->get();

         return $this->sendResponse(
             $users->toArray(),
             __('messages.retrieved', ['model' => __('users.plural')])
         );
    }

    /**
     * Store a newly created User in storage.
     * POST /users
     *
     * @param CreateUserAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateUserAPIRequest $request)
    {
        $input = $request->all();

        /** @var User $user */
        $user = User::create($input);

        return $this->sendResponse(
             $user->toArray(),
             __('messages.saved', ['model' => __('users.singular')])
        );
    }

    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('users.singular')])
            );
        }

        return $this->sendResponse(
            $user->toArray(),
            __('messages.retrieved', ['model' => __('users.singular')])
        );
    }

    /**
     * Update the specified User in storage.
     * PUT/PATCH /users/{id}
     *
     * @param int $id
     * @param UpdateUserAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserAPIRequest $request)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
           return $this->sendError(
               __('messages.not_found', ['model' => __('users.singular')])
           );
        }

        $user->fill($request->all());
        $user->save();

        return $this->sendResponse(
             $user->toArray(),
             __('messages.updated', ['model' => __('users.singular')])
        );
    }

    /**
     * Remove the specified User from storage.
     * DELETE /users/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
           return $this->sendError(
                 __('messages.not_found', ['model' => __('users.singular')])
           );
        }

        $user->delete();

         return $this->sendResponse(
             $id,
             __('messages.deleted', ['model' => __('users.singular')])
         );
    }
}
