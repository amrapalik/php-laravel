<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBugsAPIRequest;
use App\Http\Requests\API\UpdateBugsAPIRequest;
use App\Models\Bugs;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class BugsController
 * @package App\Http\Controllers\API
 */

class BugsAPIController extends AppBaseController
{
    /**
     * Display a listing of the Bugs.
     * GET|HEAD /bugs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Bugs::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $bugs = $query->get();

         return $this->sendResponse(
             $bugs->toArray(),
             __('messages.retrieved', ['model' => __('bugs.plural')])
         );
    }

    /**
     * Store a newly created Bugs in storage.
     * POST /bugs
     *
     * @param CreateBugsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBugsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Bugs $bugs */
        $bugs = Bugs::create($input);

        return $this->sendResponse(
             $bugs->toArray(),
             __('messages.saved', ['model' => __('bugs.singular')])
        );
    }

    /**
     * Display the specified Bugs.
     * GET|HEAD /bugs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Bugs $bugs */
        $bugs = Bugs::find($id);

        if (empty($bugs)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('bugs.singular')])
            );
        }

        return $this->sendResponse(
            $bugs->toArray(),
            __('messages.retrieved', ['model' => __('bugs.singular')])
        );
    }

    /**
     * Update the specified Bugs in storage.
     * PUT/PATCH /bugs/{id}
     *
     * @param int $id
     * @param UpdateBugsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBugsAPIRequest $request)
    {
        /** @var Bugs $bugs */
        $bugs = Bugs::find($id);

        if (empty($bugs)) {
           return $this->sendError(
               __('messages.not_found', ['model' => __('bugs.singular')])
           );
        }

        $bugs->fill($request->all());
        $bugs->save();

        return $this->sendResponse(
             $bugs->toArray(),
             __('messages.updated', ['model' => __('bugs.singular')])
        );
    }

    /**
     * Remove the specified Bugs from storage.
     * DELETE /bugs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Bugs $bugs */
        $bugs = Bugs::find($id);

        if (empty($bugs)) {
           return $this->sendError(
                 __('messages.not_found', ['model' => __('bugs.singular')])
           );
        }

        $bugs->delete();

         return $this->sendResponse(
             $id,
             __('messages.deleted', ['model' => __('bugs.singular')])
         );
    }
}
