<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrganizationAPIRequest;
use App\Http\Requests\API\UpdateOrganizationAPIRequest;
use App\Models\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class OrganizationController
 * @package App\Http\Controllers\API
 */

class OrganizationAPIController extends AppBaseController
{
    /**
     * Display a listing of the Organization.
     * GET|HEAD /organizations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Organization::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $organizations = $query->get();

         return $this->sendResponse(
             $organizations->toArray(),
             __('messages.retrieved', ['model' => __('organizations.plural')])
         );
    }

    /**
     * Store a newly created Organization in storage.
     * POST /organizations
     *
     * @param CreateOrganizationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOrganizationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Organization $organization */
        $organization = Organization::create($input);

        return $this->sendResponse(
             $organization->toArray(),
             __('messages.saved', ['model' => __('organizations.singular')])
        );
    }

    /**
     * Display the specified Organization.
     * GET|HEAD /organizations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Organization $organization */
        $organization = Organization::find($id);

        if (empty($organization)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('organizations.singular')])
            );
        }

        return $this->sendResponse(
            $organization->toArray(),
            __('messages.retrieved', ['model' => __('organizations.singular')])
        );
    }

    /**
     * Update the specified Organization in storage.
     * PUT/PATCH /organizations/{id}
     *
     * @param int $id
     * @param UpdateOrganizationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrganizationAPIRequest $request)
    {
        /** @var Organization $organization */
        $organization = Organization::find($id);

        if (empty($organization)) {
           return $this->sendError(
               __('messages.not_found', ['model' => __('organizations.singular')])
           );
        }

        $organization->fill($request->all());
        $organization->save();

        return $this->sendResponse(
             $organization->toArray(),
             __('messages.updated', ['model' => __('organizations.singular')])
        );
    }

    /**
     * Remove the specified Organization from storage.
     * DELETE /organizations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Organization $organization */
        $organization = Organization::find($id);

        if (empty($organization)) {
           return $this->sendError(
                 __('messages.not_found', ['model' => __('organizations.singular')])
           );
        }

        $organization->delete();

         return $this->sendResponse(
             $id,
             __('messages.deleted', ['model' => __('organizations.singular')])
         );
    }
}
