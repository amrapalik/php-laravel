<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLicenseAPIRequest;
use App\Http\Requests\API\UpdateLicenseAPIRequest;
use App\Models\License;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class LicenseController
 * @package App\Http\Controllers\API
 */

class LicenseAPIController extends AppBaseController
{
    /**
     * Display a listing of the License.
     * GET|HEAD /licenses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = License::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $licenses = $query->get();

         return $this->sendResponse(
             $licenses->toArray(),
             __('messages.retrieved', ['model' => __('licenses.plural')])
         );
    }

    /**
     * Store a newly created License in storage.
     * POST /licenses
     *
     * @param CreateLicenseAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLicenseAPIRequest $request)
    {
        $input = $request->all();

        /** @var License $license */
        $license = License::create($input);

        return $this->sendResponse(
             $license->toArray(),
             __('messages.saved', ['model' => __('licenses.singular')])
        );
    }

    /**
     * Display the specified License.
     * GET|HEAD /licenses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var License $license */
        $license = License::find($id);

        if (empty($license)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('licenses.singular')])
            );
        }

        return $this->sendResponse(
            $license->toArray(),
            __('messages.retrieved', ['model' => __('licenses.singular')])
        );
    }

    /**
     * Update the specified License in storage.
     * PUT/PATCH /licenses/{id}
     *
     * @param int $id
     * @param UpdateLicenseAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLicenseAPIRequest $request)
    {
        /** @var License $license */
        $license = License::find($id);

        if (empty($license)) {
           return $this->sendError(
               __('messages.not_found', ['model' => __('licenses.singular')])
           );
        }

        $license->fill($request->all());
        $license->save();

        return $this->sendResponse(
             $license->toArray(),
             __('messages.updated', ['model' => __('licenses.singular')])
        );
    }

    /**
     * Remove the specified License from storage.
     * DELETE /licenses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var License $license */
        $license = License::find($id);

        if (empty($license)) {
           return $this->sendError(
                 __('messages.not_found', ['model' => __('licenses.singular')])
           );
        }

        $license->delete();

         return $this->sendResponse(
             $id,
             __('messages.deleted', ['model' => __('licenses.singular')])
         );
    }
}
