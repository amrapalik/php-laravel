<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAssignmentAPIRequest;
use App\Http\Requests\API\UpdateAssignmentAPIRequest;
use App\Models\Assignment;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AssignmentController
 * @package App\Http\Controllers\API
 */

class AssignmentAPIController extends AppBaseController
{
    /**
     * Display a listing of the Assignment.
     * GET|HEAD /assignments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Assignment::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $assignments = $query->get();

         return $this->sendResponse(
             $assignments->toArray(),
             __('messages.retrieved', ['model' => __('assignments.plural')])
         );
    }

    /**
     * Store a newly created Assignment in storage.
     * POST /assignments
     *
     * @param CreateAssignmentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAssignmentAPIRequest $request)
    {
        $input = $request->all();

        /** @var Assignment $assignment */
        $assignment = Assignment::create($input);

        return $this->sendResponse(
             $assignment->toArray(),
             __('messages.saved', ['model' => __('assignments.singular')])
        );
    }

    /**
     * Display the specified Assignment.
     * GET|HEAD /assignments/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Assignment $assignment */
        $assignment = Assignment::find($id);

        if (empty($assignment)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('assignments.singular')])
            );
        }

        return $this->sendResponse(
            $assignment->toArray(),
            __('messages.retrieved', ['model' => __('assignments.singular')])
        );
    }

    /**
     * Update the specified Assignment in storage.
     * PUT/PATCH /assignments/{id}
     *
     * @param int $id
     * @param UpdateAssignmentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAssignmentAPIRequest $request)
    {
        /** @var Assignment $assignment */
        $assignment = Assignment::find($id);

        if (empty($assignment)) {
           return $this->sendError(
               __('messages.not_found', ['model' => __('assignments.singular')])
           );
        }

        $assignment->fill($request->all());
        $assignment->save();

        return $this->sendResponse(
             $assignment->toArray(),
             __('messages.updated', ['model' => __('assignments.singular')])
        );
    }

    /**
     * Remove the specified Assignment from storage.
     * DELETE /assignments/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Assignment $assignment */
        $assignment = Assignment::find($id);

        if (empty($assignment)) {
           return $this->sendError(
                 __('messages.not_found', ['model' => __('assignments.singular')])
           );
        }

        $assignment->delete();

         return $this->sendResponse(
             $id,
             __('messages.deleted', ['model' => __('assignments.singular')])
         );
    }
}
