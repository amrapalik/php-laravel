<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterGamerRequest;
use App\Jobs\SendEmail;
use App\Models\Role;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class RegisteredUserController extends Controller
{

    public function register_gamer_form()
    {
        return view('registrations.gamer_registration');
    }

    public function register_gamer(RegisterGamerRequest $request)
    {

        $user = $request->only('firstname','lastname','username','password','email','country_code','phone_no','gender','dob','address','country','state','city','zipcode');
        $user['password'] = Hash::make($user['password']);
        $user['drole'] = "Gamer";
        $user['firstname'] = ucwords( $user['firstname']);
        $user['lastname'] = ucwords( $user['lastname']);
        $user['purchaseflag'] = "No";

        $user['name'] = ucwords( $user['firstname']) .' '. ucwords( $user['lastname']);

        $user = User::create($user);

        // Assign Role
        $role = Role::where('name','=','Gamer')->first();
        $user->assignRole($role);

        event(new Registered($user));

        Auth::login($user);

        set_game_token_in_session($request,$user->id);



        $info = array(
            'fullname' => $user->name,
            'username' => $user->username,
         //   'organ_name'=>'XYZ Organization'
        );

        // return view('emails/Gamer/afterRegistrationGamer',$info  );

        $details = array(
            'fullname' => $user->name,
            'username' => $user->username,
            'view'=>'emails/Gamer/afterRegistrationGamer',
            'subject'=>'Registration Successful',
            'email'=> $user->email,
            'to_fullname'=> $user->name,
            'from'=> env('MAIL_FROM_ADDRESS','support@reallivesworld.com'),
            'from_name'=>env('MAIL_FROM_NAME',env('APP_NAME')),
            'organ_name'=>env('MAIL_FROM_NAME',env('APP_NAME'))
        );

//        $details = array(
//            'fullname' => $user->name,
//            'username' => $user->username,
//            'view'=>'emails/Gamer/afterRegistrationGamer',
//            'subject'=>'Registration Successfull',
//            'email'=>'dhsont@gmail.com',
//            'to_fullname'=>'Dhiraj Sontakke',
//            'from'=> env('MAIL_FROM_ADDRESS','support@reallivesworld.com'),
//            'from_name'=>env('MAIL_FROM_NAME',env('APP_NAME')),
//            'organ_name'=>'XYZ Organization',
//        );
        SendEmail::dispatch($details);

        return redirect('/purchase_license_list');
    }


    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register-2');
    }


    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:8',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::School);
    }
}
