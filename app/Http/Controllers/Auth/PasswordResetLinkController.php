<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class PasswordResetLinkController extends Controller
{
    /**
     * Display the password reset link request view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.forgot-password');
    }

    /**
     * Handle an incoming password reset link request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
        ]);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
//        $status = Password::sendResetLink(
//            $request->only('email')
//        );

        // Custom email generation for password reset
        $user = User::where('email', '=', $request['email'])->first();

        if ($user) {
            $token = app(\Illuminate\Auth\Passwords\PasswordBroker::class)->createToken($user);


            // Send Email
            $details = array(
                'fullname' => $user->name,
                'username' => $user->username,
                'view' => 'emails/password-reset',
                'subject' => 'Password Reset',
                'email' => $user->email,
                'to_fullname' => $user->name,
                'token' => $token,
                'from' => env('MAIL_FROM_ADDRESS', 'support@reallivesworld.com'),
                'from_name' => env('MAIL_FROM_NAME', env('APP_NAME')),

            );

            // return view('emails/password-reset',$details);
            SendEmail::dispatch($details);
            return  back()->with('success', __('passwords.sent'));

        } else {
            return back()->withInput($request->only('email'))
                ->withErrors(['email' => __('passwords.user')]);
        }
//
//return back()->with('status', __('passwords.user'));
//        dd($status);
//        dd(__($status));
//        return $status == Password::RESET_LINK_SENT
//            ? back()->with('status', __($status))
//            : back()->withInput($request->only('email'))
//                ->withErrors(['email' => __($status)]);
    }
}
