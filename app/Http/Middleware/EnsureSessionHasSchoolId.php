<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EnsureSessionHasSchoolId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        //  $request->session()->put('school_schid', 10);
        $value = $request->session()->get('school_schid');

        $role = auth()->user()->drole;

        // If user is School Admin  or Teacher then add school id to session
        if (($role == 'School Admin' || $role == 'Teacher') && $value == null)
        {
            $request->session()->put('school_schid', auth()->user()->school_schid);
            return $next($request);
        }
      //  $request->session()->put('school_schid', 10);


        // Handling it on controller level
        if ($role == 'Gamer' && $value == null)
        {
           // $request->session()->put('school_schid', auth()->user()->school_schid);
          //  return $next($request);
        }


        if($value == null)
        {
            $org= \App\Models\Organization::find(auth()->user()->organization_orgid);

            if($org->org_type == 'organization'){
                \Flash::error(__('schools.Please select a School first'),['model' => __('schools.singular')]);
            }
            else{
                \Flash::error(__('schools.Please select a Department first'),['model' => __('schools.singular')]);
            }

            return redirect('/schools');
        }
        return $next($request);
    }
}
