<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class OrgAdminAndAbove
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
		$user_role = auth()->user()->drole;
		$allowed_roles = ['Organization Admin', 'Admin'];
		if(!in_array($user_role, $allowed_roles)){
			return abort(403);
		}
        return $next($request);
    }
}
