<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class UpdateUserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

//        return [
//            'username' => "required|string|max:200|unique:users,username,{$this->user},id",
//
//        ];

//        'employee_name' => "required|webo_spaces|webo_double_spaces|string|max:100|unique:employees,employee_name,{$this->employee},employeeid",
//            'email' => "required|email|max:100|unique:employees,email,{$this->employee},employeeid",
//            'designation' => 'required|webo_spaces|webo_double_spaces|string|max:100',
//            'mobile' => "required|numeric|digits:10|unique:employees,mobile,{$this->employee},employeeid",
//

        $updateRules = [
        'organization_orgid' => 'nullable',
        'school_schid' => 'nullable',
          //  'email' => "required|email|max:100|unique:employees,email,{$this->employee},employeeid",
         //   'username' => "required|string|min:4|max:50|unique:users,username,{$this->user},id",
        'username' => 'required|string|min:4|max:50|unique:users,username,'.$this->user()->id,
        'firstname' => 'required|string|max:50',
        'lastname' => 'required|string|max:50',
        'name' => 'nullable|string|max:50',
        'type' => 'nullable|string',
        // 'password' => 'required|string|max:15',
		 'email' => 'nullable|regex:/(.+)@(.+)\.(.+)/i|max:100|unique:users,email,'.$this->user()->id,

            'password' => 'nullable|string|min:6|max:15|confirmed',
        'drole' => 'nullable|string',
        'grade' => 'nullable|string|max:24',
        'phone_no' => 'nullable|numeric|digits_between:9,11',
        'user_mobile' => 'nullable|numeric|digits_between:9,11',
        'address' => 'nullable|string|max:255',
        'country' => 'nullable|string|max:45',
        'country_code' => 'nullable|string|max:5',
        'purchaseflag' => 'nullable|string|max:45',
        'gender' => 'nullable|string',
        'avatar' => 'nullable|string|max:255',
        'alternate_address' => 'nullable|string|max:255',
        'city' => 'nullable|string|max:45',
        'state' => 'nullable|string|max:45',
        'zipcode' => 'nullable|string|max:20',
        'designation' => 'nullable|string|max:45',
        'alternate_email' => 'nullable|string|max:100',
        'facebook_profile' => 'nullable|string|max:250',
        'linkedIn_profile' => 'nullable|string|max:250',
        'email_verified_at' => 'nullable',
        'remember_token' => 'nullable|string|max:100',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'dob' => 'nullable|date|before:2015-00-00',
        'last_loggedin' => 'nullable',
        'deleted_at' => 'nullable',
        'account_expiry_date' => 'nullable',
        'account_status' => 'nullable|string|max:30',
        'deallocated' => 'nullable|boolean',
        'account_expired' => 'nullable|boolean'
    ];

		if($this->user()->drole != 'Student'){
            if($this->route('user')){
                $updateRules['email'] = 'required|regex:/(.+)@(.+)\.(.+)/i|max:100|unique:users,email,'.$this->route('user');
                $updateRules['username'] = 'required|string|min:4|max:50|unique:users,username,'.$this->route('user');
            }
            else{
                $updateRules['email'] = 'required|regex:/(.+)@(.+)\.(.+)/i|max:100|unique:users,email,'.$this->user()->id;
                $updateRules['username'] = 'required|string|min:4|max:50|unique:users,username,'.$this->user()->id;    
            } 
		}

        
        return $updateRules;
    }
}
