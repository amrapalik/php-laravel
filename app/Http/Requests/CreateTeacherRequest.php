<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Student;

class CreateTeacherRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = Student::$rules;
         unset($rules['grade']);

        $userRules = Collect($rules);
       // $userRules->diff(['grade']);


        return  $userRules->merge([ 'password' => 'required|string|min:6|max:15|confirmed',
           // 'username' => 'required|string|min:6|max:30|unique',
            'username' => "required|string|max:200|unique:users",

            'email' => 'required|regex:/(.+)@(.+)\.(.+)/i|unique:users'])->toArray();

        return Student::$rules;
    }

//    public function rules()
//    {
//
//        $userRules = Collect(Student::$rules);
//
//
//        return  $userRules->merge([ 'password' => 'required|string|min:6|max:15|confirmed',
//
//         'username' => "required|string|max:200|unique:users",
//          'email' => 'required|regex:/(.+)@(.+)\.(.+)/i',
//        ])->toArray();
//
//        return Student::$rules;
//    }
}
