<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\School;

class CreateSchoolRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = School::$rules;
		// rules for School-admin creation
		$rules['firstname'] = 'required';
		$rules['lastname'] = 'required';
		$rules['username'] = 'required|unique:users';
		$rules['email'] = 'required|email|unique:users';
		$rules['password'] = 'required|min:6|max:15';
		$rules['password_confirmation'] = 'required_with:password|same:password';
		return $rules;
    }
}
