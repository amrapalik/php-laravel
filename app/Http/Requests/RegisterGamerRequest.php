<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;
use Illuminate\Support\Collection;

class RegisterGamerRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //return User::$rules;

        $userRules = Collect(User::$rules);


       return  $userRules->merge([ 'password' => 'required|string|min:6|max:15|confirmed','email' => 'required|regex:/(.+)@(.+)\.(.+)/i',
       'g-recaptcha-response' => function ($attribute, $value, $fail) {
        $secretkey = env('GOOGLE_RECAPTCHA_SECRET');
        $response = $value;
        $userIp = $_SERVER['REMOTE_ADDR'];
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretkey&response=$response&remoteip=$userIp";
        $response = file_get_contents($url);
        $response = json_decode($response);
        if(!$response->success){
            $fail('Captcha is necessary for registration');
        }
    }
    ])->toArray();

    }
}
