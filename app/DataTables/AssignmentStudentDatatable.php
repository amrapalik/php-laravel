<?php

namespace App\DataTables;

use App\Models\StudentAssignment;
use App\Models\User;
use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Client\Request;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class AssignmentStudentDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query, \Illuminate\Http\Request $request)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable; //->addColumn('action', 'students.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(StudentAssignment $model, \Illuminate\Http\Request $request)
    {
        $classid =  $request->get('class');
        $schoolid = get_school_id();

        return $model->newQuery()->where('class_classid','=',$classid)->with('user'); //->orderBy('users.grade');
    }

    public function buttons()
    {
        $but =  [

            [
                'extend' => 'excel',
                'className' => 'btn btn-default btn-sm no-corner',
                'text' => '<i class="fa fa-download"></i> ' .__('pagination.datatable.export').''
            ],
          //            [
//                'extend' => 'print',
//                'className' => 'btn btn-default btn-sm no-corner',
//                'text' => '<i class="fa fa-print"></i> ' .__('pagination.datatable.print').''
//            ],
            [
                'extend' => 'reset',
                'className' => 'btn btn-default btn-sm no-corner',
                'text' => '<i class="fa fa-undo"></i> ' .__('pagination.datatable.reset').''
            ],
            [
                'extend' => 'reload',
                'className' => 'btn btn-default btn-sm no-corner',
                'text' => '<i class="fa fa-sync-alt"></i> ' .__('pagination.datatable.reload').''
            ],
        ];


//            if(Gate::check('permission1'))
//            {
//
//                array_push($but, [
//                'extend' => 'create',
//                'className' => 'btn btn-default btn-sm no-corner',
//                'text' => '<i class="fa fa-plus"></i> ' .__('pagination.datatable.create').''
//            ]);
//
//            }


        return  $but;
    }

    /**
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
           // ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
               // 'dom' => "<'row dhiraj-2'<'col-sm-3'l><'col-sm-6' ><'col-sm-3'f>>rtip",

               'dom' => "<'row dhiraj-2'<'col-sm-3'l><'col-sm-6'B><'col-sm-3'f>>rtip",
                'lengthMenu'=> [[50,100,500, -1], [50,100,500,"All"]],
                'order'=>[ [0, 'desc'] ],
                'scrollX' => true,
                   'buttons'   => $this->buttons(),
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('users.fields.id'), 'data' => 'student_assignment_id']),
//            'organization_orgid' => new Column(['title' => __('users.fields.organization_orgid'), 'data' => 'organization_orgid'])
//,
//            'school_schid' => new Column(['title' => __('users.fields.school_schid'), 'data' => 'school_schid'])
//,
//            'username' => new Column(['title' => __('users.fields.username'), 'data' => 'username'])
//            ,
            'name' => new Column(['title' => __('users.fields.firstname'), 'data' => 'user.name'])
            ,
//            'lastname' => new Column(['title' => __('users.fields.lastname'), 'data' => 'lastname'])
//            ,
//            'name' => new Column(['title' => __('users.fields.name'), 'data' => 'name'])
//,
            'type' => new Column(['title' => __('users.fields.type'), 'data' => 'utype']),
            'game_status' => new Column(['title' => __('assignments.fields.game_status'), 'data' => 'game_status'])
,
            'letter_status' => new Column(['title' => __('assignments.fields.letter_status'), 'data' => 'letter_status'])
,
            'obituary_status' => new Column(['title' => __('assignments.fields.obituary_status'), 'data' => 'obituary_status'])
,
//            'grade' => new Column(['title' => __('users.fields.grade'), 'data' => 'user.grade'])
//            ,
//            'phone_no' => new Column(['title' => __('users.fields.phone_no'), 'data' => 'phone_no'])
//,
//            'address' => new Column(['title' => __('users.fields.address'), 'data' => 'address'])
//,
//            'country' => new Column(['title' => __('users.fields.country'), 'data' => 'country'])
//,
//            'country_code' => new Column(['title' => __('users.fields.country_code'), 'data' => 'country_code'])
//,
//            'purchaseflag' => new Column(['title' => __('users.fields.purchaseflag'), 'data' => 'purchaseflag'])
//,
//            'gender' => new Column(['title' => __('users.fields.gender'), 'data' => 'gender'])
//,
//            'avatar' => new Column(['title' => __('users.fields.avatar'), 'data' => 'avatar'])
//,
//            'alternate_address' => new Column(['title' => __('users.fields.alternate_address'), 'data' => 'alternate_address'])
//,
//            'city' => new Column(['title' => __('users.fields.city'), 'data' => 'city'])
//,
//            'state' => new Column(['title' => __('users.fields.state'), 'data' => 'state'])
//,
//            'zipcode' => new Column(['title' => __('users.fields.zipcode'), 'data' => 'zipcode'])
//,
//            'designation' => new Column(['title' => __('users.fields.designation'), 'data' => 'designation'])
//,
//            'alternate_email' => new Column(['title' => __('users.fields.alternate_email'), 'data' => 'alternate_email'])
//,
//            'facebook_profile' => new Column(['title' => __('users.fields.facebook_profile'), 'data' => 'facebook_profile'])
//,
//            'linkedIn_profile' => new Column(['title' => __('users.fields.linkedIn_profile'), 'data' => 'linkedIn_profile'])
//,
//            'email_verified_at' => new Column(['title' => __('users.fields.email_verified_at'), 'data' => 'email_verified_at'])
//,
//            'remember_token' => new Column(['title' => __('users.fields.remember_token'), 'data' => 'remember_token'])
//,
//            'dob' => new Column(['title' => __('users.fields.dob'), 'data' => 'dob'])
//,
//            'last_loggedin' => new Column(['title' => __('users.fields.last_loggedin'), 'data' => 'last_loggedin'])
//,
//            'account_expiry_date' => new Column(['title' => __('users.fields.account_expiry_date'), 'data' => 'account_expiry_date'])
//,
//            'account_status' => new Column(['title' => __('users.fields.account_status'), 'data' => 'account_status'])
//            ,
//            'deallocated' => new Column(['title' => __('users.fields.deallocated'), 'data' => 'deallocated'])
//,
//            'account_expired' => new Column(['title' => __('users.fields.account_expired'), 'data' => 'account_expired'])

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'assignments_students_datatable_' . time();
    }
}
