<?php

namespace App\DataTables;

use App\Models\Assignment;
use App\Models\Category;
use Illuminate\Http\Request;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class AssignmentDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

//        if (auth()->user()->drole == 'Admin')
//        {
//            return $dataTable->addColumn('action', 'assignments.datatables_actions_admin');
//
//        }

        return $dataTable->addColumn('action', 'assignments.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Assignment $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Assignment $model,Request $request)
    {
        $cat = $request->get('cat');
        $modal = $model->newQuery();
        if ($cat)
        {

            $category = Category::find($cat);
            return  $modal->where('category','=',$category->catname);
        }

        return $modal;
    }

    /**
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => "<'row dhiraj-2'<'col-sm-3'l><'col-sm-6'B><'col-sm-3'f>>rtip",
                'lengthMenu'=> [[50,100,500, -1], [50,100,500,"All"]],
                'order'=>[ [0, 'desc'] ],
                'scrollX' => true,
                'buttons'   => [
//                    [
//                       'extend' => 'create',
//                       'className' => 'btn btn-default btn-sm no-corner',
//                       'text' => '<i class="fa fa-plus"></i> ' .__('pagination.datatable.create').''
//                    ],
                    [
                       'extend' => 'excel',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('pagination.datatable.export').''
                    ],
//            [
//                'extend' => 'print',
//                'className' => 'btn btn-default btn-sm no-corner',
//                'text' => '<i class="fa fa-print"></i> ' .__('pagination.datatable.print').''
//            ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('pagination.datatable.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-sync-alt"></i> ' .__('pagination.datatable.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {

        if (auth()->user()->drole == 'Admin')
        {  return [
            'asname' => new Column(['title' => __('assignments.fields.asname'), 'data' => 'asname']),
            'category' => new Column(['title' => __('assignments.fields.category'), 'data' => 'category']),
            'type' => new Column(['title' => __('assignments.fields.type'), 'data' => 'type']),
            'tag' => new Column(['title' => __('assignments.fields.tag'), 'data' => 'tag'])
        ];

        }else
        {
            return [
                'asname' => new Column(['title' => __('assignments.fields.asname'), 'data' => 'asname']),
                'category' => new Column(['title' => __('assignments.fields.category'), 'data' => 'category']),
                'type' => new Column(['title' => __('assignments.fields.type'), 'data' => 'type']),
                'tag' => new Column(['title' => __('assignments.fields.tag'), 'data' => 'tag'])

            ];
        }

    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'assignments_datatable_' . time();
    }
}
