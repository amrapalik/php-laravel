<?php

namespace App\DataTables;

use App\Models\Organization;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class OrganizationDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'organizations.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Organization $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Organization $model)
    {
        return $model->newQuery();
    }

    /**
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => "<'row dhiraj-2'<'col-sm-3'l><'col-sm-6'B><'col-sm-3'f>>rtip",
                'lengthMenu'=> [[50,100,500, -1], [50,100,500,"All"]],
                'order'=>[ [0, 'desc'] ],
                'scrollX' => true,
                'buttons'   => [
//                    [
//                       'extend' => 'create',
//                       'className' => 'btn btn-default btn-sm no-corner',
//                       'text' => '<i class="fa fa-plus"></i> ' .__('pagination.datatable.create').''
//                    ],
                    [
                       'extend' => 'excel',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('pagination.datatable.export').''
                    ],
//            [
//                'extend' => 'print',
//                'className' => 'btn btn-default btn-sm no-corner',
//                'text' => '<i class="fa fa-print"></i> ' .__('pagination.datatable.print').''
//            ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('pagination.datatable.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-sync-alt"></i> ' .__('pagination.datatable.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            //'created_by' => new Column(['title' => __('organizations.fields.created_by'), 'data' => 'created_by']),
            'org_name' => new Column(['title' => __('organizations.fields.org_name'), 'data' => 'org_name']),
            'org_email' => new Column(['title' => __('organizations.fields.org_email'), 'data' => 'org_email']),
//            'about' => new Column(['title' => __('organizations.fields.about'), 'data' => 'about']),
//            'no_of_teachers' => new Column(['title' => __('organizations.fields.no_of_teachers'), 'data' => 'no_of_teachers']),
//            'no_of_students' => new Column(['title' => __('organizations.fields.no_of_students'), 'data' => 'no_of_students']),
//            'website' => new Column(['title' => __('organizations.fields.website'), 'data' => 'website']),
//            'facebook' => new Column(['title' => __('organizations.fields.facebook'), 'data' => 'facebook']),
//            'linkedin' => new Column(['title' => __('organizations.fields.linkedin'), 'data' => 'linkedin']),
//            'latitude' => new Column(['title' => __('organizations.fields.latitude'), 'data' => 'latitude']),
//            'longitude' => new Column(['title' => __('organizations.fields.longitude'), 'data' => 'longitude']),
//            'logo' => new Column(['title' => __('organizations.fields.logo'), 'data' => 'logo']),
//            'address1' => new Column(['title' => __('organizations.fields.address1'), 'data' => 'address1']),
//            'address2' => new Column(['title' => __('organizations.fields.address2'), 'data' => 'address2']),
//            'city' => new Column(['title' => __('organizations.fields.city'), 'data' => 'city']),
//            'state' => new Column(['title' => __('organizations.fields.state'), 'data' => 'state']),
//            'country' => new Column(['title' => __('organizations.fields.country'), 'data' => 'country']),
//            'zipcode' => new Column(['title' => __('organizations.fields.zipcode'), 'data' => 'zipcode']),
            'phone' => new Column(['title' => __('organizations.fields.phone'), 'data' => 'phone']),
//            'mobile' => new Column(['title' => __('organizations.fields.mobile'), 'data' => 'mobile'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'organizations_datatable_' . time();
    }
}
