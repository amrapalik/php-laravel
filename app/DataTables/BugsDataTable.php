<?php

namespace App\DataTables;

use App\Models\Bugs;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class BugsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('created_at', function ($order) {
            return $order->created_at->format('d/m/Y');
        });

        return $dataTable->addColumn('action', 'bugs.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Bugs $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Bugs $model)
    {
        return $model->newQuery();
    }

    /**
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => "<'row dhiraj-2'<'col-sm-3'l><'col-sm-6'B><'col-sm-3'f>>rtip",
                'lengthMenu'=> [[50,100,500, -1], [50,100,500,"All"]],
                'order'=>[ [0, 'desc'] ],
                'scrollX' => true,
                'buttons'   => [
//                    [
//                       'extend' => 'create',
//                       'className' => 'btn btn-default btn-sm no-corner',
//                       'text' => '<i class="fa fa-plus"></i> ' .__('pagination.datatable.create').''
//                    ],
                    [
                       'extend' => 'excel',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('pagination.datatable.export').''
                    ],
//            [
//                'extend' => 'print',
//                'className' => 'btn btn-default btn-sm no-corner',
//                'text' => '<i class="fa fa-print"></i> ' .__('pagination.datatable.print').''
//            ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('pagination.datatable.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-sync-alt"></i> ' .__('pagination.datatable.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'title' => new Column(['title' => __('bugs.fields.title'), 'data' => 'title'])
,
            'description' => new Column(['title' => __('bugs.fields.description'), 'data' => 'description']),
            'created_at' => new Column(['title' => __('bugs.fields.created_at'), 'data' => 'created_at']),
//            'image' => new Column(['title' => __('bugs.fields.image'), 'data' => 'image'])
//,
//            'link' => new Column(['title' => __('bugs.fields.link'), 'data' => 'link'])
//,
//            'status' => new Column(['title' => __('bugs.fields.status'), 'data' => 'status'])
//,
//            'created_by`' => new Column(['title' => __('bugs.fields.created_by`'), 'data' => 'created_by`'])

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'bugs_datatable_' . time();
    }
}
