<?php

namespace App\DataTables;

use App\Models\License;
use App\Models\Order;
use Illuminate\Http\Request;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class LicenseDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('created_at', function ($order) {
            return $order->created_at->format('d/m/Y');
        });

        $dataTable->editColumn('school_schid', function ($order) {

            if ($order->school)
            {
                return $order->school->sch_name;
            }

            return 'Gamer';
        });

        $dataTable->editColumn('expiry_date', function ($order) {
            if($order->expiry_date != null){
                $s = $order->expiry_date;
                $date = strtotime($s);
                $date = date('d/m/Y', $date);
                return $date;
            }
            else{
                return NULL;
            }
        });
        return $dataTable ->addColumn('action', 'licenses.datatables_actions'); // I've to add this colomn in order datable and show the order taBle content on "Active License Page"........
                                                                                // No need to show the license table anywhere now as there are going to be a lot of licenses.
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model,Request $request)
    {
        if (auth()->user()->drole == 'Admin')
        {
            return $model->newQuery() ->with('user','product');
        }

        if(auth()->user()->drole == 'Gamer')
        {
            return $model->newQuery()->where('user_id','=',auth()->user()->id)->with('user','product');
        }
        else{
            $schoolid =   get_school_id();
            return $model->newQuery() ->where('school_schid','=',$schoolid)->with('user','product');
        }
    }

    /**
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => "<'row dhiraj-2'<'col-sm-3'l><'col-sm-6'B><'col-sm-3'f>>rtip",
                'lengthMenu'=> [[50,100,500, -1], [50,100,500,"All"]],
                'order'=>[ [0, 'desc'] ],
                'scrollX' => true,
                'buttons'   => [
//                    [
//                       'extend' => 'create',
//                       'className' => 'btn btn-default btn-sm no-corner',
//                       'text' => '<i class="fa fa-plus"></i> ' .__('pagination.datatable.create').''
//                    ],
                    [
                       'extend' => 'excel',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('pagination.datatable.export').''
                    ],
//            [
//                'extend' => 'print',
//                'className' => 'btn btn-default btn-sm no-corner',
//                'text' => '<i class="fa fa-print"></i> ' .__('pagination.datatable.print').''
//            ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('pagination.datatable.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-sync-alt"></i> ' .__('pagination.datatable.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {

//         if (auth()->user()->drole == 'Gamer')
//         {
//             return [
//                 'date' => new Column(['title' => __('licenses.fields.date'), 'data' => 'created_at']),
//                 'user' => new Column(['title' => __('licenses.fields.user'), 'data' => 'user.id']),
//                 'product' => new Column(['title' => __('licenses.fields.product'), 'data' => 'product.prod_name']),
//                 'licences' => new Column(['title' => __('licenses.fields.licences'), 'data' => 'licences']),
//     //            'licences_available' => new Column(['title' => __('licenses.fields.licences_available'), 'data' => 'licences_available']),
//                 'price' => new Column(['title' => __('licenses.fields.price'), 'data' => 'price']),
//                 'renew price' => new Column(['title' => __('licenses.fields.renew_price'), 'data' => 'renew_price']),
//     //            'paypal_orderid' => new Column(['title' => __('licenses.fields.paypal_orderid'), 'data' => 'paypal_orderid']),
//                 'status' => new Column(['title' => __('licenses.fields.status'), 'data' => 'status']),
//                 'expiry_date' => new Column(['title' => __('licenses.fields.expiry_date'), 'data' => 'expiry_date']),
//             ];
//         }else{
//             return [
//                 'date' => new Column(['title' => __('licenses.fields.date'), 'data' => 'created_at']),
//                 'user' => new Column(['title' => __('licenses.fields.user'), 'data' => 'user_id']),
//                 'school_schid' => new Column(['title' => __('licenses.fields.school'), 'data' => 'school_schid']),
//                 'product' => new Column(['title' => __('licenses.fields.product'), 'data' => 'product.prod_name']),
//                 'licences' => new Column(['title' => __('licenses.fields.licences'), 'data' => 'lives']),
// //            'licences_available' => new Column(['title' => __('licenses.fields.licences_available'), 'data' => 'licences_available']),
//                 'price' => new Column(['title' => __('licenses.fields.price'), 'data' => 'price']),
//                 'renew price' => new Column(['title' => __('licenses.fields.renew_price'), 'data' => 'renew_price']),
// //            'paypal_orderid' => new Column(['title' => __('licenses.fields.paypal_orderid'), 'data' => 'paypal_orderid']),
//                 'status' => new Column(['title' => __('licenses.fields.status'), 'data' => 'status']),
//                 'expiry_date' => new Column(['title' => __('licenses.fields.expiry_date'), 'data' => 'expiry_date']),
//             ];
//         }


        return [

            'date' => new Column(['title' => __('orders.fields.date'), 'data' => 'created_at'])
        ,
            'user_id' => new Column(['title' => __('orders.fields.user_id'), 'data' => 'user.id'])

        ,
            'product_prodid' => new Column(['title' => __('orders.fields.product_prodid'), 'data' => 'product.prod_name'])
        ,
            'licences' => new Column(['title' => __('orders.fields.licences'), 'data' => 'licences'])
        ,
            'price' => new Column(['title' => __('orders.fields.price'), 'data' => 'price'])
        ,
            'paypal_orderid' => new Column(['title' => __('orders.fields.paypal_orderid'), 'data' => 'paypal_orderid'])

        ,    'expiry_date' => new Column(['title' => __('licenses.fields.expiry_date'), 'data' => 'expiry_date'])

        ,
            'status' => new Column(['title' => __('orders.fields.status'), 'data' => 'status'])

        //,
        //            'paid' => new Column(['title' => __('orders.fields.paid'), 'data' => 'paid'])
        //,
        //            'captured' => new Column(['title' => __('orders.fields.captured'), 'data' => 'captured'])

    ];


    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'licenses_datatable_' . time();
    }
}

