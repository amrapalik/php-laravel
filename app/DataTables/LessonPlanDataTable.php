<?php

namespace App\DataTables;

use App\Models\LessonPlan;
use Illuminate\Http\Request;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class LessonPlanDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'lesson_plans.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\LessonPlan $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(LessonPlan $model, Request $request)
    {
		if($request->assignment){
			if($request->self){
        	return $model->newQuery()->where('class_classid','=',$request->get('class'))
			->with('creator')
			->where('created_by', auth()->user()->id)
			->where('school_schid', auth()->user()->school_schid)
			->where('assignment_id','>', 0)
			->orderBy('lpid','desc');
			}
			else{
        	return $model->newQuery()->where('class_classid','=',$request->get('class'))
			->with('creator')
			->where('created_by', '!=', auth()->user()->id)
			->where('school_schid', auth()->user()->school_schid)
			->where('assignment_id','>', 0)
			->orderBy('lpid','desc');
			}
		}
		else{
        return $model->newQuery()->where('class_classid','=',$request->get('class'))
			->with('creator')
			->where('school_schid', auth()->user()->school_schid)
			->where('assignment_id',null)
			->orderBy('lpid','desc');
		}


//        if ($request->has('class'))
//        {
//            return $model->newQuery()->where('class_classid','=',$request->get('class'))->orderBy('lpid','desc');
//        }else{
//            redirect('/');
//        }
    }

    /**
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => "<'row dhiraj-2'<'col-sm-3'l><'col-sm-6'B><'col-sm-3'f>>rtip",
                'lengthMenu'=> [[50,100,500, -1], [50,100,500,"All"]],
                'order'=>[ [0, 'desc'] ],
                'scrollX' => true,
                'buttons'   => [
//                    [
//                       'extend' => 'create',
//                       'className' => 'btn btn-default btn-sm no-corner',
//                       'text' => '<i class="fa fa-plus"></i> ' .__('pagination.datatable.create').''
//                    ],
                    [
                       'extend' => 'excel',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('pagination.datatable.export').''
                    ],
//            [
//                'extend' => 'print',
//                'className' => 'btn btn-default btn-sm no-corner',
//                'text' => '<i class="fa fa-print"></i> ' .__('pagination.datatable.print').''
//            ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('pagination.datatable.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-sync-alt"></i> ' .__('pagination.datatable.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
//            'class_classid' => new Column(['title' => __('lessonPlans.fields.class_classid'), 'data' => 'class_classid'])
//,
//            'school_schid' => new Column(['title' => __('lessonPlans.fields.school_schid'), 'data' => 'school_schid'])
//,
            'name' => new Column(['title' => __('lessonPlans.fields.name'), 'data' => 'name'])
,
//            'grades' => new Column(['title' => __('lessonPlans.fields.grades'), 'data' => 'grades'])
//,
//            'standards' => new Column(['title' => __('lessonPlans.fields.standards'), 'data' => 'standards'])
//,
//            'overview' => new Column(['title' => __('lessonPlans.fields.overview'), 'data' => 'overview'])
//,
 //           'objectives' => new Column(['title' => __('lessonPlans.fields.objectives'), 'data' => 'objectives'])
//,
//            'procedures' => new Column(['title' => __('lessonPlans.fields.procedures'), 'data' => 'procedures'])
//,
//            'assessment' => new Column(['title' => __('lessonPlans.fields.assessment'), 'data' => 'assessment'])
//,
//            'extension_activities' => new Column(['title' => __('lessonPlans.fields.extension_activities'), 'data' => 'extension_activities'])
//,
//            'image' => new Column(['title' => __('lessonPlans.fields.image'), 'data' => 'image'])
//,
//            'pdf' => new Column(['title' => __('lessonPlans.fields.pdf'), 'data' => 'pdf'])
//,
//            'video' => new Column(['title' => __('lessonPlans.fields.video'), 'data' => 'video'])
//,
//            'youtube_link' => new Column(['title' => __('lessonPlans.fields.youtube_link'), 'data' => 'youtube_link'])
//,
            'created_by' => new Column(['title' => __('lessonPlans.fields.created_by'), 'data' => 'creator.name'])
,
 //           'copied_from' => new Column(['title' => __('lessonPlans.fields.copied_from'), 'data' => 'copied_from'])
//,
//            'status' => new Column(['title' => __('lessonPlans.fields.status'), 'data' => 'status'])

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'lesson_plans_datatable_' . time();
    }
}
