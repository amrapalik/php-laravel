<?php

namespace App\DataTables;

use App\Models\Order;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class OrderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('created_at', function ($order) {
            return $order->created_at->format('d/m/Y');
        });

        return $dataTable; //->addColumn('action', 'orders.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {

        if (auth()->user()->drole == 'Admin')
        {
            return $model->newQuery()  ->with('user','product');
        }

       if(auth()->user()->drole == 'Gamer')
       {
           return $model->newQuery()->where('user_id','=',auth()->user()->id)->with('user','product');
       }
       else{
           $schoolid =   get_school_id();
           return $model->newQuery() ->where('school_schid','=',$schoolid)->with('user','product');
       }


    }

    /**
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
           // ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => "<'row dhiraj-2'<'col-sm-3'l><'col-sm-6' ><'col-sm-3'f>>rtip",
                'lengthMenu'=> [[50,100,500, -1], [50,100,500,"All"]],
                'order'=>[ [0, 'desc'] ],
                'scrollX' => true,
                'buttons'   => [
//                    [
//                       'extend' => 'create',
//                       'className' => 'btn btn-default btn-sm no-corner',
//                       'text' => '<i class="fa fa-plus"></i> ' .__('pagination.datatable.create').''
//                    ],
                    [
                       'extend' => 'excel',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('pagination.datatable.export').''
                    ],
//            [
//                'extend' => 'print',
//                'className' => 'btn btn-default btn-sm no-corner',
//                'text' => '<i class="fa fa-print"></i> ' .__('pagination.datatable.print').''
//            ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('pagination.datatable.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-sync-alt"></i> ' .__('pagination.datatable.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            'date' => new Column(['title' => __('orders.fields.date'), 'data' => 'created_at'])
,
            'user_id' => new Column(['title' => __('orders.fields.user_id'), 'data' => 'user.name'])

,
            'product_prodid' => new Column(['title' => __('orders.fields.product_prodid'), 'data' => 'product.prod_name'])
,
            'licences' => new Column(['title' => __('orders.fields.licences'), 'data' => 'licences'])
,
            'price' => new Column(['title' => __('orders.fields.price'), 'data' => 'price'])
,
            'paypal_orderid' => new Column(['title' => __('orders.fields.paypal_orderid'), 'data' => 'paypal_orderid'])
,
            'status' => new Column(['title' => __('orders.fields.status'), 'data' => 'status'])

//,
//            'paid' => new Column(['title' => __('orders.fields.paid'), 'data' => 'paid'])
//,
//            'captured' => new Column(['title' => __('orders.fields.captured'), 'data' => 'captured'])

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'orders_datatable_' . time();
    }
}
