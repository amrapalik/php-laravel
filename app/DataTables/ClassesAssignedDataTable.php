<?php

namespace App\DataTables;

use App\Models\Classes;
use App\Models\ClassUser;
use Illuminate\Http\Request;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ClassesAssignedDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable ->editColumn('start_date', function ($healthCard) {
            if ($healthCard->start_date){
                return $healthCard->start_date->format('d-m-Y');
            }
        });
        $dataTable ->editColumn('end_date', function ($healthCard) {
            if($healthCard->end_date){
                return $healthCard->end_date->format('d-m-Y');
            }
        });

        return $dataTable->addColumn('action', 'classes.actions_joined_classes');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Classes $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Classes $model, Request $request)
    {

        $modal = $model->newQuery();

        $classids =  ClassUser::where('user_id','=',auth()->user()->id)->pluck('class_classid');
        // dd($classids);
//        $classes = \App\Models\Classes::whereIn('classid',$classids)->get();



        return $modal->whereIn('classid',$classids)->with('created_by');;



    }

    /**
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => "<'row dhiraj-2'<'col-sm-3'l><'col-sm-6'B><'col-sm-3'f>>rtip",

                'lengthMenu'=> [[50,100,500, -1], [50,100,500,"All"]],
                'order'=>[ [0, 'desc'] ],
                'scrollX' => true,
                'buttons'   => [
//                    [
//                       'extend' => 'create',
//                       'className' => 'btn btn-default btn-sm no-corner',
//                       'text' => '<i class="fa fa-plus"></i> ' .__('pagination.datatable.create').''
//                    ],
                    [
                       'extend' => 'excel',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('pagination.datatable.export').''
                    ],
//            [
//                'extend' => 'print',
//                'className' => 'btn btn-default btn-sm no-corner',
//                'text' => '<i class="fa fa-print"></i> ' .__('pagination.datatable.print').''
//            ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('pagination.datatable.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-sync-alt"></i> ' .__('pagination.datatable.reload').''
                    ],
                ],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'classid' => new Column(['title' => __('classes.fields.classid'), 'data' => 'classid']),
            'class_name' => new Column(['title' => __('classes.fields.class_name'), 'data' => 'class_name']),
            'Created By' => new Column(['title' => __('classes.fields.created_by'), 'data' => 'created_by.name', 'searchable' => false, 'orderable' => false])

//,
//            'class_description' => new Column(['title' => __('classes.fields.class_description'), 'data' => 'class_description'])
//            ,
//            'grade' => new Column(['title' => __('classes.fields.grade'), 'data' => 'grade'])
//            ,
//            'division' => new Column(['title' => __('classes.fields.division'), 'data' => 'division'])
//            ,
//            'age_group' => new Column(['title' => __('classes.fields.age_group'), 'data' => 'age_group'])
//            ,
//            'start_date' => new Column(['title' => __('classes.fields.start_date'), 'data' => 'start_date'])
//,
//            'end_date' => new Column(['title' => __('classes.fields.end_date'), 'data' => 'end_date'])
//,
//            'created_by' => new Column(['title' => __('classes.fields.created_by'), 'data' => 'created_by'])
//,
  //          'copied_from' => new Column(['title' => __('classes.fields.copied_from'), 'data' => 'copied_from'])
//,
            //    'status' => new Column(['title' => __('classes.fields.status'), 'data' => 'status'])

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'classes_datatable_' . time();
    }
}
