<?php

namespace App\DataTables;

use App\Models\Classes;
use Illuminate\Http\Request;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ClassesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
       // $dataTable->orderColumn('createdby', false);
        $dataTable ->editColumn('start_date', function ($class) {
            if ($class->start_date){
                return $class->start_date->format('d-m-Y');
            }
        });

        $dataTable ->editColumn('end_date', function ($class) {
            if($class->end_date){
                return $class->end_date->format('d-m-Y');
            }
        });

        return $dataTable->addColumn('action', 'classes.actions_classes');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Classes $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Classes $model, Request $request)
    {

        $modal = $model->newQuery();

        $schoolid = get_school_id();

        // Class with assignment
         if($request->has('assignment'))
         {
             // Assignment Class of others
             if($request->has('other'))
             {
                 return $modal->where('school_schid','=',$schoolid)->where('type','=','Assignment')->where('created_by','!=',auth()->user()->id)->with('createdby');
             }
             // Assignment Class of own
             return $modal->where('type','=','Assignment')->where('created_by','=',auth()->user()->id)->with('createdby');
         }

         //Class with lesson plans

        if($request->has('other'))
        {
            return $modal->where('school_schid','=',$schoolid)->where('created_by','!=',auth()->user()->id)->with('createdby');
        }

         //Lesson plans own
        return $modal->where('created_by','=',auth()->user()->id)->where('type','!=','Assignment')->with('createdby');



    }

    /**
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                //'dom' => "<'row dhiraj-2'<'col-sm-3'l><'col-sm-6'><'col-sm-3'f>>rtip",
                'dom' => "<'row dhiraj-2'<'col-sm-3'l><'col-sm-6'B><'col-sm-3'f>>rtip",

                'lengthMenu'=> [[50,100,500, -1], [50,100,500,"All"]],
                'order'=>[ [0, 'desc'] ],
                'scrollX' => true,
                'buttons'   => [
//                    [
//                       'extend' => 'create',
//                       'className' => 'btn btn-default btn-sm no-corner',
//                       'text' => '<i class="fa fa-plus"></i> ' .__('pagination.datatable.create').''
//                    ],
                    [
                       'extend' => 'excel',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('pagination.datatable.export').''
                    ],
//            [
//                'extend' => 'print',
//                'className' => 'btn btn-default btn-sm no-corner',
//                'text' => '<i class="fa fa-print"></i> ' .__('pagination.datatable.print').''
//            ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('pagination.datatable.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-sync-alt"></i> ' .__('pagination.datatable.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('classes.fields.classid'), 'data' => 'classid']),

            'class_name' => new Column(['title' => __('classes.fields.class_name'), 'data' => 'class_name']),
//,
//            'type' => new Column(['title' => __('classes.fields.type'), 'data' => 'type']),

            'Created By' => new Column(['title' => __('classes.fields.created_by'), 'data' => 'createdby.name', 'searchable' => false, 'orderable' => false])
//,
//            'division' => new Column(['title' => __('classes.fields.division'), 'data' => 'division'])
//,
//            'age_group' => new Column(['title' => __('classes.fields.age_group'), 'data' => 'age_group'])
//,
//            'start_date' => new Column(['title' => __('classes.fields.start_date'), 'data' => 'start_date'])
//,
//            'end_date' => new Column(['title' => __('classes.fields.end_date'), 'data' => 'end_date'])
//,
//            'created_by' => new Column(['title' => __('classes.fields.created_by'), 'data' => 'created_by'])
//,
  //          'copied_from' => new Column(['title' => __('classes.fields.copied_from'), 'data' => 'copied_from'])
//,
        //    'status' => new Column(['title' => __('classes.fields.status'), 'data' => 'status'])

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'classes_datatable_' . time();
    }
}
