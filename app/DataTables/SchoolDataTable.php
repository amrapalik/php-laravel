<?php

namespace App\DataTables;

use App\Models\School;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class SchoolDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'schools.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\School $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(School $model)
    {

        $query = $model->newQuery();

        if (\Auth::hasUser() && \Auth::user()->drole == 'Organization Admin') {
            $organization_orgid =  \Auth::user()->organization_orgid;
            // return $query->where('id', 1);
            $query->where('organization_orgid', '=', $organization_orgid);
        }

       if (\Auth::hasUser() && \Auth::user()->drole == 'Admin') {

           // return $query->where('id', 1);
           $query->where('schid', '!=', 0);
       }


        return $query;

    }

    /**
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => "<'row dhiraj-2'<'col-sm-3'l><'col-sm-6'B><'col-sm-3'f>>rtip",
                'lengthMenu'=> [[50,100,500, -1], [50,100,500,"All"]],
                'order'=>[ [0, 'desc'] ],
                'scrollX' => true,
                'buttons'   => [
//                    [
//                       'extend' => 'create',
//                       'className' => 'btn btn-default btn-sm no-corner',
//                       'text' => '<i class="fa fa-plus"></i> ' .__('pagination.datatable.create').''
//                    ],
                    [
                       'extend' => 'excel',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('pagination.datatable.export').''
                    ],
//            [
//                'extend' => 'print',
//                'className' => 'btn btn-default btn-sm no-corner',
//                'text' => '<i class="fa fa-print"></i> ' .__('pagination.datatable.print').''
//            ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('pagination.datatable.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-sync-alt"></i> ' .__('pagination.datatable.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        // $user_org_type =  \App\Models\Organization::find(auth()->user()->organization_orgid);
        $user_org_type =  @auth()->user()->organization->org_type;
		if($user_org_type && $user_org_type == 'organization'){       
        $columns = [
//            'organization_orgid' => new Column(['title' => __('schools.fields.organization_orgid'), 'data' => 'organization_orgid'])
//,
        'id' => new Column(['title' => __('schools.fields.id'), 'data' => 'schid']),

            'sch_name' => new Column(['title' => __('schools.fields.sch_name'), 'data' => 'sch_name']),
//            'about' => new Column(['title' => __('schools.fields.about'), 'data' => 'about'])
//,
            'no_of_teachers' => new Column(['title' => __('schools.fields.no_of_teachers'), 'data' => 'no_of_teachers']),
//,
//            'no_of_students' => new Column(['title' => __('schools.fields.no_of_students'), 'data' => 'no_of_students'])
////,
//            'sch_website' => new Column(['title' => __('schools.fields.sch_website'), 'data' => 'sch_website'])
//,
//            'facebook' => new Column(['title' => __('schools.fields.facebook'), 'data' => 'facebook'])
//,
//            'linkedin' => new Column(['title' => __('schools.fields.linkedin'), 'data' => 'linkedin'])
//,
//            'latitude' => new Column(['title' => __('schools.fields.latitude'), 'data' => 'latitude'])
//,
//            'longitude' => new Column(['title' => __('schools.fields.longitude'), 'data' => 'longitude'])
//,
//            'logo' => new Column(['title' => __('schools.fields.logo'), 'data' => 'logo'])
//,
//            'address1' => new Column(['title' => __('schools.fields.address1'), 'data' => 'address1'])
//,
//            'address2' => new Column(['title' => __('schools.fields.address2'), 'data' => 'address2'])
//,
//            'city' => new Column(['title' => __('schools.fields.city'), 'data' => 'city'])
//,
//            'state' => new Column(['title' => __('schools.fields.state'), 'data' => 'state'])
//,
//            'country' => new Column(['title' => __('schools.fields.country'), 'data' => 'country'])
//,
//            'zipcode' => new Column(['title' => __('schools.fields.zipcode'), 'data' => 'zipcode'])
//,
//            'sch_phone' => new Column(['title' => __('schools.fields.sch_phone'), 'data' => 'sch_phone'])
//,
//            'sch_mobile' => new Column(['title' => __('schools.fields.sch_mobile'), 'data' => 'sch_mobile'])
//,
//            'available_licenses' => new Column(['title' => __('schools.fields.available_licenses'), 'data' => 'available_licenses'])
//,
//            'used_licenses' => new Column(['title' => __('schools.fields.used_licenses'), 'data' => 'used_licenses'])
//,
        'available_licenses' => new Column(['title' => __('schools.fields.available_licenses'), 'data' => 'available_licenses']),
        'purchased_licenses' => new Column(['title' => __('schools.fields.purchased_licenses'), 'data' => 'purchased_licenses'])

        ];
    }
    else{
        $columns = [
            'id' => new Column(['title' => __('schools.fields.id'), 'data' => 'schid']),
            'sch_name' => new Column(['title' => __('schools.fields.dept_name'), 'data' => 'sch_name']),
            'no_of_teachers' => new Column(['title' => __('schools.fields.Total Number of Faculty Members'), 'data' => 'no_of_teachers']),
            'available_licenses' => new Column(['title' => __('schools.fields.available_licenses'), 'data' => 'available_licenses']),
            'purchased_licenses' => new Column(['title' => __('schools.fields.purchased_licenses'), 'data' => 'purchased_licenses'])    
            
        ];
    }
		// $user_org_type = auth()->user()->organization->org_type;


		// $user_org_type = @auth()->user()->organization->org_type;
		// if($user_org_type && $user_org_type == 'university'){
        //     $columns['sch_name'] = new Column(['title' => __('department-name'), 'data' => 'sch_name']);
        //     $columns['no_of_teachers'] = new Column(['title' => __('No of Professors'), 'data' => 'no_of_teachers']);
		// }
		return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'schools_datatable_' . time();
    }
}
