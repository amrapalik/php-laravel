<?php

namespace App\DataTables;

use App\Models\User;
use Illuminate\Auth\Access\Gate;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class TeacherDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

		$dataTable->editColumn('drole', function($t){
			if($t->school->school_type == 'university_dept'){
                if($t->drole == 'Teacher'){
                    return __('users.Faculty Member');
                }
				elseif($t->drole == 'School Admin'){
					return __('users.Department Admin');
				}
			}
			return $t->drole;
		});


        return $dataTable->addColumn('action', 'teachers.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
//        $schoolid = session('school_schid');


        $schoolid =  get_school_id();

//        if ($schoolid == null && (auth()->user()->drole == 'Organization Admin' || auth()->user()->drole == 'Admin') )
//        {
//            return redirect ('schools');
//        }
//
//        if ( auth()->user()->drole == 'School Admin' || auth()->user()->drole == 'Teacher' )
//        {
//            $schoolid = auth()->user()->school_schid;
//
//        }

        return $model->newQuery()->where('school_schid','=',$schoolid)->whereIn('drole', ['Teacher','School Admin']);
    }

    public function buttons()
    {
        $but =  [

            [
                'extend' => 'excel',
                'className' => 'btn btn-default btn-sm no-corner',
                'text' => '<i class="fa fa-download"></i> ' .__('pagination.datatable.export').''
            ],
//                      [
//                'extend' => 'print',
//                'className' => 'btn btn-default btn-sm no-corner',
//                'text' => '<i class="fa fa-print"></i> ' .__('pagination.datatable.print').''
//            ],
            [
                'extend' => 'reset',
                'className' => 'btn btn-default btn-sm no-corner',
                'text' => '<i class="fa fa-undo"></i> ' .__('pagination.datatable.reset').''
            ],
            [
                'extend' => 'reload',
                'className' => 'btn btn-default btn-sm no-corner',
                'text' => '<i class="fa fa-sync-alt"></i> ' .__('pagination.datatable.reload').''
            ],
        ];


//            if(Gate::check('permission1'))
//            {
//
//                array_push($but, [
//                'extend' => 'create',
//                'className' => 'btn btn-default btn-sm no-corner',
//                'text' => '<i class="fa fa-plus"></i> ' .__('pagination.datatable.create').''
//            ]);
//
//            }


        return  $but;
    }

    /**
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => "<'row dhiraj-2'<'col-sm-3'l><'col-sm-6'B><'col-sm-3'f>>rtip",
                'lengthMenu'=> [[50,100,500, -1], [50,100,500,"All"]],
                'order'=>[ [0, 'desc'] ],
                'scrollX' => true,
                'buttons'   => $this->buttons(),
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $school = \App\Models\School::find(auth()->user()->school_schid);
        $isOrg = true;
        if(empty($school)){
           $isOrg = \App\Models\Organization::find(auth()->user()->organization_orgid) ;
           if($isOrg->org_type == 'family'){
               $isOrg = false;
           }
        }
        if(!empty($school) && $school->school_type == 'family'){
            $isOrg = false; 
        }
        if((!empty($school) && ($school->school_type == 'school' || $school->school_type == 'university_dept')) || $isOrg){
        return [
            'id' => new Column(['title' => __('users.fields.id'), 'data' => 'id']),
//            'organization_orgid' => new Column(['title' => __('users.fields.organization_orgid'), 'data' => 'organization_orgid'])
//,
//            'school_schid' => new Column(['title' => __('users.fields.school_schid'), 'data' => 'school_schid'])
//,

            'name' => new Column(['title' => __('users.fields.name'), 'data' => 'name'])
            , 'username' => new Column(['title' => __('users.fields.username'), 'data' => 'username'])
            ,
//            'drole' => new Column(['title' => __('users.fields.name'), 'data' => 'drole'])
//            ,

//            'name' => new Column(['title' => __('users.fields.name'), 'data' => 'name'])
//,
//            'type' => new Column(['title' => __('users.fields.type'), 'data' => 'type'])
//,
//            'password' => new Column(['title' => __('users.fields.password'), 'data' => 'password'])
//,
//            'email' => new Column(['title' => __('users.fields.email'), 'data' => 'email'])
//,
            'drole' => new Column(['title' => __('users.fields.drole'), 'data' => 'drole'])
,
//            'grade' => new Column(['title' => __('users.fields.grade'), 'data' => 'grade'])
//            ,
//            'phone_no' => new Column(['title' => __('users.fields.phone_no'), 'data' => 'phone_no'])
//,
//            'address' => new Column(['title' => __('users.fields.address'), 'data' => 'address'])
//,
//            'country' => new Column(['title' => __('users.fields.country'), 'data' => 'country'])
//,
//            'country_code' => new Column(['title' => __('users.fields.country_code'), 'data' => 'country_code'])
//,
//            'purchaseflag' => new Column(['title' => __('users.fields.purchaseflag'), 'data' => 'purchaseflag'])
//,
//            'gender' => new Column(['title' => __('users.fields.gender'), 'data' => 'gender'])
//,
//            'avatar' => new Column(['title' => __('users.fields.avatar'), 'data' => 'avatar'])
//,
//            'alternate_address' => new Column(['title' => __('users.fields.alternate_address'), 'data' => 'alternate_address'])
//,
//            'city' => new Column(['title' => __('users.fields.city'), 'data' => 'city'])
//,
//            'state' => new Column(['title' => __('users.fields.state'), 'data' => 'state'])
//,
//            'zipcode' => new Column(['title' => __('users.fields.zipcode'), 'data' => 'zipcode'])
//,
//            'designation' => new Column(['title' => __('users.fields.designation'), 'data' => 'designation'])
//,
//            'alternate_email' => new Column(['title' => __('users.fields.alternate_email'), 'data' => 'alternate_email'])
//,
//            'facebook_profile' => new Column(['title' => __('users.fields.facebook_profile'), 'data' => 'facebook_profile'])
//,
//            'linkedIn_profile' => new Column(['title' => __('users.fields.linkedIn_profile'), 'data' => 'linkedIn_profile'])
//,
//            'email_verified_at' => new Column(['title' => __('users.fields.email_verified_at'), 'data' => 'email_verified_at'])
//,
//            'remember_token' => new Column(['title' => __('users.fields.remember_token'), 'data' => 'remember_token'])
//,
//            'dob' => new Column(['title' => __('users.fields.dob'), 'data' => 'dob'])
//,
//            'last_loggedin' => new Column(['title' => __('users.fields.last_loggedin'), 'data' => 'last_loggedin'])
//,
//            'account_expiry_date' => new Column(['title' => __('users.fields.account_expiry_date'), 'data' => 'account_expiry_date'])
//,
//            'account_status' => new Column(['title' => __('users.fields.account_status'), 'data' => 'account_status'])
//            ,
//            'deallocated' => new Column(['title' => __('users.fields.deallocated'), 'data' => 'deallocated'])
//,
//            'account_expired' => new Column(['title' => __('users.fields.account_expired'), 'data' => 'account_expired'])

            ];
     }
     else{
         return [            
             'id' => new Column(['title' => __('users.fields.id'), 'data' => 'id']),
             'name' => new Column(['title' => __('users.fields.name'), 'data' => 'name']),
             'username' => new Column(['title' => __('users.fields.username'), 'data' => 'username'])
                     
         ];
     }
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'users_datatable_' . time();
    }
}
