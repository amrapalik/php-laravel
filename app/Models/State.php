<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class User
 * @package App\Models
 * @version May 30, 2020, 4:48 pm UTC
 */

class State extends Model
{
   /* use SoftDeletes;*/

    use HasFactory;
   // use SoftDeletes;

  
    public $table = 'states';
    
    
 
    public $fillable = [
        'id',
        'name',
        'country_id',
     
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
      
         'id'=> 'integer',
        'name' => 'string',
        'country_id'=> 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
          
     
      
       
    ];

    
}
