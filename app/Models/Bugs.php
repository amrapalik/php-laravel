<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Bugs
 * @package App\Models
 * @version December 11, 2021, 7:49 pm UTC
 *
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $link
 * @property string $status
 * @property integer $created_by`
 */
class Bugs extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'bugs';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    protected $primaryKey = 'bugid';

    public $fillable = [
        'title',
        'description',
        'image',
        'link',
        'type',
        'status',
        'created_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'bugid' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'image' => 'string',
        'link' => 'string',
        'status' => 'string',
        'created_by`' => 'integer'
    ];

    /**
     * Validation rules for create
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string|max:255',
        'description' => 'required|string',
        'type' => 'required|string',
        //'image' => 'nullable|string|max:100',
        'link' => 'nullable|string|max:200',
        'status' => 'nullable|string|max:50',
        'created_by`' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];


     /**
     * Validation rules for update
     *
     * @var array
     */
    public static $update_rules = [
        'title' => 'required|string|max:255',
        'description' => 'required|string',
        'image' => 'nullable|string|max:100',
        //'link' => 'nullable|string|max:200',
        'status' => 'nullable|string|max:50',
        'created_by`' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];


}
