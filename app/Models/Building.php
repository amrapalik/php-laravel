<?php

namespace App\Models;

use App\Scopes\CompanyScope;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Building
 * @package App\Models
 * @version December 31, 2020, 8:44 pm UTC
 *
 * @property string $nick_name
 * @property string $building_name
 * @property string $lat
 * @property string $lng
 * @property number $external_height
 * @property number $inside_height
 * @property string $floors
 * @property string $completion
 * @property string $material
 * @property string $building_type
 * @property integer $company_id
 * @property string $status
 */
class Building extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'buildings';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nick_name',
        'building_name',
        'lat',
        'lng',
        'external_height',
        'inside_height',
        'floors',
        'completion',
        'material',
        'building_type',
        'company_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nick_name' => 'string',
        'building_name' => 'string',
        'lat' => 'string',
        'lng' => 'string',
        'external_height' => 'float',
        'inside_height' => 'float',
        'floors' => 'string',
        'completion' => 'string',
        'material' => 'string',
        'building_type' => 'string',
        'company_id' => 'integer',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nick_name' => 'required|string|max:50',
        'building_name' => 'required|string|max:50',
        'lat' => 'required|string|max:50',
        'lng' => 'required|string|max:50',
        'external_height' => 'nullable|numeric',
        'inside_height' => 'nullable|numeric',
        'floors' => 'nullable|string|max:10',
        'completion' => 'nullable|string|max:20',
        'material' => 'nullable|string|max:50',
        'building_type' => 'nullable|string|max:50',
        'company_id' => 'required|integer',
        'status' => 'nullable|string|max:50',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public static $update_rules = [
        'nick_name' => 'nullable|string|max:50',
        'building_name' => 'required|string|max:50',
        'lat' => 'nullable|string|max:50',
        'lng' => 'nullable|string|max:50',
        'external_height' => 'nullable|numeric',
        'inside_height' => 'nullable|numeric',
        'floors' => 'nullable|string|max:10',
        'completion' => 'nullable|string|max:20',
        'material' => 'nullable|string|max:50',
        'building_type' => 'nullable|string|max:50',
        'company_id' => 'required|integer',
        'status' => 'nullable|string|max:50',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    protected static function booted()
    {     $company_id = auth()->user()->company_id ?? null;
        static::addGlobalScope(new CompanyScope($company_id));
    }


}
