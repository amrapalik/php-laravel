<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentAssignment extends Model
{
    protected $guarded = [];
    protected  $table = 'student_assignment';

    protected $primaryKey = 'student_assignment_id';
    public function user()
    {
        return $this->hasOne(User::class,'id','student_id');
    }

    public function assignment()
    {
        return $this->belongsTo(Assignment::class,'assignment_asid','asid');
    }

    public function created_by()
    {
        return $this->belongsTo(User::class,'teacher_id','id');
    }


}
