<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Organization
 * @package App\Models
 * @version November 7, 2021, 8:14 pm UTC
 *
 * @property integer $created_by
 * @property string $org_name
 * @property string $org_email
 * @property string $about
 * @property integer $no_of_teachers
 * @property integer $no_of_students
 * @property string $website
 * @property string $facebook
 * @property string $linkedin
 * @property string $latitude
 * @property string $longitude
 * @property string $logo
 * @property string $address1
 * @property string $address2
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $zipcode
 * @property string $phone
 * @property string $mobile
 */
class Organization extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'organizations';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    protected $primaryKey = 'orgid';

    public $fillable = [
        'created_by',
        'org_name',
        'org_type',
        'org_email',
        'about',
        'no_of_teachers',
        'no_of_students',
        'website',
        'facebook',
        'linkedin',
        'latitude',
        'longitude',
        'logo',
        'address1',
        'address2',
        'city',
        'state',
        'country',
        'zipcode',
        'phone',
        'mobile'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'orgid' => 'integer',
        'created_by' => 'integer',
        'org_name' => 'string',
        'org_email' => 'string',
        'about' => 'string',
        'no_of_teachers' => 'integer',
        'no_of_students' => 'integer',
        'website' => 'string',
        'facebook' => 'string',
        'linkedin' => 'string',
        'latitude' => 'string',
        'longitude' => 'string',
        'logo' => 'string',
        'address1' => 'string',
        'address2' => 'string',
        'city' => 'string',
        'state' => 'string',
        'country' => 'string',
        'zipcode' => 'string',
        'phone' => 'string',
        'mobile' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'created_by' => 'nullable',
        'org_name' => 'required|string|max:255',
        'org_email' => 'nullable|email|max:100',
        'about' => 'nullable|string',
        'no_of_teachers' => 'nullable|integer|min:1',
        'no_of_students' => 'nullable|integer|min:1',
        'website' => 'nullable|string|url|max:255',
        'facebook' => 'nullable|string|url|max:255',
        'linkedin' => 'nullable|string|url|max:255',
        'latitude' => 'nullable|string|max:255',
        'longitude' => 'nullable|string|max:255',
        'logo' => 'nullable|string|max:100',
        'address1' => 'nullable|string|max:255',
        'address2' => 'nullable|string|max:255',
        'city' => 'required|string|max:100',
        'state' => 'required|string|max:100',
        'country' => 'required|string|max:100',
        'zipcode' => 'nullable|string|max:20',
        'phone' => 'required|string|max:20',
        'mobile' => 'nullable|numeric|digits_between:9,11',
    ];


}
