<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\UserPreferences;

/**
 * Class User
 * @package App\Models
 * @version November 7, 2021, 7:36 pm UTC
 *
 * @property integer $organization_orgid
 * @property integer $school_schid
 * @property string $username
 * @property string $firstname
 * @property string $lastname
 * @property string $name
 * @property string $type
 * @property string $password
 * @property string $email
 * @property string $drole
 * @property string $grade
 * @property string $phone_no
 * @property string $address
 * @property string $country
 * @property string $country_code
 * @property string $purchaseflag
 * @property string $gender
 * @property string $avatar
 * @property string $alternate_address
 * @property string $city
 * @property string $state
 * @property string $zipcode
 * @property string $designation
 * @property string $alternate_email
 * @property string $facebook_profile
 * @property string $linkedIn_profile
 * @property string|\Carbon\Carbon $email_verified_at
 * @property string $remember_token
 * @property string $dob
 * @property string|\Carbon\Carbon $last_loggedin
 * @property string $account_expiry_date
 * @property string $account_status
 * @property boolean $deallocated
 * @property boolean $account_expired
 */
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App\Notifications\PasswordReset;
class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use SoftDeletes;
    use HasRoles;
    protected $guard_name = 'web';

    use HasFactory;

    public $table = 'users';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at','account_expiry_date'];

    public $fillable = [
        'organization_orgid',
        'school_schid',
        'username',
        'firstname',
        'lastname',
        'name',
        'type',
        'password',
        'email',
        'drole',
        'grade',
        'phone_no',
        'user_mobile',
        'address',
        'country',
        'country_code',
        'purchaseflag',
        'gender',
        'avatar',
        'alternate_address',
        'city',
        'state',
        'zipcode',
        'designation',
        'alternate_email',
        'facebook_profile',
        'linkedIn_profile',
        'email_verified_at',
        'remember_token',
        'dob',
        'order_ordid',
        'last_loggedin',
        'account_expiry_date',
        'account_status',
        'deallocated',
        'account_expired'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'organization_orgid' => 'integer',
        'school_schid' => 'integer',
        'username' => 'string',
        'firstname' => 'string',
        'lastname' => 'string',
        'name' => 'string',
        'type' => 'string',
        'password' => 'string',
        'email' => 'string',
        'drole' => 'string',
        'grade' => 'string',
        'phone_no' => 'string',
        'user_mobile' => 'string',
        'address' => 'string',
        'country' => 'string',
        'country_code' => 'string',
        'purchaseflag' => 'string',
        'gender' => 'string',
        'avatar' => 'string',
        'alternate_address' => 'string',
        'city' => 'string',
        'state' => 'string',
        'zipcode' => 'string',
        'designation' => 'string',
        'alternate_email' => 'string',
        'facebook_profile' => 'string',
        'linkedIn_profile' => 'string',
        'email_verified_at' => 'datetime',
        'remember_token' => 'string',
        'dob' => 'date',
        'last_loggedin' => 'datetime',
        'account_expiry_date' => 'date',
        'account_status' => 'string',
        'deallocated' => 'boolean',
        'account_expired' => 'boolean'
    ];

	/*
	Custom attributes
	*/
	protected $appends = ['me_or_name'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'organization_orgid' => 'nullable',
        'school_schid' => 'nullable',
        'username' => 'required|string|min:4|max:100|unique:users',
        'firstname' => 'required|string|max:50',
        'lastname' => 'required|string|max:50',
        'name' => 'nullable|string|max:50',
        'type' => 'nullable|string',
       // 'password' => 'required|string|max:15',
        'email' => 'nullable|email|max:100',
        'drole' => 'nullable|string',
        'grade' => 'nullable|string|max:24',
        'phone_no' => 'nullable|string|max:20',
        'user_mobile' => 'nullable|numeric|digits_between:9,11',
        'address' => 'nullable|string|max:255',
        'country' => 'required|string|max:45',
        'country_code' => 'required|string|max:5',
        'purchaseflag' => 'nullable|string|max:45',
        'gender' => 'required|string',
        'avatar' => 'nullable|string|max:255',
        'alternate_address' => 'nullable|string|max:255',
        'city' => 'required|string|max:45',
        'state' => 'required|string|max:45',
        'zipcode' => 'nullable|string|max:20',
        'designation' => 'nullable|string|max:45',
        'alternate_email' => 'nullable|string|max:100',
        'facebook_profile' => 'nullable|string|max:250',
        'linkedIn_profile' => 'nullable|string|max:250',
        'email_verified_at' => 'nullable',
        'remember_token' => 'nullable|string|max:100',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'dob' => 'nullable|date|before:2015-00-00',
        'last_loggedin' => 'nullable',
        'deleted_at' => 'nullable',
        'account_expiry_date' => 'nullable',
        'account_status' => 'nullable|string|max:30',
        'deallocated' => 'nullable|boolean',
        'account_expired' => 'nullable|boolean',
            // ['required', new Recaptcha()]]);
    ];
   
    public static $updateRules = [
        'organization_orgid' => 'nullable',
        'school_schid' => 'nullable',
        'username' => 'required|string|min:4|max:100|unique:users',
        'firstname' => 'required|string|max:50',
        'lastname' => 'required|string|max:50',
        'name' => 'nullable|string|max:50',
        'type' => 'nullable|string',
        // 'password' => 'required|string|max:15',
        'email' => 'nullable|email|max:100',
        'drole' => 'nullable|string',
        'grade' => 'nullable|string|max:24',
        'phone_no' => 'nullable|string|max:20',
        'user_mobile' => 'nullable|numeric|digits_between:9,11',
        'address' => 'nullable|string|max:255',
        'country' => 'required|string|max:45',
        'country_code' => 'required|string|max:5',
        'purchaseflag' => 'nullable|string|max:45',
        'gender' => 'required|string',
        'avatar' => 'nullable|string|max:255',
        'alternate_address' => 'nullable|string|max:255',
        'city' => 'required|string|max:45',
        'state' => 'required|string|max:45',
        'zipcode' => 'nullable|string|max:20',
        'designation' => 'nullable|string|max:45',
        'alternate_email' => 'nullable|string|max:100',
        'facebook_profile' => 'nullable|string|max:250',
        'linkedIn_profile' => 'nullable|string|max:250',
        'email_verified_at' => 'nullable',
        'remember_token' => 'nullable|string|max:100',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'dob' => 'nullable|date|before:2015-00-00',
        'last_loggedin' => 'nullable',
        'deleted_at' => 'nullable',
        'account_expiry_date' => 'nullable',
        'account_status' => 'nullable|string|max:30',
        'deallocated' => 'nullable|boolean',
        'account_expired' => 'nullable|boolean'
    ];

    public function scopeAllocated($query)
    {
        return $query->where('deallocated','=',0);
    }

    public static function gResponseValidation($attribute, $value, $fail) 
    {
        $secretkey = env('GOOGLE_RECAPTCHA_SECRET');
        $response = $value;
        $userIp = $_SERVER['REMOTE_ADDR'];
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretkey&response=$response&remoteip=$userIp";
        $response = file_get_contents($url);
        $response = json_decode($response);
        if(!$response->success){
            $fail('Please Give the Captcha response');
        }
    }


    public function scopeDeAllocated($query)
    {
        return $query->where('deallocated','=',1);
    }

//    public function getNameAttribute()
//    {
//        return "{$this->firstname} {$this->lastname}";
//    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }

	public function notifications(){
		return $this->hasMany(Notification::class, 'to');
	}

	public function preferences(){
		return $this->hasOne(UserPreferences::class, 'user_id');
	}

	public function setPreferences($preferences){
		$p = $this->preferences;
		if(!$p){
			$p = new UserPreferences;
			$p->user_id = $this->id;
			$p->preferences = json_encode($preferences);
		}
		else{
			$old = json_decode($p->preferences);
			foreach($preferences as $k=>$v){
				$old->$k = $v;
			}
			$p->preferences = json_encode($old);
		}
		$p->save();
	}

	public function getMeOrNameAttribute(){
		return (auth()->user()->id == $this->id)? 'me' : $this->name;
	}

	public function organization(){
		return $this->belongsTo(Organization::class, 'organization_orgid');
	}

	public function school(){
		return $this->belongsTo(School::class, 'school_schid');
	}

    

}
