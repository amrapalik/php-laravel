<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Product
 * @package App\Models
 * @version November 19, 2021, 4:00 am UTC
 *
 * @property string $prod_name
 * @property integer $no_of_licenses
 * @property string $description
 * @property number $price
 * @property number $renew_price
 * @property string $user_type
 * @property integer $max_lives
 * @property integer $validity
 * @property string $currency
 * @property string $featured
 * @property string $suspended
 */
class Product extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'products';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    protected $primaryKey = 'prodid';

    public $fillable = [
        'prod_name',
        'no_of_licenses',
        'description',
        'price',
        'renew_price',
        'user_type',
        'max_lives',
        'validity',
        'currency',
        'featured',
        'suspended',
        'expiry_type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'prodid' => 'integer',
        'prod_name' => 'string',
        'no_of_licenses' => 'integer',
        'description' => 'string',
        'price' => 'float',
        'renew_price' => 'float',
        'user_type' => 'string',
        'max_lives' => 'integer',
        'validity' => 'integer',
        'currency' => 'string',
        'featured' => 'string',
        'suspended' => 'string',
        'expiry_type' => 'string'
    ];

    /**
     * Validation rules for create
     *
     * @var array
     */
    public static $rules = [
        'prod_name' => 'required|string|max:255',
        'no_of_licenses' => 'required|integer|min:0',
        'description' => 'nullable|string|max:255',
        'price' => 'required|numeric|min:0',
        'renew_price' => 'required|numeric|min:0',
        'user_type' => 'required|string',
        'max_lives' => 'nullable|integer|min:0',
        'validity' => 'required|integer|min:1',
        'currency' => 'nullable|string|max:10',
        'featured' => 'nullable|string|max:5',
        'suspended' => 'nullable|string|max:5',
        'expiry_type' => 'required|max:20',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];


     /**
     * Validation rules for update
     *
     * @var array
     */
    public static $update_rules = [
        'prod_name' => 'nullable|string|max:255',
        'no_of_licenses' => 'required|integer|min:1',
        'description' => 'nullable|string|max:255',
        'price' => 'required|numeric|min:0',
        'renew_price' => 'required|numeric|min:0',
        'user_type' => 'required|string',
        'max_lives' => 'nullable|integer|min:0',
        'validity' => 'required|integer|min:1',
        'currency' => 'nullable|string|max:10',
        'featured' => 'nullable|string|max:5',
        'suspended' => 'nullable|string|max:5',
        'expiry_type' => 'required|string|max:20',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];


}
