<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class License
 * @package App\Models
 * @version January 17, 2022, 1:56 pm UTC
 *
 * @property integer $user_id
 * @property integer $school_schid
 * @property integer $product_prodid
 * @property integer $order_ordid
 * @property integer $licences
 * @property integer $licences_available
 * @property number $price
 * @property string $paypal_orderid
 * @property string $status
 * @property boolean $paid
 * @property string|\Carbon\Carbon $expiry_date
 * @property boolean $captured
 */
class License extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'licenses';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    protected $primaryKey = 'licid';

    public $fillable = [
        'user_id',
        'school_schid',
        'product_prodid',
        'order_ordid',
        'lives',
        'available_lives',
        'price',
        'renew_price',
        'paypal_orderid',
        'paypal_captureid',
        'status',
        'paid',
        'expiry_date',
        'captured'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'licid' => 'integer',
        'user_id' => 'integer',
        'school_schid' => 'integer',
        'product_prodid' => 'integer',
        'order_ordid' => 'integer',
        'lives' => 'integer',
        'available_lives' => 'integer',
        'price' => 'float',
        'renew_price'=> 'float',
        'paypal_orderid' => 'string',
        'status' => 'string',
        'paid' => 'boolean',
        'expiry_date' => 'datetime',
        'captured' => 'boolean'
    ];

    /**
     * Validation rules for create
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'school_schid' => 'nullable|integer',
        'product_prodid' => 'required|integer',
        'order_ordid' => 'required',
        'licences' => 'required',
        'renew_price'=>'required',
        'licences_available' => 'required|integer',
        'price' => 'required|numeric',
        'paypal_orderid' => 'required|string|max:100',
        'status' => 'required|string|max:50',
        'paid' => 'nullable|boolean',
        'expiry_date' => 'nullable',
        'captured' => 'nullable|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];


     /**
     * Validation rules for update
     *
     * @var array
     */
    public static $update_rules = [
        'user_id' => 'required',
        'school_schid' => 'nullable|integer',
        'product_prodid' => 'required|integer',
        'order_ordid' => 'required',
        'licences' => 'required',
        'renew_price'=>'required',
        'licences_available' => 'required|integer',
        'price' => 'required|numeric',
        'paypal_orderid' => 'required|string|max:100',
        'status' => 'required|string|max:50',
        'paid' => 'nullable|boolean',
        'expiry_date' => 'nullable',
        'captured' => 'nullable|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id','id');
    }

    public function product()
    {
        return $this->belongsTo(\App\Models\Product::class, 'product_prodid','prodid');
    }

    public function order()
    {
        return $this->hasOne(\App\Models\Order3::class, 'order_ordid','ordid');
    }

    public function school()
    {
        return $this->hasOne(\App\Models\School::class, 'schid','school_schid');
    }





}
