<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignmentComment extends Model
{
    protected $guarded = [];
    protected $table = 'assignment_comments';
}
