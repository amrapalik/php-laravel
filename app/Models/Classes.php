<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Classes
 * @package App\Models
 * @version November 26, 2021, 6:18 pm UTC
 *
 * @property string $class_name
 * @property string $class_description
 * @property string $grade
 * @property string $division
 * @property string $age_group
 * @property string $start_date
 * @property string $end_date
 * @property integer $created_by
 * @property integer $copied_from
 * @property boolean $status
 */
class Classes extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'classes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    protected $primaryKey = 'classid';

    public $fillable = [
        'school_schid',
        'class_name',
        'class_description',
        'grade',
        'division',
        'age_group',
        'start_date',
        'end_date',
        'created_by',
        'assignment_asid',
        'lessonplan_lpid',
        'type',
        'copied_from',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'classid' => 'integer',
        'class_name' => 'string',
        'class_description' => 'string',
        'grade' => 'string',
        'division' => 'string',
        'age_group' => 'string',
        'start_date' => 'date',
        'end_date' => 'date',
        'created_by' => 'integer',
        'copied_from' => 'integer',
        'assignment_asid'=> 'integer',
        'type'=> 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules for create
     *
     * @var array
     */
    public static $rules = [
        'class_name' => 'required|string|max:255',
        'class_description' => 'nullable|string',
        //'grade' => 'required|string|max:32',
        'assignment_asid'=> 'nullable|integer',
        'type'=> 'nullable|string|max:32',
        'division' => 'nullable|string|max:32',
        'age_group' => 'nullable|string|max:32',
       // 'start_date'      => 'required|date|before:end_date',
        'end_date'        => 'date',
       // 'students' => 'required',
      //  'teachers' => 'required',
//        'students[]' => 'required',
//        'teachers[]' => 'required',
        'created_by' => 'nullable',
        'copied_from' => 'nullable',
        'status' => 'nullable|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];


     /**
     * Validation rules for update
     *
     * @var array
     */
    public static $update_rules = [
        'class_name' => 'nullable|string|max:255',
        'class_description' => 'nullable|string',
        'grade' => 'nullable|string|max:32',
        'division' => 'nullable|string|max:32',
        'age_group' => 'nullable|string|max:32',
        'assignment_asid'=> 'nullable|integer',
        'type'=> 'nullable|string|max:32',
        'start_date' => 'nullable',
        'end_date' => 'nullable',
        'created_by' => 'nullable',
        'copied_from' => 'nullable',
        'status' => 'nullable|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function assignedUsers()
    {
        return $this->belongsToMany(User::class, 'class_users','class_classid','user_id');

    }

    public function createdby()
    {
        return $this->belongsTo(User::class,  'created_by');

    }

    public function created_by()
    {
        return $this->belongsTo(User::class,  'created_by');

    }

    public function assignment()
    {
        return $this->hasOne(Assignment::class, 'asid', 'assignment_asid');

    }
	
	public function lesson_plan(){
		return $this->belongsTo(LessonPlan::class, 'lessonplan_lpid');
	}

}
