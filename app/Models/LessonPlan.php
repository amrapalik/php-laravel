<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Assignment;

/**
 * Class LessonPlan
 * @package App\Models
 * @version December 2, 2021, 2:09 am UTC
 *
 * @property integer $class_classid
 * @property integer $school_schid
 * @property string $name
 * @property string $grades
 * @property string $standards
 * @property string $overview
 * @property string $objectives
 * @property string $procedures
 * @property string $assessment
 * @property string $extension_activities
 * @property string $image
 * @property string $pdf
 * @property string $video
 * @property string $youtube_link
 * @property integer $created_by
 * @property integer $copied_from
 * @property boolean $status
 */
class LessonPlan extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'lesson_plans';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    protected $primaryKey = 'lpid';

    public $fillable = [
        'assignment_id',
        'class_classid',
        'school_schid',
        'name',
        'grades',
        'standards',
        'overview',
        'objectives',
        'procedures',
        'assessment',
        'extension_activities',
        'image',
        'pdf',
        'video',
        'youtube_link',
        'created_by',
        'copied_from',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'lpid' => 'integer',
        'assignment_id' => 'integer',
        'class_classid' => 'integer',
        'school_schid' => 'integer',
        'name' => 'string',
        'grades' => 'string',
        'standards' => 'string',
        'overview' => 'string',
        'objectives' => 'string',
        'procedures' => 'string',
        'assessment' => 'string',
        'extension_activities' => 'string',
        'image' => 'string',
        'pdf' => 'string',
        'video' => 'string',
        'youtube_link' => 'string',
        'created_by' => 'integer',
        'copied_from' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules for create
     *
     * @var array
     */
    public static $rules = [
        'class_classid' => 'nullable',
        'school_schid' => 'nullable',
        'name' => 'required|string|max:255',
        'grades' => 'nullable|string|max:255',
        'standards' => 'nullable|string|max:255',
        'overview' => 'nullable|string',
        'objectives' => 'nullable|string',
        'procedures' => 'nullable|string',
        'assessment' => 'nullable|string',
        'extension_activities' => 'nullable|string',
       // 'image' => 'nullable|string|max:128',
        'image' => 'mimes:jpeg,jpg,png,gif,svg|max:2048',
        'pdf' => 'nullable|string|max:255',
        'video' => 'nullable|string|max:255',
        'youtube_link' => 'nullable|string|max:255',
        'created_by' => 'nullable',
        'copied_from' => 'nullable',
        'status' => 'nullable|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];


     /**
     * Validation rules for update
     *
     * @var array
     */
    public static $update_rules = [
        'class_classid' => 'nullable',
        'school_schid' => 'nullable',
        'name' => 'nullable|string|max:255',
        'grades' => 'nullable|string|max:255',
        'standards' => 'nullable|string|max:255',
        'overview' => 'nullable|string',
        'objectives' => 'nullable|string',
        'procedures' => 'nullable|string',
        'assessment' => 'nullable|string',
        'extension_activities' => 'nullable|string',
        'image' => 'nullable|string|max:128',
        'pdf' => 'nullable|string|max:255',
        'video' => 'nullable|string|max:255',
        'youtube_link' => 'nullable|string|max:255',
        'created_by' => 'nullable',
        'copied_from' => 'nullable',
        'status' => 'nullable|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

	public function assignment(){
		return $this->belongsTo(Assignment::class, 'assignment_id');
	}

	public function creator(){
		return $this->belongsTo(User::class, 'created_by');
	}

}
