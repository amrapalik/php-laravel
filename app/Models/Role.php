<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Role
 * @package App\Models
 * @version November 23, 2021, 5:35 am UTC
 *
 * @property \App\Models\ModelHasRole $modelHasRole
 * @property \Illuminate\Database\Eloquent\Collection $permissions
 * @property string $name
 * @property string $guard_name
 */
class Role   extends \Spatie\Permission\Models\Role
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'roles';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    protected $primaryKey = 'id';

    public $fillable = [
        'name',
        'guard_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'guard_name' => 'string'
    ];

    /**
     * Validation rules for create
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:255',
        'guard_name' => 'required|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];


     /**
     * Validation rules for update
     *
     * @var array
     */
    public static $update_rules = [
        'name' => 'required|string|max:255',
        'guard_name' => 'required|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];


//
//    /**
//     * @return \Illuminate\Database\Eloquent\Relations\HasOne
//     **/
//    public function modelHasRole()
//    {
//        return $this->hasOne(\App\Models\ModelHasRole::class);
//    }

//    /**
//     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
//     **/
//    public function permissions()
//    {
//        return $this->belongsToMany(\App\Models\Permission::class, 'role_has_permissions');
//    }


}
