<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Assignment
 * @package App\Models
 * @version December 15, 2021, 5:23 pm UTC
 *
 * @property string $asname
 * @property string $category
 * @property string $status
 * @property integer $countryid
 * @property string $country_name
 * @property integer $city_id
 * @property string $city_name
 * @property string $gender
 * @property string $religion
 * @property string $urban_rural
 */
class Assignment extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'assignments';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    protected $primaryKey = 'asid';

    public $fillable = [
        'asname',
        'category',
        'status',
        'countryid',
        'country_name',
        'city_id',
        'city_name',
        'gender',
        'type',
        'tag',
        'religion',
        'urban_rural'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'asid' => 'integer',
        'asname' => 'string',
        'category' => 'string',
        'status' => 'string',
        'countryid' => 'integer',
        'country_name' => 'string',
        'city_id' => 'integer',
        'city_name' => 'string',
        'gender' => 'string',
        'religion' => 'string',
        'type'=> 'string',
        'tag'=> 'string',
        'urban_rural' => 'string'
    ];

    /**
     * Validation rules for create
     *
     * @var array
     */
    public static $rules = [
        'asname' => 'required|string|max:255',
        'category' => 'required|string|max:100',
//        'status' => 'required|string|max:50',
        'countryid' => 'required|integer',
        'country_name' => 'required|string|max:200',
        'city_id' => 'required|integer',
        'city_name' => 'required|string|max:11',
        'gender' => 'required|string|max:11',
        'religion' => 'required|string|max:11',
        'urban_rural' => 'required|string|max:11',
        'type'=> 'required|string|max:11',
        'tag'=> 'required|string|max:11',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];


     /**
     * Validation rules for update
     *
     * @var array
     */
    public static $update_rules = [
        'asname' => 'required|string|max:255',
        'category' => 'required|string|max:100',
//        'status' => 'required|string|max:50',
        'countryid' => 'required|integer',
        'country_name' => 'required|string|max:200',
        'city_id' => 'required|integer',
        'city_name' => 'required|string|max:11',
        'gender' => 'required|string|max:11',
        'religion' => 'required|string|max:11',
        'urban_rural' => 'required|string|max:11',
        'type'=> 'required|string|max:11',
        'tag'=> 'required|string|max:11',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];


}
