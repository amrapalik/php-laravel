<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 * @version May 30, 2020, 4:48 pm UTC
 */
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class City extends Model
{
    

   // use SoftDeletes;

    public $table = 'cities';
    
 
    public $fillable = [
        'id',
        'name',
        'state_id',
     
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
      
         'id'=> 'integer',
        'name' => 'string',
        'state_id'=> 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
          
     
      
       
    ];

    
}
