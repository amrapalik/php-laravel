<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 * @version May 30, 2020, 4:48 pm UTC
 */
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Countries extends Model
{
 
   // use SoftDeletes;

   
    public $table = 'countries';
    
 
    public $fillable = [
        'id',
        'shortname',
        'name',
        'phonecode',
     
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
      
         'id'=> 'integer',
        'shortname' => 'string',
         'name' => 'string',
        'phonecode'=> 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
          
     
      
       
    ];

    
}
