<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class School
 * @package App\Models
 * @version November 8, 2021, 11:51 am UTC
 *
 * @property integer $organization_orgid
 * @property string $sch_name
 * @property string $about
 * @property integer $no_of_teachers
 * @property integer $no_of_students
 * @property string $sch_website
 * @property string $facebook
 * @property string $linkedin
 * @property string $latitude
 * @property string $longitude
 * @property string $logo
 * @property string $address1
 * @property string $address2
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $zipcode
 * @property string $sch_phone
 * @property string $sch_mobile
 * @property integer $available_licenses
 * @property integer $used_licenses
 * @property integer $purchased_licenses
 */
class School extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'schools';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    protected $primaryKey = 'schid';

    public $fillable = [
        'organization_orgid',
        'school_schid',
        'sch_name',
        'org_name',
        'school_type',
        'about',
        'no_of_teachers',
        'no_of_students',
        'sch_website',
        'facebook',
        'linkedin',
        'latitude',
        'longitude',
        'logo',
        'address1',
        'address2',
        'city',
        'state',
        'country',
        'zipcode',
        'sch_phone',
        'sch_mobile',
        'available_licenses',
        'used_licenses',
        'purchased_licenses'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'schid' => 'integer',
        'organization_orgid' => 'integer',
        'sch_name' => 'string',
        'about' => 'string',
        'no_of_teachers' => 'integer',
        'no_of_students' => 'integer',
        'sch_website' => 'string',
        'facebook' => 'string',
        'linkedin' => 'string',
        'latitude' => 'string',
        'longitude' => 'string',
        'logo' => 'string',
        'address1' => 'string',
        'address2' => 'string',
        'city' => 'string',
        'state' => 'string',
        'country' => 'string',
        'zipcode' => 'string',
        'sch_phone' => 'string',
        'sch_mobile' => 'string',
        'available_licenses' => 'integer',
        'used_licenses' => 'integer',
        'purchased_licenses' => 'integer'
    ];

    /**
     * Validation rules for create
     *
     * @var array
     */
    public static $rules = [
        'organization_orgid' => 'nullable',
        'sch_name' => 'required|string|max:100',
        'about' => 'nullable|string',
        'no_of_teachers' => 'nullable|integer|min:1',
        'no_of_students' => 'nullable|integer|min:1',
        'sch_website' => 'nullable|string|url|max:255',
        'facebook' => 'nullable|string|url|max:255',
        'linkedin' => 'nullable|string|url|max:255',
        'latitude' => 'nullable|string|max:255',
        'longitude' => 'nullable|string|max:255',
        'logo' => 'nullable|string|max:100',
        'address1' => 'nullable|string|max:255',
        'address2' => 'nullable|string|max:255',
        'city' => 'required|string|max:100',
        'state' => 'required|string|max:100',
        'country' => 'required|string|max:100',
        'zipcode' => 'nullable|string|max:20',
        'sch_phone' => 'required|string|max:20',
        'sch_mobile' => 'nullable|numeric|digits_between:9,11',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
//        'available_licenses' => 'required|integer',
//        'used_licenses' => 'required|integer',
//        'purchased_licenses' => 'required|integer',
        'deleted_at' => 'nullable'
    ];


     /**
     * Validation rules for update
     *
     * @var array
     */
    public static $update_rules = [
        'organization_orgid' => 'nullable',
        'sch_name' => 'required|string|max:255',
        'about' => 'nullable|string',
        'no_of_teachers' => 'nullable|integer',
        'no_of_students' => 'nullable|integer',
        'sch_website' => 'nullable|string|max:255',
        'facebook' => 'nullable|string|max:255',
        'linkedin' => 'nullable|string|max:255',
        'latitude' => 'nullable|string|max:255',
        'longitude' => 'nullable|string|max:255',
        'logo' => 'nullable|string|max:100',
        'address1' => 'nullable|string|max:255',
        'address2' => 'nullable|string|max:255',
        'city' => 'nullable|string|max:100',
        'state' => 'nullable|string|max:100',
        'country' => 'nullable|string|max:100',
        'zipcode' => 'nullable|string|max:20',
        'sch_phone' => 'nullable|string|max:20',
        'sch_mobile' => 'nullable|string|max:20',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'available_licenses' => 'nullable|integer',
        'used_licenses' => 'nullable|integer',
        'purchased_licenses' => 'nullable|integer',
        'deleted_at' => 'nullable'
    ];


}
