<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Person extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'Person';

    protected $fillable = [];
}
