<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Order
 * @package App\Models
 * @version November 22, 2021, 6:32 pm UTC
 *
 * @property integer $user_id
 * @property integer $product_prodid
 * @property integer $licences
 * @property number $price
 * @property string $paypal_orderid
 * @property string $status
 * @property boolean $paid
 * @property boolean $captured
 */
class Order extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'orders';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    protected $primaryKey = 'ordid';

    public $fillable = [
        'user_id',
        'product_prodid',
        'licences',
        'licences_available',
        'price',
        'paypal_orderid',
        'paypal_captureid',
        'status',
        'paid',
        'temp',
        'school_schid',
        'expiry_date',
        'captured'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ordid' => 'integer',
        'user_id' => 'integer',
        'product_prodid' => 'integer',
        'licences' => 'integer',
        'price' => 'float',
        'paypal_orderid' => 'string',
        'status' => 'string',
        'paid' => 'boolean',
        'captured' => 'boolean',
        'school_schid'=> 'integer',
        'expiry_date'=> 'datetime',
    ];

    /**
     * Validation rules for create
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'product_prodid' => 'required|integer',
        'licences' => 'required',
        'price' => 'required|numeric',
        'paypal_orderid' => 'required|string|max:100',
        'status' => 'required|string|max:50',
        'paid' => 'nullable|boolean',
        'captured' => 'nullable|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];


     /**
     * Validation rules for update
     *
     * @var array
     */
    public static $update_rules = [
        'user_id' => 'required',
        'product_prodid' => 'required|integer',
        'licences' => 'required',
        'price' => 'required|numeric',
        'paypal_orderid' => 'required|string|max:100',
        'status' => 'required|string|max:50',
        'paid' => 'nullable|boolean',
        'captured' => 'nullable|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id','id');
    }

    public function product()
    {
        return $this->belongsTo(\App\Models\Product::class, 'product_prodid','prodid');
    }

}
