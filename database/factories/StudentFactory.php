<?php

namespace Database\Factories;

use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'organization_orgid' => $this->faker->word,
        'school_schid' => $this->faker->word,
        'username' => $this->faker->word,
        'firstname' => $this->faker->word,
        'lastname' => $this->faker->word,
        'name' => $this->faker->word,
        'type' => $this->faker->word,
        'password' => $this->faker->word,
        'email' => $this->faker->word,
        'drole' => $this->faker->word,
        'grade' => $this->faker->word,
        'phone_no' => $this->faker->word,
        'user_mobile' => $this->faker->word,
        'address' => $this->faker->word,
        'country' => $this->faker->word,
        'country_code' => $this->faker->word,
        'purchaseflag' => $this->faker->word,
        'gender' => $this->faker->word,
        'avatar' => $this->faker->word,
        'alternate_address' => $this->faker->word,
        'city' => $this->faker->word,
        'state' => $this->faker->word,
        'zipcode' => $this->faker->word,
        'designation' => $this->faker->word,
        'alternate_email' => $this->faker->word,
        'facebook_profile' => $this->faker->word,
        'linkedIn_profile' => $this->faker->word,
        'email_verified_at' => $this->faker->date('Y-m-d H:i:s'),
        'remember_token' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'dob' => $this->faker->word,
        'last_loggedin' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s'),
        'account_expiry_date' => $this->faker->word,
        'account_status' => $this->faker->word,
        'deallocated' => $this->faker->word,
        'account_expired' => $this->faker->word
        ];
    }
}
