<?php

namespace Database\Factories;

use App\Models\Bugs;
use Illuminate\Database\Eloquent\Factories\Factory;

class BugsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Bugs::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word,
        'description' => $this->faker->text,
        'image' => $this->faker->word,
        'link' => $this->faker->word,
        'status' => $this->faker->word,
        'created_by`' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
