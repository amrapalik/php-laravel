<?php

namespace Database\Factories;

use App\Models\School;
use Illuminate\Database\Eloquent\Factories\Factory;

class SchoolFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = School::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'organization_orgid' => $this->faker->word,
        'sch_name' => $this->faker->word,
        'about' => $this->faker->text,
        'no_of_teachers' => $this->faker->randomDigitNotNull,
        'no_of_students' => $this->faker->randomDigitNotNull,
        'sch_website' => $this->faker->word,
        'facebook' => $this->faker->word,
        'linkedin' => $this->faker->word,
        'latitude' => $this->faker->word,
        'longitude' => $this->faker->word,
        'logo' => $this->faker->word,
        'address1' => $this->faker->word,
        'address2' => $this->faker->word,
        'city' => $this->faker->word,
        'state' => $this->faker->word,
        'country' => $this->faker->word,
        'zipcode' => $this->faker->word,
        'sch_phone' => $this->faker->word,
        'sch_mobile' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'available_licenses' => $this->faker->randomDigitNotNull,
        'used_licenses' => $this->faker->randomDigitNotNull,
        'purchased_licenses' => $this->faker->randomDigitNotNull,
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
