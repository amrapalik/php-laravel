<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'prod_name' => $this->faker->word,
        'no_of_licenses' => $this->faker->randomDigitNotNull,
        'description' => $this->faker->word,
        'price' => $this->faker->randomDigitNotNull,
        'renew_price' => $this->faker->randomDigitNotNull,
        'user_type' => $this->faker->word,
        'max_lives' => $this->faker->randomDigitNotNull,
        'validity' => $this->faker->randomDigitNotNull,
        'currency' => $this->faker->word,
        'featured' => $this->faker->word,
        'suspended' => $this->faker->word,
        'deleted_at' => $this->faker->date('Y-m-d H:i:s'),
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
