<?php

namespace Database\Factories;

use App\Models\Organization;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrganizationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Organization::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'created_by' => $this->faker->word,
        'org_name' => $this->faker->word,
        'org_email' => $this->faker->word,
        'about' => $this->faker->text,
        'no_of_teachers' => $this->faker->randomDigitNotNull,
        'no_of_students' => $this->faker->randomDigitNotNull,
        'website' => $this->faker->word,
        'facebook' => $this->faker->word,
        'linkedin' => $this->faker->word,
        'latitude' => $this->faker->word,
        'longitude' => $this->faker->word,
        'logo' => $this->faker->word,
        'address1' => $this->faker->word,
        'address2' => $this->faker->word,
        'city' => $this->faker->word,
        'state' => $this->faker->word,
        'country' => $this->faker->word,
        'zipcode' => $this->faker->word,
        'phone' => $this->faker->word,
        'mobile' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
