<?php

namespace Database\Factories;

use App\Models\Assignment;
use Illuminate\Database\Eloquent\Factories\Factory;

class AssignmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Assignment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'asname' => $this->faker->word,
        'category' => $this->faker->word,
        'status' => $this->faker->word,
        'countryid' => $this->faker->randomDigitNotNull,
        'country_name' => $this->faker->word,
        'city_id' => $this->faker->randomDigitNotNull,
        'city_name' => $this->faker->word,
        'gender' => $this->faker->word,
        'religion' => $this->faker->word,
        'urban_rural' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
