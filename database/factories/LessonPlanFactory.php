<?php

namespace Database\Factories;

use App\Models\LessonPlan;
use Illuminate\Database\Eloquent\Factories\Factory;

class LessonPlanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LessonPlan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'class_classid' => $this->faker->word,
        'school_schid' => $this->faker->word,
        'name' => $this->faker->word,
        'grades' => $this->faker->word,
        'standards' => $this->faker->word,
        'overview' => $this->faker->text,
        'objectives' => $this->faker->text,
        'procedures' => $this->faker->word,
        'assessment' => $this->faker->word,
        'extension_activities' => $this->faker->text,
        'image' => $this->faker->word,
        'pdf' => $this->faker->word,
        'video' => $this->faker->word,
        'youtube_link' => $this->faker->word,
        'created_by' => $this->faker->word,
        'copied_from' => $this->faker->word,
        'status' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
