<?php

namespace Database\Factories;

use App\Models\License;
use Illuminate\Database\Eloquent\Factories\Factory;

class LicenseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = License::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->word,
        'school_schid' => $this->faker->randomDigitNotNull,
        'product_prodid' => $this->faker->randomDigitNotNull,
        'order_ordid' => $this->faker->word,
        'licences' => $this->faker->word,
        'licences_available' => $this->faker->randomDigitNotNull,
        'price' => $this->faker->randomDigitNotNull,
        'paypal_orderid' => $this->faker->word,
        'status' => $this->faker->word,
        'paid' => $this->faker->word,
        'expiry_date' => $this->faker->date('Y-m-d H:i:s'),
        'captured' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
