<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LessonPlanWithAssignmentId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('lesson_plans', function ($table) {
        $table->bigInteger("assignment_id")->after('lpid')->nullable();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('lesson_plans', function ($table) {
        $table->dropColumn("assignment_id");
    	});
    }
}
