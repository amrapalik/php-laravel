<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UniversityReg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('organizations', function ($table) {
        	$table->string("org_type")->after('org_name')->default('organization');
    	});

	    Schema::table('schools', function ($table) {
        	$table->string("org_name")->before('sch_name')->nullable();
        	$table->string("school_type")->after('sch_name')->default('school');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('organizations', function ($table) {
        	$table->dropColumn("org_type");
    	});

	    Schema::table('schools', function ($table) {
        	$table->dropColumn("org_name");
        	$table->dropColumn("school_type");
    	});
    }
}
