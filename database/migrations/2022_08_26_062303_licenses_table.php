<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('licenses', function (Blueprint $table) {
            $table->integer('licences')->change();
            $table->integer('licences_available')->change();
            $table->renameColumn('licences','lives');
            $table->renameColumn('licences_available','available_lives');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('licenses', function (Blueprint $table) {
            $table->renameColumn('lives','licences');
            $table->renameColumn('available_lives','licences_available');

        });
    }
    }
