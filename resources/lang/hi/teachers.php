<?php

return array (
    'singular' => 'Teacher',
    'plural' => 'Teachers',
    'role_updated' => 'Teacher Role Updated',
    'role_cant_update' => 'Cant Update Role',
    'role_cant_update_own_role' => 'Cant Update Your Own Role',

    'fields' =>
        array (
            'id' => 'id',
            'schid' => 'Schid',
        ),
);
