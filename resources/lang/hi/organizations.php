<?php

return array (
  'singular' => 'Organization',
  'plural' => 'Organizations',
  'fields' =>
  array (
    'orgid' => 'Orgid',
    'created_by' => 'Created By',
    'org_name' => 'Name of Organization',
    'org_email' => 'Email',
    'about' => 'About your Organization',
    'no_of_teachers' => 'No Of Teachers Spread Across all schools',
    'no_of_students' => 'No Of Students Spread Across all schools',
    'website' => 'Website of Organization',
    'facebook' => 'Facebook Page',
    'linkedin' => 'Linkedin Page',
    'latitude' => 'Latitude',
    'longitude' => 'Longitude',
    'logo' => 'Logo',
    'address1' => 'Address1',
    'address2' => 'Address2',
    'city' => 'City',
    'state' => 'State',
    'country' => 'Country',
    'zipcode' => 'Zipcode',
    'phone' => 'Phone',
    'mobile' => 'Mobile',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
