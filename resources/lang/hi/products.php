<?php

return array (
  'singular' => 'Product',
  'plural' => 'Products',
    'purchase' => 'Purchase License',
  'fields' =>
  array (
    'prodid' => 'Prodid',
    'prod_name' => 'Prod Name',
    'no_of_licenses' => 'No Of Licenses',
    'description' => 'Description',
    'price' => 'Price',
    'renew_price' => 'Renew Price',
    'user_type' => 'User Type',
    'max_lives' => 'Max Lives',
    'validity' => 'Validity',
    'currency' => 'Currency',
    'featured' => 'Featured',
    'suspended' => 'Suspended',
    'deleted_at' => 'Deleted At',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
