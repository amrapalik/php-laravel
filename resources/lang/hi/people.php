<?php

return array (
  'singular' => 'Person',
  'plural' => 'People',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'username' => 'Username',
    'email' => 'Email',
    'email_verified_at' => 'Email Verified At',
    'password' => 'Password',
    'remember_token' => 'Remember Token',
    'updated_at' => 'Updated At',
  ),
);
