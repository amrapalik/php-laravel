<?php

return array (
  'singular' => 'License',
  'plural' => 'Licenses',
  'select' => 'Select',
    'amount' => 'Amount',
  'Licenses Plans'=>'Licenses Plans',
    'Page_title'=> 'Purchase License',

  'gamer_license_page_detail'=>'License plans for Gamer',
  'school_license_page_detail'=>'We have plans and prices that fit your school perfectly. Make your Student future ready with our products.',
  'fields' =>
  array (
    'licid' => 'Licid',
    'user_id' => 'User Id',
    'user' => 'User',
    'product' => 'Product',
    'date' => 'Date',
    'renew_price' => 'Renew Price',
    'school_schid' => 'School id',
    'product_prodid' => 'Product Id',
    'order_ordid' => 'Order Id',
    'licences' => 'Licences',
    'licences_available' => 'Lic. Available',
    'price' => 'Price',
    'paypal_orderid' => 'Paypal Orderid',
    'status' => 'Status',
    'paid' => 'Paid',
    'expiry_date' => 'Expiry Date',
    'captured' => 'Captured',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
