<?php

return array (
  'singular' => 'Classes',
  'plural' => 'Classes',
  'My Classes' => 'क्लास लेसन प्लान',
  'Other Teacher Classes' => 'Other Teacher Classes',
  'My Assigned Classes'=>  'My Assigned Classes',
  'My Classes with Assignment'=>  'Class with Assignment',
  'Other Teacher with Assignment'=>  'Other Teacher with Assignment',
  'My Joined Assignments'=>'My Joined Assignments',
  'My Joined Classes'=>'My Joined Classes',
  'Students of Class'=>  'Students of Class',
  'Students of Assignment'=>  'Students of Assignment',
  'Lesson Plans'=>  'Lesson Plans',

  'fields' =>
  array (
    'classid' => 'Classid',
    'letterstatus'=> 'Letter Status',
    'gamestatus'=> 'Game Status',
    'class_name' => 'Class Name',
    'class_description' => 'Class Description',
    'grade' => 'Grade',
    'division' => 'Division',
    'age_group' => 'Age Group',
    'start_date' => 'Start Date',
    'end_date' => 'End Date',
    'created_by' => 'Created By',
    'copied_from' => 'Copied From',
    'status' => 'Status',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
