<?php

return array (
    'BugReport' => 'Bug Report',
    'Feedback' => 'Feedback',
    'OrganDonation' => 'Organ Donation',
    'BloodDonation' => 'Blood Donation',
    'obituarydata' => 'Obituary',
    'letterdata' => 'Letter Data',
    'sdgComments' => 'SDG Comments',
    'BornReligion' => 'Born Religion',
    'Lives Experienced In Countries' => 'Lives lived in various countries',
    'SdgData' => 'SDG Data',
    'SDGs chosen while creating a Life' => 'SDG\'s chosen while creating a Life',
    'Organ Donated By Me While Playing RealLives' => 'Organ Donated By Me While Playing RealLives',
    'Gameplay Data' => 'Gameplay Data',
    'Lives in Progress' => 'Lives in Progress',
    'Lives Completed' => 'Lives Completed',
    'Male Lives' => 'Male Lives',
    'Female Lives' => 'Female Lives',
    'Countries Covered' => 'Countries Covered',
    'Obituaries Written' => 'Obituaries Written',
    'Letters Written' => 'Letters Written',
    'SDG Comments' => 'SDG Comments',
    'SchoolGameplay' => 'School Gameplay',
    'User Gameplay Dashboard'=>'My Gameplay Dashboard',

    'Donated' =>'Donated',
        'Not Yet Donated' => 'Not Yet Donated'


);
