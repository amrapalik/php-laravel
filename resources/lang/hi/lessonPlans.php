<?php

return array (
  'singular' => 'Lesson Plan',
  'plural' => 'Lesson Plans',
  'fields' =>
  array (
    'lpid' => 'Lpid',
    'class_classid' => 'Class Classid',
    'school_schid' => 'School Schid',
    'name' => 'Lesson Name',
    'grades' => 'Grades',
    'standards' => 'Standards',
    'overview' => 'Overview',
    'objectives' => 'Objectives',
    'procedures' => 'Procedures',
    'assessment' => 'Assessment',
    'extension_activities' => 'Extension Activities',
    'image' => 'Image',
    'pdf' => 'Pdf',
    'video' => 'Video',
    'youtube_link' => 'Youtube Link',
    'created_by' => 'Created By',
    'copied_from' => 'Copied From',
    'status' => 'Status',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
