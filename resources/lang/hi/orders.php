<?php

return array (
  'singular' => 'Order',
  'plural' => 'Orders',
  'fields' =>
  array (
    'ordid' => 'Ordid',
    'user_id' => 'User',
      'date' => 'Date',
    'product_prodid' => 'Product',
    'licences' => 'Licences',
    'price' => 'Price',
    'paypal_orderid' => 'Paypal Orderid',
    'status' => 'Status',
    'paid' => 'Paid',
    'captured' => 'Captured',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
