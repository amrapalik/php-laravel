<?php

return array (
  'singular' => 'Assignment',
  'plural' => 'Assignments',
  'fields' =>
  array (
    'asid' => 'Asid',
      'tag' => 'Tag',
      'type' => 'Type',
      'game_status' => 'Game Status',
      'letter_status' => 'Letter Status',
      'obituary_status' => 'Obituary Status',
    'asname' => 'Assignment name',
    'category' => 'Category',
    'status' => 'Status',
    'countryid' => 'Countryid',
    'country_name' => 'Country Name',
    'city_id' => 'City Id',
    'city_name' => 'City Name',
    'gender' => 'Gender',
    'religion' => 'Religion',
    'urban_rural' => 'Urban Rural',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
