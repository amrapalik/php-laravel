<?php

return array (
    'singular' => 'Registration',
    'title' => 'Registration',
    'organization' =>
        [
            'title' => 'Organization Registration',
            'heading' => 'Organization Registration',
            'subheading' => 'If you have many schools',
            'register' => 'Register',
            'description' => 'Description',
            'register_success' => 'Organization Registered successfully',
        ],

    'school' =>
        [
            'title' => 'School Registration',
            'heading' => 'School Registration',
            'subheading' => 'If you have one school',
            'register' => 'Register',
            'description' => 'Description',
            'register_success' => 'Registered successfully',
        ],

    'gamer' =>
        [
            'title' => 'Gamer Registration',
            'heading' => 'Gamer Registration',
            'subheading' => 'If you are individual gamer',
            'register' => 'Register',
            'description' => 'Description',
            'register_success' => 'Organization Registered successfully',
        ],
);
