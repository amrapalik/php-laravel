<?php

return array (
  'singular' => '조직',
  'plural' => '조직',
  'fields' =>
  array (
    'orgid' => 'Orgid',
    'created_by' =>'만든 사람',
    'org_name' =>  '조직명',
    'org_email' =>  '이메일',
    'about' =>  '조직 정보',
    'no_of_teachers' =>  '모든 학교에 퍼져있는 교사의 수',
    'no_of_students' =>  '전교에 분포된 학생 수',
    'website' =>  '조직의 웹사이트',
    'website_help' => 'Ex. http://reallive.com',
    'facebook' =>  '페이스북 페이지',
    'facebook_help' => 'Ex. http://facebook',
    'linkedin' =>  '링크드인 페이지',
    'linkedin_help' => 'Ex. http://linkdin',
    'latitude' =>  '위도',
    'longitude' =>  '경도',
    'logo' =>  '심벌 마크',
    'address1' =>  '주소 1',
    'address2' =>  '주소 2',
    'city' =>  '도시',
    'state' =>  '상태',
    'country' =>  '국가',
    'zipcode' =>  '우편 번호',
    'phone' =>  '핸드폰',
    'mobile' =>  '이동하는',
    'created_at' =>  '만든 시간',
    'updated_at' =>  '업데이트 시간',
    'deleted_at' =>  '삭제 시간',

  ),
);
