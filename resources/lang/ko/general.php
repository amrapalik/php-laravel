<?php

return array (
    


    'My Account' =>'내 계정',
    'Edit Avatar' =>  '아바타 편집',
    'Logout' =>  '로그 아웃',
    'allocated' =>  '할당',
    'dallocated' =>  '할당',
    'Feedback' =>  '피드백',
    'Feedback' =>  '피드백',
    'Feedback' =>  '피드백',
    'Add new' =>'새로운 걸 더하다',
    'Add' =>'추가하다',
    'Buy license' =>'라이센스 구매',
    'familydata' => '가족 게임 플레이 데이터',
    'Change Department'=>'부서 변경',
    'Create'=>'만들다 ',
    'Edit'=>'편집하다 ',
    'Show'=>'보여 주다 ',
    'Edit My Account'=>'내 계정 수정',
    'Upload Image'=>'이미지 업로드',
    'Click on'=>'클릭',
    'family'=>'가족',
    
    'Change school'=>'학교를 바꾸다',
    'Profile Updated Successfully'=>'프로필이 업데이트되었습니다!',
    'Upload Your Profile Image'=>'프로필 이미지 업로드',
    'Upload Your School Logo'=>'학교 로고 업로드',
    'Upload'=>'학교 로고 업로드',
    'Choose a data tool'=>'데이터 도구 선택',

    'Choose a data tool'=>'RealLives 세계 데이터 학습 도구',
    'Description' => '설명',
    'Data Learning Tool'=>array(
        'Experience'=>'경험',
        'World Data + SDG Tools'=>'세계 데이터 + SDG 도구',
        "You can 'live a life' according to the selected tool"=>"선택한 도구에 따라 '인생'을 할 수 있습니다.",
        'Data Tools'=>'데이터 도구',
        'SDG tool title'=>"UN의 지속 가능한 개발 목표(SDGs) 도구",
        'Country Disparity tool title'=>"국가 비교 학습 도구 및 데이터 시각화",
        'Compare Country Data'=>"국가 데이터 비교",
        'Country Groups Comparison'=>"국가 그룹 비교",

        'Learn SDG status of Countries'=>"1. 국가의 SDG 현황을 알아봅니다.",
        'Compare SDG country score and rank'=>"2. SDG 국가 점수와 순위를 비교합니다.",
        'SDG country comparative statement generator'=>"3. SDG 국가 비교 명세서 생성기.",

        'Data Disparity Tool Desc'=>"두 국가를 비교하여 데이터 시각화 및 데이터에 의해 생성된 동적 진술의 도움으로 사회 경제적, 건강 및 인구 통계학적 데이터의 차이를 이해합니다.",
        'Compare Country Data Desc'=>"국가 데이터를 Lorenz 곡선, 부서진 사다리 척도, SDG 과제 및 국가 그룹과 비교하기 위한 완벽한 도구입니다.",
        'Country Groups Comparison Desc'=>"100개 이상의 국가 그룹을 경험하세요. 국가 그룹 내 국가 간 격차에 대해 알아보세요.",
        
    )



);
