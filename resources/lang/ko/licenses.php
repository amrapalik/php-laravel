<?php

return array (
  'Active Licenses of'=>'활성 라이선스 ',
  'account innactive please contact your teacher'=>'계정이 비활성화되어 있습니다. 계정을 활성화하려면 선생님에게 문의하세요.',
    'Page_title'=> 'Purchase License',
  'to renew the license' => "라이센스를 갱신합니다. 오른쪽으로 스크롤하면 찾을 수 있습니다.",
    'renew_page_title'=>'라이선스 갱신',
    'Purchase school licenses to play game'=>'RealLives를 플레이하려면 하나 이상의 학교 라이선스를 구입하십시오.',

    'under Action tab'=>'작업 탭 아래',

  'singular' =>'특허',
  'plural' =>  '라이센스',
  'select' =>  '고르다',
    'amount' =>  '양',
  'Licenses Plans'=>'라이선스 계획',
    'Page_title'=>  '구매 라이선스',
    'Year Validity'=>  '연도 유효 기간',
  'On Renewal' =>  '갱신 후',
    'Active Licenses' =>  '활성 라이선스',
  'License management'=>'라이센스 관리',
  'Active Account'=>'활성 계정',
  'License Not Purchased'=>'라이선스를 구매하지 않음',
  'gamer_license_page_detail'=>'게이머를 위한 라이선스 계획',
  'school_license_page_detail'=>'당신의 학교에 딱 맞는 플랜과 가격이 있습니다. 우리 제품으로 학생의 미래를 준비하십시오.',

  'fields' =>
  array (
    'licid' => 'Licid',
    'user_id' => 'User Id',
    'user' =>'사용자',
    'product' =>  '제품',
    'date' =>  '날짜',
    'renew_price' =>  '리뉴 가격',
    'school_schid' =>  '학교 아이디',
    'product_prodid' =>  '제품 ID',
    'order_ordid' =>  '주문 아이디',
    'licences' =>  '라이센스',
    'licences_available' =>  '릭. 사용 가능',
    'price' =>  '가격',
    'paypal_orderid' =>  '페이팔 주문 ID',
    'status' =>  '상태',
    'paid' =>  '유급의',
    'expiry_date' =>  '만료일',
    'captured' =>  '캡처',
    'created_at' =>  '만든 시간',
    'updated_at' =>  '업데이트 시간',
    'deleted_at' =>  '삭제 시간',

  ),
);
