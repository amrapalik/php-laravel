<?php

return array (
  'singular' => '사람',
  'plural' => '사람들',
  'fields' => 
  array (
    'id' => 'Id',
    
    'name' =>'이름',
    'username' =>  '사용자 이름',
    'email' =>  '이메일',
    'email_verified_at' =>  '확인된 이메일 주소',
    'password' =>  '비밀번호',
    'remember_token' =>  '리멤버 토큰',
    'updated_at' =>  '업데이트 시간',

  ),
);
