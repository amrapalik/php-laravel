<?php

return array (
    'dashboard' => '내 게임 대시보드',
    'gamedashboard' => '게임 대시보드',

    'fields' =>
        array (
            
            'classid' =>'클래식',
            'letterstatus'=>  '문자 상태',
            'gamestatus'=>  '게임 상태',
            'class_name' =>  '클래스 이름',
            'class_description' =>  '클래스 설명',
            'grade' =>  '등급',
            'division' =>  '분할',
            'age_group' =>  '연령대',
            'start_date' =>  '시작일',
            'end_date' =>  '종료일',
            'created_by' =>  '만든 사람',
            'copied_from' =>  '에서 복사',
            'status' =>  '상태',
            'created_at' =>  '만든 시간',
            'updated_at' =>  '업데이트 시간',
            'deleted_at' =>  '삭제 시간',

        ),
);
