<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute must only contain letters.',
    'alpha_dash' => 'The :attribute must only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'The :attribute must only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',

    'accepted' =>':attribute 는 허용되어야 합니다.',
    'active_url' =>  ':attribute 은(는) 유효한 URL이 아닙니다.',
    'after' =>  ':attribute 는 :date 이후의 날짜여야 합니다.',
    'after_or_equal' =>  ':attribute는 날짜가 :date 이후이거나 같아야 합니다.',
    'alpha' =>  ':attribute에는 문자만 포함되어야 합니다.',
    'alpha_dash' =>  ':attribute에는 문자, 숫자, 대시 및 밑줄만 포함되어야 합니다.',
    'alpha_num' =>  ':attribute에는 문자와 숫자만 포함되어야 합니다.',
    'array' =>  ':attribute는 배열이어야 합니다.',
    'before' =>  ' :attribute는 :date 이전의 날짜여야 합니다.',
    'before_or_equal' =>  ':attribute는 :date 이전의 날짜이거나 같아야 합니다.',

    'between' => [
        
        'numeric' =>':attribute는 :min과 :max 사이여야 합니다.',
        'file' =>  ':attribute는 :min과 :max 킬로바이트 사이여야 합니다.',
        'string' =>  ':attribute는 :min과 :max 사이에 있어야 합니다.',
        'array' =>  ':attribute에는 :min과 :max 항목 사이에 있어야 합니다.',

    ],
    
    'boolean' =>':attribute 필드는 true 또는 false여야 합니다.',
    'confirmed' =>  ':attribute 확인이 일치하지 않습니다.',
    'date' =>  ':attribute은(는) 유효한 날짜가 아닙니다.',
    'date_equals' =>  ':attribute는 날짜가 :date와 같아야 합니다.',
    'date_format' =>  ':attribute가 형식:format과 일치하지 않습니다.',
    'different' =>  ' :attribute 및 :other는 달라야 합니다.',
    'digits' =>  ' :attribute는 :digits 숫자여야 합니다.',
    'digits_between' =>  ':attribute는 :min과 :max 사이에 있어야 합니다.',
    'dimensions' =>  ':attribute의 이미지 크기가 잘못되었습니다.',
    'distinct' =>  ':attribute 필드에 중복 값이 있습니다.',
    'email' =>  ':attribute는 유효한 이메일 주소여야 합니다.',
    'ends_with' =>  ':attribute는 다음 중 하나로 끝나야 합니다. :values.',
    'exists' =>  '선택한 :속성이 잘못되었습니다.',
    'file' =>  ':attribute는 파일이어야 합니다.',
    'filled' =>  ':attribute 필드에는 값이 있어야 합니다.',

    'gt' => [
        
        'numeric' =>':attribute는 :value보다 커야 합니다.',
        'file' =>  ':attribute는:value 킬로바이트보다 커야 합니다 (KB).',
        'string' =>  ':attribute는 :value 문자보다 커야 합니다.',
        'array' =>  ':attribute에는 다음보다 많은 :value 항목이 있어야 합니다.',

    ],
    'gte' => [
        
         'numeric' =>':attribute는 :value보다 크거나 같아야 합니다.',
        'file' =>  ' :attribute는 :value KB보다 크거나 같아야 합니다 (KB).',
        'string' =>  ':attribute는 :value 문자보다 크거나 같아야 합니다.',
        'array' =>  ':attribute에는 :value 항목 이상이 있어야 합니다.',

    ],
    

    'image' =>':attribute는 이미지여야 합니다.',
    'in' =>  '선택한 :속성이 잘못되었습니다.',
    'in_array' =>  ':attribute 필드가 :other에 존재하지 않습니다.',
    'integer' =>  ':attribute는 정수여야 합니다.',
    'ip' =>  ':attribute는 유효한 IP 주소여야 합니다.',
    'ipv4' =>  ':attribute는 유효한 IPv4 주소여야 합니다.',
    'ipv6' =>  ':attribute는 유효한 IPv6 주소여야 합니다.',
    'json' =>  ':attribute는 유효한 JSON 문자열이어야 합니다.',

    'lt' => [
        
        'numeric' =>':attribute는:value보다 작아야 합니다.',
        'file' =>  ':attribute는 :value KB(킬로바이트)보다 작아야 합니다.',
        'string' =>  ':attribute는 :value 문자보다 작아야 합니다.',
        'array' =>  ':attribute에는 :value보다 작은 항목이 있어야 합니다.',

    ],
    'lte' => [
        
        'numeric' =>':attribute는:value보다 작거나 같아야 합니다.',
        'file' =>  ':attribute는 :value KB(킬로바이트)보다 작거나 같아야 합니다.',
        'string' =>  ':attribute는 :value 문자보다 작거나 같아야 합니다.',
        'array' =>  ':attribute에는 :value 항목보다 많을 수 없습니다.',

    ],
    'max' => [
       
        'numeric' =>' :attribute는 :max보다 클 수 없습니다.',
        'file' =>  ':attribute는 최대 KB(킬로바이트)보다 커서는 안 됩니다.',
        'string' =>  ' :attribute는 :max 문자보다 커서는 안 됩니다.',
        'array' =>  ':attribute에는 최대 :max 항목이 없어야 합니다.',

    ],
    'mimes' =>':attribute는::values 유형의 파일이어야 합니다.',
    'mimetypes' =>  ':attribute는::values 유형의 파일이어야 합니다.',

    
    'min' => [
       
        'numeric' =>':attribute는 최소한 :min이어야 합니다.',
        'file' =>  ':attribute는 최소 :min 킬로바이트(KB) 이상이어야 합니다.',
        'string' =>  ':attribute는 최소한 :min 문자여야 합니다.',
        'array' =>  ':attribute에는 최소한 :min 항목이 있어야 합니다.',

    ],
    
    
    'multiple_of' =>':attribute는:value의 배수여야 합니다.',
    'not_in' =>  '선택한 :속성이 잘못되었습니다.',
    'not_regex' =>  ':attribute 형식이 잘못되었습니다.',
    'numeric' =>  ':attribute는 숫자여야 합니다.',
    'password' =>  '비밀번호가 틀립니다.',
    'present' =>  ':attribute 필드가 있어야 합니다.',
    'regex' =>  ':attribute 형식이 잘못되었습니다.',
    'required' =>  ':attribute 필드는 필수입니다.',
    'required_if' =>  ' :other가 :value일 때 :attribute 필드가 필요합니다.',
    'required_unless' =>  ':other가 :values에 있지 않으면 :attribute 필드가 필요합니다.',
    'required_with' =>  ' :values가 있는 경우 :attribute 필드가 필요합니다.',
    'required_with_all' =>  ' :values가 있는 경우 :attribute 필드가 필요합니다.',
    'required_without' =>  ':values가 없으면 :attribute 필드가 필요합니다.',
    'required_without_all' =>  ':values가 없을 때 :attribute 필드가 필요합니다.',
    'prohibited' =>  ':attribute 필드는 금지되어 있습니다.',
    'prohibited_if' =>  ':other가 :value일 때 :attribute 필드는 금지됩니다.',
    'prohibited_unless' =>  ':other가 :values에 있지 않으면 :attribute 필드가 금지됩니다.',
    'same' => ':attribute 및 :other가 일치해야 합니다..',

    'size' => [
    
        'numeric' =>':attribute는:size여야 합니다.',
        'file' =>  ':attribute는:size 킬로바이트여야 합니다.',
        'string' =>  ' :attribute는 :size 문자여야 합니다.',
        'array' =>  ':attribute에는 :size 항목이 포함되어야 합니다.',

    ],

    
    'starts_with' =>':attribute는 다음 중 하나로 시작해야 합니다. :values.',
    'string' =>  ':attribute는 문자열이어야 합니다.',
    'timezone' =>  ':attribute는 유효한 영역이어야 합니다.',
    'unique' =>  ':attribute는 이미 사용 중입니다.',
    'uploaded' =>  ':attribute를 업로드하지 못했습니다.',
    'url' =>  ':attribute 형식이 잘못되었습니다.',
    'uuid' =>  ':attribute는 유효한 UUID여야 합니다.',


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
