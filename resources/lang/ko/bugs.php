<?php

return array (
  
  'singular' =>'피드백/버그 신고',
  'plural' =>  '피드백/버그 신고',
  'Submit Bug' =>  '피드백/버그 신고',
  'Bug Submitted' =>  '성공적으로 제출했습니다',

  'fields' =>
  array (

    'bugid' =>'부기드',
    'title' =>  '제목',
    'description' =>  '설명',
    'type'=>  '피드백/버그 신고',
    'image1' =>  '영상',
    'image' => '이미지(피드백/버그가 보고된 게임 페이지의 스크린샷)',
    'link' =>  '링크',
    'status' =>  '상태',
    'created_by`' =>  '만든 사람',
    'created_at' =>  '만든 시간',
    'updated_at' =>  '업데이트 시간',
    'deleted_at' =>  '삭제 시간',

  ),
);
