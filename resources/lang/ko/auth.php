<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'failed'   =>'이 자격 증명은 우리 기록과 일치하지 않습니다.',
    'throttle' =>  ' 로그인 시도 횟수가 너무 많습니다. :초 초 후에 다시 시도하십시오.',

    'full_name'        =>  '이름',
    'email'            =>  '이메일',
    'password'         =>  '비밀번호',
    'confirm_password' =>  '비밀번호 확인',
    'remember_me'      =>  '날 기억해',
    'sign_in'          =>  '로그인',
    'sign_out'         =>  '로그아웃',
    'register'         =>  '등록하다',



    'login' => [
        'title'               =>'세션을 시작하려면 로그인하세요',
        'forgot_password'     =>  '비밀번호를 잊어버렸어요',
        'register_membership' =>  '신규 회원가입',

    ],

    'registration' => [

        'title'           =>'신규 회원가입',
        'i_agree'         =>  '나는 동의한다',
        'terms'           =>  '조건',
        'have_membership' =>  '이미 멤버십이 있습니다',

    ],

    'forgot_password' => [
        
        'title'          =>'비밀번호를 재설정하려면 이메일을 입력하세요',
        'send_pwd_reset' =>  '비밀번호 재설정 링크 보내기',

    ],

    'reset_password' => [
        'title'         =>'비밀번호를 재설정',
        'reset_pwd_btn' =>  '암호를 재설정',

    ],

    'confirm_passwords' => [
        'title'                =>'계속하기 전에 비밀번호를 확인하세요.',
        'forgot_your_password' =>  '비밀번호를 잊어 버렸습니까?',

    ],

    'verify_email' => [
        
        'title'       =>'이메일 주소를 확인',
        'success'     =>  '새로운 인증 링크가 귀하의 이메일 주소로 전송되었습니다',
        'notice'      =>  '진행하기 전에 이메일에서 인증 링크를 확인하세요. 이메일을 받지 못하셨다면',
        'another_req' =>  '다른 것을 요청하려면 여기를 클릭하세요',

    ],

    'emails' => [
        'password' => [
            'reset_link' =>'비밀번호를 재설정하려면 여기를 클릭하세요',
        ],
    ],

    'app' => [
        
        'member_since' =>'회원 가입일',
        'messages'     =>  '메시지',
        'settings'     =>  '설정',
        'lock_account' =>  '계정 잠금',
        'profile'      =>  '프로필',
        'online'       =>  '온라인',
        'search'       =>  '검색',
        'create'       =>  '만들다',
        'export'       =>  '내 보내다',
        'print'        =>  '인쇄',
        'reset'        =>  '초기화',
        'reload'       =>  '리로드',

    ],
];
