<?php

return array (
  'singular' => '과제',
  'plural' => '과제',
  'play assignment'=>    '임무를 수행하고 그곳에서 인생을 살아라',
  'load assignment'=>    '계속 할당',
  'complete assignment'=>    '완료된 과제 보기',
  'You are deallocated can not play assignment'=>    '할당이 해제되어 과제를 재생할 수 없습니다.',
'show assignment'=> '과제를 보여주다',
'select an Assignment' => '"+"를 클릭하여 과제를 선택합니다.',
'choose a subject' => '과제를 만들 주제 선택:',

  'fields' =>
  array (
    

    'tag' =>'꼬리표',
      'type' =>  '유형',
      'game_status' =>  '게임 상태',
      'letter_status' =>  '문자 상태',
      'obituary_status' =>  '부고 상태',
    'asname' =>  '과제 이름',
    'category' =>  '범주',
    'status' =>  '상태',
    'countryid' =>  '컨트리아이드',
    'country_name' =>  '나라 이름',
    'city_id' =>  '시티 아이디',
    'city_name' =>  '도시 이름',
    'gender' =>  '성별',
    'religion' =>  '종교',
    'urban_rural' =>  '도시 농촌',
    'created_at' =>  '만든 시간',
    'updated_at' =>  '업데이트 시간',
    'deleted_at' =>  '삭제 시간'

  ),
);
