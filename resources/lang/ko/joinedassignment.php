<?php

return array (
    'singular' => '과제',
    'plural' => '과제',
    'fields' =>
        array (
            
            'asid' =>'아시드',
            'tag' =>  '꼬리표',
            'type' =>  '유형',
            'game_status' =>  '게임 상태',
            'letter_status' =>  '문자 상태',
            'obituary_status' =>  '부고 상태',
            'asname' =>  '과제 이름',
            'category' =>  '범주',
            'status' =>  '상태',
            'countryid' =>  '컨트리아이드',
            'country_name' =>  '나라 이름',
            'city_id' =>  '시티 아이디',
            'city_name' =>  '도시 이름',
            'gender' =>  '성별',
            'religion' =>  '종교',
            'urban_rural' =>  '도시 농촌',
            'created_at' =>  '만든 시간',
            'updated_at' =>  '업데이트 시간',
            'deleted_at' =>  '삭제 시간',

        ),
);
