<?php

return array (
    'singular' => '선생님',

    'plural' => '교사',
    'Teachers of' => "RealLives @ 선생님들 ",
    'Parents' => "부모님",
    'Family Members' => "부모님",
    'Add a Parent' => "부모 추가",
    'Family Member' => "가족 구성원 만들기",
    'Faculty List' => '교수진 목록',
    'Import Faculty' => '임포트 교수진',
    'Faculties at' => "학부 @ ",
    'Add a faculty member' => "교직원 추가",
    'Faculty Member' => "교수진 만들기",
    'Total teachers' =>'총교사',
    'Import Faculty Members of' => "의 교수진 가져오기 ",
    'Import Faculty Members' => "교수진 가져오기",
    'Show teachers' => '선생님을 보여주세요',
    'create teacher' => '선생님 만들기',
    'freeLicense'=>"교사의 라이센스는 무료로 제공됩니다. 라이선스를 소비하지 않습니다.",
    'role_updated' => '교사 역할 업데이트',
    'role_cant_update' => '역할을 업데이트할 수 없음',
    'teacher Created successfully' => '선생님이 성공적으로 생성되었습니다',
    'role_cant_update_own_role' => '자신의 역할을 업데이트할 수 없음',
    'Add new teacher' => '새 선생님 추가',
    'Hand icon to change role Dept admin <-> Faculty member.'=>'역할을 변경하는 손 아이콘 부서 관리자 <-> 교수진 구성원.',
    'Hand icon to change role School admin <-> Teacher.'=>'(손 모양 아이콘) 역할 변경: 학교 관리자 <-> 교사. 여러 관리자를 설정할 수 있습니다',
    'freeLicense'=>"교사의 라이센스는 무료로 제공됩니다. 라이선스를 소비하지 않습니다.",
    'Import Teachers for' => '의 교사 가져오기 ',
    'freeLicense2'=>"교수의 라이선스는 무료로 제공됩니다. 라이선스를 소비하지 않습니다.",

    'Click on'=>'클릭',
    'Import Teachers' => '가져오기 선생님',
    'Import Teachers Info' => '가져오기 선생님',
    'teachers excel' =>'교사 엑셀 파일',
    'Download Sample Excel'=>'샘플 엑셀 다운로드',
    'Teachers import page heading'=>'샘플 엑셀파일을 다운받아 선생님 정보를 작성해주세요. 엑셀 시트 작성 후',
    'Note'=>'메모',
    'Teacher Import instructions'=>"샘플 엑셀 파일을 다운로드하고 제목에 표시된 대로 필요한 정보를 입력합니다. 그런 다음 저장합니다. 파일을 저장하지 않고 컴퓨터에서 열지 않으면 파일을 업로드할 수 없습니다. 파일을 컴퓨터에 저장했으면 이 페이지로 돌아와 'Browse' 버튼을 클릭하여 해당 파일을 선택하십시오. 학생/교사 샘플 파일 선택 후 확인을 클릭합니다. 파일을 업로드하는 데 최대 5분이 소요될 수 있습니다. 이 시간 동안 브라우저를 닫거나 새로 고치지 마십시오. 파일이 성공적으로 업로드되면 '파일이 성공적으로 업로드되었습니다'라는 메시지가 표시됩니다. '학생' 또는 '교사' 탭으로 이동하여 모든 학생과 교사가 나열되어 있는지 확인합니다. 중복 항목이 있는 경우 라이선스 수를 절약하기 위해 할당을 해제해야 합니다. 현재 시스템에서는 교사와 학생을 삭제할 수 없습니다. 그러나 할당을 취소할 수 있습니다.",

    'fields' =>
        array (
            'id' => 'id',
            'schid' => 'Schid',
        ),
);
