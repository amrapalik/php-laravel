<?php

return array (
  'singular' => '주문',
  'plural' => '주문',
  'please select a school' => '먼저 학교를 선택하세요.',
  'please select a department' => '먼저 부서를 선택하세요.',
  'Orders for' => '다음에 대한 주문 상태 ',

  'fields' =>
  array (
    'ordid' => 'Ordid',
    'user_id' =>'사용자',
      'date' =>  '날짜',
    'product_prodid' =>  '제품',
    'licences' =>  '라이센스',
    'price' =>  '가격',
    'paypal_orderid' =>  '페이팔 주문 ID',
    'status' =>  '상태',
    'paid' =>'유급의',
    'captured' =>  '캡처',
    'created_at' =>  '만든 시간',
    'updated_at' =>  '업데이트 시간',
    'deleted_at' =>  '삭제 시간',

  ),
);
