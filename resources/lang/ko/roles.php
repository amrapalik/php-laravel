<?php

return array (
  'singular' => '역할',
  'plural' => '역할',
  'fields' => 
  array (
    'id' => 'Id',
    'name' =>'이름',
    'guard_name' =>  '경호원 이름',
    'created_at' =>  '만든 시간',
    'updated_at' =>  '업데이트 시간',
    'deleted_at' =>  '삭제 시간',

  ),
);
