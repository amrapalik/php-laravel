<?php

return array (



    'BugReport' =>'버그 리포트',
    'Bugs Submitted By Me' => '내가 제출한 버그',
    'Feedback' =>  '피드백',
    'OrganDonation' =>  '장기 기증',
    'BloodDonation' =>  '헌혈',
    'obituarydata' =>  '사망 기사',
    'letterdata' => '문자로 보내는 편지',
    'Blood Bottles Donated By Me' => '내가 기증한 혈액병',
    'Vital Organs Donated By Me' => '내가 기증한 중요한 장기',
    'departmemntGameplay' => '부서 게임 플레이 데이터',
    "SDG's chosen while creating a Life" => "삶을 창조하면서 선택한 SDG",
    'SDGs chosen while creating a Life School' => '삶을 창조하면서 선택한 SDG',
    'SDGs chosen while creating a Life Family' => '당신의 가족이 삶을 창조하면서 선택한 SDG',
    'by family members' => '가족 구성원에 의해',

    'Letter Written To Your Characters' => '당신이 당신의 캐릭터에게 쓴 편지',
    'sdgComments' =>  'SDG 의견',
    'familydata' => '가족 게임 플레이 데이터',
    'Lives Experienced In Countries School' => '다양한 나라에서 살아온 삶',
    'BornReligion' =>  '태어난 종교',
    'Lives Experienced In Countries' =>  '다양한 나라에서 살아온 삶',
    'SdgData' =>  'SDG 데이터',
    'SDGs chosen while creating a Life' =>  '삶을 창조하면서 선택한 "SDG\'s',
    'Organ Donated By Me While Playing RealLives' =>  '내가 기증한 장기',
    'Gameplay Data' =>  '게임 플레이 데이터',
    'My Gameplay Data' =>  '내 게임 플레이 데이터',
    'My Gameplay Data Inner' =>  '내 게임 플레이 데이터',
    'by students & teachers'=>'학생과 교사에 의한',
    'familydata of' => '게임 플레이 데이터 ',
    'family' => ' 가족',

    'School Gameplay Data' =>  '학교 게임 플레이 데이터',
    'School Gameplay Data of' => '학교 게임 플레이 데이터 ',
    'Lives in Progress' =>  '진행 중인 삶',
    'Lives Completed' =>  '완성된 삶',
    'Male Lives' =>  '남자의 삶',
    'Female Lives' =>  '여성의 삶',
    'Countries Covered' =>  '대상 국가',
    'Obituaries Written' =>  '기록된 부고',
    'Letters Written' =>  '쓰는 편지',
    'SDG Comments' =>  'SDG 의견',
    'SchoolGameplay' =>  '학교 게임 플레이 데이터',
    'User Gameplay Dashboard'=>'내 게임 플레이 대시보드',
    'School Gameplay Dashboard'=>'학교 게임 플레이 대시보드',
    'Donated' =>'기부',
    'Not Yet Donated' =>  '아직 기부되지 않음',

);
