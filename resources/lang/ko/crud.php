<?php

return [


    'add_new'      =>'새로운 걸 더하다',
    'cancel'       =>  '취소',
    'save'         =>  '구하다',
    'edit'         =>  '편집하다',
    'detail'       =>  '세부 사항',
    'back'         =>  '뒤',
    'action'       =>  '동작',
    'id'           =>  'ID',
    'created_at'   =>  '만든 시간',
    'updated_at'   =>  '업데이트 시간',
    'deleted_at'   =>  '삭제 시간',
    'are_you_sure' =>  '확실해?',

];
