<?php

return array (
  

  'singular' =>'클래스',
  'plural' =>  '클래스',
  'classManagement'=>'수업관리',
  'Create a Class' => '클래스 만들기',
  'Give An Assignment To Your Children' => '자녀에게 과제를 주십시오',

  'Action'=>  '"동작"',
  'My Joined Assignments'=>'내 조인 과제 수업',
  'Only allocated students can be seen below'=>'배정된 학생만 아래에서 볼 수 있습니다.',
  'Search Selectable'=>'검색 선택 가능',
  'Create an Assignment'=>'과제 만들기',
  'Other Teacher Classes with Assignment + Lesson Plan' => '과제 + 수업 계획이 있는 기타 교사 수업',
  'Other Teacher Classes with Assignment'=>'글로벌 시민 과제가 있는 기타 교사 수업',
  'to check the status of given assignments'=>  '참가자에게 주어진 과제의 상태를 확인하기 위해',
  'My Classes' =>  '수업계획이 있는 수업',
  'Global Citizenship Assignment'=>'글로벌 시민권 할당',
  'Other Teacher Classes' =>  '다른 교사 수업',
  'My Assigned Classes'=>   '나에게 주어진 수업',
  'My Classes with Assignment'=>   '글로벌 시민의식 과제가 있는 수업',
  'Other Teacher with Assignment'=>  '과제 있는 다른 선생님',
  'My joined assignments classes'=>'내가 참여한 과제',
  'Attach Lesson Plan + Assignment'=>'클래스를 만들기 위해 가져오기',
  'My Joined Classes'=>'나의 합류 수업',
  'My joined assignments'=>'내가 참여한 과제',

  'My joined lesson plans'=>'나의 결합된 수업 계획',
  'Students of Class'=>   '반 학생들',
  'Students of Assignment'=>   '과제생',
  'My Classes with Assignment Menu'=>  '글로벌 시민권 지정 수업',
  'Class with an assignment'=>'글로벌 시민권 지정 수업',
  'Lesson Plans'=>   '수업 계획',
  'Create Classes'=>'클래스 만들기',
  'Create Class with a lesson plan'=>'수업 계획으로 수업 만들기',
  'My joined assignments classes'=>'내가 가입한 글로벌 시민권 배정 수업',
  'Create class with an assignment'=>'과제로 수업 만들기',

  'Class with a lesson plan'=>'과제 + 수업 계획이 있는 수업',
  'Create Classes'=>'클래스 생성/보기',
  'Create Class with a lesson plan'=>'내 저장된 "과제 + 수업 계획으로 이동',
  'create class instruction'=> '수업 생성은 생성된 수업을 추가된 학생 및 교사에게 보냅니다. 대시보드에서 수업에 참여하고 과제를 완료할 수 있습니다.',
  'Other Faculty Classes'=>'다른 교수진 수업',
  'class requirement family'=>'클래스를 만들려면 클래스에 추가된 할당된 자식이 하나 이상 필요합니다.',

  'class requirement'=>'수업을 만들려면 수업에 할당된 학생이 한 명 이상 추가되어야 합니다.',
    'Class with an assignment'=>'글로벌 시민권 지정 수업',
    'Lesson plans with assignment'=>'과제가 있는 수업 계획',
    'Lesson plans without assignment'=>'수업 계획 템플릿',
    'You have been added to class -'=>'당신은 수업에 추가되었습니다 - ',

  'Create'=>'만들다',
  'List of assignemtns'=>  '과제 목록',
	'List'=>'수업 목록',
  'Assignment + Lesson Plan classes'=>'과제 + 수업 계획 수업',
	'My Joined Assignment + Lesson Plan Classes'=>'내 참여 과제 + 수업 계획 수업',
  
	'My Joined Assignment + Lesson Plan'=>'내 참여 과제 + 수업 계획',
	'Assignment classes'=>'과제 수업',

  'class with an assignment'=>'과제 수업',
  'Lesson plans'=>'수업 계획',



    'fields' =>
  array (
    
    'classid' =>'클래식',
    'letterstatus'=>  '문자 상태',
    'gamestatus'=>  '게임 상태',
    'class_name' =>  '클래스 이름',
    'class_description' =>  '클래스 설명',
    'grade' =>  '등급',
    'division' =>  '분할',
    'age_group' =>  '연령대',
    'start_date' =>  '시작일',
    'end_date' =>  '종료일',
    'created_by' =>  '만든 사람',
    'copied_from' =>  '복사본',
    'status' =>  '상태',
    'created_at' =>  '만든 시간',
    'updated_at' =>  '업데이트 시간',
    'deleted_at' =>  '삭제 시간',

  ),
);
