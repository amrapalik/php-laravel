<?php

return [
    
    'retrieved' =>  ':model 성공적으로 검색되었습니다.',
    'saved'     =>  ':model 성공적으로 저장되었다.',
    'updated'   =>  ':model 성공적으로 업데이트되었습니다.',
    'deleted'   =>  ':model 성공적으로 삭제되었습니다.',
    'not_found' =>  ':model 찾을 수 없음',

];
