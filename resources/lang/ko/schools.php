<?php

return array (
  'singular' => '학교',
  'plural' => '학교',
  'Please select School first' =>'학교를 먼저 선택하세요',
  'dashboard'=>'계기반',
  'Please select a School first' => '먼저 학교를 선택하세요.',
  'Please select a Department first' => '먼저 부서를 선택하세요.',
  'License purchased'=>'라이선스 구매',
  'License available'=>'사용 가능한 라이선스',
  'School dashboard'=>'학교 대시보드',
  'Schools in your organisation'=>'조직의 학교',
  'to select a school' => '(손 모양 아이콘) 학교 선택',
  'Create a school'=>'학교 만들기',
  'add son/daughter' => '딸/아들 추가',
  'university_dept' => '대학교 부서',
  'Departments' => '부서',
  'Your University Departments'=>'대학 부서',
  'to select a department' => '(손 모양 아이콘) 부서 선택',
  'Create a Department'=>'부서 만들기',
  'School dashboard menu'=>' 관리 대시보드',




//    ''=>'',
//    ''=>'',



  'fields' =>
  array (
      'id' => 'id',
    'schid' => 'Schid',
    'organization_orgid' => 'Organization Orgid',
    'sch_name' =>  '학교 이름',
    'dept_name' => '부서 이름',
    'about' =>  '학교 소개',
    'no_of_teachers' =>  '총 교사 수',
    'no_of_students' =>  '총 학생 수',
    'sch_website' =>  '학교 홈페이지',
'latitude' =>  '위도',
    'longitude' =>  '경도',
    'logo' =>  '심벌 마크',
    'address1' =>  '주소 라인 1',
    'address2' =>  '주소 2',
    'Total Number of Faculty Members' => '총 교원 수',

    'city' =>  '도시',
    'state' =>  '상태',
    'country' =>  '국가',
    'zipcode' =>  '우편 번호',
    'sch_phone' =>  '핸드폰',
    'sch_mobile' =>  '이동하는',
    'created_at' =>  '만든 시간',
    'updated_at' =>  '업데이트 시간',
    'available_licenses' =>  '사용 가능한 라이선스',
    'used_licenses' =>  '사용된 라이선스',
    'purchased_licenses' =>  '구매한 라이선스',
    'deleted_at' =>  '삭제 시간',


      'website_help' => 'Ex. http://reallive.com',
      'facebook' => '페이스북 페이지',
      'facebook_help' => 'Ex. http://facebook',
      'linkdin' => '링크드인 페이지',
      'linkdin_help' => 'Ex. http://linkdin',

    
  ),
);
