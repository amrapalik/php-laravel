<?php

return array (
  'singular' => '수업 계획',
  'plural' => '수업 계획',
    'View lesson plan' => '수업 계획 보기',
    'Lesson Plan Repo' => '과제 + 수업 계획 저장소(학교)',
    'Create Your Lesson Plan' => '수업 계획 만들기',
    'create button' => '과제 만들기 + 수업 계획 수업',
    'My Lesson Plan Repo department' => '수업 계획 저장소(학과)',
    'Lesson Plan Repo Department' => '과제 + 수업 계획 저장소(학과)',
    'Lesson Plan Re Department' => '수업 계획 저장소(학과)',
    'Lesson Plan Re' => '수업 계획 저장소(학교)',
    'create a lesson plan for the selected assignment' => '선택한 과제에 대한 수업 계획 만들기',
    'My Lesson Plan Repo' => '내 과제 + 수업 계획 저장소',
    'lessonplan repo instruction' => '학교의 동료 교사가 만든 모든 과제 + 수업 계획을 볼 수 있습니다.',
    'lessonplan repo instruction2' => '수업 계획 템플릿을 사용하여 귀하와 학교의 동료 교사가 만든 모든 수업 계획을 볼 수 있습니다. 우리는 학생들에게 제공할 수 있는 수업 계획의 PDF를 만들기 위해 노력하고 있습니다. PDF 기능이 작동하면 업데이트하겠습니다.',
    'lessonplan repo instruction3' => '당신은 당신이 만든 모든 수업 계획을 볼 수 있습니다.',
    'lessonplan repo instruction department' => '당신은 당신의 부서의 동료 교사가 만든 모든 과제 + 수업 계획을 볼 수 있습니다.',

      'View lesson plan' => '보기/편집',
      'Instruction' => '이 과제를 위해 수업 계획을 준비하고 저장합니다. 저장 후에는 "수업 만들기" 버튼을 클릭하여 수업에 학생과 교사를 추가할 수 있는 저장소 섹션에 나타납니다. 학생들에게 과제만 주고 싶다면 "글로벌 시민 과제 수업"으로 가십시오.',
      'Instruction2' => '수업 계획을 작성하는 데 사용할 수 있는 수업 계획 템플릿입니다. 우리는 당신이나 당신의 학생들이 인쇄하여 학습에 사용할 수 있도록 수업 계획을 과제에 추가하고 수업 계획을 PDF로 저장하기 위해 노력하고 있습니다.',
  
  
  'fields' =>
  array (
    

    'lpid' =>'lpid',
    'class_classid' =>  '클래스 아이디',
    'school_schid' =>  '학교 아이디',
    'name' =>  '수업명',
    'grades' =>  '등급',
    'standards' =>  '기준',
    'overview' =>  '개요',
    'objectives' =>  '목표',
    'procedures' =>  '절차',
    'assessment' =>  '평가',
    'extension_activities' =>  '확장 활동',
    'image' =>  '영상',
    'pdf' =>  'PDF',
    'video' =>  '동영상',
    'youtube_link' =>  '유튜브 링크',
    'created_by' =>  '만든 사람',
    'copied_from' =>  '복사본',
    'status' =>  '상태',
    'created_at' =>  '만든 시간',
    'updated_at' =>  '업데이트 시간',

  ),
);
