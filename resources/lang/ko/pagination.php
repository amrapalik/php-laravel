<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'previous' => '&laquo; 이전의',
    'next' => '다음 &raquo;',
    'datatable'=> [
        'export'=>'내 보내다',
        'print'=>'인쇄',
        'reset'=>'초기화',
        'reload'=>'새로고침',


        ],
    'datatable.create'=>'만들다',


];
