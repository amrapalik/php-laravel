<?php

return array (
  'singular' => '제품',
  'plural' => '제품',
    'purchase' => '라이선스 구매',
  'fields' =>
  array (
    'prodid' => 'Prodid',
    'prod_name' => '제품 이름',
    'no_of_licenses' => '라이센스 수',
    'description' =>'설명',
    'price' =>  '가격',
    'renew_price' =>  '리뉴 가격',
    'user_type' =>  '사용자 유형',
    'max_lives' =>  '맥스 라이브',
    'validity' =>  '타당성',
    'currency' =>  '통화',
    'featured' =>  '추천',
    'suspended' =>  '정지 된',
    'deleted_at' =>  '삭제 시간',
    'created_at' =>  '만든 시간',
    'updated_at' =>  '업데이트 시간',

  ),
);
