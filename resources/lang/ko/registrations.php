<?php

return array (
    'singular' => 'Registration',
    'title' => 'Registration',
    'organization' =>
        [
            'title' => 'Organization Registration',
            'heading' => 'Organization Registration',
            'subheading' => 'If you have many schools',
            'register' => 'Register',
            'description' => 'Description',
            'register_success' => 'Organization Registered successfully',

            'title' =>'단체등록',
            'heading' =>  '단체등록',
            'subheading' =>  '학교가 많다면',
            'register' =>  '등록하다',
            'description' =>  '설명',
            'register_success' =>  '조직이 성공적으로 등록되었습니다',

        ],

    'school' =>
        [
            'title' =>'단체등록',
            'heading' =>  '단체등록',
            'subheading' =>  '학교가 많다면',
            'register' =>  '등록하다',
            'description' =>  '설명',
            'register_success' =>  '조직이 성공적으로 등록되었습니다',

        ],

    'gamer' =>
        [
            'title' =>'게이머 등록',
            'heading' =>  '게이머 등록',
            'subheading' =>  '개인 게이머라면',
            'register' =>  '등록하다',
            'description' =>  '설명',
            'register_success' =>  '조직이 성공적으로 등록되었습니다',

        ],
);
