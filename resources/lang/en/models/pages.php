<?php

return array (
  'singular' => 'Page',
  'plural' => 'Pages',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'description' => 'Description',
    'grade' => 'Grade',
    'division' => 'Division',
    'age_group' => 'Age Group',
    'start_dt' => 'Start Dt',
    'end_dt' => 'End Dt',
    'created_by' => 'Created By',
    'copied_from' => 'Copied From',
    'status' => 'Status',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
