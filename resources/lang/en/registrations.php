<?php

return array (
    'singular' => 'Registration',
    'title' => 'Registration',
	'Register Your university' => 'Register Your University',
	'Register Your organization' => 'Register Your Organization',
	'Register Your school' => 'Register Your School',
	'Register Your family' => 'Register Your family',
	'Register Your university_dept' => 'Register Your University Department',
    'family'=>
        [

        ],
    'school_options' =>
        [
            'title' => 'Organization Registration',
            'heading' => 'Schools',
            'subheading' => 'If you are a school or a group of schools',
            'register' => 'Register',
            'description' => 'Description',
            'register_success' => 'Organization Registered successfully',
        ],
    'university_options' =>
        [
            'title' => 'Universities',
            'heading' => 'Universities',
            'subheading' => 'If you are a university or a university department',
            'register' => 'Register',
            'description' => 'Description',
            'register_success' => 'Organization Registered successfully',
        ],
    'organization' =>
        [
			'singular'=>'Organization',
            'title' => 'Organization Registration',
            'heading' => 'School Organization Registration',
            'subheading' => 'If you are a group of schools and wish to buy licenses for more than one school',
            'register' => 'Register',
            'description' => 'Description',
            'register_success' => 'Organization Registered successfully',
        ],
    'university' =>
        [
			'singular'=>'University',
            'title' => 'University Registration',
            'heading' => 'University Registration',
            'subheading' => 'If you are a university and want to buy licenses for various departments under your university',
            'register' => 'Register',
            'description' => 'Description',
            'register_success' => 'University Registered successfully',
        ],
    'uni_dept' =>
        [
            'title' => 'Dept. Registration',
            'heading' => 'University Deptartment Registration',
            'subheading' => 'If you want to purchase a license for a single department within a university',
            'register' => 'Register',
            'description' => 'Description',
            'register_success' => 'Department Registered successfully',
        ],

    'school' =>
        [
            'title' => 'School Registration',
            'Single'=>'Single',
            'heading' => 'School Registration',
            'subheading' => 'If you are a single school, please register your school here',
            'register' => 'Register',
            'description' => 'Description',
            'register_success' => 'Registered successfully',
        ],

    'gamer' =>
        [
            'title' => 'Gamer Registration',
            'heading' => 'Gamer Registration',
            'subheading' => 'Buy a single gamer license',
            'register' => 'Register',
            'description' => 'Description',
            'register_success' => 'Organization Registered successfully',
        ],
);
