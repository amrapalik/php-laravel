<?php

return array (
    'play assignment'=>    'Take on an assignment and live your life there',
    'load assignment'=>    'Continue Assignment',
    'complete assignment'=>    'View Completed Assignment',
    'You are deallocated can not play assignment'=>    'You are deallocated can\'t play assignment',
  'show assignment'=> 'show assignment',
  'singular' => 'Assignment',
  'plural' => 'Assignments',
  'select an Assignment' => 'Select an Assignment by clicking "+"',
  'choose a subject' => 'Choose a subject to create an assignment :',
  'fields' =>
  array (
    'asid' => 'Asid',
      'tag' => 'Tag',
      'type' => 'Type',
      'game_status' => 'Game Status',
      'letter_status' => 'Letter Status',
      'obituary_status' => 'Obituary Status',
    'asname' => 'Assignment name',
    'category' => 'Category',
    'status' => 'Status',
    'countryid' => 'Countryid',
    'country_name' => 'Country Name',
    'city_id' => 'City Id',
    'city_name' => 'City Name',
    'gender' => 'Gender',
    'religion' => 'Religion',
    'urban_rural' => 'Urban Rural',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
