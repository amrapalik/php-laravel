<?php

return array (
    'singular' => 'Student',
    'plural' => 'Students',
    'children' => 'Children',
    'import students'=>'Import Students',
    'no_license_reallocate_message'=>'Students cannot be reallocated unless school has active licenses available.',
    'no_license_reallocate_message_children'=>'Children cannot be reallocated unless family has active licenses available.',
    'Show students'=>'Show Allocated Students',
    'Total students'=>'Total students',
    'Add new student' => 'Add new student',
    'add son/daughter' => 'Add Your Daughter/Son',
    'Import students'=> 'Import students',
    'allocated members'=> 'Allocated Children',
    'Add Children'=> 'Add Children',
    'Deallocated members'=> 'Deallocated Children',
    'Download Sample Excel'=>'Download Sample Excel Sheet',
    'allocatedStudents'=>'Allocated Students',
    'plural' => 'Students',
    'Student List' => 'Student List',
    'Deallocated Student List' => 'Deallocated Student List',
    '(Hand icon) for Deallocate student' => '(Hand icon) for Deallocate student',
    'to deallocate a student' => 'to deallocate a student',
    'to deallocate a child' => 'to deallocate your child',
    'Click on' => 'Click on',
    'kids' => 'Children',
    '(Hand icon) for Allocate student' => '(Hand icon) for Allocate student',
    'to allocate student' => 'to allocate a student',
    'to allocate children' => 'to allocate your child',
    'deallocated'=>'Deallocated Students',
    'Deallocated Students'=>'Deallocated Students',
    'Import Students'=>'Import Students',
    'Download sample excel file info'=>'Download sample excel file and fill student information, after filling form upload to server.',
    'You can insert only'=>'The number of students you can insert is ',
    'School Available Licenses'=>'School Available Licenses',
    'students.'=>'students.',
    'If Students are more than'=>'In a case where there are more than',
    'then first'=>'students, the first',
    'students will be activated and remaining will be deallocated.'=>'students will be allocated the license, and the rest will remain deallocated.',
    'Note'=>'Note',
    'Student excel file'=>'Student excel file (.xlsx)',
    'instructions'=>"Download the sample excel file and fill in the required information as indicated in the headings. Then save it. The file won't be able to be uploaded if it isn't saved and open on your computer. Once you have saved the file on your computer, return to this page and click the 'Browse' button to select that file. Click OK after selecting the student/teacher sample file. The upload of the file may take up to five minutes. Please do not close or refresh the browser during this time. When the file has been successfully uploaded, you will see a message that says 'file uploaded successfully'. Go to the 'Students' or 'Teachers' tabs to verify that all our students and teachers are listed. If there are any duplicate student entries, you should deallocate them to save your license count. Teachers and students cannot be deleted in the current system. However, you can deallocate them.",
    'instruction2'=>"In case you don't have Excel on your PC, you can use Google sheets and upload the sheet from there. (the format should be .xlsx)",




    'User not found'=>'User not found',
    'Invalid school'=>'Invalid school',
    'Licenses are not available at school'=>'It appears that licenses are not available for your school. In order to continue, you will need to purchase more licenses from license management or please contact support@reallivesworld.com',
    'Licenses are not available at family'=>'It appears that licenses are not available for your family. In order to continue, you will need to purchase more licenses from license management or please contact support@reallivesworld.com',
    'Student allocated'=>'Student allocated',
    'allocated students list'=>'Allocated Student List',
    'deallocated children'=>'Deallocated Children',
    'Add your daughter/son'=>'Add your daughter/son',
    'allocated children of'=>'Children of ',
    'Children List'=>'Children List',
    'allocated students'=>'Allocated Students',
    'Student deallocated'=>'Student deallocated',
    'contact your teacher to play game'=>"Because you are deallocated you can't play. Contact your teacher for reallocation.",
    'Deallocated Children List'=>"Deallocated Children List",



);
