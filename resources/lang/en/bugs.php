<?php

return array (
  'singular' => 'Feedback / Bug Report',
  'plural' => 'Feedback / Bug Report',
  'Submit Bug' => 'Feedback / Bug Report',
  'Bug Submitted' => 'Submitted Successfully',
  'fields' =>
  array (
    'bugid' => 'Bugid',
    'title' => 'Title',
    'description' => 'Description',
    'type'=> 'Feedback / Bug Report',
    'image' => 'Image (Screenshot of game page where feedback/bug is reported)',
    'image1' => 'Image ',
    'link' => 'Link',
    'status' => 'Status',
    'created_by`' => 'Created By`',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
