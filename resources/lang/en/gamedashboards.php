<?php

return array (
    'dashboard' => 'My Game Dashboard',
    'gamedashboard' => 'Game Dashboard',

    'fields' =>
        array (
            'classid' => 'Classid',
            'letterstatus'=> 'Letter Status',
            'gamestatus'=> 'Game Status',
            'class_name' => 'Class Name',
            'class_description' => 'Class Description',
            'grade' => 'Grade',
            'division' => 'Division',
            'age_group' => 'Age Group',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'created_by' => 'Created By',
            'copied_from' => 'Copied From',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
        ),
);
