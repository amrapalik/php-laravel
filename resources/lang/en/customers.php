<?php

return array (
  'singular' => 'Customer',
  'plural' => 'Customers',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'email' => 'Email',
    'email_verified_at' => 'Email Verified At',
    'password' => 'Password',
    'remember_token' => 'Remember Token',
    'updated_at' => 'Updated At',
  ),
);
