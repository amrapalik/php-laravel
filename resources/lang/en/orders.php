<?php

return array (
  'singular' => 'Order',
  'plural' => 'Orders',
  'Orders for' => 'Order Status For ',
  'please select a school' => 'Please select a school first',
  'please select a department' => 'Please select a department first',
  'fields' =>
  array (
    'ordid' => 'Ordid',
    'user_id' => 'User',
      'date' => 'Date',
    'product_prodid' => 'Product',
    'licences' => 'Licenses',
    'price' => 'Price',
    'paypal_orderid' => 'Paypal Order Id',
    'status' => 'Status',
    'paid' => 'Paid',
    'captured' => 'Captured',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
