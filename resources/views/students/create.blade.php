@extends('layouts.vertical', ["page_title"=> __('general.Create').__('students.singular')])
<?php
$user = auth()->user();
$school = \App\Models\School::find($user->school_schid);
?>
@section('content')
<!-- Start Content-->
<div class="container-fluid">

    @if($school->school_type == 'family')
    @include('layouts.shared/page-title', ['title' => __('students.add son/daughter')])
    @else
    @include('layouts.shared/page-title', ['title' => __('general.Create').__('students.singular')])
    @endif
        
    @include('layouts.common.errors')
    <!-- Start Form  -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if($school->school_type != 'family')
                        <h4 class="header-title">@lang('students.singular')</h4>
                    @endif
                     {!! Former::vertical_open_for_files()->id('create-students-form')->method('POST')->route('students.store') !!}

                        @include('students.fields')

                    {!! Former::close() !!}

                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end form -->
</div> <!-- container -->

@endsection


@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

   {!! JsValidator::formRequest('App\Http\Requests\CreateStudentRequest') !!}
@endsection
