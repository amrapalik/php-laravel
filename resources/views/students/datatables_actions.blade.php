{!! Form::open(['route' => ['students.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>

{{--    @can('View students')--}}
{{--    @endcan--}}

    @if($deallocated == 0)
        @if(auth()->user()->drole == 'School Admin')
            <a href="#" onclick="deallocate(event,{{$id}})" title="Deallocate" class='btn btn-default btn-xs'>
                <i class="fa fa-times"></i>
            </a>
        @endif
    @else
        <a href="#" onclick="deallocate(event,{{$id}})" title="Reallocate" class='btn btn-default btn-xs'>
            <i class="fa fa-check"></i>
        </a>
    @endif

    @can('Edit students')
        <a href="{{ route('students.edit', $id) }}" title="Edit" class='btn btn-default btn-xs'>
            <i class="fa fa-edit"></i>
        </a>
    @endcan

{{--    @can('Delete students')--}}
{{--        @if(auth()->user()->drole == 'Organization Admin'|| auth()->user()->drole == 'School Admin')--}}
{{--        {!! Form::button('<i class="fa fa-trash" title="Delete"></i>', [--}}
{{--            'type' => 'submit',--}}
{{--            'class' => 'btn btn-danger btn-xs',--}}
{{--            'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'--}}
{{--        ]) !!}--}}
{{--            @endif--}}
{{--    @endcan--}}
</div>
{!! Form::close() !!}
