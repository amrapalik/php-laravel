@extends('layouts.vertical', ["page_title"=> __('students.plural')])

@section('css')
    <!-- third party css -->
    <link href="{{asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
@endsection


@section('content')
    <!-- Start Content-->
    <div class="container-fluid">


    <?php

    $deallocated = app('request')->input('deallocated');
    $school = \App\Models\School::find(auth()->user()->school_schid);


    ?>

    @if($deallocated == 'true')

        <!-- start page title -->
        @if($school->school_type == 'family')
            @include('layouts.shared/page-title', ['title' => "  ".__('students.deallocated children')])
        @else
            @include('layouts.shared/page-title', ['title' => "  ".__('students.deallocated')])
        @endif
        <!-- end page title -->
    @else
        <!-- start page title -->
        @if($school->school_type == 'family')
            @include('layouts.shared/page-title', ['title' => "  ".__('students.allocated children of').__($school->sch_name).__('general.family')])
        @else    
            @include('layouts.shared/page-title', ['title' => "  ".__('students.allocated students list')])
        @endif
        <!-- end page title -->

    @endif





        @include('flash::message')

        <div class="clearfix"></div>

        <div class="row">  <!-- Data table-->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <!--  Title and buttons -->
                        <div class="row">
                            <div class="col-9">

                            @if($deallocated == 'true')

                                <!-- start page title -->
                                @if($school->school_type == 'family')
                                    <h4 class="header-title" style="float: left; margin-top: 10px">@lang('students.Deallocated Children List')</h4>
                                @else  
                                    <h4 class="header-title" style="float: left; margin-top: 10px">@lang('students.Deallocated Student List')</h4>
                                @endif
                                <!-- end page title -->
                            @else
                                <!-- start page title -->
                                @if($school->school_type == 'family')
                                    <h4 class="header-title" style="float: left; margin-top: 10px">@lang('students.Children List')</h4>
                                @else
                                    <h4 class="header-title" style="float: left; margin-top: 10px">@lang('students.Student List')</h4>
                                @endif
                                <!-- end page title -->

                                @endif

                                <br>

                            </div>
                                <div class="col-3">
                                    @if($deallocated != 'true')

                                        @if(in_array(auth()->user()->drole, ['School Admin','Organization Admin']))
                                            <a href="{{ route('students.create') }}" style="float: right" class="btn btn-success rounded-pill waves-effect waves-light">
                                                @if($school->school_type == 'family')
                                                    @lang('students.Add your daughter/son')
                                                @else
                                                    @lang('students.Add new student')
                                                @endif
                                            </a>
                                        @endif

                                    @endif
                                </div>

                            <p class="text-muted font-13 mb-4">
                             <span style="color:#00acc1">

                                @if($deallocated == 'true')
                                    @if($school->school_type == 'family')
                                        @lang('students.no_license_reallocate_message_children')
                                    @else    
                                        @lang('students.no_license_reallocate_message')
                                    @endif    
                                @endif

                            </p>
                        </div> <!-- / End Title and buttons -->

                        <p class="text-muted font-13 mb-4">
                            @if(in_array(auth()->user()->drole, ['School Admin','Organization Admin']))

                                @if($deallocated == 'true')
                                    @if($school->school_type == 'family')
                                        <span style="color:#00acc1"> @lang('students.Click on') ( <i class="fa fa-check"></i> ) @lang('students.to allocate children').</span>
                                    @else
                                        <span style="color:#00acc1"> @lang('students.Click on') ( <i class="fa fa-check"></i> ) @lang('students.to allocate student').</span>
                                    @endif
                                @else
                                    @if($school->school_type == 'family')
                                        <span style="color:#00acc1"> @lang('students.Click on') (<i class="fa fa-times"></i>)  @lang('students.to deallocate a child').</span>
                                    @else
                                        <span style="color:#00acc1"> @lang('students.Click on') (<i class="fa fa-times"></i>)  @lang('students.to deallocate a student').</span>
                                    @endif
                                @endif

                            @endif


                        </p>

                        {!! $dataTable->table(['width' => '100%', 'class' => 'table dt-responsive nowrap w-100 ']) !!}
                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->
    </div> <!-- end container -->
@endsection


@section('script')
    @include('layouts.datatables_js')
    {{--    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>--}}
    {!! $dataTable->scripts() !!}


    <script>

        function deallocate(e,recid)
        {
            e.preventDefault();

            $('#loading-indicator').show();
            $.ajax({
                type: "GET",
                url : "/toggleAllocate?userid="+recid,
                cache: false,
                success: function (data) {
                    $('#loading-indicator').hide();

                    if(data.status == 'success')
                    toastr.success( data.message, {timeOut: 6000});
                    else
                        toastr.error(  data.message, {timeOut: 6000});
                    //  location.reload();
                    //TODO:: Reload table
                    $('#dataTableBuilder').DataTable().ajax.reload();
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert (textStatus);
                    $('#loading-indicator').hide();
                }
            });
        }
    </script>

@endsection
