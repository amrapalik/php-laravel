<div class="row">
	<?php
$role = auth()->user()->drole;
$school = \App\Models\School::find(auth()->user()->school_schid);

?>
<!-- Firstname Field -->
{!! Former::text('firstname')->label(__('users.fields.firstname').':')->addGroupClass('col-sm-3')->placeholder('Firstname') ->required()!!}

<!-- Lastname Field -->
{!! Former::text('lastname')->label(__('users.fields.lastname').':')->addGroupClass('col-sm-3')->placeholder('Lastname')->required() !!}


<!-- Username Field -->
<!-- {!! Former::text('username')->label(__('users.fields.username').':')->addGroupClass('col-sm-3')->placeholder('Username')->required() !!} -->


@if($school->school_type == 'family')
    {!! Former::text('grade')->label(__('users.fields.age').':')->addGroupClass('col-sm-3')->placeholder('Age')->required() !!}
@else
    {!! Former::text('grade')->label(__('users.fields.grade').':')->addGroupClass('col-sm-3')->placeholder('Grade/Standard')->required() !!}

@endif

<?php

$options = ['Organization Admin'=>'Organization Admin','School Admin'=>'School Admin','Teacher'=>'Teacher','Student'=>'Student','Gamer'=>'Gamer'];


$options =[];
if($role == 'Organization Admin' || $role == 'School Admin')
{
    $options = [ null =>'Select user Type', 'School Admin'=>'School Admin','Teacher'=>'Teacher','Student'=>'Student'];
}

if($role == 'Teacher')
{
    $options = [ null =>'Select user Type', 'Student'=>'Student'];
}

$gender = [ null =>'Select Gender', 'Male'=>'Male','Female'=>'Female','Other'=>'Other'];

//    if($role == 'School Admin')
//    {
//        $options = [ 'Teacher'=>'Teacher','Student'=>'Student'];
//    }
//
//    if($role == 'School Admin')
//    {
//        $options = [ 'Teacher'=>'Teacher','Student'=>'Student'];
//    }



?>
<!-- Type Field -->

    {!! Former::hidden('drole')->label(__('users.fields.drole').':')->value('Student')->addGroupClass('col-sm-3')->placeholder('Password')->required() !!}

</div>



<div class="row">
    <!-- Password Field -->
{!! Former::password('password')->label(__('users.fields.password').':')->addGroupClass('col-sm-3')->placeholder('Password')->required() !!}

{!! Former::password('password_confirmation')->label(__('users.fields.confirmpassword').':')->addGroupClass('col-sm-3')->placeholder(__('users.fields.confirmpassword'))->required()->required() !!}

<!-- Gender Field -->

    {!! Former::select('gender')->label(__('users.fields.gender').':')->options($gender)->addGroupClass('col-sm-3')

    ->state('warning')->required() !!}

</div>


<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-info']) !!}
    <a href="{{ URL::previous() }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
