<!-- Organization Orgid Field -->
<div class="col-sm-12">
    {!! Form::label('organization_orgid', __('students.fields.organization_orgid').':') !!}
    <p>{{ $student->organization_orgid }}</p>
</div>


<!-- School Schid Field -->
<div class="col-sm-12">
    {!! Form::label('school_schid', __('students.fields.school_schid').':') !!}
    <p>{{ $student->school_schid }}</p>
</div>


<!-- Username Field -->
<div class="col-sm-12">
    {!! Form::label('username', __('students.fields.username').':') !!}
    <p>{{ $student->username }}</p>
</div>


<!-- Firstname Field -->
<div class="col-sm-12">
    {!! Form::label('firstname', __('students.fields.firstname').':') !!}
    <p>{{ $student->firstname }}</p>
</div>


<!-- Lastname Field -->
<div class="col-sm-12">
    {!! Form::label('lastname', __('students.fields.lastname').':') !!}
    <p>{{ $student->lastname }}</p>
</div>


<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('students.fields.name').':') !!}
    <p>{{ $student->name }}</p>
</div>


<!-- Type Field -->
<div class="col-sm-12">
    {!! Form::label('type', __('students.fields.type').':') !!}
    <p>{{ $student->type }}</p>
</div>


<!-- Password Field -->
<div class="col-sm-12">
    {!! Form::label('password', __('students.fields.password').':') !!}
    <p>{{ $student->password }}</p>
</div>


<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', __('students.fields.email').':') !!}
    <p>{{ $student->email }}</p>
</div>


<!-- Drole Field -->
<div class="col-sm-12">
    {!! Form::label('drole', __('students.fields.drole').':') !!}
    <p>{{ $student->drole }}</p>
</div>


<!-- Grade Field -->
<div class="col-sm-12">
    {!! Form::label('grade', __('students.fields.grade').':') !!}
    <p>{{ $student->grade }}</p>
</div>


<!-- Phone No Field -->
<div class="col-sm-12">
    {!! Form::label('phone_no', __('students.fields.phone_no').':') !!}
    <p>{{ $student->phone_no }}</p>
</div>


<!-- User Mobile Field -->
<div class="col-sm-12">
    {!! Form::label('user_mobile', __('students.fields.user_mobile').':') !!}
    <p>{{ $student->user_mobile }}</p>
</div>


<!-- Address Field -->
<div class="col-sm-12">
    {!! Form::label('address', __('students.fields.address').':') !!}
    <p>{{ $student->address }}</p>
</div>


<!-- Country Field -->
<div class="col-sm-12">
    {!! Form::label('country', __('students.fields.country').':') !!}
    <p>{{ $student->country }}</p>
</div>


<!-- Country Code Field -->
<div class="col-sm-12">
    {!! Form::label('country_code', __('students.fields.country_code').':') !!}
    <p>{{ $student->country_code }}</p>
</div>


<!-- Purchaseflag Field -->
<div class="col-sm-12">
    {!! Form::label('purchaseflag', __('students.fields.purchaseflag').':') !!}
    <p>{{ $student->purchaseflag }}</p>
</div>


<!-- Gender Field -->
<div class="col-sm-12">
    {!! Form::label('gender', __('students.fields.gender').':') !!}
    <p>{{ $student->gender }}</p>
</div>


<!-- Avatar Field -->
<div class="col-sm-12">
    {!! Form::label('avatar', __('students.fields.avatar').':') !!}
    <p>{{ $student->avatar }}</p>
</div>


<!-- Alternate Address Field -->
<div class="col-sm-12">
    {!! Form::label('alternate_address', __('students.fields.alternate_address').':') !!}
    <p>{{ $student->alternate_address }}</p>
</div>


<!-- City Field -->
<div class="col-sm-12">
    {!! Form::label('city', __('students.fields.city').':') !!}
    <p>{{ $student->city }}</p>
</div>


<!-- State Field -->
<div class="col-sm-12">
    {!! Form::label('state', __('students.fields.state').':') !!}
    <p>{{ $student->state }}</p>
</div>


<!-- Zipcode Field -->
<div class="col-sm-12">
    {!! Form::label('zipcode', __('students.fields.zipcode').':') !!}
    <p>{{ $student->zipcode }}</p>
</div>


<!-- Designation Field -->
<div class="col-sm-12">
    {!! Form::label('designation', __('students.fields.designation').':') !!}
    <p>{{ $student->designation }}</p>
</div>


<!-- Alternate Email Field -->
<div class="col-sm-12">
    {!! Form::label('alternate_email', __('students.fields.alternate_email').':') !!}
    <p>{{ $student->alternate_email }}</p>
</div>


<!-- Facebook Profile Field -->
<div class="col-sm-12">
    {!! Form::label('facebook_profile', __('students.fields.facebook_profile').':') !!}
    <p>{{ $student->facebook_profile }}</p>
</div>


<!-- Linkedin Profile Field -->
<div class="col-sm-12">
    {!! Form::label('linkedIn_profile', __('students.fields.linkedIn_profile').':') !!}
    <p>{{ $student->linkedIn_profile }}</p>
</div>


<!-- Email Verified At Field -->
<div class="col-sm-12">
    {!! Form::label('email_verified_at', __('students.fields.email_verified_at').':') !!}
    <p>{{ $student->email_verified_at }}</p>
</div>


<!-- Remember Token Field -->
<div class="col-sm-12">
    {!! Form::label('remember_token', __('students.fields.remember_token').':') !!}
    <p>{{ $student->remember_token }}</p>
</div>


<!-- Dob Field -->
<div class="col-sm-12">
    {!! Form::label('dob', __('students.fields.dob').':') !!}
    <p>{{ $student->dob }}</p>
</div>


<!-- Last Loggedin Field -->
<div class="col-sm-12">
    {!! Form::label('last_loggedin', __('students.fields.last_loggedin').':') !!}
    <p>{{ $student->last_loggedin }}</p>
</div>


<!-- Account Expiry Date Field -->
<div class="col-sm-12">
    {!! Form::label('account_expiry_date', __('students.fields.account_expiry_date').':') !!}
    <p>{{ $student->account_expiry_date }}</p>
</div>


<!-- Account Status Field -->
<div class="col-sm-12">
    {!! Form::label('account_status', __('students.fields.account_status').':') !!}
    <p>{{ $student->account_status }}</p>
</div>


<!-- Deallocated Field -->
<div class="col-sm-12">
    {!! Form::label('deallocated', __('students.fields.deallocated').':') !!}
    <p>{{ $student->deallocated }}</p>
</div>


<!-- Account Expired Field -->
<div class="col-sm-12">
    {!! Form::label('account_expired', __('students.fields.account_expired').':') !!}
    <p>{{ $student->account_expired }}</p>
</div>


