@extends('layouts.vertical', ["page_title"=> __('general.Create').__('students.singular')])

@section('content')
<!-- Start Content-->
<div class="container-fluid">

    @include('layouts.shared/page-title', ['title' => __('students.import students')])

    @include('layouts.common.errors')

    @include('flash::message')
    <!-- Start Form  -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title"> @lang('students.Import students')</h4>
                    <p class="sub-header">

                        @lang('students.Download sample excel file info')<br><br>


                        <a href="/assets/sample_excel/Sample_Students.xlsx" class="btn btn-danger"> @lang('students.Download Sample Excel')</a>
                    </p>
                    <p>
                        <span style="color:#00acc1"> @lang('students.instruction2')</span> 
                    </p>
                    <?php
                        use Carbon\Carbon;

                        $schoolid =  session('school_schid');
                        $school =  \App\Models\School::find($schoolid);
                        $license_available = \App\Models\License::where('school_schid' ,'=',$schoolid)->whereDate('expiry_date', '>=', Carbon::now())
                            ->where('user_id','==',0)->count();

                    

                    ?>
                    <h5> @lang('students.School Available Licenses')  <span class="badge bg-success">{!! $license_available !!}</span></h5>

                    <p class="sub-header warning">
                        <span style="color:#00acc1"> @lang('students.You can insert only')  <span class="badge bg-warning">{!! $license_available !!}</span>

                    <br>
                       <span style="color:#00acc1"> @lang('students.If Students are more than')  <span class="badge bg-warning">{!! $license_available !!}</span>  @lang('students.then first')  <span class="badge bg-warning">{!! $license_available !!}</span>  @lang('students.students will be activated and remaining will be deallocated.')</span>
                    </p>

@if(isset($errors) && $errors->any())
<div class="alert alert-danger">
   @foreach($errors->all() as $error)
       {!! $error !!}
   @endforeach
</div>
@endif



@if(session()->has('failures'))

<table class="table table-danger">
   <tr>
       <th>Row</th>
       <th>Attribute</th>
       <th>Errors</th>
       <th>Value</th>
   </tr>

   @foreach(session()->get('failures') as $validation)
       <tr>
           <td>{!! $validation->row() !!}</td>
           <td>{!! $validation->attribute() !!}</td>
           <td>{!! $validation->row() !!}</td>
           <td>
               <ul>
                   @foreach($validation->errors() as $e)
                       <li> {!! $e !!}</li>
                   @endforeach
               </ul>
           </td>
       </tr>
   @endforeach
</table>
@endif

{!! Former::vertical_open_for_files()->id('create-students-form')->method('POST')->route('import') !!}

<div class="row">
{!! Former::file('file')->label(__('students.Student excel file').':')->addGroupClass('col-sm-3')->placeholder('File') !!}
</div>
<br> <br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
{!! Form::submit(__('crud.save'), ['class' => 'btn btn-info']) !!}

</div>

{!! Former::close() !!}


                    <br>
                    <br>

<h5>@lang('students.Note')</h5>


                    <p>
                        @lang('students.instructions')
                    </p>
                  
</div> <!-- end card-body -->
</div> <!-- end card-->
</div> <!-- end col -->
</div>
<!-- end form -->
</div> <!-- container -->

@endsection


@section('scripts')
<!-- Laravel Javascript Validation -->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

{!! JsValidator::formRequest('App\Http\Requests\CreateStudentRequest') !!}
@endsection
