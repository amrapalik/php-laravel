@extends('layouts.vertical', ["page_title"=> __('lessonPlans.plural')])

@section('css')
    <!-- third party css -->
    <link href="{{asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
@endsection

<?php
$schoolid =  session('school_schid');

$school =  \App\Models\School::find($schoolid);

?> 
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">




        <!-- start page title -->
        @if (request()->assignment)
            @if($school->school_type == 'family')
                @include('layouts.shared/page-title', ['title' =>  __('lessonPlans.Lesson Plan Repo family')]) 
            @else
                @if( Route::is('myLessonPlansWithAssignment'))
                    @if($school->school_type == 'school')
                        @include('layouts.shared/page-title', ['title' =>  __('lessonPlans.My Lesson Plan Repo')])
                    @else
                        @include('layouts.shared/page-title', ['title' =>  __('lessonPlans.My Lesson Plan Repo department')])
                    @endif
                @else
                    @if($school->school_type == 'school')
                        @include('layouts.shared/page-title', ['title' =>  __('lessonPlans.Lesson Plan Repo')])
                    @else
                        @include('layouts.shared/page-title', ['title' =>  __('lessonPlans.Lesson Plan Repo Department')])
                    @endif
                @endif    
            @endif    
            @include('flash::message')
            <p class="text-muted font-13 mb-4">
                <span style="color:#00acc1">
                    @if( Route::is('myLessonPlansWithAssignment'))
                        @lang('lessonPlans.lessonplan repo instruction3')</span>
                       
                @else
                    @if($school->school_type == 'school')
                        @lang('lessonPlans.lessonplan repo instruction')</span>
                    @else
                        @lang('lessonPlans.lessonplan repo instruction department')</span>
                    @endif    
                @endif    
    
           </p>
        @else
          @if($school->school_type == 'school')
            @include('layouts.shared/page-title', ['title' =>  __('lessonPlans.Lesson Plan Re')]) 
          @else
            @include('layouts.shared/page-title', ['title' =>  __('lessonPlans.Lesson Plan Re Department')]) 
          @endif    
            @include('flash::message')
            <p class="text-muted font-13 mb-4">
                
                <span style="color:#00acc1">
                        @lang('lessonPlans.lessonplan repo instruction2')</span>
    
           </p>

        @endif    
    <!-- end page title -->

       
<div class="clearfix"></div>
        <div class="clearfix"></div>

        <div class="row">  <!-- Data table-->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <!--  Title and buttons -->
                        <div class="row">
                            <div class="col-9">
                                @if (request()->assignment)
                                    @if($school->school_type == 'family')
                                        <h4 class="header-title" style="float: left; margin-top: 10px">@lang('lessonPlans.Lesson Plan Repo family')</h4>
                                    @else    
                                        @if( Route::is('myLessonPlansWithAssignment'))
                                            <h4 class="header-title" style="float: left; margin-top: 10px">@lang('lessonPlans.My Lesson Plan Repo')</h4>
                                        @else
                                            @if($school->school_type == 'school')
                                                <h4 class="header-title" style="float: left; margin-top: 10px">@lang('lessonPlans.Lesson Plan Repo')</h4>
                                            @else
                                                <h4 class="header-title" style="float: left; margin-top: 10px">@lang('lessonPlans.Lesson Plan Repo Department')</h4>    
                                            @endif    
                                        @endif    
                                    @endif    
                                @else
                                    @if($school->school_type == 'school')
                                        <h4 class="header-title" style="float: left; margin-top: 10px">@lang('lessonPlans.Lesson Plan Re')</h4>
                                    @else
                                        <h4 class="header-title" style="float: left; margin-top: 10px">@lang('lessonPlans.Lesson Plan Re Department')</h4>
                                    @endif   
                                @endif
                            </div>
                                <div class="col-3">
                                    @can('Create lessonPlans')
                                        <!--<a href="{{ route('lessonPlans.create') }}" style="float: right" class="btn btn-success rounded-pill waves-effect waves-light"> @lang('pagination.datatable.create') @lang('lessonPlans.singular')</a> -->
									@if (request()->assignment)
                                        <a href="{{ route('assignment_subjects').'?create=lesson_plan' }}" style="float: right" class="btn btn-success rounded-pill waves-effect waves-light"> @lang('lessonPlans.create button')</a>
									@else
                                        <a href="{{ route('lessonPlans.create') }}" style="float: right" class="btn btn-success rounded-pill waves-effect waves-light"> @lang('lessonPlans.Create Your Lesson Plan')</a>
									@endif
                                    @endcan
                                </div>
                        </div> <!-- / End Title and buttons -->

                        <p class="text-muted font-13 mb-4">
                        </p>

                        {!! $dataTable->table(['width' => '100%', 'class' => 'table dt-responsive nowrap w-100 ']) !!}
                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->
    </div> <!-- end container -->
@endsection


@section('script')
    @include('layouts.datatables_js')
    {{--    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>--}}
    {!! $dataTable->scripts() !!}
@endsection
