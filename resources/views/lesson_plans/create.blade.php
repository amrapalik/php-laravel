@extends('layouts.vertical', ["page_title"=> __('general.Create').__('lessonPlans.singular')])

@section('content')
<!-- Start Content-->
<div class="container-fluid">
    @if (request()->assignment)
        @include('layouts.shared/page-title', ['title' =>__('lessonPlans.create a lesson plan for the selected assignment')])
    @else
        @include('layouts.shared/page-title', ['title' => __('general.Create').__('lessonPlans.lesson plan title')])
    @endif
    @include('flash::message')
        <p class="text-muted font-13 mb-4">
                             <span style="color:#00acc1">
                                @if (request()->assignment)
                                    @lang('lessonPlans.Instruction')</span>
                                @else
                                    @lang('lessonPlans.Instruction2')</span>
                                @endif

                        </p>
        <div class="clearfix"></div>

    @include('layouts.common.errors')

    <!-- Start Form  -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">@lang('lessonPlans.singular')</h4>
                    <h4 class="header-title">@lang('assignments.singular'): {{ empty($assignment->asname) ? 'None' : $assignment->asname }}</h4>

                     {!! Former::vertical_open_for_files()->id('create-lessonPlans-form')->method('POST')->route('lessonPlans.store') !!}

                        @include('lesson_plans.fields')

                    {!! Former::close() !!}

                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end form -->
</div> <!-- container -->

@endsection


@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

   {!! JsValidator::formRequest('App\Http\Requests\CreateLessonPlanRequest') !!}

    <script src="/vendor/tinymce/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
        tinymce.init({
            selector: 'textarea#procedures',
            menubar: true
        });

        tinymce.init({
            selector: 'textarea#assessment',
            menubar: true
        });
    </script>

@endsection
