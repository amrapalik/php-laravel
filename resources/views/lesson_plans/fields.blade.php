<div class="row">
<input type="hidden" name="assignment_id" value="{{ request()->get('asid') }}" />
    <!-- Name Field -->
    {!! Former::text('name')->label(__('lessonPlans.fields.name').':')->addGroupClass('col-sm-6')->placeholder(__('lessonPlans.fields.name').':') !!}

<!-- Image Field -->
{!! Former::file('image')->label(__('lessonPlans.fields.image').': &nbsp;&nbsp;&nbsp; ')->class('form-control')->addGroupClass('col-sm-6')->placeholder(__('lessonPlans.fields.image').':') !!}


</div>



{{--<div class="row">--}}
{{--    <!-- Standards Field -->--}}
{{--    {!! Former::text('standards')->label(__('lessonPlans.fields.standards').':')->addGroupClass('col-sm-6')->placeholder(__('lessonPlans.fields.standards').':') !!}--}}
{{--<!-- Grades Field -->--}}
{{--    {!! Former::text('grades')->label(__('lessonPlans.fields.grades').':')->addGroupClass('col-sm-6')->placeholder(__('lessonPlans.fields.grades').':') !!}--}}



{{--</div>--}}
<div class="row">

    <!-- Youtube Link Field -->
    {!! Former::text('youtube_link')->label(__('lessonPlans.fields.youtube_link').':')->addGroupClass('col-sm-6')->placeholder(__('lessonPlans.fields.youtube_link').':') !!}
</div>


<div class="row">
    <!-- Overview Field -->
    {!! Former::textarea('overview')->label(__('lessonPlans.fields.overview').':')->addGroupClass('col-sm-12 col-lg-12')->placeholder(__('lessonPlans.fields.overview').':') !!}

    <!-- Objectives Field -->
    {!! Former::textarea('objectives')->label(__('lessonPlans.fields.objectives').':')->addGroupClass('col-sm-12 col-lg-12')->placeholder(__('lessonPlans.fields.objectives').':') !!}

</div>

<div class="row">

    <!-- Procedures Field -->
    {!! Former::textarea('procedures')->label(__('lessonPlans.fields.procedures').':')->addGroupClass('col-sm-12 col-lg-12')->placeholder(__('lessonPlans.fields.procedures').':') !!}

    <!-- Assessment Field -->
    {!! Former::textarea('assessment')->label(__('lessonPlans.fields.assessment').':')->addGroupClass('col-sm-12 col-lg-12')->placeholder(__('lessonPlans.fields.assessment').':') !!}

</div>



<div class="row">

    <!-- Extension Activities Field -->
    {!! Former::textarea('extension_activities')->label(__('lessonPlans.fields.extension_activities').':')->addGroupClass('col-sm-12 col-lg-12')->placeholder(__('lessonPlans.fields.extension_activities').':') !!}
</div>


<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-info']) !!}
    @if(Route::is('lessonPlans.create') && Request::has('asid'))
         <a href="{{ route('myLessonPlansWithAssignment') }}" class="btn btn-default">@lang('crud.cancel')</a> 
    @else
        <a href="{{ route('lessonPlans.index') }}" class="btn btn-default">@lang('crud.cancel')</a>

    @endif
</div>
