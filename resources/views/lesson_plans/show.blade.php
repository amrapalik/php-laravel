@extends('layouts.vertical', ["page_title"=> "Form Components"])

@section('content')
 <!-- Start Content-->
    <div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">

                </div>
                <h4 class="page-title">@lang('lessonPlans.singular')</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-4">Default Tabs</h4>

                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a href="#home" data-bs-toggle="tab" aria-expanded="true" class="nav-link active">
                                    Lesson Plan
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#profile" data-bs-toggle="tab" aria-expanded="false" class="nav-link">
                                    View Comments
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#messages" data-bs-toggle="tab" aria-expanded="false" class="nav-link">
                                    Add Comments
                                </a>
                            </li>

                            @if ($lessonPlan->pdf != NULL)
                                <li class="nav-item">
                                    <a href="/download-pdf/{{ $lessonPlan->pdf }}" class="nav-link">
                                        Download Lesson Plan PDF
                                    </a>
                                </li>
                            @endif


                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane  show active" id="home">
                                <strong>Assignment</strong>: {{ @$lesson_plan->assignment->asname }} <br />
                                <strong>School</strong>: {{ $lessonPlan->schoolName }} <br />
                                <strong>Teacher</strong>: {{ $lessonPlan->teacherName }} <br />
                                <strong>Email</strong>: {{ $lessonPlan->teacherEmail }} <br />
                                <strong>Lesson Plan Name</strong>: {{ $lessonPlan->name }} <br />
                                <strong>Grades / Standards Applicable</strong>: {{ $lessonPlan->grades }} <br />
                                <strong>National Education Standards</strong>: {{ $lessonPlan->standards }} <br /><br />
                                <p><strong>Overview</strong>: <br />
                                    {{ $lessonPlan->overview }}
                                </p>
                                <p><strong>Objectives</strong>: <br />
                                    {{ $lessonPlan->objectives }}
                                </p>
                                <p><strong>Procedures</strong>: <br />
                                    {!! $lessonPlan->procedures !!}
                                </p>
                                <p><strong>Assessment Criteria</strong>: <br />
                                    {!! $lessonPlan->assessment !!}
                                </p>
                                <p><strong>Extension Activities</strong>: <br />
                                    {{ $lessonPlan->extension_activities }}
                                </p>

                                @if($lessonPlan->youtube_link != NULL)
                                    <strong>YouTube Link</strong>: <a href="{{ $lessonPlan->youtube_link }}" target="_blank">{{ $lessonPlan->youtube_link }}</a>
                                @endif

                                @if($lessonPlan->image != NULL)
                                    <br /><br /><br />
                                    <img class="" src="{{ asset('/storage/lesson_plans/' . $lessonPlan->image) }}" />
                                @endif

                                @if($lessonPlan->created_by == auth()->id())
                                    <br /><br />
                                    <a href="{{ url('/lesson-plan') .'/'. $lessonPlan->lpid }}"><button type="button" class="btn btn-primary waves-effect waves-light">Edit Lesson Plan</button></a>
                                @endif

                                </div>
                            <div class="tab-pane" id="profile">
                                @if(count($comments) == 0)
                                    No Comments
                                @endif


                                @foreach ($comments as $comment)

                                    <div class="border border-light p-2 mb-3">
                                        <div class="media">
                                            <img class="mr-2 avatar-sm rounded-circle" src="{{ asset('/storage/users/' . $comment->gender.".jpg") }}">
                                            <div class="media-body">
                                                <h5 class="m-0">{{ $comment->name }}</h5>
                                                <p class="text-muted"><strong>{{ ucfirst($comment->drole) }}</strong> - <small>{{ $comment->created_at }}</small></p>
                                            </div>
                                        </div>
                                        <p>{{ $comment->comment }}</p>
                                    </div>
                                @endforeach

                               </div>
                            <div class="tab-pane" id="messages">
                                <form method="POST" name="addComment" action="{{ URL('/add-comment') }}">
                                    @csrf

                                    <input type="hidden" name="lesson_plan_id" value="{{ Request::segment(2) }}" />
                                    <input type="hidden" name="replied_to" value="NULL" />
                                    <input type="hidden" name="lessonPlanName" value="{{ $lessonPlan->name }}" />

                                    <textarea class="form-control" id="extension_activities" rows="3" name="comment"></textarea><br />

                                    <button type="submit" class="btn btn-info waves-effect waves-light">Add Comment</button>

                                </form>
                              </div>
                        </div>
                    </div>
                </div> <!-- end card-->
            </div> <!-- end col -->


        </div>
    </div>

@endsection
