<!-- Class Classid Field -->
<div class="col-sm-12">
    {!! Form::label('class_classid', __('lessonPlans.fields.class_classid').':') !!}
    <p>{{ $lessonPlan->class_classid }}</p>
</div>


<!-- School Schid Field -->
<div class="col-sm-12">
    {!! Form::label('school_schid', __('lessonPlans.fields.school_schid').':') !!}
    <p>{{ $lessonPlan->school_schid }}</p>
</div>


<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('lessonPlans.fields.name').':') !!}
    <p>{{ $lessonPlan->name }}</p>
</div>


<!-- Grades Field -->
<div class="col-sm-12">
    {!! Form::label('grades', __('lessonPlans.fields.grades').':') !!}
    <p>{{ $lessonPlan->grades }}</p>
</div>


<!-- Standards Field -->
<div class="col-sm-12">
    {!! Form::label('standards', __('lessonPlans.fields.standards').':') !!}
    <p>{{ $lessonPlan->standards }}</p>
</div>


<!-- Overview Field -->
<div class="col-sm-12">
    {!! Form::label('overview', __('lessonPlans.fields.overview').':') !!}
    <p>{{ $lessonPlan->overview }}</p>
</div>


<!-- Objectives Field -->
<div class="col-sm-12">
    {!! Form::label('objectives', __('lessonPlans.fields.objectives').':') !!}
    <p>{{ $lessonPlan->objectives }}</p>
</div>


<!-- Procedures Field -->
<div class="col-sm-12">
    {!! Form::label('procedures', __('lessonPlans.fields.procedures').':') !!}
    <p>{{ $lessonPlan->procedures }}</p>
</div>


<!-- Assessment Field -->
<div class="col-sm-12">
    {!! Form::label('assessment', __('lessonPlans.fields.assessment').':') !!}
    <p>{{ $lessonPlan->assessment }}</p>
</div>


<!-- Extension Activities Field -->
<div class="col-sm-12">
    {!! Form::label('extension_activities', __('lessonPlans.fields.extension_activities').':') !!}
    <p>{{ $lessonPlan->extension_activities }}</p>
</div>


<!-- Image Field -->
<div class="col-sm-12">
    {!! Form::label('image', __('lessonPlans.fields.image').':') !!}
    <p>{{ $lessonPlan->image }}</p>
</div>


<!-- Pdf Field -->
<div class="col-sm-12">
    {!! Form::label('pdf', __('lessonPlans.fields.pdf').':') !!}
    <p>{{ $lessonPlan->pdf }}</p>
</div>


<!-- Video Field -->
<div class="col-sm-12">
    {!! Form::label('video', __('lessonPlans.fields.video').':') !!}
    <p>{{ $lessonPlan->video }}</p>
</div>


<!-- Youtube Link Field -->
<div class="col-sm-12">
    {!! Form::label('youtube_link', __('lessonPlans.fields.youtube_link').':') !!}
    <p>{{ $lessonPlan->youtube_link }}</p>
</div>


<!-- Created By Field -->
<div class="col-sm-12">
    {!! Form::label('created_by', __('lessonPlans.fields.created_by').':') !!}
    <p>{{ $lessonPlan->created_by }}</p>
</div>


<!-- Copied From Field -->
<div class="col-sm-12">
    {!! Form::label('copied_from', __('lessonPlans.fields.copied_from').':') !!}
    <p>{{ $lessonPlan->copied_from }}</p>
</div>


<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', __('lessonPlans.fields.status').':') !!}
    <p>{{ $lessonPlan->status }}</p>
</div>


