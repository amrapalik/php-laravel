{!! Form::open(['route' => ['lessonPlans.destroy', $lpid], 'method' => 'delete']) !!}
<div class='btn-group'>

@php
	$lesson_plan = \App\Models\LessonPlan::find($lpid);
	@$asid = $lesson_plan->assignment->asid;
@endphp


	@if ($lesson_plan->assignment_id)
    <a href="{{ route('classes.create') }}?lessonplan=true&lpid={{$lpid}}&assignment=true&asid={{ $asid }}" title="Create class" class='btn btn-success rounded-pill waves-effect waves-light btn-xs'>
        <i class="fa fa-plus"></i>
        @lang('classes.Attach Lesson Plan + Assignment')
    </a>
	@endif

    &nbsp;&nbsp;
            <a href="{{ route('lessonPlans.show', $lpid) }}" title="View lesson plan" class='btn btn-warning rounded-pill waves-effect waves-light btn-xs'>
                <i class="fa fa-eye"></i>
                @lang('lessonPlans.View lesson plan')
            </a>

    @can('View lesson_plans')
    @endcan

    @can('Edit lesson_plans')
        <a href="{{ route('lessonPlans.edit', $lpid) }}" class='btn btn-default btn-xs'>
            <i class="fa fa-edit"></i>
        </a>
    @endcan

{{--    @can('Delete lesson_plans')--}}
{{--        {!! Form::button('<i class="fa fa-trash"></i>', [--}}
{{--            'type' => 'submit',--}}
{{--            'class' => 'btn btn-danger btn-xs',--}}
{{--            'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'--}}
{{--        ]) !!}--}}
{{--    @endcan--}}
</div>
{!! Form::close() !!}
