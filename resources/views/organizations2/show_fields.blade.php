<!-- Created By Field -->
<div class="col-sm-12">
    {!! Form::label('created_by', __('organizations.fields.created_by').':') !!}
    <p>{{ $organization->created_by }}</p>
</div>


<!-- Org Name Field -->
<div class="col-sm-12">
    {!! Form::label('org_name', __('organizations.fields.org_name').':') !!}
    <p>{{ $organization->org_name }}</p>
</div>


<!-- About Field -->
<div class="col-sm-12">
    {!! Form::label('about', __('organizations.fields.about').':') !!}
    <p>{{ $organization->about }}</p>
</div>


<!-- No Of Teachers Field -->
<div class="col-sm-12">
    {!! Form::label('no_of_teachers', __('organizations.fields.no_of_teachers').':') !!}
    <p>{{ $organization->no_of_teachers }}</p>
</div>


<!-- No Of Students Field -->
<div class="col-sm-12">
    {!! Form::label('no_of_students', __('organizations.fields.no_of_students').':') !!}
    <p>{{ $organization->no_of_students }}</p>
</div>


<!-- Website Field -->
<div class="col-sm-12">
    {!! Form::label('website', __('organizations.fields.website').':') !!}
    <p>{{ $organization->website }}</p>
</div>


<!-- Facebook Field -->
<div class="col-sm-12">
    {!! Form::label('facebook', __('organizations.fields.facebook').':') !!}
    <p>{{ $organization->facebook }}</p>
</div>


<!-- Linkedin Field -->
<div class="col-sm-12">
    {!! Form::label('linkedin', __('organizations.fields.linkedin').':') !!}
    <p>{{ $organization->linkedin }}</p>
</div>


<!-- Latitude Field -->
<div class="col-sm-12">
    {!! Form::label('latitude', __('organizations.fields.latitude').':') !!}
    <p>{{ $organization->latitude }}</p>
</div>


<!-- Longitude Field -->
<div class="col-sm-12">
    {!! Form::label('longitude', __('organizations.fields.longitude').':') !!}
    <p>{{ $organization->longitude }}</p>
</div>


<!-- Logo Field -->
<div class="col-sm-12">
    {!! Form::label('logo', __('organizations.fields.logo').':') !!}
    <p>{{ $organization->logo }}</p>
</div>


<!-- Address1 Field -->
<div class="col-sm-12">
    {!! Form::label('address1', __('organizations.fields.address1').':') !!}
    <p>{{ $organization->address1 }}</p>
</div>


<!-- Address2 Field -->
<div class="col-sm-12">
    {!! Form::label('address2', __('organizations.fields.address2').':') !!}
    <p>{{ $organization->address2 }}</p>
</div>


<!-- City Field -->
<div class="col-sm-12">
    {!! Form::label('city', __('organizations.fields.city').':') !!}
    <p>{{ $organization->city }}</p>
</div>


<!-- State Field -->
<div class="col-sm-12">
    {!! Form::label('state', __('organizations.fields.state').':') !!}
    <p>{{ $organization->state }}</p>
</div>


<!-- Country Field -->
<div class="col-sm-12">
    {!! Form::label('country', __('organizations.fields.country').':') !!}
    <p>{{ $organization->country }}</p>
</div>


<!-- Zipcode Field -->
<div class="col-sm-12">
    {!! Form::label('zipcode', __('organizations.fields.zipcode').':') !!}
    <p>{{ $organization->zipcode }}</p>
</div>


<!-- Phone Field -->
<div class="col-sm-12">
    {!! Form::label('phone', __('organizations.fields.phone').':') !!}
    <p>{{ $organization->phone }}</p>
</div>


<!-- Mobile Field -->
<div class="col-sm-12">
    {!! Form::label('mobile', __('organizations.fields.mobile').':') !!}
    <p>{{ $organization->mobile }}</p>
</div>


