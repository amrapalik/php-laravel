@extends('layouts.vertical', ["page_title"=> "Purchase Details "])

@section('content')
<!-- Start Content-->
<div class="container-fluid">

    @include('layouts.shared/page-title', ['title' => "Payment Receipt"])
    @include('layouts.common.errors')
    <!-- Start Form  -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">@lang('orders.singular')</h4>
 <?php
        $user = auth()->user();
        $school_id = session('school_schid');
        $school = \App\Models\School::find($school_id);
        ?>
                     <?php 
//echo '<pre>';
//print_r($order[0]); 

?>

<div class="row my-3 justify-content-center">
             
                <div class="col-md-6">
                    <div class="card card-pricing" >
                        <div id="GFG" class="card-body text-center" style="border: 1px solid #0bc9e4;" >
                            <h2 class="card-pricing-price" style="color: red;
    font-size: 25px;">Payment Receipt </h2> <br><br>
                            <p class="card-pricing-plan-name fw-bold text-uppercase" style="color: green;">Your Licence Purchased Successfully</p>
                            <h3 class="mb-2"></h3>
                            <?php //die(); ?>
                            <p class="card-pricing-plan-name fw-bold text-uppercase">Transaction Id: {!! $order[0]['paypal_orderid'] !!}</p>

                            @if($user->drole != "Gamer")

                                 <p class="card-pricing-plan-name fw-bold text-uppercase">Buyer Name: {!! ucwords( $user->name) !!}</p>

                               <p class="card-pricing-plan-name fw-bold text-uppercase">School Name: {!! ucwords( $school->sch_name) !!}</p>

                             @endif

                             <p class="card-pricing-plan-name fw-bold text-uppercase">Amount: ${!! $order[0]['price'] !!}</p>

                             <p class="card-pricing-plan-name fw-bold text-uppercase">Licence Qty: {!! $order[0]['licences'] !!}</p>

                             <?php 
                             $product =   App\Models\Product::where('prodid',$order[0]['product_prodid'])->first();
                             ?>
                              <p class="card-pricing-plan-name fw-bold text-uppercase">Plan name: {!! $product->prod_name !!}</p>
                              <p class="card-pricing-plan-name fw-bold text-uppercase">Validity: {!! $product->validity !!} Year</p>


                           <!--  <h2 class="card-pricing-price">${!! $order[0]['price'] !!} </h2>  --><!-- <span>/ Month</span> -->
                           
                            <button onclick="printDiv()" class="btn btn-info waves-effect waves-light mt-4 mb-2 width-sm">Print</button>
                        </div>
                    </div> <!-- end Pricing_card -->
                </div> <!-- end col -->
                


            </div>

                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end form -->
</div> <!-- container -->

@endsection


@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
      <script>
        function printDiv() {
            var divContents = document.getElementById("GFG").innerHTML;
            var a = window.open('', '', 'height=500, width=300');
            a.document.write('<html>');
            a.document.write('<body > <h3>RealLives <br>');
            a.document.write(divContents);
            a.document.write('</body></html>');
            a.document.close();
            a.print();
        }
    </script>

   {!! JsValidator::formRequest('App\Http\Requests\CreateOrderRequest') !!}
@endsection
