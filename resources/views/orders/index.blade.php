@extends('layouts.vertical', ["page_title"=> __('orders.plural')])

@section('css')
    <!-- third party css -->
    <link href="{{asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
@endsection

<?php
$schoolid =  session('school_schid');
$user = auth()->user();
$school =  \App\Models\School::find($schoolid);

?> 
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">


        <!-- start page title -->
        @if($user->drole == 'School Admin' || $user->drole == 'Organization Admin')
        @if($school->school_type == 'family')
            @include('layouts.shared/page-title', ['title' => "  ".__('orders.Orders for').__($school->sch_name).__('gamedata.family')])
        @else
            @include('layouts.shared/page-title', ['title' => "  ".__('orders.Orders for').__($school->sch_name)])

        @endif
        <!-- end page title -->
        @else
            @include('layouts.shared/page-title', ['title' => "  ".__('orders.Orders for').__($user->firstname)])
        @endif
        @include('flash::message')

        <div class="clearfix"></div>

        <div class="row">  <!-- Data table-->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <!--  Title and buttons -->
                        <div class="row">
                            <div class="col-9">
                                <h4 class="header-title" style="float: left; margin-top: 10px">@lang('orders.plural')</h4>
                            </div>
                                <div class="col-3">

                                </div>
                        </div> <!-- / End Title and buttons -->

                        <p class="text-muted font-13 mb-4">
                        </p>

                        {!! $dataTable->table(['width' => '100%', 'class' => 'table dt-responsive nowrap w-100 ']) !!}
                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->
    </div> <!-- end container -->
@endsection


@section('script')
    @include('layouts.datatables_js')
    {{--    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>--}}
    {!! $dataTable->scripts() !!}
@endsection
