<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', __('orders.fields.user_id').':') !!}
    <p>{{ $order->user_id }}</p>
</div>


<!-- Product Prodid Field -->
<div class="col-sm-12">
    {!! Form::label('product_prodid', __('orders.fields.product_prodid').':') !!}
    <p>{{ $order->product_prodid }}</p>
</div>


<!-- Licences Field -->
<div class="col-sm-12">
    {!! Form::label('licences', __('orders.fields.licences').':') !!}
    <p>{{ $order->licences }}</p>
</div>


<!-- Price Field -->
<div class="col-sm-12">
    {!! Form::label('price', __('orders.fields.price').':') !!}
    <p>{{ $order->price }}</p>
</div>


<!-- Paypal Orderid Field -->
<div class="col-sm-12">
    {!! Form::label('paypal_orderid', __('orders.fields.paypal_orderid').':') !!}
    <p>{{ $order->paypal_orderid }}</p>
</div>


<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', __('orders.fields.status').':') !!}
    <p>{{ $order->status }}</p>
</div>


<!-- Paid Field -->
<div class="col-sm-12">
    {!! Form::label('paid', __('orders.fields.paid').':') !!}
    <p>{{ $order->paid }}</p>
</div>


<!-- Captured Field -->
<div class="col-sm-12">
    {!! Form::label('captured', __('orders.fields.captured').':') !!}
    <p>{{ $order->captured }}</p>
</div>


