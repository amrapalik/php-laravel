{!! Form::open(['route' => ['orders.destroy', $ordid], 'method' => 'delete']) !!}
<div class='btn-group'>

 @can('View_orders')
    <a href="{{ route('orders.show', $ordid) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
@endcan
     @can('Edit_orders')
    <a href="{{ route('orders.edit', $ordid) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
@endcan
     @can('Delete_orders')
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}

    @endcan
</div>
{!! Form::close() !!}
