<div class="row">
    <!-- User Id Field -->
    {!! Former::number('user_id')->label(__('orders.fields.user_id').':')->addGroupClass('col-sm-6')->placeholder(__('orders.fields.user_id').':') !!}
</div>



<div class="row">
    <!-- Product Prodid Field -->
    {!! Former::number('product_prodid')->label(__('orders.fields.product_prodid').':')->addGroupClass('col-sm-6')->placeholder(__('orders.fields.product_prodid').':') !!}
</div>



<div class="row">
    <!-- Licences Field -->
    {!! Former::number('licences')->label(__('orders.fields.licences').':')->addGroupClass('col-sm-6')->placeholder(__('orders.fields.licences').':') !!}
</div>



<div class="row">
    <!-- Price Field -->
    {!! Former::number('price')->label(__('orders.fields.price').':')->addGroupClass('col-sm-6')->placeholder(__('orders.fields.price').':') !!}
</div>



<div class="row">
    <!-- Paypal Orderid Field -->
    {!! Former::text('paypal_orderid')->label(__('orders.fields.paypal_orderid').':')->addGroupClass('col-sm-6')->placeholder(__('orders.fields.paypal_orderid').':') !!}
</div>



<div class="row">
    <!-- Status Field -->
    {!! Former::text('status')->label(__('orders.fields.status').':')->addGroupClass('col-sm-6')->placeholder(__('orders.fields.status').':') !!}
</div>



<div class="row">
    <!-- Paid Field -->
    {!! Former::checkbox('paid')->text(__('orders.fields.paid').':')->check()->addGroupClass('col-sm-4') !!}
</div>


<div class="row">
    <!-- Captured Field -->
    {!! Former::checkbox('captured')->text(__('orders.fields.captured').':')->check()->addGroupClass('col-sm-4') !!}
</div>

<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-info']) !!}
    <a href="{{ route('orders.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
