@extends('layouts.vertical', ["page_title"=> __('general.Data Learning Tool.Data Tools')])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

    @include('layouts.shared/page-title', ['title' => __('general.Choose a data tool')])

    @include('layouts.common.errors')

<!--        --><?php
//
//        $categories = \App\Models\Category::all();
//        ?>
  @include('flash::message')
  <p class="text-muted font-13 mb-4">
                       <span style="color:#00acc1">

                                 @lang("general.Data Learning Tool.You can 'live a life' according to the selected tool")</span>

                  </p>

        <div class="row">

            <div class="col-md-6">{{--UN's Sustainable Development Goals (SDGs) Tool--}}
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="d-flex" style="text-align: center;"><img src="/assets/images/tools/sdggoalstoolicon.svg" width="50px" style="margin-bottom: 13px;" alt="">
                                        <h3>@lang('general.Data Learning Tool.SDG tool title')</h3><br>
                                    </div>
                                         <h5>@lang('general.Description'):</h5>
                                        <span style="color:aliceblue">@lang('general.Data Learning Tool.Learn SDG status of Countries')<br>
                                            @lang('general.Data Learning Tool.Compare SDG country score and rank')<br>
                                            @lang('general.Data Learning Tool.SDG country comparative statement generator')
                                        </span>
                                        <div class="text-center">
                                            <a href="/sdgTool"> <button class="button-one" style="background-color: #36404a;border-radius: 0px;width: 200px;border-width: 0.1px;height: 37px;color: #0fc6e0;margin-top: 10px;margin-top: 41px;">@lang('general.Data Learning Tool.Experience') >></button></a>
                                        </div>

                                </div>
                            </div>

                        </div>
                    </div> <!-- end card-->
                {{-- </a> --}}
            </div> <!-- end col -->
            <div class="col-md-6">{{--Country Data Comparison Visualisation--}}
                {{-- <a href="/sdgTool"> --}}
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-12">
                                    {{-- <div class="d-flex"><img src="/assets/images/tools/sdggoalstoolicon.svg" width="50px" style="margin-bottom: 13px;" alt="">
                                         <h3 style="text-align: center" >@lang('general.Data Learning Tool.Country Disparity tool title')</h3><br>
                                    </div>      --}}
                                    <div class="d-flex" style="text-align: center;"><img src="/assets/images/tools/countrydisparitytoolicon.svg" width="50px" style="margin-bottom: 13px;" alt="">
                                        <h3>@lang('general.Data Learning Tool.Country Disparity tool title')</h3><br>
                                    </div>
                                         <h5>@lang('general.Description'):</h5>
                                        <span style="color:aliceblue">@lang('general.Data Learning Tool.Data Disparity Tool Desc')
                                        </span>
                                        <div class="text-center">
                                            <a href="/countryDisaparity"><button class="button-one" style="background-color: #36404a;border-radius: 0px;width: 200px;border-width: 0.1px;height: 37px;color: #0fc6e0;margin-top: 10px;margin-top: 41px;">@lang('general.Data Learning Tool.Experience') >></button></a>
                                        </div>    
                                </div>
                            </div>

                        </div>
                    </div> <!-- end card-->
                {{-- </a> --}}
            </div> <!-- end col -->


            <div class="col-md-6">{{--Compare Country Data--}}
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-12">
                                    <div class="d-flex" style="text-align: center;"><img src="/assets/images/tools/countrylearningtoolicon.svg" width="50px" style="margin-bottom: 13px;" alt="">
                                    <h3>@lang('general.Data Learning Tool.Compare Country Data')</h3><br>
                                    </div>
                                    <h5>@lang('general.Description'):</h5>
                                    <span style="color:aliceblue">@lang('general.Data Learning Tool.Compare Country Data Desc')
                                   </span>
                                   <div class="text-center">
                                    <a href="/educationTool"><button class="button-one" style="background-color: #36404a;border-radius: 0px;width: 200px;border-width: 0.1px;height: 37px;color: #0fc6e0;margin-top: 10px;margin-top: 41px;">@lang('general.Data Learning Tool.Experience') >></button></a>
                                   </div>
                           </div>
                            </div>

                        </div>
                    </div> <!-- end card-->
            </div> <!-- end col -->

          
            <div class="col-md-6">{{--Country Groups Comparison--}}
                {{-- <a href="/sdgTool"> --}}
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                

                                <div class="col-12">
                                    <div class="d-flex" style="text-align: center;"><img src="/assets/images/tools/sdggoalstoolicon.svg" width="50px" style="margin-bottom: 13px;" alt="">
                                         <h3>@lang('general.Data Learning Tool.Country Groups Comparison')</h3><br>
                                    </div>
                                         <h5>@lang('general.Description'):</h5>
                                        <span style="color:aliceblue">@lang('general.Data Learning Tool.Country Groups Comparison Desc')<br>
                                        </span>
                                        <div class="text-center">
                                            <a href="/countryGroup"><button class="button-one" style="background-color: #36404a;border-radius: 0px;width: 200px;border-width: 0.1px;height: 37px;color: #0fc6e0;margin-top: 10px;margin-top: 41px;">@lang('general.Data Learning Tool.Experience') >></button></a>
                                        </div>
                                </div>
                            </div>

                        </div>
                    </div> <!-- end card-->
                {{-- </a> --}}
            </div> <!-- end col -->
          

        </div>
        <!-- end row -->
        <!-- end form -->
    </div> <!-- container -->

@endsection


@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

    {{--   {!! JsValidator::formRequest('App\Http\Requests\CreateAssignmentRequest') !!}--}}
@endsection
