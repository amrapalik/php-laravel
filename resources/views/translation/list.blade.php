@extends('layouts.vertical', ["page_title"=> __('general.Create').__('users.singular')])

@section('css')
        <!-- third party css -->
        <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ag-grid-community@29.0.0/styles/ag-grid.css">
       <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ag-grid-community@29.0.0/styles/ag-theme-alpine.css">
        <!-- third party css end -->
@endsection

@section('content')
<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="row g-3">
                        <div class="col-md-4">
                            <select class="form-control" id="lang" name="lang" required>
                                <option value="">Select Lang</option>
                                @foreach($langs as $c => $n)
                                <option value="{{$c}}" {{$lang==$c?'selected="selected"':''}}>{{$n}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-5">
                            <select class="form-control" id="type" name="type" required>
                            <option value="">Select Type</option>
                            @foreach($types as $ty)
                            <option value="{{$ty}}" {{$type==$ty?'selected="selected"':''}}>{{ucwords($ty)}}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Show</button>
                        </div>
                    </form>
                </div>
                <h4 class="page-title">Manage Translations</h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 
    @if(session()->has('message'))
    <div class="alert alert-success">
    {{ session()->get('message') }}
    </div>
    @endif
    <div id="notifications"></div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div id="myGrid" style="height:80vh;width:100%;" class="ag-theme-alpine"></div>
            </div> <!-- end card-box -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->
</div> <!-- container -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-bs-dismiss="modal">&times;</button>
        <h4 class="modal-title">Examples</h4>
      </div>
      <div id="modal_body" class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<style>
    /* placing the footer on top */
    thead {display: table-header-group;}
    tfoot {display: table-header-group;}
    #con-close-modal .checkbox input[type=checkbox] {margin-left: -16px;}
    .mdi-square-edit-outline{color: #FF5722;}
    .mdi-account-circle{color: #4CAF50;}
    .ag-header-group-cell-with-group, .ag-header-cell {border-left: 1px solid #ccc}
    .rag-green-outer {background: rgb(194, 255, 194)!important;}
</style>

@endsection


@section('script')
    <script src="https://unpkg.com/ag-grid-community/dist/ag-grid-community.min.noStyle.js"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <!-- third party js ends -->
    <script type="text/javascript">

    //   export class LargeTextCellEditor implements ICellEditor {

    //     private eInput: HTMLTextAreaElement;
    //     private container: HTMLDivElement;
        
    //     constructor(public updateService: UpdateService) {
    //     }
        
    //     init(params:ICellEditorParams):void {
    //         this.container = document.createElement('div');
    //         this.container.style.color = "red";
    //         this.container.style.fontSize = "12px";
    //         this.container.style.marginTop = "5px";
    //         this.container.style.borderRadius = "15px";
    //         this.container.style.borderRadius = "15px";
    //         this.container.style.borderRadius = "15px";
    //         this.container.style.border = "1px solid grey";
    //         this.container.style.width = "300px";
    //         this.container.style.height = "100px";
    //         this.container.style.textAlign = "center";
    //         this.container.style.display = "inline-block";
    //         this.container.tabIndex = 0; // to allow the div to capture key presses
            
    //         // create the cell
    //         this.eInput = document.createElement('textarea');
    //         this.eInput.cols = 40;
    //         this.eInput.rows = 5;
    //         this.eInput.value = params.value;

    //         this.container.appendChild(this.eInput);
    //     }

    //     afterGuiAttached():void {
    //         this.eInput.focus();
    //         this.eInput.select();
    //     }

    //     destroy():void {
    //     }

    //     isPopup():boolean {
    //         return true;
    //     }

    //     getGui():HTMLElement {
    //         return this.container;
    //     }

    //     getValue():any {
    //         return this.eInput.value;
    //     }

    //     newValueHandler(params):void {
    //         this.updateService.update(params.data);
    //     }
    // }

        function CheckboxRenderer() {}

        CheckboxRenderer.prototype.init = function(params) {
         
          this.params = params;

          this.eGui = document.createElement('input');
          this.eGui.type = 'checkbox';
          this.eGui.checked = params.value;

          this.checkedHandler = this.checkedHandler.bind(this);
          this.eGui.addEventListener('click', this.checkedHandler);
        }

        CheckboxRenderer.prototype.checkedHandler = function(e) {
          let checked = e.target.checked;
          let colId = this.params.column.colId;
          this.params.node.setDataValue(colId, checked);
        }

        CheckboxRenderer.prototype.getGui = function(params) {
          return this.eGui;
        }

        CheckboxRenderer.prototype.destroy = function(params) {
          this.eGui.removeEventListener('click', this.checkedHandler);
        }


      function BtnCellRenderer() {}

      BtnCellRenderer.prototype.init = function(params) {
        
        this.params = params;
       
        this.eGui = document.createElement("button");
        if(params.value==1) {
          this.eGui.innerHTML = "Disable";
          this.eGui.classList.add("btn-danger");
        } else {
          this.eGui.innerHTML = "Enable";
          this.eGui.classList.add("btn-success");
        }
        
        this.eGui.classList.add("btn");
        this.eGui.classList.add("btn-xs");

        this.btnClickedHandler = this.btnClickedHandler.bind(this);
        this.eGui.addEventListener("click", this.btnClickedHandler);
      };

      BtnCellRenderer.prototype.getGui = function() {
        return this.eGui;
      };

      BtnCellRenderer.prototype.destroy = function() {
        this.eGui.removeEventListener("click", this.btnClickedHandler);
      };

      BtnCellRenderer.prototype.btnClickedHandler = function(event) {      
        this.params.clicked(this.params);
      };

        $(document).ready(function($) {
          
            var columnDefs = [
              
              {
                headerName: "#",
                valueGetter: "node.rowIndex + 1",
                width: 40,
                
              },
            
              
              //{headerName: "Is Approved", field: "is_approved", sortable: true, filter: true, cellStyle: {'white-space': 'normal'},autoHeight: true, cellRenderer: 'checkboxRenderer', width:'50px'},
              {headerName: "Source String", field: "source_string", sortable: true, filter: true, cellStyle: {'white-space': 'normal'},autoHeight: true,cellRenderer: params => {
                      if (params.data.source_string.includes("||")){
                        return params.data.source_string+'<br><a href="javascript:ShowExample(\''+params.data.identifier+'\')">View Example</a>';
                      }else{
                        return params.data.source_string;
                      }
                      
                  }},
              {headerName: "Translation", field: "translated_string", sortable: true, filter: true, editable: function(params){return params.data.is_approved !== true;}, cellStyle: {'white-space': 'normal'},autoHeight: true, cellEditor: 'agLargeTextCellEditor', cellEditorParams: {maxLength: '5000'}},
              {headerName: "Is Approved", field: "is_approved", sortable: true, filter: true, cellStyle: {'white-space': 'normal'},autoHeight: true, cellRenderer: 'checkboxRenderer', width:'50px'}
            ];

            var gridOptions = {
                defaultColDef:{singleClickEdit: true},
                columnDefs: columnDefs,
                onCellValueChanged: function(params) {
                    $.ajax({
                        type: "POST",
                        url: '/translation/saverow',
                        data: {data:params.data, '_token':'{{ csrf_token() }}'},
                        success: function( data ) {
                            console.log(data)
                        },
                    });
                },
                rowClassRules: {
                    'rag-green-outer': function(params) { return params.data.is_approved === true; },
                },
                components: {
                  btnCellRenderer: BtnCellRenderer,
                  checkboxRenderer: CheckboxRenderer
                },
                rowData: []
            };
            
            var eGridDiv = document.querySelector('#myGrid');
            new agGrid.Grid(eGridDiv, gridOptions);

            // agGrid.simpleHttpRequest({url: '/translation/ajaxlist?type={{$type}}&lang={{$lang}}'}).then(function(data) {
            //         gridOptions.api.setRowData(data.events);
            //         gridOptions.api.sizeColumnsToFit();
            //         gridOptions.api.resetRowHeights();
            // });
            
            fetch("/translation/ajaxlist?type={{$type}}&lang={{$lang}}")
       .then(response => response.json())
       .then(data => {
         // load fetched data into grid
         
         gridOptions.api.setRowData(data.events);
         gridOptions.api.sizeColumnsToFit();
         gridOptions.api.resetRowHeights();
       });
            
             

        });
        function ShowExample(id){
          $.ajax({
                        type: "POST",
                        url: '/translation/getexample',
                        data: {identifier:id, _token:'{{ csrf_token() }}'},
                        dataType: 'json',
                        success: function( data ) {
                          $("#modal_body").html(data.examples)
                          $("#myModal").modal('show');
                        },
                    });
         
        }    

    </script>

@endsection