@extends('layouts.master-admin')

@section('css')
        <!-- third party css -->
        <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ag-grid-community@29.0.0/styles/ag-grid.css">
       <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ag-grid-community@29.0.0/styles/ag-theme-alpine.css">
        <!-- third party css end -->
@endsection

@section('content')
<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">

                <h4 class="page-title">Manage Events</h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 
    @if(session()->has('message'))
    <div class="alert alert-success">
    {{ session()->get('message') }}
    </div>
    @endif
    <div id="notifications"></div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div id="myGrid" style="height:80vh;width:100%;" class="ag-theme-alpine"></div>
            </div> <!-- end card-box -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->
</div> <!-- container -->
<style>
    /* placing the footer on top */
    thead {display: table-header-group;}
    tfoot {display: table-header-group;}
    #con-close-modal .checkbox input[type=checkbox] {margin-left: -16px;}
    .mdi-square-edit-outline{color: #FF5722;}
    .mdi-account-circle{color: #4CAF50;}
    .ag-header-group-cell-with-group, .ag-header-cell {border-left: 1px solid #ccc}
</style>

@endsection


@section('script')
    <script src="https://unpkg.com/ag-grid-community/dist/ag-grid-community.min.noStyle.js"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <!-- third party js ends -->
    <script type="text/javascript">

    //   export class LargeTextCellEditor implements ICellEditor {

    //     private eInput: HTMLTextAreaElement;
    //     private container: HTMLDivElement;
        
    //     constructor(public updateService: UpdateService) {
    //     }
        
    //     init(params:ICellEditorParams):void {
    //         this.container = document.createElement('div');
    //         this.container.style.color = "red";
    //         this.container.style.fontSize = "12px";
    //         this.container.style.marginTop = "5px";
    //         this.container.style.borderRadius = "15px";
    //         this.container.style.borderRadius = "15px";
    //         this.container.style.borderRadius = "15px";
    //         this.container.style.border = "1px solid grey";
    //         this.container.style.width = "300px";
    //         this.container.style.height = "100px";
    //         this.container.style.textAlign = "center";
    //         this.container.style.display = "inline-block";
    //         this.container.tabIndex = 0; // to allow the div to capture key presses
            
    //         // create the cell
    //         this.eInput = document.createElement('textarea');
    //         this.eInput.cols = 40;
    //         this.eInput.rows = 5;
    //         this.eInput.value = params.value;

    //         this.container.appendChild(this.eInput);
    //     }

    //     afterGuiAttached():void {
    //         this.eInput.focus();
    //         this.eInput.select();
    //     }

    //     destroy():void {
    //     }

    //     isPopup():boolean {
    //         return true;
    //     }

    //     getGui():HTMLElement {
    //         return this.container;
    //     }

    //     getValue():any {
    //         return this.eInput.value;
    //     }

    //     newValueHandler(params):void {
    //         this.updateService.update(params.data);
    //     }
    // }

      function BtnCellRenderer() {}

      BtnCellRenderer.prototype.init = function(params) {
        this.params = params;

        this.eGui = document.createElement("button");
        if(params.value==1) {
          this.eGui.innerHTML = "Disable";
          this.eGui.classList.add("btn-danger");
        } else {
          this.eGui.innerHTML = "Enable";
          this.eGui.classList.add("btn-success");
        }
        
        this.eGui.classList.add("btn");
        this.eGui.classList.add("btn-xs");

        this.btnClickedHandler = this.btnClickedHandler.bind(this);
        this.eGui.addEventListener("click", this.btnClickedHandler);
      };

      BtnCellRenderer.prototype.getGui = function() {
        return this.eGui;
      };

      BtnCellRenderer.prototype.destroy = function() {
        this.eGui.removeEventListener("click", this.btnClickedHandler);
      };

      BtnCellRenderer.prototype.btnClickedHandler = function(event) {
        this.params.clicked(this.params);
      };

        $(document).ready(function($) {

            var columnDefs = [
              {
                headerName: "#",
                valueGetter: "node.rowIndex + 1",
                width: 100,
              },
              {headerName: "Name", field: "name", sortable: true, filter: true, cellStyle: {'white-space': 'normal'},autoHeight: true},
              {headerName: "Event Text", field: "event_str", sortable: true, filter: true, editable: false, cellStyle: {'white-space': 'normal'},autoHeight: true, cellEditor: 'agLargeTextCellEditor', cellEditorParams: {maxLength: '1000'}, width:600},
              {headerName: "Factoid", field: "factroid_str", sortable: true, filter: true, editable: true, cellStyle: {'white-space': 'normal'},autoHeight: true, cellEditor: 'agLargeTextCellEditor', cellEditorParams: {maxLength: '1000'}, width:600},
              {headerName: "Dairy Addline", field: "dairy_addline_str", sortable: true, filter: true, editable: true, cellStyle: {'white-space': 'normal'},autoHeight: true, cellEditor: 'agLargeTextCellEditor', cellEditorParams: {maxLength: '1000'}, width:600},
            ];

            var gridOptions = {
                defaultColDef:{singleClickEdit: true},
                columnDefs: columnDefs,
                onCellValueChanged: function(params) {
                    $.ajax({
                        type: "POST",
                        url: '/translation/saveeventrow',
                        data: {data:params.data, '_token':'{{ csrf_token() }}'},
                        success: function( data ) {
                            console.log(data)
                        },
                    });
                },
                components: {
                  btnCellRenderer: BtnCellRenderer
                },
                rowData: []
            };
            
            var eGridDiv = document.querySelector('#myGrid');
            new agGrid.Grid(eGridDiv, gridOptions);

            // agGrid.simpleHttpRequest({url: '/translation/eventajaxlist'}).then(function(data) {
            //         gridOptions.api.setRowData(data.events);
            //         //gridOptions.api.sizeColumnsToFit();
            //         gridOptions.api.resetRowHeights();
            // });   
            fetch("/translation/eventajaxlist")
       .then(response => response.json())
       .then(data => {
          gridOptions.api.setRowData(data.events);
          // gridOptions.api.sizeColumnsToFit();
          gridOptions.api.resetRowHeights();

       });     
           
        });
    </script>

@endsection
