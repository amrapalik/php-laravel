<!DOCTYPE html>
<html lang="en">
<style type="text/css">
    .form-check-input:checked {
    background-color: #4fc6e1 !important;
    border-color: #0c768e !important;
}
</style>
<head>

    @include('layouts.shared/title-meta', ['title' => "Log In"])
    @include('layouts.shared/head-css', ["mode" => $mode ?? '', "demo" => $demo ?? ''])


    <style>

        .game-background{
            background: url(../assets/images/RL-people-graphic.jpg);
            -khtml-opacity:.50;
            -moz-opacity:.50;
            -ms-filter:"alpha(opacity=50)";
            filter:alpha(opacity=50);
            filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0.5);
            opacity:.50;
        }

    </style>

</head>

<body class="loading game-background">

    <div class="account-pages mt-5 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-4">
                    <div class="card bg-pattern">

                        <div class="card-body p-4">

                            <div class="text-center w-75 m-auto">
                                <div class="auth-logo">
                                    <a href="{{route('gamedashboard')}}" class="logo logo-dark text-center">
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/rl_logo_black.svg')}}" alt="" height="62">
                                        </span>
                                    </a>

                                    <a href="{{route('gamedashboard')}}" class="logo logo-light text-center">
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/logo-light.png')}}" alt="" height="62">
                                        </span>
                                    </a>
                                </div>
                                <p class="text-muted mb-4 mt-3">Log in to access the RealLives dashboard</p>
                            </div>


                            @if(session('error'))<div class="alert alert-danger">{{ session('error') }}</div>
                            <br>@endif
                            @if(session('success'))<div class=" alert alert-success">{{ session('success') }}
                            </div>
                            <br>@endif

                            @if (sizeof($errors) > 0)
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li class="text-danger">{{ $error }}</li>
                                @endforeach
                            </ul>
                            @endif

                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="mb-3">
                                    <label for="username" class="form-label">Username</label>
                                    <input class="form-control" type="text" name="username" id="username" required="" placeholder="Enter your username">
                                </div>

                                <div class="mb-3">
                                    <label for="password" class="form-label">Password</label>
                                    <div class="input-group input-group-merge">
                                        <input type="password" name="password" id="password" class="form-control" placeholder="Enter your password">
                                        <div class="input-group-text" data-password="false">
                                            <span class="password-eye"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center d-grid">
                                    <button class="btn btn-info" type="submit"> Log In </button>
                                </div>
                            </form>

                        </div> <!-- end card-body -->
                    </div>
                    <!-- end card -->

                    <div class="row mt-3">
                        <div class="col-12 text-center">
                            <p> <a href="{{route('password.request')}}" class="text-white-50 ms-1">Forgot your password?</a></p>
                            <p class="text-white-50">Don't have an account? <a href="{{route('academic-registration')}}" class="text-white ms-1"><b>Sign Up</b></a></p>
                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->

                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end page -->

{{--    <footer class="footer footer-alt">--}}
{{--        2015 - <script>--}}
{{--            document.write(new Date().getFullYear())--}}
{{--        </script> &copy; UBold theme by <a href="" class="text-white-50">Coderthemes</a>--}}
{{--    </footer>--}}

    @include('layouts.shared/footer-script')


        <!-- Laravel Javascript Validation -->
        <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

        {!! JsValidator::formRequest('App\Http\Requests\Auth\LoginRequest') !!}


</body>

</html>
