<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('permissions.fields.name').':') !!}
    <p>{{ $permission->name }}</p>
</div>


<!-- Guard Name Field -->
<div class="col-sm-12">
    {!! Form::label('guard_name', __('permissions.fields.guard_name').':') !!}
    <p>{{ $permission->guard_name }}</p>
</div>


