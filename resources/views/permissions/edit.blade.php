@extends('layouts.vertical', ["page_title"=> __('general.Edit').__('permissions.singular')])

@section('content')
<!-- Start Content-->
<div class="container-fluid">

    @include('layouts.shared/page-title', ['title' => __('general.Edit').__('permissions.singular')])

    <!-- Start Form  -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">@lang('permissions.singular')</h4>


                    {!! Former::vertical_open_for_files()->id('edit-permissions-form')->populate($permission)->setOption('live_validation', true)
                    ->method('PATCH')->route('permissions.update', $permission->id) !!}

                        @include('permissions.fields')



                    {!! Former::close() !!}

                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end form -->
</div> <!-- container -->

@endsection


@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

    {!! JsValidator::formRequest('App\Http\Requests\UpdatePermissionRequest') !!}
@endsection
