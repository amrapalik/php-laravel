<?php
$user = auth()->user();

$school = \App\Models\School::find($user->school_schid)

?>
<style>

    .center
    {
        width: 100%;
        display:block;
        text-align: center;
    }

</style>


@if($school->purchased_licenses > 0)

    <a href="/playgame" class="center" ><img src="/assets/images/live-a-life-btn.svg" width="200px" style="margin-bottom: 18px;" alt=""></a>
    
    {{-- <a href="/educationTool" class="center" ><img src="/assets/images/education_button.svg" width="160px" alt=""></a><br>
    <a href="/sdgTool" class="center" ><img src="/assets/images/sdgTool_button.svg" width="160px" alt=""></a><br>
    <a href="/countryGroup" class="center" ><img src="/assets/images/countryGroup_button.svg" width="160px" alt=""></a><br>
    <a href="/countryDisaparity" class="center" ><img src="/assets/images/countryDisaparity_button.svg" width="160px" alt=""></a><br> --}}

    {{-- <a href="/educationTool"><button class="button-one" style="margin-left: 19px;background-color: #37424c;border-radius: 0px;width: 200px;border-width: 0.1px;height: 37px;color: #0fc6e0;">Education Tool</button></a><br>
    <a href="/sdgTool"></a><button class="button-one" style="margin-left: 19px;background-color: #37424c;border-radius: 0px;width: 200px;border-width: 0.1px;height: 37px;margin-top: 6px;color: #0fc6e0;">SDG Tool</button></a><br>
    <a href="/countryGroup"> <button class="button-one" style="margin-left: 19px;background-color: #37424c;border-radius: 0px;width: 200px;border-width: 0.1px;height: 37px;margin-top: 6px;color: #0fc6e0;">Country Group Tool</button></a>
    <a href="/countryDisaparity"><button class="button-one" style="margin-left: 19px;background-color: #37424c;border-radius: 0px;width: 200px;border-width: 0.1px;height: 37px;margin-top: 6px;color: #0fc6e0;margin-bottom: 10px;">Country Learning Tool</button></a> --}}

@else
    <a href="#"  onclick="showToast('')" class="center" ><img src="/assets/images/live-a-life-btn.svg" width="200px" alt=""></a>

    <script>
        function showToast(){
            toastr.error( "@lang('licenses.Purchase school licenses to play game')" , {timeOut: 6000});
        }
    </script>
@endif



<script>
    function student() {
        alert('Contact Your  Admin')
    }
    function purchase() {

        // window.open("/purchase_license_list","_self");
        window.location.replace("/purchase_license_list");

        //  alert('Purchase License First')
    }
</script>

    <li>
        <a href="/gamedashboard">
            <i data-feather="airplay"></i>
            <span> @lang('gamedashboards.dashboard') </span>
        </a>
    </li>


    <li>
        <a href="/dataTools">
            <i data-feather="airplay"></i>
            <span> @lang('general.Data Learning Tool.World Data + SDG Tools') </span>
        </a>
    </li>

    @can('View classes menu')

    <?php

    $cmenu= '';
    $clesson = '';
    $createlesson = '';
    $cassignment = '';
    $clpassignment = '';
    $cOther = '';
    $cOther_assignment = '';
    $class_lp_menu_active= '';
    $class_assignment_menu_active= '';
    $class_ot_menu_active= '';
	$collapse_lp = ''; $collapse_assignment = '';
	$collapse_ot='';
    if(Request::has('cmenu')  ){

        $clesson  = 'active';
        $collapse_lp = 'show';
        $class_lp_menu_active = 'menuitem-active' ;
    }
	else if(Route::is('assignment_subjects_for_lesson_plans')){
        $createlesson  = 'active';
        $collapse_lp = 'show';
        $class_lp_menu_active = 'menuitem-active' ;
	}
    elseif( Request::has('assignment') && Route::is('classes.index'))
    {
		if( Request::has('other')){
        	$cOther_assignment= 'active';
        	$collapse_ot = 'show';
        	$class_ot_menu_active = 'menuitem-active' ;
		}
		else{
        	$cassignment= 'active';
        	$collapse_assignment = 'show';
        	$class_assignment_menu_active = 'menuitem-active' ;
		}
    }
	elseif(Route::is('lessonPlans.index')){
		if(Request::has('assignment')){
			$class_lp_menu_active = 'menuitem-active';
			$clpassignment = 'active';
			$collapse_lp = 'show';
			$clessonplan = '';
		}
	}
    elseif( Request::has('other'))
    {
		if( Request::has('assignment'))
			$cOther_assignment = 'active';
		else
        	$cOther= 'active';
        $collapse_ot = 'show';
        //$class_ot_menu_active = 'menuitem-active' ;
    }
    else

    {
        $deallocated = '';
        $collapse = '';
    }

    ?>

 @can('View classes menu')
    @if($school->school_type != 'family')
        <li class="{{$class_lp_menu_active}}">
            <a href="#class_lp_menu" data-bs-toggle="collapse" class="collapsed" aria-expanded="false">
                <i data-feather="book-open"></i>
                <span> @lang('classes.Class with a lesson plan') </span>
                <span class="menu-arrow"></span>
            </a>
            <div class="collapse {{$collapse_lp}}" id="class_lp_menu" style="">
                <ul class="nav-second-level">
                        <li class="nav-item {{ $createlesson }}">
                            <a href="{{ route('assignment_subjects_for_lesson_plans') }}?create=lesson_plan"  class="nav-link {{ $createlesson }}">
                                <span> @lang('classes.Create a Class') </span>
                            </a>
                        </li>
                        <li class="nav-item {{-- $clesson --}}">
                            <a href="{{ route('classes.index') }}?cmenu=true"  class="nav-link {{ $clesson }}">
                                <span> @lang('classes.List') </span>
                            </a>
                        </li>
                        <li class="nav-item {{ $clpassignment }}">
                            <a href="{{ route('myLessonPlansWithAssignment') }}"  class="nav-link {{ $clpassignment }}">
                                @if($school->school_type == 'school')
                                    <span> @lang('lessonPlans.My Lesson Plan Repo') </span>
                                @else
                                    <span> @lang('lessonPlans.My Lesson Plan Repo department') </span>
                                @endif    
                            </a>
                        </li>
                        <li class="nav-item {{ $clpassignment }}">
                            <a href="{{ route('lessonPlansWithAssignment') }}"  class="nav-link {{ $clpassignment }}">
                                @if($school->school_type != 'family')
                                    @if($school->school_type == 'school')
                                        <span> @lang('lessonPlans.Lesson Plan Repo') </span>
                                    @else
                                        <span> @lang('lessonPlans.Lesson Plan Repo Department') </span>
                                    @endif    
          
                                @else
                                    <span> @lang('lessonPlans.Lesson Plan Repo family') </span>
                                @endif
                            </a>
                        </li>
                </ul>
            </div>
        </li>
    @endif    
    <li class="{{$class_assignment_menu_active}}">
        <a href="#class_assignment_menu" data-bs-toggle="collapse" class="collapsed" aria-expanded="false">
            
            <i data-feather="book-open"></i>
            @if($school->school_type == 'family')
                <span> @lang('classes.Global Citizenship Assignment') </span>
            @else
                <span> @lang('classes.Class with an assignment') </span>
            @endif    
            <span class="menu-arrow"></span>
        </a>
        <div class="collapse {{$collapse_assignment}}" id="class_assignment_menu" style="">
            <ul class="nav-second-level">

                    <li class="nav-item {{-- $clesson --}}">
                        <a href="{{ route('assignment_subjects') }}"  class="nav-link {{-- $clesson --}}">
                            @if($school->school_type == 'family')
                            <span> @lang('classes.Create an Assignment') </span>
                            @else
                            <span> @lang('classes.Create a Class') </span>
                            @endif
                        </a>
                    </li>
                    <li class="nav-item {{ $cassignment }}">

                        <a href="{{ route('classes.index') }}?assignment=true"
                           class="nav-link {{ $cassignment }}">
                           @if($school->school_type == 'family')
                                <span> @lang('classes.List of assignemtns') </span>
                           @else
                                <span> @lang('classes.List') </span> 
                            @endif
                        </a>
                    </li>
            </ul>
        </div>
    </li>
@endcan



   



 @can('View Other Teachers Classes menu')
 @if($school->school_type != 'family')
    <li class="{{$class_ot_menu_active}}">
        <a href="#class_ot_menu" data-bs-toggle="collapse" class="collapsed" aria-expanded="false">
            <i data-feather="book-open"></i>
            <span> @lang('classes.Other Teacher Classes') </span>
            <span class="menu-arrow"></span>
        </a>
        <div class="collapse {{$collapse_ot}}" id="class_ot_menu" style="">
            <ul class="nav-second-level">
                @if($school->school_type != 'family')
                    <li class="nav-item {{$cOther}}">
                        <a href="{{ route('classes.index') }}?other"
                           class="nav-link {{ $cOther }}">
                            <span> @lang('classes.Assignment + Lesson Plan classes')  </span>
                        </a>
                    </li>
                @endif    
                    <li class="nav-item {{$cOther_assignment}}">
                        <a href="{{ route('classes.index') }}?other&assignment=true"
                           class="nav-link {{ $cOther_assignment }}">
                            <span> @lang('classes.Assignment classes')  </span>
                        </a>
                    </li>
            </ul>
        </div>
    </li>
    @endcan
@endif    
@endcan






<li class=" ">
    <a href="#sidebarCrm3" data-bs-toggle="collapse" class="collapsed" aria-expanded="flase">
        <i data-feather="thumbs-up"></i>
        @if($school->school_type == 'family')
            <span> @lang('classes.My Joined Assignments')</span>
        @else
            <span> @lang('classes.My Joined Classes')</span>
        @endif    
        <span class="menu-arrow"></span>
    </a>
    <div class="collapse " id="sidebarCrm3" style="">
        <ul class="nav-second-level">
            @if($school->school_type != 'family')
                <li class="nav-item  {{ Request::is('assignedClasses*') ? 'active' : '' }}">
                    <a href="/assignedClasses">
                        <span> @lang('classes.Assignment + Lesson Plan classes') </span>
                    </a>
                </li>
            @endif    
            <li class="nav-item  {{ Request::is('myJoinedAssignmentClasses*') ? 'active' : '' }}">
                <a href="/myJoinedAssignmentClasses">
                    <span> @lang('classes.Assignment classes') </span>
                </a>
            </li>

        </ul>
    </div>
</li>
<?php
$lesson_plans_main_menu_active= '';
$calessonplan= '';
$clessonplan = '';
?>
@if($school->school_type != 'family')
    <li class="nav-item">
        <a href="{{ route('lessonPlans.index') }}"
        class="nav-link">
            <i data-feather="book"></i>
                <span> @lang('lessonPlans.plural')  </span>
            </a>
        </li>
@endif    
@if($school->school_type != 'family')
<li>
    <a href="{{ route('school_dashboard') }}">
           <i data-feather="grid"></i>
            <span> @lang('schools.School dashboard menu') </span>
        </a>
</li>
@endif

<li class="nav-item">
    <a href="{{ route('teachers_list') }}"
       class="nav-link {{ Request::is('teachers_list*') ? 'active' : '' }}">
        <i data-feather="users"></i>
        @if($school->school_type == 'school')
            <span> @lang('teachers.plural')  </span>
        @elseif($school->school_type == 'university_dept')    
            <span> @lang('teachers.Faculty List')  </span>
        @else 
            <span> @lang('teachers.Family Members')  </span>
        @endif
    </a>
</li>

<?php

$dmenu= '';
$deallocated = '';
$student_active = '' ;
$addChild='';
if(Request::has('deallocated')  ){

    $deallocated = 'active';
    $collapse = 'show';
    $student_active = 'menuitem-active' ;
}elseif( Request::has('dmenu'))
{
    $dmenu= 'active';
    $collapse = 'show';
    $student_active = 'menuitem-active' ;
}elseif( Request::has('addChild'))
{
    $addChild= 'active';
    $collapse = 'show';
    $student_active = 'menuitem-active' ;

}
else

{
    $deallocated = '';
    $collapse = '';
}

?>

@if(in_array($user->drole, ['School Admin','Organization Admin']))

@if($school->school_type == 'school' || $school->school_type == 'university_dept')
<li class="nav-item">
    <a href="/importTeachers"
       class="nav-link {{ Request::is('importTeachers*') ? 'active' : '' }}">
        <i data-feather="plus-square"></i>
        @if($school->school_type == 'school')
        <span>  @lang("teachers.Import Teachers") </span>
        @else
            <span> @lang('teachers.Import Faculty')  </span>
        @endif    
    </a>
</li>
@endif

@endif
<li class="  {!! $student_active !!}">
    <a href="#sidebarCrmw" data-bs-toggle="collapse" class="collapsed" aria-expanded="flase">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
        @if($school->school_type == 'family')
            <span> @lang('students.kids') </span>
        @else
            <span> @lang('students.plural') </span>
        @endif    
        <span class="menu-arrow"></span>
    </a>
    <div class="collapse {{ $collapse }}" id="sidebarCrmw" style="">
        <ul class="nav-second-level">

        @if($school->school_type == 'family')

            <li class="nav-item {{ $addChild }}">
                <a href="{{route('students.create')}}?addChild"
                class="nav-link {{ $addChild }}">
                    {{--                        <i data-feather="users"></i>--}}
                    <span> @lang('students.Add Children') </span>
                </a>
            </li>
        @endif

        @if($user->drole != 'Admin' && $school->school_type == 'school' || $school->school_type == 'university_dept')
        <li class="nav-item">
            <a href="/importStudents"
               class="nav-link {{ Request::is('importStudents*') ? 'active' : '' }}">
                {{--                        <i data-feather="users"></i>--}}
                <span> @lang('students.Import Students')   </span>
            </a>
        </li>
        @endif

            <li class="nav-item {{ $dmenu }}">
                <a href="{{ route('students.index') }}?dmenu=true"  class="nav-link {{ $dmenu }}">
                    {{--                        <i data-feather="message-square"></i>--}}
                    @if($school->school_type == 'family')
                        <span> @lang('students.allocated members') </span>
                    @else
                        <span> @lang('students.allocated students') </span>
                    @endif    
                </a>
            </li>
           
    

            <li class="nav-item {{ $deallocated }}">
                <a href="/students?deallocated=true"
                   class="nav-link {{ $deallocated }}">
                    {{--                        <i data-feather="users"></i>--}}
                    @if($school->school_type == 'family')
                        <span> @lang('students.Deallocated members') </span>
                    @else
                        <span> @lang('students.Deallocated Students')   </span>
                    @endif
                </a>
            </li>

           

        </ul>
    </div>
</li>

<li>
    <a href="#sidebarCrmw1" data-bs-toggle="collapse" class="collapsed" aria-expanded="flase">
        <i data-feather="briefcase"></i>
        <span> @lang('gamedata.My Gameplay Data') </span>
        <span class="menu-arrow"></span>
    </a>
    <div class="collapse" id="sidebarCrmw1" style="">
        <ul class="nav-second-level">
        <li class="nav-item">
                <a href="/Gameplay/BornReligion">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.BornReligion') </span>
                </a>
            </li>

           

            <li class="nav-item">
                <a href="/Gameplay/SdgComment">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.sdgComments') </span>
                </a>
            </li>

           

            <li class="nav-item">
                <a href="/Gameplay/SdgData">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.SdgData') </span>
                </a>
            </li>

            <li class="nav-item">
                <a href="/Gameplay/obituaries">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.obituarydata') </span>
                </a>
            </li>

            <li class="nav-item">
                <a href="/Gameplay/letters">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.letterdata') </span>
                </a>
            </li>

            <li class="nav-item">
                <a href="/Gameplay/BloodDonation">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.BloodDonation') </span>
                </a>
            </li>

            <li class="nav-item">
                <a href="/Gameplay/OrganDonation">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.OrganDonation') </span>
                </a>
            </li>


            <li class="nav-item">
                <a href="/Gameplay/FeedbackData">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.Feedback') </span>
                </a>
            </li>

            <li class="nav-item">
                <a href="/Gameplay/BugReportData">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.BugReport') </span>
                </a>
            </li>
        </ul>
    </div>
</li>







<li class="nav-item">
    <a href="/Gameplay/SchoolGameplay">
        <i data-feather="database"></i>
        @if($school->school_type == 'family')
            <span> @lang('gamedata.familydata') </span>
        @elseif($school->school_type == 'school')
            <span> @lang('gamedata.SchoolGameplay') </span>
        @else
            <span> @lang('gamedata.departmemntGameplay') </span>
        @endif    
    </a>
</li>



   







<!--
	<li class="{{$lesson_plans_main_menu_active}}">
		<a href="#lesson_plans_menu" data-bs-toggle="collapse" class="collapsed" aria-expanded="false">
            <i data-feather="book"></i>
            <span> @lang('lessonPlans.plural') </span>
            <span class="menu-arrow"></span>
        </a>
		<div class="collapse {{$collapse}}" id="lesson_plans_menu" style="">
            <ul class="nav-second-level">
                <li class="nav-item {{ $calessonplan }}">
                    <a href="{{ route('lessonPlansWithAssignment') }}">
                        <span> @lang('classes.Lesson plans with assignment') </span>
                    </a>
                </li>
			</ul>
		</div>
	</li>
-->




    







@can('View assignments menu')
    <li>
        <a href="{{ route('assignments.index') }}">
            <i data-feather="message-square"></i>
            <span> @lang('assignments.plural') </span>
        </a>
    </li>
@endcan
<li class=" ">
    <a href="#sidebarCrm4" data-bs-toggle="collapse" class="collapsed" aria-expanded="flase">
        <i data-feather="clock"></i>

        <span> @lang('licenses.License management')   </span>
        <span class="menu-arrow"></span>
    </a>
    <div class="collapse " id="sidebarCrm4" style="">
        <ul class="nav-second-level">

                @if(in_array($user->drole, ['School Admin','Organization Admin','Admin']))

                    <li>
                        <a href="{{ route('licenses.index') }}">
                            {{--                            <i data-feather="message-square"></i>--}}
                            <span> @lang('licenses.Active Licenses') </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('orders.index') }}">
{{--                            <i data-feather="menu"></i>--}}
                            <span> @lang('orders.plural') </span>
                        </a>
                    </li>

                @endif
                    @if(in_array($user->drole, ['School Admin','Organization Admin']))
                        <li class="nav-item  {{ Request::is('purchase_license_list*') ? 'active' : '' }}">
                            <a href="{{ route('purchase_license_list') }}">
                                {{--                        <i data-feather="dollar-sign"></i>--}}
                                <span> @lang('general.Buy license') </span>
                            </a>
                        </li>
                    @endif

        </ul>
    </div>
</li>



    <li>
        <a href="{{ route('bugs.index') }}/create">
            <i data-feather="message-square"></i>
            <span> @lang('bugs.Submit Bug') </span>
        </a>
    </li>




