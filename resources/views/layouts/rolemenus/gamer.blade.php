<?php
$user = auth()->user();
// $query = App\Models\License::find($user->id);
$query = App\Models\Order::where('user_id','=',$user->id)->where('status','=','COMPLETED')->count();
$license = App\Models\License::where('user_id','=',$user->id)->whereDate('expiry_date', '>=', Carbon\Carbon::now())->count();
?>
<style>

    .center
    {
        width: 100%;
        display:block;
        text-align: center;
    }

</style>
{{-- demo-life-btn --}}

    @if($user->purchaseflag == 'Yes')
        @if($license > 0)
            {{-- <a href="/playgame" class="center" ><img src="/assets/images/live-a-life-btn.svg" width="200px" alt=""></a> --}}
            <a href="/playgame" class="center" ><img src="/assets/images/live-a-life-btn.svg" width="200px" style="margin-bottom: 18px;" alt=""></a>
    
            {{-- <a href="/educationTool"><button class="button-one" style="margin-left: 19px;background-color: #37424c;border-radius: 0px;width: 200px;border-width: 0.1px;height: 37px;color: #0fc6e0;">Education Tool</button></a><br>
            <a href="/sdgTool"></a><button class="button-one" style="margin-left: 19px;background-color: #37424c;border-radius: 0px;width: 200px;border-width: 0.1px;height: 37px;margin-top: 6px;color: #0fc6e0;">SDG Tool</button></a><br>
            <a href="/countryGroup"> <button class="button-one" style="margin-left: 19px;background-color: #37424c;border-radius: 0px;width: 200px;border-width: 0.1px;height: 37px;margin-top: 6px;color: #0fc6e0;">Country Group Tool</button></a>
            <a href="/countryDisaparity"><button class="button-one" style="margin-left: 19px;background-color: #37424c;border-radius: 0px;width: 200px;border-width: 0.1px;height: 37px;margin-top: 6px;color: #0fc6e0;margin-bottom: 10px;">Country Learning Tool</button></a> --}}
        
        @else
            <a href="#"  onclick="showToast('')" class="center" ><img src="/assets/images/live-a-life-btn.svg" width="200px" alt=""></a>
        @endif       
    @elseif($user->purchaseflag == 'No' && $query > 0)
        <a href="#" onclick="purchase()" class="center" ><img src="/assets/images/live-a-life-btn.svg" width="200px" alt=""></a>
    @elseif($user->purchaseflag == 'No')
        <a href="/playgame" class="center" ><img src="/assets/images/demo-life-btn.svg" width="200px" alt=""></a>
    @endif


    <script>

        function purchase() {
            window.location.replace("/purchase_license_list");
           // alert('Purchase License First')
        }
        function showToast(){
            toastr.error( "@lang('licenses.licences_expired')" , {timeOut: 6000});
        }
    </script>



    <li>
        <a href="/gamedashboard">
            <i data-feather="airplay"></i>
            <span> @lang('gamedashboards.dashboard') </span>
        </a>
    </li>

    <li>
        <a href="/dataTools">
            <i data-feather="airplay"></i>
            <span> @lang('general.Data Learning Tool.World Data + SDG Tools') </span>
        </a>
    </li>

<li>
    <a href="#sidebarCrmw" data-bs-toggle="collapse" class="collapsed" aria-expanded="flase">
        <i data-feather="briefcase"></i>
        <span> @lang('gamedata.My Gameplay Data') </span>
        <span class="menu-arrow"></span>
    </a>
    <div class="collapse " id="sidebarCrmw" style="">
        <ul class="nav-second-level">
            
            <li class="nav-item">
                <a href="/Gameplay/BornReligion">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.BornReligion') </span>
                </a>
            </li>

            <li class="nav-item">
                <a href="/Gameplay/SdgComment">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.sdgComments') </span>
                </a>
            </li>


            <li class="nav-item">
                <a href="/Gameplay/SdgData">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.SdgData') </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="/Gameplay/obituaries">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.obituarydata') </span>
                </a>
            </li>

            <li class="nav-item">
                <a href="/Gameplay/letters">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.letterdata') </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="/Gameplay/BloodDonation">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.BloodDonation') </span>
                </a>
            </li>

            <li class="nav-item">
                <a href="/Gameplay/OrganDonation">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.OrganDonation') </span>
                </a>
            </li>


            <li class="nav-item">
                <a href="/Gameplay/FeedbackData">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.Feedback') </span>
                </a>
            </li>

            <li class="nav-item">
                <a href="/Gameplay/BugReportData">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.BugReport') </span>
                </a>
            </li>
        </ul>
    </div>
</li>

<li>
    <a href="{{ route('licenses.index') }}">
        <i data-feather="clock"></i>
        <span> @lang('licenses.Active Licenses') </span>
    </a>
</li>

<li>
    <a href="{{ route('orders.index') }}">
        <i data-feather="menu"></i>
        <span> @lang('orders.plural') </span>
    </a>
</li>

    @if($user->drole == 'Gamer' && $user->purchaseflag != 'Yes')

        <li>
            <a href="{{ route('purchase_license_list') }}">
                <i data-feather="dollar-sign"></i>
                <span> @lang('general.Buy license') </span>
            </a>
        </li>
    @endif



    <li>
        <a href="{{ route('bugs.index') }}/create">
            <i data-feather="message-square"></i>
            <span> @lang('bugs.Submit Bug') </span>
        </a>
    </li>








