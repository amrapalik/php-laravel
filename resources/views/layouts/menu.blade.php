<?php
$user = auth()->user();

$school = \App\Models\School::find($user->school_schid);
$org = \App\Models\Organization::find($user->organization_orgid);

?>

<style>

    .center
    {
        width: 100%;
        display:block;
        text-align: center;
    }

</style>

@if($user->drole != 'Admin' && $user->drole != 'Organization Admin')
    @if($school->purchased_licenses > 0)
        <a href="/playgame" class="center"><img src="/assets/images/live-a-life-btn.svg" width="200px" alt=""></a>
    @else
        <a href="#"  onclick="showToast('')"  class="center" ><img src="/assets/images/live-a-life-btn.svg" width="200px" alt=""></a>

    @endif
@endif



@if( $user->drole == 'Organization Admin')

    @php
        $lic =  \App\Models\School::where('organization_orgid','=',$user->organization_orgid)->sum('purchased_licenses');
    @endphp

    @if($lic > 0)
        <a href="/playgame" class="center"><img src="/assets/images/live-a-life-btn.svg" width="200px" alt=""></a>
    @else
        <a href="#"  onclick="showToast('')"  class="center" ><img src="/assets/images/live-a-life-btn.svg" width="200px" alt=""></a>
    @endif
@endif



<script>
    function showToast(){

        toastr.error( "@lang('licenses.Purchase school licenses to play game')" , {timeOut: 6000});
    }
</script>



@if($user->drole != 'Admin')
<li>
    <a href="/gamedashboard">
        <i data-feather="airplay"></i>
        <span> @lang('gamedashboards.dashboard') </span>
    </a>
</li>
@endif



@if($user->drole == 'Organization Admin' || $user->drole == 'School Admin' )



    <li>
    <a href="#sidebarCrmw1" data-bs-toggle="collapse" class="collapsed" aria-expanded="flase">
        <i data-feather="briefcase"></i>
        <span> @lang('gamedata.My Gameplay Data') </span>
        <span class="menu-arrow"></span>
    </a>
    <div class="collapse " id="sidebarCrmw1" style="">
        <ul class="nav-second-level">
           
            <li class="nav-item">
                <a href="/Gameplay/BornReligion">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.BornReligion') </span>
                </a>
            </li>

            <li class="nav-item">
                <a href="/Gameplay/SdgComment">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.sdgComments') </span>
                </a>
            </li>


            <li class="nav-item">
                <a href="/Gameplay/SdgData">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.SdgData') </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="/Gameplay/obituaries">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.obituarydata') </span>
                </a>
            </li>

            <li class="nav-item">
                <a href="/Gameplay/letters">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.letterdata') </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="/Gameplay/BloodDonation">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.BloodDonation') </span>
                </a>
            </li>

            <li class="nav-item">
                <a href="/Gameplay/OrganDonation">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.OrganDonation') </span>
                </a>
            </li>


            <li class="nav-item">
                <a href="/Gameplay/FeedbackData">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.Feedback') </span>
                </a>
            </li>

            <li class="nav-item">
                <a href="/Gameplay/BugReportData">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('gamedata.BugReport') </span>
                </a>
            </li>
        </ul>
    </div>
</li>


    @if($user->drole == 'Organization Admin')
        @php
        $schoolid = get_school_id_or_null();
        @endphp

        @if($schoolid != null)
        <li class="nav-item">
            <a href="/Gameplay/SchoolGameplay">
                <i data-feather="database"></i>
                <span> @lang('gamedata.SchoolGameplay') </span>
            </a>
        </li>
        @endif
    @else
    <li class="nav-item">
        <a href="/Gameplay/SchoolGameplay">
            <i data-feather="database"></i>
            <span> @lang('gamedata.SchoolGameplay') </span>
        </a>
    </li>
    @endif
@endif

{{--<li>--}}
{{--    <a href="/gamedashboard">--}}
{{--        <i data-feather="airplay"></i>--}}
{{--        <span> @lang('Dashboard') </span>--}}
{{--    </a>--}}
{{--</li>--}}


{{--Superadmin--}}
@can('View organizations menu')
<li class="nav-item">
    <a href="{{ route('organizations.index') }}"
       class="nav-link {{ Request::is('organizations*') ? 'active' : '' }}">
        <i data-feather="sunrise"></i>
        <span> @lang('organizations.plural')  </span>
    </a>
</li>
@endcan

@can('View products menu')
    <li>
        <a href="{{ route('products.index') }}">
            <i data-feather="message-square"></i>
            <span> @lang('products.plural') </span>
        </a>
    </li>
@endcan
{{-- End Super Admin Superadmin--}}





@can('View schools menu')
<li>
    <a href="{{ route('schools.index') }}">
        <i data-feather="sun"></i>
        @if($org && $org->org_type == 'university')
            <span> @lang('schools.Departments') </span>
        @else    
            <span> @lang('schools.plural') </span>
        @endif    
    </a>
</li>
@endcan

@if($user->drole == 'Admin')
<li>
    <a href="{{ route('users.index') }}">
        <i data-feather="users"></i>
        <span> @lang('users.plural') </span>   
    </a>
</li>
<li>
    <a href="{{ route('schools.index') }}">
        <i data-feather="sun"></i>
        @if($org && $org->org_type == 'university')
            <span> @lang('schools.Departments') </span>
        @else    
            <span> @lang('schools.plural') </span>
        @endif    
    </a>
</li>
@endif

@if( $user->drole == 'School Admin')

    <li>
           <a href="{{ route('school_dashboard') }}">
                  <i data-feather="grid"></i>
                   <span> @lang('schools.School dashboard menu') </span>
               </a>
       </li>
@endif
{{--<li>--}}
{{--    <a href="{{ route('schools.index') }}"--}}
{{--       class="nav-link {{ Request::is('schools*') ? 'active' : '' }}">--}}
{{--        <i data-feather="sun"></i>--}}
{{--        <span> @lang('schools.plural')  </span>--}}
{{--    </a>--}}
{{--</li>--}}

{{--@can('View users menu')--}}
{{--<li class="nav-item">--}}
{{--    <a href="{{ route('users.index') }}"--}}
{{--       class="nav-link {{ Request::is('users*') ? 'active' : '' }}">--}}
{{--        <i data-feather="users"></i>--}}
{{--        <span> @lang('users.plural')  </span>--}}
{{--    </a>--}}
{{--</li>--}}
{{--@endcan--}}



{{--<li class="nav-item">--}}
{{--    <a href="{{ route('students_list') }}"--}}
{{--       class="nav-link {{ Request::is('students_list*') ? 'active' : '' }}">--}}
{{--        <i data-feather="users"></i>--}}
{{--        <span> @lang('Studentss')  </span>--}}
{{--    </a>--}}
{{--</li>--}}


@can('View teachers menu')
    <li class="nav-item">
        
        <a href="{{ route('teachers_list') }}"
           class="nav-link {{ Request::is('teachers_list*') ? 'active' : '' }}">
            <i data-feather="users"></i>
            @if($org->org_type == 'organization')
                <span> @lang('teachers.plural')  </span>
            @else
                <span> @lang('teachers.Faculty List')  </span>
            @endif    
    
        </a>
    </li>
@endcan







    @if(in_array($user->drole, ['School Admin','Organization Admin']))

        <li class="nav-item">
            <a href="/importTeachers"
               class="nav-link {{ Request::is('importTeachers*') ? 'active' : '' }}">
                <i data-feather="plus-square"></i>
                @if($org->org_type == 'organization')
                <span>  @lang("teachers.Import Teachers") </span>
                @else
                    <span> @lang('teachers.Import Faculty')  </span>
                @endif    

            </a>
        </li>

    @endif






<?php

$dmenu= '';
$deallocated = '';
$student_active = '' ;
if(Request::has('deallocated')  ){

    $deallocated = 'active';
    $collapse = 'show';
    $student_active = 'menuitem-active' ;
}elseif( Request::has('dmenu'))
{
    $dmenu= 'active';
    $collapse = 'show';
    $student_active = 'menuitem-active' ;
}
else

{
    $deallocated = '';
    $collapse = '';
}

?>

@if(in_array($user->drole, ['School Admin','Admin']))

<li class="  {!! $student_active !!}">
    <a href="#sidebarCrmw" data-bs-toggle="collapse" class="collapsed" aria-expanded="flase">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
        <span> @lang('students.plural') </span>
        <span class="menu-arrow"></span>
    </a>
    <div class="collapse {{ $collapse }}" id="sidebarCrmw" style="">
        <ul class="nav-second-level">



            <li class="nav-item {{ $dmenu }}">
                <a href="{{ route('students.index') }}?dmenu=true"  class="nav-link {{ $dmenu }}">
                    {{--                        <i data-feather="message-square"></i>--}}
                    <span> @lang('students.plural') </span>
                </a>
            </li>

            {{--                <li class="nav-item">--}}
            {{--                    <a href="/students?deallocated=true"--}}
            {{--                       class="nav-link {{ Request::is('students*') ? 'active' : '' }}">--}}
            {{--                        <i data-feather="users"></i>--}}
            {{--                        <span> Deallocated Students  </span>--}}
            {{--                    </a>--}}
            {{--                </li>--}}




            <li class="nav-item {{ $deallocated }}">
                <a href="/students?deallocated=true"
                   class="nav-link {{ $deallocated }}">
                    {{--                        <i data-feather="users"></i>--}}
                    <span> @lang('students.Deallocated Students')   </span>
                </a>
            </li>


            @if($user->drole != 'Admin')
                <li class="nav-item">
                    <a href="/importStudents"
                       class="nav-link {{ Request::is('importStudents*') ? 'active' : '' }}">
                        {{--                        <i data-feather="users"></i>--}}
                        <span> @lang('students.Import Students')   </span>
                    </a>
                </li>
            @endif
            {{--            <li class="nav-item">--}}
            {{--                    <a href="/teachers_list"--}}
            {{--                       class="nav-link {{ Request::is('teachers_list*') ? 'active' : '' }}">--}}
            {{--                        <i data-feather="users"></i>--}}
            {{--                        <span> Teachers  </span>--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            {{--            @endif--}}


        </ul>
    </div>
</li>
@endif

@can('View classes menu')

    <?php

    $cmenu= '';
    $clesson = '';
    $cassignment = '';
    $cOther = '';
    $class_main_menu_active= '';
    if(Request::has('cmenu')  ){

        $clesson  = 'active';
        $collapse = 'show';
        $class_main_menu_active = 'menuitem-active' ;
    }
    elseif( Request::has('assignment'))
    {
        $cassignment= 'active';
        $collapse = 'show';
        $class_main_menu_active = 'menuitem-active' ;
    }
    elseif( Request::has('other'))
    {
        $cOther= 'active';
        $collapse = 'show';
        $class_main_menu_active = 'menuitem-active' ;
    }
    else

    {
        $deallocated = '';
        $collapse = '';
    }

    ?>


    <li class="{{$class_main_menu_active}}">
        <a href="#sidebarCrm" data-bs-toggle="collapse" class="collapsed" aria-expanded="flase">
            <i data-feather="book-open"></i>

{{--            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>--}}
            <span> @lang('classes.Create Classes') </span>
            <span class="menu-arrow"></span>
        </a>
        <div class="collapse {{$collapse}}" id="sidebarCrm" style="">
            <ul class="nav-second-level">
                {{--            @can('View classes menu')--}}
                                <li>
                                    <a href="{{ route('lessonPlans.index') }}">
{{--                                                                <i data-feather="message-square"></i>--}}
                                        <span> @lang('classes.Lesson plans') </span>
                                    </a>
                                </li>

                {{--                <li>--}}
                {{--                    <a href="/assignment_subjects">--}}
                {{--                        --}}{{--                        <i data-feather="book-open"></i>--}}
                {{--                        <span> @lang('classes.My Classes with Assignment') </span>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
                {{--            @endcan--}}


                @can('View classes menu')
                    <li class="nav-item {{ $clesson }}">
                        <a href="{{ route('classes.index') }}?cmenu=true"  class="nav-link {{ $clesson }}">
                            {{--                        <i data-feather="message-square"></i>--}}
                            <span> @lang('classes.My Classes') </span>
                        </a>
                    </li>

                    <li class="nav-item {{ $cassignment }}">

                        <a href="{{ route('classes.index') }}?assignment=true"
                           class="nav-link {{ $cassignment }}">
                            {{--                        <i data-feather="book-open"></i>--}}
                            <span> @lang('classes.My Classes with Assignment Menu') </span>
                        </a>
                    </li>
                @endcan

                @can('View Other Teachers Classes menu')
                    <li class="nav-item {{$cOther}}">
                        <a href="{{ route('classes.index') }}?other"
                           class="nav-link {{ $cOther }}">
                            {{--                        <i data-feather="users"></i>--}}
                            <span> @lang('classes.Other Teacher Classes')  </span>
                        </a>
                    </li>
                @endcan

            </ul>
        </div>
    </li>
@endcan


@if($user->drole == 'School Admin')
<li class=" ">
    <a href="#sidebarCrm3" data-bs-toggle="collapse" class="collapsed" aria-expanded="flase">
        <i data-feather="thumbs-up"></i>

        <span> @lang('classes.My Joined Classes')   </span>
        <span class="menu-arrow"></span>
    </a>
    <div class="collapse " id="sidebarCrm3" style="">
        <ul class="nav-second-level">

                <li class="nav-item  {{ Request::is('assignedClasses*') ? 'active' : '' }}">
                    <a href="/assignedClasses">
{{--                        <i data-feather="message-square"></i>--}}
                        <span> @lang('classes.Class with a lesson plan') </span>
                    </a>
                </li>

            </li>
            <li class="nav-item  {{ Request::is('myJoinedAssignmentClasses*') ? 'active' : '' }}">
                <a href="/myJoinedAssignmentClasses">
{{--                    <i data-feather="book-open"></i>--}}
                    <span> @lang('classes.class with an assignment') </span>
                </a>
            </li>


        </ul>
    </div>
</li>


@endif


<li class=" ">
    <a href="#sidebarCrm4" data-bs-toggle="collapse" class="collapsed" aria-expanded="flase">
        <i data-feather="clock"></i>

        <span> @lang('licenses.License management')   </span>
        <span class="menu-arrow"></span>
    </a>
    <div class="collapse " id="sidebarCrm4" style="">
        <ul class="nav-second-level">


                @if(in_array($user->drole, ['School Admin','Organization Admin','Admin']))

                    <li>
                        <a href="{{ route('licenses.index') }}">
                            {{--                            <i data-feather="message-square"></i>--}}
                            <span> @lang('licenses.Active Licenses') </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('orders.index') }}">
{{--                            <i data-feather="menu"></i>--}}
                            <span> @lang('orders.plural') </span>
                        </a>
                    </li>

                @endif

                    @if(in_array($user->drole, ['School Admin','Organization Admin']))
                        <li class="nav-item  {{ Request::is('purchase_license_list*') ? 'active' : '' }}">
                            <a href="{{ route('purchase_license_list') }}">
                                {{--                        <i data-feather="dollar-sign"></i>--}}
                                <span> @lang('general.Buy license') </span>
                            </a>
                        </li>
                    @endif

        </ul>
    </div>
</li>












{{--<li>--}}
{{--    <a href="{{ route('licenses.index') }}">--}}
{{--        <i data-feather="message-square"></i>--}}
{{--        <span> @lang('licenses.plural') </span>--}}
{{--    </a>--}}
{{--</li>--}}




@can('View permissions menu')
<li>
    <a href="{{ route('permissions.index') }}">
        <i data-feather="message-square"></i>
        <span> @lang('permissions.plural') </span>
    </a>
</li>
@endcan

@can('View roles menu')
<li>
    <a href="{{ route('roles.index') }}">
        <i data-feather="message-square"></i>
        <span> @lang('roles.plural') </span>
    </a>
</li>
@endcan












{{--@can('View lessonPlans menu')--}}
{{--<li>--}}
{{--    <a href="{{ route('lessonPlans.index') }}">--}}
{{--        <i data-feather="message-square"></i>--}}
{{--        <span> @lang('lessonPlans.plural') </span>--}}
{{--    </a>--}}
{{--</li>--}}
{{--@endcan--}}


{{--<li>--}}
{{--    <a href="{{ route('assignments.index') }}">--}}
{{--        <i data-feather="book-open"></i>--}}
{{--        <span> @lang('assignments.plural') </span>--}}
{{--    </a>--}}
{{--</li>--}}

{{--<li>--}}
{{--    <a href="{{ route('assignments.index') }}">--}}
{{--        <i data-feather="message-square"></i>--}}
{{--        <span> @lang('assignments.plural') </span>--}}
{{--    </a>--}}
{{--</li>--}}

@can('View assignments menu')
    <li>
        <a href="{{ route('assignments.index') }}">
            <i data-feather="message-square"></i>
            <span> @lang('assignments.plural') </span>
        </a>
    </li>
@endcan
{{--@can('View bugs menu')--}}

{{--@endcan--}}
@if($user->drole == 'Admin')
    <li>
        <a href="{{ route('bugs.index') }}">
            <i data-feather="message-square"></i>
            <span> @lang('bugs.Submit Bug') </span>
        </a>
    </li>
@endif
@if($user->drole != 'Admin')

<li>
    <a href="{{ route('bugs.index') }}/create">
        <i data-feather="message-square"></i>
        <span> @lang('bugs.Submit Bug') </span>
    </a>
</li>



@endif


@if($user->drole == 'Translator' || $user->drole == 'Admin')
<li>
    <a href="/translation/list">
        <i data-feather="message-square"></i>
        <span> Translations </span>
    </a>
</li>
@endif


{{--@can('View assignments')--}}
{{--<li>--}}
{{--    <a href="{{ route('assignments.index') }}">--}}
{{--        <i data-feather="message-square"></i>--}}
{{--        <span> @lang('assignments.plural') </span>--}}
{{--    </a>--}}
{{--</li>--}}
{{--@endcan--}}




