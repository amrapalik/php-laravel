<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

    <div class="h-100" data-simplebar>

        <!-- User box -->
        <div class="user-box text-center">
            <img src="{{asset('assets/images/users/user-9.jpg')}}" alt="user-img" title="Mat Helme" class="rounded-circle avatar-md">
            <div class="dropdown">
                <a href="javascript: void(0);" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-bs-toggle="dropdown">James Kennedy</a>
                <div class="dropdown-menu user-pro-dropdown">

                    <!-- item -->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-user me-1"></i>
                        <span>My Account</span>
                    </a>

                    <!-- item -->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-settings me-1"></i>
                        <span>Settings</span>
                    </a>


                    <!-- item -->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-lock me-1"></i>
                        <span>Lock Screen</span>
                    </a>

                    <!-- item -->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-log-out me-1"></i>
                        <span>Logout</span>
                    </a>

                </div>
            </div>
            <p class="text-muted">Admin Head</p>
        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul id="side-menu">
{{--            <li class="menu-title">Navigation</li>--}}
                @if(auth()->user()->drole == 'Gamer')
                    @include('layouts.rolemenus.gamer')
                @elseif(auth()->user()->drole == 'Student')
                    @include('layouts.rolemenus.student')
                @elseif(auth()->user()->drole == 'Teacher')
                    @include('layouts.rolemenus.teacher')
                @elseif(auth()->user()->drole == 'School Admin')
                    @include('layouts.rolemenus.schooladmin') 
                @else
                    @include('layouts.menu')
                @endif
            </ul>

            @if(auth()->user()->school_schid)
                <div class="div" style="text-align: center;">
                    @if(auth()->user()->drole == 'School Admin' || auth()->user()->drole == 'Organization Admin')
                    <a href="/upload_photo_form">
                        <img class="schoollogo" s src="/storage/schools/{{ auth()->user()->school_schid.'.jpg'}}" alt="">
                    </a>
                    @else
                        <img class="schoollogo" s src="/storage/schools/{{ auth()->user()->school_schid.'.jpg'}}" alt="">

                    @endif
                </div>
            @endif
        </div>
        <!-- End Sidebar -->



        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
