<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">

            <?php
            $school=0;
            $drole = auth()->user()->drole;
            if($drole == "School Admin" || $drole == "Teacher"|| $drole == "Student")
            {
                $school =    \App\Models\School::find(auth()->user()->school_schid);
            }
            elseif($drole == "Admin" || $drole == "Organization Admin"){
                $school =    \App\Models\School::find(session('school_schid'));
            }

            if($drole == "Organization Admin"){
                $schools =    \App\Models\School::where('organization_orgid','=',auth()->user()->organization_orgid)->get();

            }


            ?>

            @if($school)
                <div class="page-title-right">
                    <ol class="breadcrumb m-0" >

                        @if($drole == "Organization Admin")
                        <li class=" breadcrumb-item">

                            <select   id="setschoolid"  class="btn btn-info  waves-effect waves-light" style="background-color:#71d3ea00">
                                @if($school->school_type == 'school')
                                <option value=""> @lang('general.Change school')</option>
                                @else
                                <option value=""> @lang('general.Change Department')</option>
                                @endif    
                                @foreach($schools as $schoolss)
                                    <option value="{{ $schoolss->schid }}">{{ $schoolss->sch_name }}</option>
                                @endforeach
                            </select>
                        </li>
                        @endif

                        <li class="breadcrumb-item"><a href="javascript: void(0);" style="font-size: 30px; font-weight: bold;color:#00acc1">{!! $school->sch_name !!}</a></li>
                    </ol>
                </div>
            @endif



            <h4 class="page-title">{{$title ?? ''}}</h4>
        </div>
    </div>
</div>
<!-- end page title -->
