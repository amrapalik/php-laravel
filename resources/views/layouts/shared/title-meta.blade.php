<meta charset="utf-8" />
<title>{{$title ?? ''}} | RealLives </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="Real Lives Game." name="description" />
<meta content="Coderthemes" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- App favicon -->
<link rel="shortcut icon" href="../assets/images/favicon.ico">
