<!-- Topbar Start -->
<?php
$school = \App\Models\School::find(auth()->user()->school_schid);
$org = \App\Models\Organization::find(auth()->user()->organization_orgid);
?>
<div class="navbar-custom">
    <div class="container-fluid">



        <ul class="list-unstyled topnav-menu float-end mb-0">

            <li class="d-none d-lg-block">

                <div style="margin-top: 25px">


                    @if(auth()->user()->drole == 'Student')
                        @if(auth()->user()->deallocated == 0)
                            <p style="color: #2edd2e">@lang('licenses.Active Account')</p>
                        @else
                        <p style="color: red">@lang('licenses.Inactive Account')</p>
                        
                        @endif
                    @endif

                    @if(auth()->user()->drole == 'Gamer')
                        @if(auth()->user()->purchaseflag == 'Yes')
                            <p style="color: #2edd2e">@lang('licenses.Active Account')</p>
                        @else
                            <p style="color: red">Purchase License</p>
                        @endif
                    @endif

                    @if(auth()->user()->drole == 'School Admin' || auth()->user()->drole == 'Teacher')
                        @if($school->purchased_licenses > 0)
                            <p style="color: #2edd2e">Active Account</p>
                        @else
                            @if(auth()->user()->drole == 'School Admin')
                                <a href="/purchase_license_list" style="color: red">Purchase License</a>
                            @else
                                <p style="color: red">Purchase License</p>
                            @endif
                        @endif
                    @endif
                        &nbsp;&nbsp;&nbsp;
                </div>


            </li>

{{--            <li class="dropdown d-inline-block d-lg-none">--}}
{{--                <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">--}}
{{--                    <i class="fe-search noti-icon"></i>--}}
{{--                </a>--}}
{{--                <div class="dropdown-menu dropdown-lg dropdown-menu-end p-0">--}}
{{--                    <form class="p-3">--}}
{{--                        <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </li>--}}

{{--            <li class="dropdown d-none d-lg-inline-block">--}}
{{--                <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="fullscreen" href="#">--}}
{{--                    <i class="fe-maximize noti-icon"></i>--}}
{{--                </a>--}}
{{--            </li>--}}

{{--            <li class="dropdown d-none d-lg-inline-block topbar-dropdown">--}}
{{--                <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">--}}
{{--                    <i class="fe-grid noti-icon"></i>--}}
{{--                </a>--}}
{{--                <div class="dropdown-menu dropdown-lg dropdown-menu-end">--}}

{{--                    <div class="p-lg-1">--}}
{{--                        <div class="row g-0">--}}
{{--                            <div class="col">--}}
{{--                                <a class="dropdown-icon-item" href="#">--}}
{{--                                    <img src="{{asset('assets/images/brands/slack.png')}}" alt="slack">--}}
{{--                                    <span>Slack</span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="col">--}}
{{--                                <a class="dropdown-icon-item" href="#">--}}
{{--                                    <img src="{{asset('assets/images/brands/github.png')}}" alt="Github">--}}
{{--                                    <span>GitHub</span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="col">--}}
{{--                                <a class="dropdown-icon-item" href="#">--}}
{{--                                    <img src="{{asset('assets/images/brands/dribbble.png')}}" alt="dribbble">--}}
{{--                                    <span>Dribbble</span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row g-0">--}}
{{--                            <div class="col">--}}
{{--                                <a class="dropdown-icon-item" href="#">--}}
{{--                                    <img src="{{asset('assets/images/brands/bitbucket.png')}}" alt="bitbucket">--}}
{{--                                    <span>Bitbucket</span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="col">--}}
{{--                                <a class="dropdown-icon-item" href="#">--}}
{{--                                    <img src="{{asset('assets/images/brands/dropbox.png')}}" alt="dropbox">--}}
{{--                                    <span>Dropbox</span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="col">--}}
{{--                                <a class="dropdown-icon-item" href="#">--}}
{{--                                    <img src="{{asset('assets/images/brands/g-suite.png')}}" alt="G Suite">--}}
{{--                                    <span>G Suite</span>--}}
{{--                                </a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </li>--}}

            <li class="dropdown d-none d-lg-inline-block topbar-dropdown">
                <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    Change Language
                </a>
                <div class="dropdown-menu dropdown-menu-end">

                    <!-- item-->
                    <a href="/setLang/en" class="dropdown-item">
                          <span class="align-middle">English</span>
                    </a>
                    <!-- item-->
                    <a href="/setLang/ko" class="dropdown-item">
                        <span class="align-middle">Korean</span>
                    </a>

{{--                    <!-- item-->--}}
{{--                    <a href="/setLang/hi" class="dropdown-item">--}}
{{--                        <span class="align-middle">Hindi</span>--}}
{{--                    </a>--}}


                </div>
            </li>

            <li class="dropdown notification-list topbar-dropdown">
{{--                <a class="nav-link dropdown-toggle waves-effect waves-light" data-bs-toggle="dropdown" href="/notifications" role="button" aria-haspopup="false" aria-expanded="false"> --}}
                <a class="nav-link waves-effect waves-light" href="/notifications" role="button" aria-haspopup="false" aria-expanded="false">
                    <i class="fe-bell noti-icon"></i>
                    <span class="badge bg-danger rounded-circle noti-icon-badge">{{ auth()->user()->notifications->count() }}</span>
                </a>
{{--                <div class="dropdown-menu dropdown-menu-end dropdown-lg">--}}

{{--                    <!-- item-->--}}
{{--                    <div class="dropdown-item noti-title">--}}
{{--                        <h5 class="m-0">--}}
{{--                            <span class="float-end">--}}
{{--                                <a href="" class="text-dark">--}}
{{--                                    <small>Clear All</small>--}}
{{--                                </a>--}}
{{--                            </span>Notification--}}
{{--                        </h5>--}}
{{--                    </div>--}}

{{--                    <div class="noti-scroll" data-simplebar>--}}

{{--                        <!-- item-->--}}
{{--                        <a href="javascript:void(0);" class="dropdown-item notify-item active">--}}
{{--                            <div class="notify-icon">--}}
{{--                                <img src="{{asset('assets/images/users/user-1.jpg')}}" class="img-fluid rounded-circle" alt="" />--}}
{{--                            </div>--}}
{{--                            <p class="notify-details">Cristina Pride</p>--}}
{{--                            <p class="text-muted mb-0 user-msg">--}}
{{--                                <small>Hi, How are you? What about our next meeting</small>--}}
{{--                            </p>--}}
{{--                        </a>--}}

{{--                        <!-- item-->--}}
{{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
{{--                            <div class="notify-icon bg-primary">--}}
{{--                                <i class="mdi mdi-comment-account-outline"></i>--}}
{{--                            </div>--}}
{{--                            <p class="notify-details">Caleb Flakelar commented on Admin--}}
{{--                                <small class="text-muted">1 min ago</small>--}}
{{--                            </p>--}}
{{--                        </a>--}}

{{--                        <!-- item-->--}}
{{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
{{--                            <div class="notify-icon">--}}
{{--                                <img src="{{asset('assets/images/users/user-4.jpg')}}" class="img-fluid rounded-circle" alt="" />--}}
{{--                            </div>--}}
{{--                            <p class="notify-details">Karen Robinson</p>--}}
{{--                            <p class="text-muted mb-0 user-msg">--}}
{{--                                <small>Wow ! this admin looks good and awesome design</small>--}}
{{--                            </p>--}}
{{--                        </a>--}}

{{--                        <!-- item-->--}}
{{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
{{--                            <div class="notify-icon bg-warning">--}}
{{--                                <i class="mdi mdi-account-plus"></i>--}}
{{--                            </div>--}}
{{--                            <p class="notify-details">New user registered.--}}
{{--                                <small class="text-muted">5 hours ago</small>--}}
{{--                            </p>--}}
{{--                        </a>--}}

{{--                        <!-- item-->--}}
{{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
{{--                            <div class="notify-icon bg-info">--}}
{{--                                <i class="mdi mdi-comment-account-outline"></i>--}}
{{--                            </div>--}}
{{--                            <p class="notify-details">Caleb Flakelar commented on Admin--}}
{{--                                <small class="text-muted">4 days ago</small>--}}
{{--                            </p>--}}
{{--                        </a>--}}

{{--                        <!-- item-->--}}
{{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
{{--                            <div class="notify-icon bg-secondary">--}}
{{--                                <i class="mdi mdi-heart"></i>--}}
{{--                            </div>--}}
{{--                            <p class="notify-details">Carlos Crouch liked--}}
{{--                                <b>Admin</b>--}}
{{--                                <small class="text-muted">13 days ago</small>--}}
{{--                            </p>--}}
{{--                        </a>--}}
{{--                    </div>--}}

{{--                    <!-- All-->--}}
{{--                    <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">--}}
{{--                        View all--}}
{{--                        <i class="fe-arrow-right"></i>--}}
{{--                    </a>--}}

{{--                </div>--}}
{{--            </li>--}}

            <li class="dropdown notification-list topbar-dropdown">
                <a class="nav-link dropdown-toggle nav-user me-0 waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">

                    <img src="/storage/users/{{ auth()->user()->avatar }}" alt="user-image" class="rounded-circle">

                    <span class="pro-user-name ms-1">
                        {{ auth()->user()->name }} <i class="mdi mdi-chevron-down"></i>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-end profile-dropdown ">
                    <!-- item-->
                    <div class="dropdown-header noti-title">
                        <h6 class="text-overflow m-0"> {!! (auth()->user()->username) !!}</h6>
                    </div>

                    <div class="dropdown-header noti-title">
                        <h6 class="text-overflow m-0"> {!! ucfirst(auth()->user()->firstname) !!}</h6>
                    </div>
                    <div class="dropdown-header noti-title">
                        @if(auth()->user()->drole == 'Organization Admin')
                            @if($org->org_type == 'organization')
                                <h6 class="text-overflow m-0">  {!! __('users.fields.drole')  !!} :- {!! auth()->user()->drole !!}</h6>
                            @else
                                <h6 class="text-overflow m-0">  {!! __('users.fields.drole')  !!} :- {!! __('general.University Admin') !!}</h6>
                            @endif
                        @elseif(auth()->user()->drole == 'School Admin')    
                             @if($school->school_type == 'school')
                                <h6 class="text-overflow m-0">  {!! __('users.fields.drole')  !!} :- {!! auth()->user()->drole !!}</h6>
                            @elseif($school->school_type == 'university_dept')
                                <h6 class="text-overflow m-0">  {!! __('users.fields.drole')  !!} :- {!! __('general.Department Admin') !!}</h6>
                            @else  
                                <h6 class="text-overflow m-0">  {!! __('users.fields.drole')  !!} :- {!! __('general.Family Member') !!}</h6>  
                            @endif
                        @elseif(auth()->user()->drole == 'Teacher')    
                            @if($school->school_type == 'school')
                               <h6 class="text-overflow m-0">  {!! __('users.fields.drole')  !!} :- {!! auth()->user()->drole !!}</h6>
                           @else
                               <h6 class="text-overflow m-0">  {!! __('users.fields.drole')  !!} :- {!! __('general.Faculty Member') !!}</h6>
                           @endif  
                        @elseif(auth()->user()->drole == 'Student')
                            @if($school->school_type == 'family')
                                <h6 class="text-overflow m-0">  {!! __('users.fields.drole')  !!} :- {!! __('general.Child') !!}</h6>     
                            @else
                                <h6 class="text-overflow m-0">  {!! __('users.fields.drole')  !!} :- {!! auth()->user()->drole !!}</h6>
                            @endif    
                        @else   
                            <h6 class="text-overflow m-0">  {!! __('users.fields.drole')  !!} :- {!! auth()->user()->drole !!}</h6>
                        @endif
                    </div>

                    <!-- item-->

                    <a href="{{ route('profile') }}" class="dropdown-item notify-item">
                        <i class="fe-user"></i>
                        <span>@lang('general.My Account')</span>
                    </a>

                    <a href="/upload_photo_form" class="dropdown-item notify-item">
                        <i class="fe-image"></i>
                        <span>@lang('general.Edit Avatar')</span>
                    </a>


{{--                    <!-- item-->--}}
{{--                    <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
{{--                        <i class="fe-settings"></i>--}}
{{--                        <span>Settings</span>--}}
{{--                    </a>--}}

                    <!-- item-->
                   <!--  <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-lock"></i>
                        <span>Lock Screen</span>
                    </a> -->

                    <div class="dropdown-divider"></div>

                    <!-- item-->
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <a href="javascript:void(0);" class="dropdown-item notify-item" onclick="event.preventDefault();
                                                this.closest('form').submit();">
                            <i class="fe-log-out"></i>
                            <span>@lang('general.Logout')</span>
                        </a>
                    </form>

                </div>
            </li>

           <!--  <li class="dropdown notification-list">
                <a href="javascript:void(0);" class="nav-link right-bar-toggle waves-effect waves-light">
                    <i class="fe-settings noti-icon"></i>
                </a>
            </li> -->

        </ul>


        <!-- LOGO -->
        <div class="logo-box">
            <a href="/profile" class="logo logo-dark text-center">
                <span class="logo-sm">
                    <img src="{{asset('assets/images/logo-sm1.png')}}" alt="" height="22">
                    <!-- <span class="logo-lg-text-light">UBold</span> -->
                </span>
                <span class="logo-lg">
                    <img src="{{asset('assets/images/rl_logo_black.svg')}}" alt="" height="50">
                    <!-- <span class="logo-lg-text-light">U</span> -->
                </span>
            </a>

            <a href="{{route('gamedashboard')}}" class="logo logo-light text-center">
                <span class="logo-sm">
                    <img src="{{asset('assets/images/logo-sm1.png')}}" alt="" height="22">
                </span>
                <span class="logo-lg">
                    <img src="{{asset('assets/images/rl_logo.svg')}}" alt="" height="50">
                </span>
            </a>
        </div>

        <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
            <li>
                <button class="button-menu-mobile waves-effect waves-light">
                    <i class="fe-menu"></i>
                </button>
            </li>

            <li>
                <!-- Mobile menu toggle (Horizontal Layout)-->
                <a class="navbar-toggle nav-link" data-bs-toggle="collapse" data-bs-target="#topnav-menu-content">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </li>

{{--            <li class="dropdown d-none d-xl-block">--}}
{{--                <a class="nav-link dropdown-toggle waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">--}}
{{--                    Create New--}}
{{--                    <i class="mdi mdi-chevron-down"></i>--}}
{{--                </a>--}}
{{--                <div class="dropdown-menu">--}}
{{--                    <!-- item-->--}}
{{--                    <a href="javascript:void(0);" class="dropdown-item">--}}
{{--                        <i class="fe-briefcase me-1"></i>--}}
{{--                        <span>New Projects</span>--}}
{{--                    </a>--}}

{{--                    <!-- item-->--}}
{{--                    <a href="javascript:void(0);" class="dropdown-item">--}}
{{--                        <i class="fe-user me-1"></i>--}}
{{--                        <span>Create Users</span>--}}
{{--                    </a>--}}

{{--                    <!-- item-->--}}
{{--                    <a href="javascript:void(0);" class="dropdown-item">--}}
{{--                        <i class="fe-bar-chart-line- me-1"></i>--}}
{{--                        <span>Revenue Report</span>--}}
{{--                    </a>--}}

{{--                    <!-- item-->--}}
{{--                    <a href="javascript:void(0);" class="dropdown-item">--}}
{{--                        <i class="fe-settings me-1"></i>--}}
{{--                        <span>Settings</span>--}}
{{--                    </a>--}}

{{--                    <div class="dropdown-divider"></div>--}}

{{--                    <!-- item-->--}}
{{--                    <a href="javascript:void(0);" class="dropdown-item">--}}
{{--                        <i class="fe-headphones me-1"></i>--}}
{{--                        <span>Help & Support</span>--}}
{{--                    </a>--}}

{{--                </div>--}}
{{--            </li>--}}

{{--            <li class="dropdown dropdown-mega d-none d-xl-block">--}}
{{--                <a class="nav-link dropdown-toggle waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">--}}
{{--                    Mega Menu--}}
{{--                    <i class="mdi mdi-chevron-down"></i>--}}
{{--                </a>--}}
{{--                <div class="dropdown-menu dropdown-megamenu">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-sm-8">--}}

{{--                            <div class="row">--}}
{{--                                <div class="col-md-4">--}}
{{--                                    <h5 class="text-dark mt-0">UI Components</h5>--}}
{{--                                    <ul class="list-unstyled megamenu-list">--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Widgets</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Nestable List</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Range Sliders</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Masonry Items</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Sweet Alerts</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Treeview Page</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Tour Page</a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}

{{--                                <div class="col-md-4">--}}
{{--                                    <h5 class="text-dark mt-0">Applications</h5>--}}
{{--                                    <ul class="list-unstyled megamenu-list">--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">eCommerce Pages</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">CRM Pages</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Email</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Calendar</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Team Contacts</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Task Board</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Email Templates</a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}

{{--                                <div class="col-md-4">--}}
{{--                                    <h5 class="text-dark mt-0">Extra Pages</h5>--}}
{{--                                    <ul class="list-unstyled megamenu-list">--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Left Sidebar with User</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Menu Collapsed</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Small Left Sidebar</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">New Header Style</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Search Result</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Gallery Pages</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Maintenance & Coming Soon</a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-sm-4">--}}
{{--                            <div class="text-center mt-3">--}}
{{--                                <h3 class="text-dark">Special Discount Sale!</h3>--}}
{{--                                <h4>Save up to 70% off.</h4>--}}
{{--                                <button class="btn btn-primary btn-rounded mt-3">Download Now</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </li>--}}
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
<!-- end Topbar -->
