

<div class="row">
    <!-- Org Name Field -->
    {!! Former::text('org_name')->label(__('organizations.fields.org_name').':')->addGroupClass('col-sm-6')->placeholder('Org Name') !!}

    <!-- Org Email Field -->
    {!! Former::text('org_email')->label(__('organizations.fields.org_email').':')->addGroupClass('col-sm-6')->placeholder('Org Email') !!}
</div>



<div class="row">
    <!-- About Field -->
    {!! Former::textarea('about')->label(__('organizations.fields.about').':')->addGroupClass('col-sm-12 col-lg-12')->placeholder('About') !!}
</div>


<div class="row">
    <!-- No Of Teachers Field -->
    {!! Former::number('no_of_teachers')->label(__('organizations.fields.no_of_teachers').':')->addGroupClass('col-sm-3')->placeholder('No Of Teachers') !!}

    <!-- No Of Students Field -->
    {!! Former::number('no_of_students')->label(__('organizations.fields.no_of_students').':')->addGroupClass('col-sm-3')->placeholder('No Of Students') !!}

    <!-- Website Field -->
    {!! Former::text('website')->label(__('organizations.fields.website').':')->addGroupClass('col-sm-6')->placeholder('Website') !!}
</div>



<div class="row">
    <!-- Facebook Field -->
    {!! Former::text('facebook')->label(__('organizations.fields.facebook').':')->addGroupClass('col-sm-6')->placeholder('Facebook') !!}

    <!-- Linkedin Field -->
    {!! Former::text('linkedin')->label(__('organizations.fields.linkedin').':')->addGroupClass('col-sm-6')->placeholder('Linkedin') !!}
</div>





<div class="row">
    <!-- Address1 Field -->
    {!! Former::text('address1')->label(__('organizations.fields.address1').':')->addGroupClass('col-sm-6')->placeholder('Address1') !!}

    <!-- Address2 Field -->
    {!! Former::text('address2')->label(__('organizations.fields.address2').':')->addGroupClass('col-sm-6')->placeholder('Address2') !!}
</div>



<div class="row">
    <!-- City Field -->
    {!! Former::text('city')->label(__('organizations.fields.city').':')->addGroupClass('col-sm-3')->placeholder('City') !!}

    <!-- State Field -->
    {!! Former::text('state')->label(__('organizations.fields.state').':')->addGroupClass('col-sm-3')->placeholder('State') !!}

    <!-- Country Field -->
    {!! Former::text('country')->label(__('organizations.fields.country').':')->addGroupClass('col-sm-3')->placeholder('Country') !!}

    <!-- Zipcode Field -->
    {!! Former::text('zipcode')->label(__('organizations.fields.zipcode').':')->addGroupClass('col-sm-3')->placeholder('Zipcode') !!}
</div>



<div class="row">
    <!-- Phone Field -->
    {!! Former::text('phone')->label(__('organizations.fields.phone').':')->addGroupClass('col-sm-6')->placeholder('Phone') !!}

    <!-- Mobile Field -->
    {!! Former::text('mobile')->label(__('organizations.fields.mobile').':')->addGroupClass('col-sm-6')->placeholder('Mobile') !!}
</div>


<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-info']) !!}
    <a href="{{ route('organizations.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
