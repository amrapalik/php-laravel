@extends('layouts.no-sidebar', ["page_title"=> __('general.Create').__('organizations.singular')])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

    @include('layouts.shared/page-title', ['title' => "Register ".__('organizations.singular')])

    <!-- Start Form  -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <div class="text-center w-75 m-auto">
                            <div class="auth-logo">
                                <a href="{{route('gamedashboard')}}" class="logo logo-dark text-center">
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/rl_logo.svg')}}" alt="" height="62">
                                        </span>
                                </a>

                                <a href="{{route('gamedashboard')}}" class="logo logo-light text-center">
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/logo-light.png')}}" alt="" height="62">
                                        </span>
                                </a>
                            </div>

                        </div>

                        <h4 class="header-title">@lang('organizations.singular') Info</h4>

                        {!! Former::vertical_open_for_files()->id('create-organizations-form')->method('POST')->route('register-organisation-store') !!}

                        @include('organizations.forms')

                        {!! Former::close() !!}

                    </div> <!-- end card-body -->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
        <!-- end form -->
    </div> <!-- container -->

@endsection


@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

    {!! JsValidator::formRequest('App\Http\Requests\RegisterOrganizationRequest') !!}
@endsection
