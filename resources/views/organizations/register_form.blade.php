<div class="form-row">
    <div class="form-group col-md-6">
        <label for="name" class="col-form-label">Name</label>
        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="Name" name="name" required autofocus value="{{ old('name') }}">

        @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="website" class="col-form-label">Website</label>
        <input type="text" class="form-control{{ $errors->has('website') ? ' is-invalid' : '' }}" id="website" placeholder="Website URL" name="website" required value="{{ old('website') }}">

        @if ($errors->has('website'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('website') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="form-group">
    <label for="about" class="col-form-label">About</label>
    <textarea class="form-control" id="about" rows="3" name="about"></textarea>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="no_of_teachers" class="col-form-label">Number of Teachers</label>
        <input type="text" class="form-control{{ $errors->has('no_of_teachers') ? ' is-invalid' : '' }}" id="no_of_teachers" name="no_of_teachers" required value="{{ old('no_of_teachers') }}">

        @if ($errors->has('no_of_teachers'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('no_of_teachers') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="no_of_students" class="col-form-label">Number of Students</label>
        <input type="text" class="form-control{{ $errors->has('no_of_students') ? ' is-invalid' : '' }}" id="no_of_students" name="no_of_students" required value="{{ old('no_of_students') }}">

        @if ($errors->has('no_of_students'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('no_of_students') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="address1" class="col-form-label">Address 1</label>
        <input type="text" class="form-control{{ $errors->has('address1') ? ' is-invalid' : '' }}" id="address1" placeholder="1234 Main St" name="address1" required value="{{ old('address1') }}">

        @if ($errors->has('address1'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('address1') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="address2" class="col-form-label">Address 2</label>
        <input type="text" class="form-control" id="address2" name="address2" placeholder="Apartment, studio, or floor" value="{{ old('address2') }}">
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-3">
        <label for="city" class="col-form-label">City</label>
        <input type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" id="city" name="city" required value="{{ old('city') }}">

        @if ($errors->has('city'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('city') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group col-md-3">
        <label for="state" class="col-form-label">State/Region</label>
        <input type="text" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" id="state" name="state" required value="{{ old('state') }}">

        @if ($errors->has('state'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('state') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group col-md-3">
        <label for="country" class="col-form-label">Country</label>
        <input type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" id="country" name="country" required value="{{ old('country') }}">

        @if ($errors->has('country'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('country') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group col-md-3">
        <label for="zipcode" class="col-form-label">Zip Code</label>
        <input type="text" class="form-control{{ $errors->has('zipcode') ? ' is-invalid' : '' }}" id="zipcode" name="zipcode" required value="{{ old('zipcode') }}">

        @if ($errors->has('zipcode'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('zipcode') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-4">
        <label for="phone" class="col-form-label">Contact Number</label>
        <input type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" id="phone" name="phone" required value="{{ old('phone') }}">

        @if ($errors->has('phone'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('phone') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group col-md-4">
        <label for="latitude" class="col-form-label">Latitude</label>
        <input type="text" class="form-control{{ $errors->has('latitude') ? ' is-invalid' : '' }}" id="latitude" name="latitude" required value="{{ old('latitude') }}">

        @if ($errors->has('latitude'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('latitude') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group col-md-4">
        <label for="longitude" class="col-form-label">Longitude</label>
        <input type="text" class="form-control{{ $errors->has('longitude') ? ' is-invalid' : '' }}" id="longitude" name="longitude" required value="{{ old('longitude') }}">

        @if ($errors->has('longitude'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('longitude') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="facebook" class="col-form-label">Facebook URL</label>
        <input type="text" class="form-control" id="facebook" placeholder="https://..." name="facebook" value="{{ old('facebook') }}">
    </div>
    <div class="form-group col-md-6">
        <label for="linkedin" class="col-form-label">Linkedin URL</label>
        <input type="text" class="form-control" id="linkedin" name="linkedin" placeholder="https://..." value="{{ old('linkedin') }}">
    </div>
</div>

<p>&nbsp;</p>
<h4 class="header-title">INFORMATION OF ADMIN WHO WILL MANAGE REALLIVES ACCOUNT</h4>
<hr />
<p />

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="firstname" class="col-form-label">First Name</label>
        <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}" required>

        @if ($errors->has('firstname'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('firstname') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="lastname" class="col-form-label">Last Name</label>
        <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required>

        @if ($errors->has('lastname'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('lastname') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="designation" class="col-form-label">Designation</label>
        <input id="designation" type="text" class="form-control{{ $errors->has('designation') ? ' is-invalid' : '' }}" name="designation" value="{{ old('designation') }}" required>

        @if ($errors->has('designation'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('designation') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="phone_no" class="col-form-label">Phone</label>
        <input id="phone_no" type="text" class="form-control{{ $errors->has('phone_no') ? ' is-invalid' : '' }}" name="phone_no" value="{{ old('phone_no') }}" required>

        @if ($errors->has('phone_no'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('phone_no') }}</strong>
        </span>
        @endif

    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="email" class="col-form-label">Email</label>
        <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" required value="{{ old('email') }}">

        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif

    </div>
    <div class="form-group col-md-6">
        <label for="username" class="col-form-label">Username</label>
        <input type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" id="username" name="username" required value="{{ old('username') }}">

        @if ($errors->has('username'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('username') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="password" class="col-form-label">Password</label>
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="password-confirm" class="col-form-label">Confirm Password</label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="gender" class="col-form-label">Gender</label>
        <select class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender" value="{{ old('gender') }}" required>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
            <option value="Other">Other</option>
        </select>

        @if ($errors->has('gender'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('gender') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="birthDate" class="col-form-label">Birth Date</label>
        <input type="date" id="birthDate" class="form-control{{ $errors->has('birthDate') ? ' is-invalid' : '' }}" name="birthDate" value="{{ old('birthDate') }}" required>

        @if ($errors->has('birthDate'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('birthDate') }}</strong>
        </span>
        @endif
    </div>
</div>

<!-- <div class="form-group">
    <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="customCheck11">
        <label class="custom-control-label" for="customCheck11">Check this custom checkbox</label>
    </div>
</div> -->
<br />
<button type="submit" class="btn btn-primary waves-effect waves-light">Register</button>
