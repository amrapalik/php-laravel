@extends('layouts.vertical', ["page_title"=> __('classes.plural')])

@section('css')
    <!-- third party css -->
    <link href="{{asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
@endsection


@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->


    @if( Request::has('assignment'))
        @include('layouts.shared/page-title', ['title' => "  ".__('classes.Students of Assignment')])
    @else
        @include('layouts.shared/page-title', ['title' => "  ".__('classes.Students of Class')])
    @endif


    <!-- end page title -->

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="row">  <!-- Data table-->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!--  Title and buttons -->
                        <div class="row">
                            <div class="col-9">

                                @if( Request::has('assignment'))
                                    <h4 class="header-title" style="float: left; margin-top: 10px">@lang('classes.Students of Assignment')</h4>
                                @else
                                    <h4 class="header-title" style="float: left; margin-top: 10px">@lang('classes.Students of Class')</h4>
                                @endif

                            </div>
                            <div class="col-3">

                            </div>
                        </div> <!-- / End Title and buttons -->

                        <p class="text-muted font-13 mb-4">
                        </p>

                        {!! $dataTable->table(['width' => '100%', 'class' => 'table dt-responsive nowrap w-100 ']) !!}
                    </div> <!-- end card body-->
                    <div class="row">
                        <span class="col-sm-1"></span>

                        <a class="btn btn-primary col-sm-1" href="{{ URL::previous() }}">Back</a>
                    </div>
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->
    </div> <!-- end container -->
@endsection


@section('script')
    @include('layouts.datatables_js')
    {{--    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>--}}
    {!! $dataTable->scripts() !!}
@endsection
