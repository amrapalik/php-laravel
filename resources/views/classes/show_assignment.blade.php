@extends('layouts.vertical', ["page_title"=> __("classes.My Assigned Classes")])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">

                    </div>
{{--                    @if($classes->type == 'Assignment' ) --}}
                        <h4 class="page-title">@lang('classes.My Assigned Classes')</h4>
{{--                    @else
                        <h4 class="page-title">@lang('classes.singular')</h4>
                    @endif
--}}
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="content px-3">
            <div class="card">
                <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a href="#home" data-bs-toggle="tab" aria-expanded="true" class="nav-link active">
                                    Assignment
                                </a>
                            </li>
							@if ($classes->lessonplan_lpid)
                            <li class="nav-item">
                                <a href="#lessonplan" data-bs-toggle="tab" aria-expanded="false" class="nav-link">
                                    Lesson Plan
                                </a>
                            </li>
							@endif
						</ul>
					<div class="tab-content">
                    <div class="tab-pane  show active" id="home">
                            <?php
                            $comments = \App\Models\AssignmentComment::where('assignment_asid','=',$classes-> assignment_asid)->get();
                            ?>
                        @include('classes.show_assignment_fields')
                        <h5>Comments:</h5>
                        @foreach($comments as $comment)
                            <div class="row">
                                <div class="col-md-6">{{$comment->comment}} <br><br></div>
                            </div>
                        @endforeach


{{--                            <div class="tab-pane" id="messages">--}}
{{--                                <form method="POST" name="addComment" action="{{ URL('/add-comment-assignment') }}">--}}
{{--                                    @csrf--}}

{{--                                    <input type="hidden" name="assignment_asid" value="{{ $student_assignment->assignment_asid }}" />--}}
{{--                                    <input type="hidden" name="replied_to" value="NULL" />--}}
{{--                                    <input type="hidden" name="student_assignment_id" value="{{ $student_assignment->student_assignment_id    }}" />--}}

{{--                                    <textarea class="form-control" id="extension_activities" rows="3" name="comment"></textarea><br />--}}

{{--                                    <button type="submit" class="btn btn-info waves-effect waves-light">Add Comment</button>--}}

{{--                                </form>--}}
{{--                            </div>--}}

                    </div>
					@if ($classes->lessonplan_lpid)
					<div class="tab-pane" id="lessonplan">
						@php
							$lessonPlan = $classes->lesson_plan;
						@endphp
						@include('lesson_plans.show_fields')
					</div>
					@endif
					</div><!-- tab-content -->
                </div>
            </div>
        </div>
    </div>
@endsection
