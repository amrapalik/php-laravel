@extends('layouts.vertical', ["page_title"=> __('general.Create').__('classes.singular')])
@section('css')

    <link href="{{asset('assets/bootstrap-multiselect.css')}}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
    <link href="{{asset('assets/libs/multiselect/multiselect.min.css')}}" rel="stylesheet" type="text/css" />

    <style>
        .multiselect-container {
            width: 100% !important;
        }
    </style>

@endsection



@php

    $grades = array_values(array_unique(array_column($students, 'grade')));
    $school = \App\Models\School::find(auth()->user()->school_schid);
    $Grades_dropdown = [];
    foreach ($grades as $grade)
    {
     $Grades_dropdown[$grade] = $grade;
    }
//dd($teachers->toArray());
    $teachers_dropdown = [];

    foreach ($teachers2 as $ss)
    {
         //dd($teacherd->name);
     $teachers_dropdown[$ss->id] = $ss->name;
    }
  // dd($teachers_dropdown);
    $gradesNew = [];
    foreach ($students as $student) {
        //var_dump(array_intersect($grades, $student));
        $gradeValue = array_values(array_intersect($grades, $student));
        //var_dump($gradeValue);

        if (count($gradeValue) > 0) {
            $gradesNew[$gradeValue[0]][] = $student;
        }
    }

    $gradeCount = count($gradesNew);
    //dd($grades);
    //dd($gradesNew);

    $gradeOptions = array();

    $is_assignment = app('request')->has('assignment');

    if($is_assignment)
    {
       $asid = app('request')->input('asid');
       $assignment2 = \App\Models\Assignment::find($asid);
    }

    $is_lessonplan = app('request')->has('lessonplan');
    if ($is_lessonplan)
    {
        $lpid = app('request')->input('lpid');
        $lessonplan = \App\Models\LessonPlan::find($lpid);
    }

@endphp

@section('content')
<!-- Start Content-->
<div class="container-fluid">
    @if($school->school_type == 'family')
       @include('layouts.shared/page-title', ['title' => __('classes.Give An Assignment To Your Children')])
    @else
       @include('layouts.shared/page-title', ['title' => __('classes.Create a Class')])
    @endif
    <p class="text-muted font-13 mb-4">
        <span style="color:#00acc1">
            @if($school->school_type != 'family')
                @lang('classes.class requirement')</span>
            @else
                @lang('classes.class requirement family')</span>
            @endif    
   </p>

    @include('layouts.common.errors')
    @include('flash::message')

        <style>
            .multiselect-container{

            max-height:500px;
            overflow:auto;
            }
        </style>
    <!-- Start Form  -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
{{--                    <h4 class="header-title">@lang('classes.singular')</h4>--}}

                     {!! Former::vertical_open_for_files()->id('create-classes-form')->method('POST')->route('classes.store') !!}

                    @if($is_assignment == "true")
                        <h5>@lang('Assignment Name'):- {{ @$assignment2->asname }}</h5>
                    @endif

                    @if($is_lessonplan)
                        <h5>@lang('Lessonplan Name'):- {{ @$lessonplan->name }}</h5>
                    @endif

                    @if($is_assignment == "true")

                        {!! Former::hidden('type')->label(__('classes.fields.class_name').':')->value('Assignment')  ->addGroupClass('col-sm-6')->placeholder(__('classes.fields.class_name').':') !!}

                        {!! Former::hidden('assignment_asid')->label(__('classes.fields.class_name').':')->value($asid)->addGroupClass('col-sm-6')->placeholder(__('classes.fields.class_name').':') !!}


                    <!-- Category Field -->
                        {{--                        {!! Former::select('assignment_asid')->class("form-select")->options([''=>'Select Assignment'])->label(__('assignments.fields.asname').':')->addGroupClass('col-sm-3')->placeholder(__('assignments.fields.asname')) !!}--}}

                    @endif
                    @if($is_lessonplan)
                        {!! Former::hidden('type')->label(__('classes.fields.class_name').':')->value('lesson')  ->addGroupClass('col-sm-6')->placeholder(__('classes.fields.class_name').':') !!}


                        {!! Former::hidden('lessonplan_lpid')->label(__('classes.fields.class_name').':')->value($lpid)->addGroupClass('col-sm-6')->placeholder(__('classes.fields.class_name').':') !!}


                    @endif


                    <div class="row">
                        <!-- Class Name Field -->
                    {!! Former::text('class_name')->label(__('classes.fields.class_name').':')->addGroupClass('col-sm-6')->placeholder(__('classes.fields.class_name').':') !!}

                    <!-- Class Description Field -->
                        {!! Former::textarea('class_description')->label(__('classes.fields.class_description').':')->addGroupClass('col-sm-12 col-lg-6')->placeholder(__('classes.fields.class_description').':') !!}
                    </div>

                    <div class="row">
{{--                    <!-- Start Date Field -->--}}
{{--                        {!! Former::date('start_date')->label(__('classes.fields.start_date').':')->addGroupClass('col-sm-3')->placeholder('Start Date') !!}--}}

{{--                        @push('page_scripts')--}}
{{--                            <script type="text/javascript">--}}
{{--                                $('#start_date').datetimepicker({--}}
{{--                                    format: 'YYYY-MM-DD HH:mm:ss',--}}
{{--                                    useCurrent: true,--}}
{{--                                    sideBySide: true--}}
{{--                                })--}}
{{--                            </script>--}}
{{--                        @endpush--}}
                    <!-- End Date Field -->
                        {!! Former::date('end_date')->label(__('classes.fields.end_date').':')->addGroupClass('col-sm-3')->placeholder('End Date') !!}

                        @push('page_scripts')
                            <script type="text/javascript">
                                $('#end_date').datetimepicker({
                                    format: 'YYYY-MM-DD HH:mm:ss',
                                    useCurrent: true,
                                    sideBySide: true
                                })
                            </script>
                        @endpush

                    <!-- Age Group Field -->
                        {!! Former::text('age_group')->label(__('classes.fields.age_group').':')->addGroupClass('col-sm-3')->placeholder(__('classes.fields.age_group').':') !!}




                    </div>
                    <div class="row" style="height: 20px">
                        <br>  <br>  <br>
                    </div>

                    <div class="row">
                        <!-- Age Group Field -->
                        <div class="form-group col-12">
                            @if($school->school_type != 'family')
                                <label for="students" class="col-form-label">@lang('students.plural'): (@lang('classes.Only allocated students can be seen below'))</label>
                            @else
                                <label for="students" class="col-form-label">@lang('students.children'):</label>
                            @endif    
                            <select id="students" class="bigmultiselect" name="students[]" multiple="multiple" tabindex="-1" required>

                                @foreach( $students2 as $grade =>$val)
                                    <optgroup label="{{$grade}}">
                                        @foreach( $val as $stud)
                                            <option value="{{ $stud['id']  }}">{{ $stud['name'] }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-12 mt-2">
                            @if($school->school_type != 'family')
                                <label for="teachers" class="col-form-label">@lang('teachers.plural')</label>
                            @else
                                <label for="teachers" class="col-form-label">@lang('general.Family Member')</label>
                            @endif
                            <select id="teachers" class="bigmultiselect" name="teachers[]" multiple="multiple" tabindex="-1" required>


                                @foreach( $teachers_dropdown as $id =>$name)
                                    <option value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <br><br>
                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">

                        <button type="submit" name="submit" value="save" class="btn btn-info waves-effect waves-light">@lang('classes.Attach Lesson Plan + Assignment')</button>
                    </div> <br>
                    @include('flash::message')
                    <p class="text-muted font-13 mb-4">
                                         <span style="color:#00acc1">
                                            @if($school->school_type != 'family')
                                                @lang('classes.create class instruction')</span>
                                            @endif    
            
                                    </p>
                    <div class="clearfix"></div>
                    {!! Former::close() !!}

                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <br><br> <br><br> <br><br> <br><br> <br><br> <br><br> <br><br>
    <br><br> <br><br> <br><br> <br><br> <br><br> <br><br> <br><br>
    <!-- end form -->
</div> <!-- container -->

<style type="text/css">
    .ms-container {max-width: 100%}
</style>

@endsection


@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

    @if(Request::has('assignment'))
        {!! JsValidator::formRequest('App\Http\Requests\CreateClassesWithAssignmentRequest') !!}
    @else
        {!! JsValidator::formRequest('App\Http\Requests\CreateClassesRequest') !!}
    @endif

    <script src="/assets/bootstrap-multiselect.js"></script>

    <script>
        $('#students-error').hide();
        $(document)
            .on('click', 'form button[type=submit]', function(e) {

                var selectedStudents = $('#students').val();

                if(selectedStudents == '')
                {
                   // alert('Please select Students')
                    $('#students-error').show();
                    e.preventDefault();
                }

                // if($('#teachers').val() == '')
                // {
                //     alert('Please select Teachers')
                //     e.preventDefault();
                // }

                //alert(selectedStudents);

                var isValid = $(e.target).parents('form').isValid();
                if(!isValid) {
                    e.preventDefault(); //prevent the default action
                }
            });
    </script>

    <!-- Include the plugin's CSS and JS: -->
    <script type="text/javascript" src="/storage/dist/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="/storage/dist/css/bootstrap-multiselect.css" type="text/css"/>

    <script src="{{asset('assets/libs/multiselect/multiselect.min.js')}}"></script>
    <script src="{{asset('assets/libs/multiselect/jquery.quicksearch.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            $('.bigmultiselect').multiSelect({
              selectableHeader: "<div class='custom-header'><strong>Selectable</strong></div><input type='text' class='form-control' autocomplete='off' placeholder='Search Selectable'>",
              selectionHeader: "<div class='custom-header'><strong>Selected</strong></div><input type='text' class='form-control' autocomplete='off' placeholder='Search Selected'>",
              afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                  if (e.which === 40){
                    that.$selectableUl.focus();
                    return false;
                  }
                });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                  if (e.which == 40){
                    that.$selectionUl.focus();
                    return false;
                  }
                });
              },
              afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
              },
              afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
              }
            });

            $('#teachers').multiselect({
                enableClickableOptGroups: true,
                //   enableCollapsibleOptGroups: true,
                buttonClass: 'form-select',
                buttonWidth: '400px',
                templates: {
                    button: '<button type="button" class="multiselect dropdown-toggle" data-bs-toggle="dropdown"><span class="multiselect-selected-text"></span></button>',
                }
            });


        });


    </script>
@endsection
