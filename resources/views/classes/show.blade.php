@extends('layouts.vertical', ["page_title"=> "Form Components"])

@section('content')
 <!-- Start Content-->
    <div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">

                </div>
                @if($classes->type == 'Assignment' )
                    <h4 class="page-title">@lang('classes.My Assigned Classes')</h4>
                @else
                    <h4 class="page-title">@lang('classes.singular')</h4>
                @endif
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="content px-3">
        <div class="card">
            <div class="card-body">
                <div class="row">

                    @if($classes->type == 'Assignment' )
                        <?php
                       $student_assignment = \App\Models\StudentAssignment::where(['class_classid'=>$classes->classid,'student_id'=> auth()->user()->id ])->first();
                       $comments = \App\Models\AssignmentComment::where('student_assignment_id','=',$student_assignment->student_assignment_id)->get();
                      // dd($assignmentid->toarray());
                       ?>
                    @endif



                    @include('classes.show_fields')

                        <h5>Comments:</h5>

                        @foreach($comments as $comment)
                            <div class="row">
                                <div class="col-md-6">{{$comment->comment}} <br><br></div>
                            </div>
                        @endforeach

                        @if($classes->type == 'Assignment' )
                            <div class="tab-pane" id="messages">
                                <form method="POST" name="addComment" action="{{ URL('/add-comment-assignment') }}">
                                    @csrf

                                    <input type="hidden" name="assignment_asid" value="{{ $student_assignment->assignment_asid }}" />
                                    <input type="hidden" name="replied_to" value="NULL" />
                                    <input type="hidden" name="student_assignment_id" value="{{ $student_assignment->student_assignment_id }}" />

                                    <textarea class="form-control" id="extension_activities" rows="3" name="comment"></textarea><br />

                                    <button type="submit" class="btn btn-info waves-effect waves-light">Add Comment</button>
                                </form>
                            </div>
                        @endif
					HERE
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
