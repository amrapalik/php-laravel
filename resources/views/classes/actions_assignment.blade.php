{!! Form::open(['route' => ['classes.destroy', $classid], 'method' => 'delete']) !!}
<div class='btn-group'>


    <?php
    $assignment = app('request')->input('assignment');
    ?>


        @if($type == 'Assignment')

            <!-- TODO:: show data for non joined Assignment -->
{{--            <a href="{{ route('classes.index') }}/{!! $classid !!}" class='btn btn-default btn-xs'>--}}
{{--                <i class="fa fa-eye"></i>--}}
{{--            </a>--}}
        @else
            <a href="{{ route('lessonPlans.create') }}?class={!! $classid !!}" class='btn btn-default btn-xs'>
                <i class="fa fa-plus"></i>
            </a>
        @endif

    @can('View classes')
        <a href="{{ route('lessonPlans.index') }}?id={!! $classid !!}" class='btn btn-default btn-xs'>
            <i class="fa fa-eye"></i>
        </a>
    @endcan

{{--    @can('Edit classes')--}}
{{--        <a href="{{ route('classes.edit', $classid) }}" class='btn btn-default btn-xs'>--}}
{{--            <i class="fa fa-edit"></i>--}}
{{--        </a>--}}
{{--    @endcan--}}

{{--    @can('Delete classes')--}}
{{--        {!! Form::button('<i class="fa fa-trash"></i>', [--}}
{{--            'type' => 'submit',--}}
{{--            'class' => 'btn btn-danger btn-xs',--}}
{{--            'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'--}}
{{--        ]) !!}--}}
{{--    @endcan--}}
</div>
{!! Form::close() !!}
