{!! Form::open(['route' => ['classes.destroy', $class_classid], 'method' => 'delete']) !!}
<div class='btn-group'>


    <?php
    $assignment = app('request')->input('assignment');
    ?>



        <a href="{{ route('classes.index') }}/{!! $class_classid !!}" class='btn btn-default btn-xs'>
            <i class="fa fa-eye"></i>
        </a>


    @can('View classes')
        <a href="{{ route('lessonPlans.index') }}?id={!! $class_classid !!}" class='btn btn-default btn-xs'>
            <i class="fa fa-eye"></i>
        </a>
    @endcan

    {{--    @can('Edit classes')--}}
    {{--        <a href="{{ route('classes.edit', $classid) }}" class='btn btn-default btn-xs'>--}}
    {{--            <i class="fa fa-edit"></i>--}}
    {{--        </a>--}}
    {{--    @endcan--}}


</div>
{!! Form::close() !!}
