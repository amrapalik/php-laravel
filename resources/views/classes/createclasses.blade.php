@php

$grades = array_values(array_unique(array_column($students, 'grade')));
//dd($students);

foreach ($students as $student) {
//var_dump(array_intersect($grades, $student));
$gradeValue = array_values(array_intersect($grades, $student));
//var_dump($gradeValue);

if (count($gradeValue) > 0) {
$gradesNew[$gradeValue[0]][] = $student;
}
}

$gradeCount = count($gradesNew);
//dd($grades);
//dd($gradesNew);

$gradeOptions = array();

@endphp

 @extends('layouts.vertical', ["page_title"=> __('general.Create').__('classes.singular')])



@section('content')

<!-- Start Content-->
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h1 class="page-title">Create Class</h1>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card">
{{--                @include('layouts.messages')--}}
                <div class="card-body">
                    <h4 class="header-title">INFORMATION ABOUT YOUR CLASS</h4>
                    <hr />
                    <p />

                    <form method="POST" name="create_class" action="{{ URL('/create-class') }}">
                        @csrf
                        <input type="hidden" name="copied_from" value="{{ (is_numeric(Request::segment(2))) ? Request::segment(2) : 0 }}" />

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name" class="col-form-label">Class Name</label>
                                <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="Class Name" name="name" required autofocus value="{{ (isset($class->name) ? $class->name : old('name')) }}">

                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="grade" class="col-form-label">Grade / Standard</label>
                                <input type="text" class="form-control{{ $errors->has('grade') ? ' is-invalid' : '' }}" id="grade" placeholder="Grade / Standard" name="grade" required value="{{ (isset($class->grade) ? $class->grade : old('grade')) }}">

                                @if ($errors->has('grade'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('grade') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-form-label">Description</label>
                            <textarea class="form-control" id="description" rows="3" required name="description">{{ (isset($class->description) ? $class->description : old('description')) }}</textarea>

                            @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="division" class="col-form-label">Division</label>
                                <input type="text" class="form-control{{ $errors->has('division') ? ' is-invalid' : '' }}" id="division" placeholder="Division" name="division" required value="{{ (isset($class->division) ? $class->division : old('division')) }}">

                                @if ($errors->has('division'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('division') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="age_group" class="col-form-label">Age Group</label>
                                <input type="text" class="form-control{{ $errors->has('age_group') ? ' is-invalid' : '' }}" id="age_group" placeholder="Age Group" name="age_group" required value="{{ (isset($class->age_group) ? $class->age_group : old('age_group')) }}">

                                @if ($errors->has('age_group'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('age_group') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="start_dt" class="col-form-label">Start Date</label>
                                <input type="date" id="start_dt" class="form-control{{ $errors->has('start_dt') ? ' is-invalid' : '' }}" name="start_dt" value="{{ (isset($class->start_dt) ? $class->start_dt : old('start_dt')) }}" required>

                                @if ($errors->has('start_dt'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('start_dt') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="end_dt" class="col-form-label">End Date</label>
                                <input type="date" id="end_dt" class="form-control{{ $errors->has('end_dt') ? ' is-invalid' : '' }}" name="end_dt" value="{{ (isset($class->end_dt) ? $class->end_dt : old('end_dt')) }}" required>

                                @if ($errors->has('end_dt'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('end_dt') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">



                        <div class="mb-3">
                            <label for="example-multiselect" class="form-label">Multiple Select</label>
                            <select id="example-multiselect" multiple class="form-select">
                                @foreach ($grades as $grade)
                                    @php
                                        if (in_array($grade, $gradeOptions)) {
                                        break;
                                        }
                                    @endphp
                                    @php
                                        $gradeOptions[] = $grade;
                                    @endphp
                                    <optgroup label="Grade {{ $grade }}">
                                        @foreach ($gradesNew[$grade] as $gradeNew)

                                            <option value="{{ $gradeNew['id'] }}" {{ ($gradeNew['purchaseflag'] == 'F') ? '' : 'disabled=""' }} {{ (in_array($gradeNew['id'], $classUserStudents)) ? 'selected' : '' }}>{{ $gradeNew['name']}}
                                                <span>
                                                    {{ ($gradeNew['purchaseflag'] == 'F' ? '(Unregistered)' : 'dfdss') }}
                                                </span>
                                            </option>

                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="students" class="col-form-label">Select Students</label><br />
                                <span class="multiselect-native-select">
                                    <select id="classStudents" name=students[] multiple="multiple" required>

                                        @foreach ($grades as $grade)
                                        @php
                                        if (in_array($grade, $gradeOptions)) {
                                        break;
                                        }
                                        @endphp
                                        @php
                                        $gradeOptions[] = $grade;
                                        @endphp
                                        <optgroup label="Grade {{ $grade }}">
                                            @foreach ($gradesNew[$grade] as $gradeNew)

                                            <option value="{{ $gradeNew['id'] }}" {{ ($gradeNew['purchaseflag'] == 'Completed') ? '' : 'disabled=""' }} {{ (in_array($gradeNew['id'], $classUserStudents)) ? 'selected' : '' }}>{{ $gradeNew['name']}}
                                                <span>
                                                    {{ ($gradeNew['purchaseflag'] == null ? '(Unregistered)' : '') }}
                                                </span>
                                            </option>

                                            @endforeach
                                        </optgroup>
                                        @endforeach
                                    </select>
                                </span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="classTeachers" class="col-form-label">Select Teachers</label><br />
                                <span class="multiselect-native-select">
                                    <select id="classTeachers" name=teachers[] multiple="multiple" required>
                                        @foreach($teachers as $teacher)
                                        <option value="{{ $teacher->id }}" {{ (in_array($teacher->id, $classUserTeachers)) ? 'selected' : '' }}>{{ $teacher->name }}</option>
                                        @endforeach
                                    </select>
                                </span>
                            </div>
                        </div>

                        <br />
                        <button type="submit" name="submit" value="save" class="btn btn-primary waves-effect waves-light">Save Class</button>
                        <button type="submit" name="submit" value="save_n_create" class="btn btn-primary waves-effect waves-light">Save Class and Create Lesson Plan</button>
                    </form>

                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end row -->
</div>
<!-- end row-->
@endsection

@section('script')

<script type="text/javascript">
    // $(document).ready(function() {
    //     $('#classStudents').multiselect({
    //         enableClickableOptGroups: true,
    //         enableCollapsibleOptGroups: true,
    //         enableFiltering: true,
    //         includeSelectAllOption: true,
    //         collapseOptGroupsByDefault: true
    //     });
    //
    //     $('#classTeachers').multiselect({
    //         enableFiltering: true,
    //         includeSelectAllOption: true,
    //     });
    // });
</script>


@endsection
