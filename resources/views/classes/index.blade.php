@extends('layouts.vertical', ["page_title"=> __('classes.plural')])

@section('css')
    <!-- third party css -->
    <link href="{{asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
@endsection


@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->


    @if(Request::has('other') && Request::has('assignment'))
        @include('layouts.shared/page-title', ['title' => "  ".__('classes.Other Teacher Classes with Assignment')])
    @elseif(Request::has('assignment') &&  Request::has('other') == null)
        @include('layouts.shared/page-title', ['title' => "  ".__('classes.My Classes with Assignment')])
    @elseif(Request::has('other') && Request::has('assignment') == null)
        @include('layouts.shared/page-title', ['title' => "  ".__('classes.Other Teacher Classes with Assignment + Lesson Plan')])
    @else
        @include('layouts.shared/page-title', ['title' => "  ".__('classes.My Classes')])
    @endif

    <!-- end page title -->

        @include('flash::message')
        <p class="text-muted font-13 mb-4">
                             <span style="color:#00acc1">

                                       @lang('teachers.Click on') @lang("classes.Action")</i> @lang('classes.to check the status of given assignments')</span>

                        </p>
        <div class="clearfix"></div>

        <div class="row">  <!-- Data table-->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!--  Title and buttons -->
                        <div class="row">
                            <div class="col-8">
                                @if(Request::has('other') && Request::has('assignment'))
                                    <h4 class="header-title" style="float: left; margin-top: 10px">@lang('classes.Other Teacher Classes with Assignment')</h4>
                                @elseif(Request::has('assignment') &&  Request::has('other') == null)
                                    <h4 class="header-title" style="float: left; margin-top: 10px">@lang('classes.My Classes with Assignment')</h4>
                                @elseif(Request::has('other') && Request::has('assignment') == null)
                                    <h4 class="header-title" style="float: left; margin-top: 10px">@lang('classes.Other Teacher Classes with Assignment + Lesson Plan')</h4>
                                @else
                                    <h4 class="header-title" style="float: left; margin-top: 10px">@lang('classes.My Classes')</h4>
                                @endif
                            </div>
                                <div class="col-4">
                                    @can('Create classes')
                                        @if(Request::has('assignment'))
                                            <a href="/assignment_subjects" style="float: right" class="btn btn-success rounded-pill waves-effect waves-light">  @lang('classes.Create class with an assignment')</a>
                                        @else

                                            @if(!Request::has('other'))
                                                <a href="{{ route('lessonPlansWithAssignment') }}" style="float: right" class="btn btn-success rounded-pill waves-effect waves-light"> @lang('classes.Create Class with a lesson plan') </a>
                                            @endif
                                        @endif
                                    @endcan
                                </div>
                        </div> <!-- / End Title and buttons -->

                        <p class="text-muted font-13 mb-4">
                        </p>
                        {!! $dataTable->table(['width' => '100%', 'class' => 'table dt-responsive nowrap w-100 ']) !!}
                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->
    </div> <!-- end container -->
@endsection


@section('script')
    @include('layouts.datatables_js')
    {{--    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>--}}
    {!! $dataTable->scripts() !!}
@endsection
