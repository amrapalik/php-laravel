<!-- Class Name Field -->
<div class="col-sm-12">
    {!! Form::label('class_name', __('classes.fields.class_name').':') !!}
    <p>{{ $classes->class_name }}</p>
</div>


<!-- Class Description Field -->
<div class="col-sm-12">
    {!! Form::label('class_description', __('classes.fields.class_description').':') !!}
    <p>{{ $classes->class_description }}</p>
</div>

<?php

$game_token = app('request')->session()->get('game_token');

$usr = auth()->user();

if (in_array($usr->drole, ['School Admin','Teacher']))
{
    $school = \App\Models\School::find($usr->school_schid);

}
//dd($user_type);

?>

<div class="row">
    <div class="col-sm-3">
{{--        @if($classes->type == 'Assignment' ) --}}
            <?php
            $student_assignment = \App\Models\StudentAssignment::where(['class_classid'=>$classes->classid,'student_id'=> auth()->user()->id ])->first();
            $comments = \App\Models\AssignmentComment::where('student_assignment_id','=',$student_assignment->student_assignment_id)->get();
            //dd($student_assignment);
            //dd($usr);
            ?>
                @if($usr->drole == 'Student')
                    @if($usr->deallocated == 0)

                        @if($student_assignment->game_status == 'Pending')

                        <a class="btn btn-primary"   href="{{env('REALLIVES_GAME_URL')}}/createlifeWithAssignment?id={{$student_assignment->student_assignment_id}}&token={{$game_token}}"> @lang('assignments.play assignment')  </a>
                        @elseif($student_assignment->game_status == 'Incomplete')
                         <a class="btn btn-primary"   href="{{env('REALLIVES_GAME_URL')}}/loadgameanimation?flag=false&id={{$student_assignment->game_id}}&token={{$game_token}}"> @lang('assignments.load assignment')   </a>

                        {{-- <a class="btn btn-primary"   href="{{env('REALLIVES_GAME_URL')}}/createlifeWithAssignment?id={{$student_assignment->student_assignment_id}}&token={{$game_token}}"> @lang('assignments.play assignment')  </a> --}}
                        @elseif($student_assignment->game_status == 'Completed')
                         <a class="btn btn-primary"   href="{{env('REALLIVES_GAME_URL')}}/loadgameanimation?flag=false&id={{$student_assignment->game_id}}&token={{$game_token}}"> @lang('assignments.complete assignment')   </a>

                        @else
                            <a class="btn btn-primary"   href="{{env('REALLIVES_GAME_URL')}}/play-life?flag=false&id={{$student_assignment->game_id}}&token={{$game_token}}"> @lang('assignments.play assignment')   </a>
                        @endif
                    @else
                        @if($student_assignment->game_status == 'Pending')

                        <a class="btn btn-primary"   href="#" onclick="showToast('')"> @lang('assignments.play assignment')  </a>
                        @elseif($student_assignment->game_status == 'Incomplete')
                        <a class="btn btn-primary"   href="#" onclick="showToast('')"> @lang('assignments.load assignment')   </a>

                        {{-- <a class="btn btn-primary"   href="{{env('REALLIVES_GAME_URL')}}/createlifeWithAssignment?id={{$student_assignment->student_assignment_id}}&token={{$game_token}}"> @lang('assignments.play assignment')  </a> --}}
                        @elseif($student_assignment->game_status == 'Completed')
                        <a class="btn btn-primary"   href="#" onclick="showToast('')"> @lang('assignments.complete assignment')   </a>

                        @else
                            <a class="btn btn-primary"   href="#" onclick="showToast('')"> @lang('assignments.play assignment')   </a>
                        @endif
                    @endif
                @else

                    @if(@$school->purchased_licenses > 0)

                        @if($student_assignment->game_status == 'Pending')

                            <a class="btn btn-primary"   href="{{env('REALLIVES_GAME_URL')}}/createlifeWithAssignment?id={{$student_assignment->student_assignment_id}}&token={{$game_token}}"> @lang('assignments.play assignment')  </a>
                        @elseif($student_assignment->game_status == 'Incomplete')
                            <a class="btn btn-primary"   href="{{env('REALLIVES_GAME_URL')}}/loadgameanimation?flag=false&id={{$student_assignment->game_id}}&token={{$game_token}}"> @lang('assignments.load assignment')   </a>
                        @elseif($student_assignment->game_status == 'Completed')
                            <a class="btn btn-primary"   href="{{env('REALLIVES_GAME_URL')}}/loadgameanimation?flag=false&id={{$student_assignment->game_id}}&token={{$game_token}}"> @lang('assignments.complete assignment')   </a>
   
                        @else
                            <a class="btn btn-primary"   href="{{env('REALLIVES_GAME_URL')}}/play-life?flag=false&id={{$student_assignment->game_id}}&token={{$game_token}}"> @lang('assignments.play assignment')   </a>
                        @endif

                    @endif

                @endif

            <br><br>
{{--        @endif --}}
    </div>
</div>




<script>
function showToast(){

toastr.error( "@lang('licenses.account innactive please contact your teacher')" , {timeOut: 6000});
}
</script>
{{--<!-- Grade Field -->--}}
{{--<div class="col-sm-12">--}}
{{--    {!! Form::label('grade', __('classes.fields.grade').':') !!}--}}
{{--    <p>{{ $classes->grade }}</p>--}}
{{--</div>--}}


{{--<!-- Division Field -->--}}
{{--<div class="col-sm-12">--}}
{{--    {!! Form::label('division', __('classes.fields.division').':') !!}--}}
{{--    <p>{{ $classes->division }}</p>--}}
{{--</div>--}}


<!-- Age Group Field -->
<div class="col-sm-12">
    {!! Form::label('age_group', __('classes.fields.age_group').':') !!}
    <p>{{ $classes->age_group }}</p>
</div>


{{--<!-- Start Date Field -->--}}
{{--<div class="col-sm-12">--}}
{{--    {!! Form::label('start_date', __('classes.fields.start_date').':') !!}--}}
{{--    <p>{{ $classes->start_date->format('d-m-Y') }}</p>--}}
{{--</div>--}}


<!-- End Date Field -->
<div class="col-sm-12">
    {!! Form::label('end_date', __('classes.fields.end_date').':') !!}
    <p>{{ $classes->end_date->format('d-m-Y') }}</p>
</div>


<!-- Created By Field -->
<div class="col-sm-12">
    {!! Form::label('created_by', __('classes.fields.created_by').':') !!}
    <p>{{ $classes->createdby->name }}</p>
</div>


@if($classes->type == 'Assignment' )
<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', __('classes.fields.gamestatus').':') !!}
    <p>{{ $student_assignment->game_status }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', __('classes.fields.letterstatus').':') !!}
    <p>{{ $student_assignment->game_status }}</p>
</div>

@endif
