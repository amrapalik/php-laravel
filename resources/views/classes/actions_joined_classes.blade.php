{!! Form::open(['route' => ['classes.destroy', $classid], 'method' => 'delete']) !!}
<div class='btn-group'>


    <?php
    $assignment = app('request')->input('assignment');
    ?>


    @if($type == 'Assignment')
        <a href="{{ route('classes.index') }}/{!! $classid !!}?type=lp" class='btn btn-default btn-xs'>
            <i class="fa fa-eye"></i>
        </a>

        @else
            <a href="{{ route('classes.index') }}/{!! $classid !!}?type=as"  class='btn btn-default btn-xs'>
                <i class="fa fa-eye"></i>
            </a>

        @endif
{{--        <a href="{{ route('lessonPlans.create') }}?class={!! $classid !!}" class='btn btn-default btn-xs'>--}}
{{--            <i class="fa fa-plus"></i>--}}
{{--        </a>--}}
{{--   // @endif--}}





    {{--    @can('Edit classes')--}}
    {{--        <a href="{{ route('classes.edit', $classid) }}" class='btn btn-default btn-xs'>--}}
    {{--            <i class="fa fa-edit"></i>--}}
    {{--        </a>--}}
    {{--    @endcan--}}

</div>
{!! Form::close() !!}
