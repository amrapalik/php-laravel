{!! Form::open(['route' => ['classes.destroy', $classid], 'method' => 'delete']) !!}
<div class='btn-group'>

    <?php
    $assignment = app('request')->input('assignment');

    $other = app('request')->input('other');
    ?>



        @if($type == 'Assignment')
        <a href="{{ route('assignmentstudents') }}?class={{$classid}}" class='btn btn-default btn-xs'>
            <i class="fa fa-list" title="Joined Students to Assignment"></i>
        </a>
        @else

            <a href="{{ route('classtudents') }}?class={{$classid}}" class='btn btn-default btn-xs'>
                <i class="fa fa-list" title="Joined Students Lesson plan"></i>
            </a>


            <a href="{{ route('classes.index') }}/{!! $classid !!}" title="See Lesson Plan" class='btn btn-default btn-xs'>
                <i class="fa fa-eye"></i>
            </a>

        @endif

    @can('View classes')
        <a href="{{ route('lessonPlans.index') }}?class={!! $classid !!}" class='btn btn-default btn-xs'>
            <i class="fa fa-eye"></i>
        </a>
    @endcan

{{--    @can('Edit classes')--}}
{{--        <a href="{{ route('classes.edit', $classid) }}" class='btn btn-default btn-xs'>--}}
{{--            <i class="fa fa-edit"></i>--}}
{{--        </a>--}}
{{--    @endcan--}}

{{--        @if(!Request::has('other'))--}}

{{--            @can('Delete classes')--}}
{{--            {!! Form::button('<i class="fa fa-trash"></i>', [--}}
{{--                'type' => 'submit',--}}
{{--                'class' => 'btn btn-danger btn-xs',--}}
{{--                'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'--}}
{{--            ]) !!}--}}
{{--            @endcan--}}
{{--        @endif--}}
</div>
{!! Form::close() !!}
