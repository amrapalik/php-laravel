<div class="row">
    <!-- Class Name Field -->
    {!! Former::text('class_name')->label(__('classes.fields.class_name').':')->addGroupClass('col-sm-6')->placeholder(__('classes.fields.class_name').':') !!}

    <!-- Class Description Field -->
    {!! Former::textarea('class_description')->label(__('classes.fields.class_description').':')->addGroupClass('col-sm-12 col-lg-6')->placeholder(__('classes.fields.class_description').':') !!}
</div>




<div class="row">
    <!-- Grade Field -->
    {!! Former::text('grade')->label(__('classes.fields.grade').':')->addGroupClass('col-sm-6')->placeholder(__('classes.fields.grade').':') !!}

    <!-- Division Field -->
    {!! Former::text('division')->label(__('classes.fields.division').':')->addGroupClass('col-sm-6')->placeholder(__('classes.fields.division').':') !!}
</div>



<div class="row">
    <!-- Age Group Field -->
    {!! Former::text('age_group')->label(__('classes.fields.age_group').':')->addGroupClass('col-sm-6')->placeholder(__('classes.fields.age_group').':') !!}

    <!-- Copied From Field -->
    {!! Former::number('copied_from')->label(__('classes.fields.copied_from').':')->addGroupClass('col-sm-6')->placeholder(__('classes.fields.copied_from').':') !!}
</div>
<div class="row">
 <!-- Start Date Field -->
{!! Former::date('start_date')->label(__('classes.fields.start_date').':')->addGroupClass('col-sm-6')->placeholder('Start Date') !!}

@push('page_scripts')
    <script type="text/javascript">
        $('#start_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush


 <!-- End Date Field -->
{!! Former::date('end_date')->label(__('classes.fields.end_date').':')->addGroupClass('col-sm-6')->placeholder('End Date') !!}

@push('page_scripts')
    <script type="text/javascript">
        $('#end_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush
</div>

<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-info']) !!}
    <a href="{{ route('classes.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
