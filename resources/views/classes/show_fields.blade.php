<!-- Class Name Field -->
<div class="col-sm-12">
    {!! Form::label('class_name', __('classes.fields.class_name').':') !!}
    <p>{{ $classes->class_name }}</p>
</div>


<!-- Class Description Field -->
<div class="col-sm-12">
    {!! Form::label('class_description', __('classes.fields.class_description').':') !!}
    <p>{{ $classes->class_description }}</p>
</div>

<?php

$game_token = app('request')->session()->get('game_token');
//dd($user_type);

?>

<div class="row">
    <div class="col-sm-3">
        @if($classes->type == 'Assignment' )

            @if(auth()->user()->purchaseflag == 'Yes')
            <a class="btn btn-primary" href="{{env('REALLIVES_GAME_URL')}}/createlifeWithAssignment/{{$student_assignment->student_assignment_id}}/{{$game_token}}"> Play Assignment  </a>
            @else
                <h4>You are deallocated</h4>
            @endif
            <br><br>
        @endif
    </div>
</div>


{{--<!-- Grade Field -->--}}
{{--<div class="col-sm-12">--}}
{{--    {!! Form::label('grade', __('classes.fields.grade').':') !!}--}}
{{--    <p>{{ $classes->grade }}</p>--}}
{{--</div>--}}


{{--<!-- Division Field -->--}}
{{--<div class="col-sm-12">--}}
{{--    {!! Form::label('division', __('classes.fields.division').':') !!}--}}
{{--    <p>{{ $classes->division }}</p>--}}
{{--</div>--}}


<!-- Age Group Field -->
<div class="col-sm-12">
    {!! Form::label('age_group', __('classes.fields.age_group').':') !!}
    <p>{{ $classes->age_group }}</p>
</div>


{{--<!-- Start Date Field -->--}}
{{--<div class="col-sm-12">--}}
{{--    {!! Form::label('start_date', __('classes.fields.start_date').':') !!}--}}
{{--    <p>{{ $classes->start_date->format('d-m-Y') }}</p>--}}
{{--</div>--}}


<!-- End Date Field -->
<div class="col-sm-12">
    {!! Form::label('end_date', __('classes.fields.end_date').':') !!}
    <p>{{ $classes->end_date->format('d-m-Y') }}</p>
</div>


<!-- Created By Field -->
<div class="col-sm-12">
    {!! Form::label('created_by', __('classes.fields.created_by').':') !!}
    <p>{{ $classes->createdby->name }}</p>
</div>


@if($classes->type == 'Assignment' )
<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', __('classes.fields.gamestatus').':') !!}
    <p>{{ $student_assignment->game_status }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', __('classes.fields.letterstatus').':') !!}
    <p>{{ $student_assignment->game_status }}</p>
</div>

@endif
