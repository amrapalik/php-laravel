
<div id="paypal-button-container"></div>
@section('script')
    <script src="https://www.paypal.com/sdk/js?client-id=AW-PUpbxe9awgeAit-Qpp1S9zuidbLzc_RPVvOCzpv_6-u6fTlzWlhAwsNIZdOaIKUmxCsODAZKjHBZ_&currency=USD"></script>
    <script>
        // Render the PayPal button into #paypal-button-container
        // X-CSRF-TOKEN is also required to add in request, otherwise you will not be able to access the createorder url
        paypal.Buttons({

            style: {
                shape: 'pill',
                color: 'gold',
                layout: 'horizontal',
                label: 'buynow',

            },

            // Call your server to set up the transaction
            createOrder: function(data, actions) {
                var _token = "{{ csrf_token() }}";
                return fetch('/createorder?product=3', {
                    method: 'post',
                    headers: {
                        'X-CSRF-TOKEN': _token,
                        'Content-Type': 'application/json',
                    },
                }).then(function(res) {
                    return res.json();
                }).then(function(orderData) {
                    return orderData.result.id;
                });
            },
            // Call your server to finalize the transaction
            onApprove: function(data, actions) {

                console.log("dfdfdf")
                var _token = "{{ csrf_token() }}";
                return fetch('http://ubold20.loc/captureorder/' + data.orderID + '/capture/', {
                    method: 'post',
                    headers: {
                        'X-CSRF-TOKEN': _token,
                        'Content-Type': 'application/json',
                    },
                }).then(function(res) {
                    return res.json();
                }).then(function(orderData) {
                    // Three cases to handle:
                    //   (1) Recoverable INSTRUMENT_DECLINED -> call actions.restart()
                    //   (2) Other non-recoverable errors -> Show a failure message
                    //   (3) Successful transaction -> Show a success / thank you message
                    // Your server defines the structure of 'orderData', which may differ
                    var errorDetail = Array.isArray(orderData.details) && orderData.details[0];
                    if (errorDetail && errorDetail.issue === 'INSTRUMENT_DECLINED') {
                        // Recoverable state, see: "Handle Funding Failures"
                        // https://developer.paypal.com/docs/checkout/integration-features/funding-failure/
                        return actions.restart();
                    }
                    if (errorDetail) {
                        var msg = 'Sorry, your transaction could not be processed.';
                        if (errorDetail.description) msg += '\n\n' + errorDetail.description;
                        if (orderData.debug_id) msg += ' (' + orderData.debug_id + ')';
                        // Show a failure message
                        return alert(msg);
                    }
                    // Show a success message to the buyer
                    alert('Transaction completed by ' + orderData.result.payer.name.given_name);
                });
            }
        }).render('#paypal-button-container');
    </script>
