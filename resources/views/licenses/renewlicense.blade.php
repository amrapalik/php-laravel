@extends('layouts.vertical', ["page_title"=> __('licenses.renew_page_title') ])

@section('content')

    <style>


        .card-pricing .card-pricing-price {
            padding: 0px 0 0;
        }



        .card-pricing .card-pricing-plan-name {
            padding-bottom: 2px;
        }

        .card-pricing-features {
            color: #98a6ad;
            list-style: none;
            margin: 0;
            padding: 2px 0 0 0;
        }

        .card-pricing .card-pricing-features li {
            padding: 5px;
        }
        .card-pricing-features {
            color: #98a6ad;
            list-style: none;
            margin: 0;
            padding: 2px 0 0 0;
        }

        .card {
            box-shadow: 0 0.75rem 6rem rgb(56 65 74 / 3%);
            margin-bottom: 2px;
        }


    </style>
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-4">
                <div class="page-title-box">
                    <div class="page-title-right">

                    </div>
                    <h4 class="page-title ">@lang('licenses.renew_page_title')</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <?php
        $user = auth()->user();
        $school_id = session('school_schid');
        $school = \App\Models\School::find($school_id);
        $orders = \App\Models\Order::where('school_schid','=',$school_id)->where('status','=','COMPLETED')->get();
        ?>

        <div class="content px-3">

            @if($user->drole == "Gamer")
                <div class="row justify-content-center">
                    <div class="mb-3 col-md-6">
                        <h4 class="page-title" style="text-align: center">{!! ucwords( $user->name) !!}</h4>
                    </div>
                </div>
            @else
                <div class="row justify-content-center">
                    <div class="mb-3 col-md-4">
                        <h4 class="page-title" style="text-align: center">{!! ucwords( $school->sch_name) !!}</h4>
                    </div>

                    <div class="mb-3 col-md-4">
                        <h4 class="page-title" style="text-align: center">Available Licenses : {!! $school->available_licenses !!}</h4>
                    </div>
                </div>
        @endif
        <!-- Pricing Title-->



            <div class="row my-3 justify-content-center">
                    <div class="col-md-4">
                        <div class="card card-pricing">
                            <div class="card-body text-center">
                                <h3 class="mb-2">{!! $product->prod_name !!}</h3>
                                <p class="card-pricing-plan-name fw-bold text-uppercase">{!! $product->description !!}</p>
                                <h2 class="card-pricing-price">${!! $product->price !!} </h2> <!-- <span>/ Month</span> -->
                                <ul class="card-pricing-features">
                                    <li>{!! $product->no_of_licenses !!} @lang('licenses.plural')</li>
                                    <li>{!! $product->validity !!} @lang('licenses.Year Validity')</li>
                                    <li>${!! $product->renew_price !!} @lang('licenses.On Renewal') </li>
                                {{--                                <li>1 School</li>--}}
                                <!--  <li>Email Support</li>
                                <li>24x7 Support</li> -->
                                </ul>

                                <br> <br>
                                <div  style="text-align: center"> <div id="paypal-button-container"></div></div>
                             </div>
                        </div> <!-- end Pricing_card -->
                    </div> <!-- end col -->
            </div> 
            <!-- end row --><!-- end row --><!-- end row --><!-- end row --><!-- end row --><!-- end row --><!-- end row --><!-- end row --><!-- end row --> 
           


            
        </div>
    </div>
@endsection

@section('scripts')


    <script src="https://www.paypal.com/sdk/js?client-id=AW-PUpbxe9awgeAit-Qpp1S9zuidbLzc_RPVvOCzpv_6-u6fTlzWlhAwsNIZdOaIKUmxCsODAZKjHBZ_&currency=USD"></script>
    <script>
        // Render the PayPal button into #paypal-button-container
        // X-CSRF-TOKEN is also required to add in request, otherwise you will not be able to access the createorder url
        paypal.Buttons({

            style: {
                shape: 'pill',
                color: 'gold',
                layout: 'horizontal',
                label: 'buynow',

            },

            // Call your server to set up the transaction
            createOrder: function(data, actions) {
                var _token = "{{ csrf_token() }}";
                return fetch('/createorder_renew?lic={{$license->ordid}}&prod={{$product->prodid }}', {
                    method: 'post',
                    headers: {
                        'X-CSRF-TOKEN': _token,
                        'Content-Type': 'application/json',
                    },
                }).then(function(res) {
                    return res.json();
                }).then(function(orderData) {
                    return orderData.result.id;
                });
            },
            // Call your server to finalize the transaction
            onApprove: function(data, actions) {

                console.log("approved")
                var _token = "{{ csrf_token() }}";
                return fetch('/captureRenew/' + data.orderID + '/capture?lic={{$license->ordid}}', {
                    method: 'get',
                    headers: {
                        'X-CSRF-TOKEN': _token,
                        'Content-Type': 'application/json',
                    },
                }).then(function(res) {
                    return res.json();
                }).then(function(orderData) {
                    // Three cases to handle:
                    //   (1) Recoverable INSTRUMENT_DECLINED -> call actions.restart()
                    //   (2) Other non-recoverable errors -> Show a failure message
                    //   (3) Successful transaction -> Show a success / thank you message
                    // Your server defines the structure of 'orderData', which may differ
                    var errorDetail = Array.isArray(orderData.details) && orderData.details[0];
                    if (errorDetail && errorDetail.issue === 'INSTRUMENT_DECLINED') {
                        // Recoverable state, see: "Handle Funding Failures"
                        // https://developer.paypal.com/docs/checkout/integration-features/funding-failure/
                        return actions.restart();
                    }
                    if (errorDetail) {
                        var msg = 'Sorry, your transaction could not be processed.';
                        if (errorDetail.description) msg += '\n\n' + errorDetail.description;
                        if (orderData.debug_id) msg += ' (' + orderData.debug_id + ')';
                        // Show a failure message
                        return alert(msg);
                    }
                    //  console.log(orderData.result.id);

                    //orderData.result.id
                    //orderData.result.payer.name.given_name
                    //orderData.result.purchase_units.amount.currency_code.value

                    // Show a success message to the buyer

                    window.location.href = "{{ route('purchaseDetails')}}"+'?id='+orderData.result.id;
                    //  alert('Transaction completed by ' + orderData.result.payer.name.given_name);
                });
            }
        }).render('#paypal-button-container');
    </script>

@endsection
