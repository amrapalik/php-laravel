<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', __('licenses.fields.user_id').':') !!}
    <p>{{ $license->user_id }}</p>
</div>


<!-- School Schid Field -->
<div class="col-sm-12">
    {!! Form::label('school_schid', __('licenses.fields.school_schid').':') !!}
    <p>{{ $license->school_schid }}</p>
</div>


<!-- Product Prodid Field -->
<div class="col-sm-12">
    {!! Form::label('product_prodid', __('licenses.fields.product_prodid').':') !!}
    <p>{{ $license->product_prodid }}</p>
</div>


<!-- Order Ordid Field -->
<div class="col-sm-12">
    {!! Form::label('order_ordid', __('licenses.fields.order_ordid').':') !!}
    <p>{{ $license->order_ordid }}</p>
</div>


<!-- Licences Field -->
<div class="col-sm-12">
    {!! Form::label('licences', __('licenses.fields.licences').':') !!}
    <p>{{ $license->lives }}</p>
</div>


<!-- Licences Available Field -->
<div class="col-sm-12">
    {!! Form::label('licences_available', __('licenses.fields.licences_available').':') !!}
    <p>{{ $license->available_lives }}</p>
</div>


<!-- Price Field -->
<div class="col-sm-12">
    {!! Form::label('price', __('licenses.fields.price').':') !!}
    <p>{{ $license->price }}</p>
</div>


<!-- Paypal Orderid Field -->
<div class="col-sm-12">
    {!! Form::label('paypal_orderid', __('licenses.fields.paypal_orderid').':') !!}
    <p>{{ $license->paypal_orderid }}</p>
</div>


<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', __('licenses.fields.status').':') !!}
    <p>{{ $license->status }}</p>
</div>


<!-- Paid Field -->
<div class="col-sm-12">
    {!! Form::label('paid', __('licenses.fields.paid').':') !!}
    <p>{{ $license->paid }}</p>
</div>


<!-- Expiry Date Field -->
<div class="col-sm-12">
    {!! Form::label('expiry_date', __('licenses.fields.expiry_date').':') !!}
    <p>{{ $license->expiry_date }}</p>
</div>


<!-- Captured Field -->
<div class="col-sm-12">
    {!! Form::label('captured', __('licenses.fields.captured').':') !!}
    <p>{{ $license->captured }}</p>
</div>


