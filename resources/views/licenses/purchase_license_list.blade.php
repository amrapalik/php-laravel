@extends('layouts.vertical', ["page_title"=> __('licenses.Page_title') ])

@section('content')

    <style>


        .card-pricing .card-pricing-price {
            padding: 0px 0 0;
        }



        .card-pricing .card-pricing-plan-name {
            padding-bottom: 2px;
        }

        .card-pricing-features {
            color: #98a6ad;
            list-style: none;
            margin: 0;
            padding: 2px 0 0 0;
        }

        .card-pricing .card-pricing-features li {
            padding: 5px;
        }
        .card-pricing-features {
            color: #98a6ad;
            list-style: none;
            margin: 0;
            padding: 2px 0 0 0;
        }

        .card {
            box-shadow: 0 0.75rem 6rem rgb(56 65 74 / 3%);
             margin-bottom: 2px;
        }


    </style>
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-4">
                <div class="page-title-box">
                    <div class="page-title-right">

                    </div>
                    <h4 class="page-title ">@lang('licenses.Page_title')</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <?php
        $user = auth()->user();
        $school_id = session('school_schid');
        $school = \App\Models\School::find($school_id);
        $mode = env('PAYPAL_MODE','production');
        ?>

        <div class="content px-3">

            @if($user->drole == "Gamer")
                <div class="row justify-content-center">
                    <div class="mb-3 col-md-6">
                            <h4 class="page-title" style="text-align: center">{!! ucwords( $user->name) !!}</h4>
                    </div>
                </div>
            @else
                <div class="row justify-content-center">
                    <div class="mb-3 col-md-4">
                            <h3 class="page-title" style="text-align: center" >Buy License for {!! ucwords( $school->sch_name) !!}</h3>
                    </div>

                    <div class="mb-3 col-md-4">
                            <h3 class="page-title" style="text-align: center">Available Licenses : {!! $school->available_licenses !!}</h3>
                    </div>
                </div>
            @endif
 <!-- Pricing Title-->
            <div class="text-center">
                <h3 class="mb-2">  <b>{!! __('licenses.Licenses Plans') !!}</b></h3>

                @if($user->drole == "Gamer")
                <p class="text-muted w-50 m-auto">
                    {!! __('licenses.gamer_license_page_detail') !!}
                </p>
                @else
                    <p class="text-muted w-50 m-auto">
                        @if($school->school_type == 'family')
                            {!! __('licenses.family_license_page_detail') !!}
                        @else
                            {!! __('licenses.school_license_page_detail') !!}
                        @endif    
                    </p>
                @endif
            </div>
           <!--  <div class="row justify-content-center">


                @foreach($products as $plan)
                <div class="col-md-4 col-lg-4 col-xl-4">
                    <div class="card bg-pattern">

                        <div class="card-body p-8">

                            <div class="text-center w-75 m-auto">

                                <h4>{!! $plan->prod_name !!}</h4>
                                <h5>{!! $plan->description !!}</h5>   <br>



                                <h5>No Of Licenses: <span class="badge bg-success rounded-pill">
                                         {!! $plan->no_of_licenses !!}
                                    </span></h5>



                                <h5  >Validity: {!! $plan->validity !!} Year </h5>

                                <h3 class="badge bg-primary  " style="padding: 10px" >Price:  {!! $plan->price !!}$</h3>


                                <h5>Renew Price:   <br><span class="badge bg-danger " style="padding: 5px">

                                         {!! $plan->renew_price !!}$
                                    </span></h5>

                                <span onclick="selectProduct({!! $plan->prodid !!},{!! $plan->price !!})" class="btn btn-danger  waves-effect waves-light">Select</span>
                            </div>
                        </div> <!-- end card-body
                    </div> -->
                    <!-- end card

                </div>--> <!-- end col -->

             <!--    @endforeach
            </div> -->

            <div class="row my-3 justify-content-center">
              @foreach($products as $plan)
                <div class="col-md-4">
                    <div class="card card-pricing">
                        <div class="card-body text-center">
                            <h3 class="mb-2">{!! $plan->prod_name !!}</h3>
                            <p class="card-pricing-plan-name fw-bold text-uppercase">{!! $plan->description !!}</p>

                            <h2 class="card-pricing-price">${!! $plan->price !!} </h2> <!-- <span>/ Month</span> -->
                            <ul class="card-pricing-features">
                                <li>{!! $plan->no_of_licenses !!} @lang('licenses.plural')</li>
                                <li>{!! $plan->validity !!} @lang('licenses.Year Validity')</li>
                                <li>${!! $plan->renew_price !!} @lang('licenses.On Renewal') </li>
{{--                                <li>1 School</li>--}}
                               <!--  <li>Email Support</li>
                                <li>24x7 Support</li> -->
                            </ul>
                            <button id = "button-select{!! $plan->prodid !!}" onclick="selectProduct({!! $plan->prodid !!},{!! $plan->price !!})" class="btn btn-info waves-effect waves-light mt-4 mb-2 width-sm"> {!! __('licenses.select') !!}</button> 
                            {{-- <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">

                            <input type="hidden" name="cmd" value="_s-xclick">

                            <input type="hidden" name="hosted_button_id" value="6ZXGTNFPNUFPC">

                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">

                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">

                            </form> --}}
                            <div id="paypal-button-container{!! $plan->prodid !!}"></div>
                        </div>
                    </div> <!-- end Pricing_card -->
                </div> <!-- end col -->
                 @endforeach


            </div>
            <!-- end row -->

            <div class="row">

                <div class="col-md-3"> </div>
                <div class="col-md-3" > <h4> {!! __('licenses.amount') !!} : <span id="setPrice"></span>$</h4></div>
                <div class="col-md-3"> <div  id="paypal-button-container"></div></div>


            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>

         $('#paypal-button-container').hide()
         price = 0;
         products = 0;
         function selectProduct(product,price) {

            $('#paypal-button-container'+product).hide()
            $('#button-select'+product).hide()

            console.log(products)
            products = product;



            $('#setPrice').html(price)
            $('#paypal-button-container'+product).show()
            renderPaypal(product);
            console.log(products)

        }
    </script>
   @if($mode == 'sandbox')
    <script src="https://www.paypal.com/sdk/js?client-id=AdIUc2xBoS4nCtLQT32tLkRFfV_9GooIPO-qmqxmZ98w27cJg9kg2IfSIVUOUFOpEyvCJomEK8CpQVGa"></script>
   @else
    <script src="https://www.paypal.com/sdk/js?client-id=AeS1w7O6WYYHvHDmh9X7aPyUM1r-EIznZUtzH11I6QpTLvwT9jR-DOg23nAt4Vrphd3uns-I5c3T-C-f"></script>

   @endif
    <script>
        // Render the PayPal button into #paypal-button-container
        // X-CSRF-TOKEN is also required to add in request, otherwise you will not be able to access the createorder url
       function renderPaypal(prodid){
          
        paypal.Buttons({

            style: {
                shape: 'pill',
                color: 'gold',
                layout: 'horizontal',
                label: 'buynow',

            },

            // Call your server to set up the transaction
            createOrder: function(data, actions) {
                var _token = "{{ csrf_token() }}";
                return fetch('/createorder?product='+prodid, {
                    method: 'post',
                    headers: {
                        'X-CSRF-TOKEN': _token,
                        'Content-Type': 'application/json',
                    },
                }).then(function(res) {
                    return res.json();
                }).then(function(orderData) {
                    return orderData.result.id;
                });
            },
            // Call your server to finalize the transaction
            onApprove: function(data, actions) {

                console.log("dfdfdf")
                var _token = "{{ csrf_token() }}";
                return fetch('/captureorder/' + data.orderID + '/capture', {
                    method: 'get',
                    headers: {
                        'X-CSRF-TOKEN': _token,
                        'Content-Type': 'application/json',
                    },
                }).then(function(res) {
                    return res.json();
                }).then(function(orderData) {
                    // Three cases to handle:
                    //   (1) Recoverable INSTRUMENT_DECLINED -> call actions.restart()
                    //   (2) Other non-recoverable errors -> Show a failure message
                    //   (3) Successful transaction -> Show a success / thank you message
                    // Your server defines the structure of 'orderData', which may differ
                    var errorDetail = Array.isArray(orderData.details) && orderData.details[0];
                    if (errorDetail && errorDetail.issue === 'INSTRUMENT_DECLINED') {
                        // Recoverable state, see: "Handle Funding Failures"
                        // https://developer.paypal.com/docs/checkout/integration-features/funding-failure/
                        return actions.restart();
                    }
                    if (errorDetail) {
                        var msg = 'Sorry, your transaction could not be processed.';
                        if (errorDetail.description) msg += '\n\n' + errorDetail.description;
                        if (orderData.debug_id) msg += ' (' + orderData.debug_id + ')';
                        // Show a failure message
                        return alert(msg);
                    }
                  //  console.log(orderData.result.id);

                    //orderData.result.id
                    //orderData.result.payer.name.given_name
                    //orderData.result.purchase_units.amount.currency_code.value

                    // Show a success message to the buyer

                   window.location.href = "{{ route('purchaseDetails')}}"+'?id='+orderData.result.id;
                  //  alert('Transaction completed by ' + orderData.result.payer.name.given_name);
                });
            }
        }).render('#paypal-button-container'+prodid);
    }
    </script>

@endsection
