<div class="row">
    <!-- User Id Field -->
    {!! Former::number('user_id')->label(__('licenses.fields.user_id').':')->addGroupClass('col-sm-6')->placeholder(__('licenses.fields.user_id').':') !!}
</div>



<div class="row">
    <!-- School Schid Field -->
    {!! Former::number('school_schid')->label(__('licenses.fields.school_schid').':')->addGroupClass('col-sm-6')->placeholder(__('licenses.fields.school_schid').':') !!}
</div>



<div class="row">
    <!-- Product Prodid Field -->
    {!! Former::number('product_prodid')->label(__('licenses.fields.product_prodid').':')->addGroupClass('col-sm-6')->placeholder(__('licenses.fields.product_prodid').':') !!}
</div>




<div class="row">
    <!-- Order Ordid Field -->
    {!! Former::number('order_ordid')->label(__('licenses.fields.order_ordid').':')->addGroupClass('col-sm-6')->placeholder(__('licenses.fields.order_ordid').':') !!}
</div>



<div class="row">
    <!-- Licences Field -->
    {!! Former::number('licences')->label(__('licenses.fields.licences').':')->addGroupClass('col-sm-6')->placeholder(__('licenses.fields.licences').':') !!}
</div>



<div class="row">
    <!-- Licences Available Field -->
    {!! Former::number('licences_available')->label(__('licenses.fields.licences_available').':')->addGroupClass('col-sm-6')->placeholder(__('licenses.fields.licences_available').':') !!}
</div>



<div class="row">
    <!-- Price Field -->
    {!! Former::number('price')->label(__('licenses.fields.price').':')->addGroupClass('col-sm-6')->placeholder(__('licenses.fields.price').':') !!}
</div>


<div class="row">
    <!-- Product Prodid Field -->
    {!! Former::number('renew_price')->label(__('licenses.fields.renew_price').':')->addGroupClass('col-sm-6')->placeholder(__('licenses.fields.product_prodid').':') !!}
</div>


<div class="row">
    <!-- Paypal Orderid Field -->
    {!! Former::text('paypal_orderid')->label(__('licenses.fields.paypal_orderid').':')->addGroupClass('col-sm-6')->placeholder(__('licenses.fields.paypal_orderid').':') !!}
</div>



<div class="row">
    <!-- Status Field -->
    {!! Former::text('status')->label(__('licenses.fields.status').':')->addGroupClass('col-sm-6')->placeholder(__('licenses.fields.status').':') !!}
</div>



<div class="row">
    <!-- Paid Field -->
    {!! Former::checkbox('paid')->text(__('licenses.fields.paid').':')->check()->addGroupClass('col-sm-4') !!}
</div>


 <!-- Expiry Date Field -->
{!! Former::date('expiry_date')->label(__('licenses.fields.expiry_date').':')->addGroupClass('col-sm-6')->placeholder('Expiry Date') !!}

@push('page_scripts')
    <script type="text/javascript">
        $('#expiry_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush


<div class="row">
    <!-- Captured Field -->
    {!! Former::checkbox('captured')->text(__('licenses.fields.captured').':')->check()->addGroupClass('col-sm-4') !!}
</div>

<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('licenses.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
