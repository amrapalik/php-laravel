<!DOCTYPE html>
<html>

<head>
    <title></title>
</head>

<body>
    <center>
        <h1>Lesson Plan</h1>
    </center>
    <strong>School</strong>: {{ $schoolName }} <br />
    <strong>Teacher</strong>: {{ $teacherName }} <br />
    <strong>Email</strong>: {{ $teacherEmail }} <br />
    <strong>Lesson Plan Name</strong>: {{ $lessonPlanName }} <br />
    <strong>Grades / Standards Applicable</strong>: {{ $grades }} <br />
    <strong>National Education Standards</strong>: {{ $standards }} <br /><br />
    <p><strong>Overview</strong>: <br />
        {{ $overview }}
    </p>
    <p><strong>Objectives</strong>: <br />
        {{ $objectives }}
    </p>
    <p><strong>Procedures</strong>: <br />
        {!! $procedures !!}
    </p>
    <p><strong>Assessment Criteria</strong>: <br />
        {!! $assessment !!}
    </p>
    <p><strong>Extension Activities</strong>: <br />
        {{ $extension_activities }}
    </p>

    @if($youtube_link != NULL)
    <strong>YouTube Link</strong>: <a href="{{ $youtube_link }}" target="_blank">{{ $youtube_link }}</a>
    @endif

    @if($image != NULL)
    <br /><br /><br />
    <img class="" src="{{ asset('/storage/lesson_plans/' . $image) }}" />
    @endif
</body>

</html>