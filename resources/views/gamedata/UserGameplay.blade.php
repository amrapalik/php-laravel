@extends('layouts.vertical', ["page_title"=> "".__('gamedata.User Gameplay Dashboard')])

@section('css')

    <style>

        .enlarged .logo-box {
            width: 114px !important;
        }

        .slogan {
            margin-top: 28px;
            float: left;
            letter-spacing: 4px;
            color: white;
            font-size: 10px;
        }

        .intro-text {
            text-align: center;
            width: 100%;
        }

        .cursor-pointer {
            cursor: pointer;
        }

        .student-list .badge, .student-list-reg .badge {
            display: inline-block;
            padding: 5px;
            font-size: 75%;
            font-weight: 100;
            line-height: 1;
            text-align: center;
            letter-spacing: 1px;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: 0.25rem;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        .sdg-image {
            width: 116px;
        }

        .organdonation li, .sdgs li {
            display: inline-block;
            padding: 6px;
        }

        .sdgs li {
            padding: 16px;
        }

        .sdgs, .organdonation {
            padding-left: 0px;
        }

        .organdonation {
            margin-bottom: 34px;
        }

        .right-border {
            border-right: 1px solid #04a5b9;
        }

        .academic a,
        .academic a:hover,
        .academic a:focus,
        .academic a:active {
            color: #6e768e;
        }

        .sidebar-hr {
            width: 90%;
            margin-top: 4px;
            margin-bottom: 4px;
        }

        .organDonate{
            width: 100px;
        }

    </style>
@endsection

@section('content')
@section('content')
<?php
$user = auth()->user();
?>
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
    @include('layouts.shared/page-title', ['title' => "  ".__($user->firstname).__("'s  ").__('gamedata.User Gameplay Dashboard')])
    <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @include('layouts.common.errors')
                        @include('flash::message')

                        <div class="card-header">
                            <strong>@lang('gamedata.Lives Experienced In Countries')</strong>
                        </div>
                        <div id="map-canvas" style="width:100%;height:500px;">
                            {!! Mapper::render() !!}
                        </div>

                        <br/><br/>
                        <div class="card-header">
                            <strong>@lang("gamedata.SDGs chosen while creating a Life")</strong>
                        </div>
                        <ul class="sdgs text-center">
                            <li>
                                <img class="sdg-image" alt="SUSTAINABLE DEVELOPMENT GOALS" src="/assets/images/sdg/{{app()->getLocale()}}/SDG_logo.jpg">
                                <br/>SDG's
                            </li>
                            @foreach ($value['sdgSelected'] as $sdgid => $sdgcount)
                                <li>
                                    <img class="sdg-image" alt="SUSTAINABLE DEVELOPMENT GOALS" src="/assets/images/sdg/{{app()->getLocale()}}/SDG{{$sdgid}}.jpg">
                                    <br/>{{$sdgcount}}
                                </li>
                            @endforeach
                        </ul>

                        <div class="card-header">
                            <strong>@lang("gamedata.Organ Donated By Me While Playing RealLives")</strong>
                        </div>
                        <ul class="organdonation text-center">
                            <li>
                                <img alt="Liver" class="img-fluid organDonate" src="/assets/images/organs/liver.png">
                                <br/>
                                <span class="">
                                    {{ ($value['organsDonatedCount']['Liver'] > 0) ? __('gamedata.Donated').' : '.$value['organsDonatedCount']['Liver'] : __('gamedata.Not Yet Donated') }}
                                </span>
                            </li>

                            <li>
                                <img alt="Eyes" class="img-fluid organDonate" src="/assets/images/organs/eyes.png">
                                <br/>
                                <span class="">
                                    {{ ($value['organsDonatedCount']['Eyes'] > 0) ? __('gamedata.Donated').' : '.$value['organsDonatedCount']['Eyes'] : __('gamedata.Not Yet Donated') }}
                                </span>
                            </li>

                            <li>
                                <img alt="Heart" class="img-fluid organDonate" src="/assets/images/organs/heart.png">
                                <br/>
                                <span class="">
                                    {{ ($value['organsDonatedCount']['Heart'] > 0) ? __('gamedata.Donated').' : '.$value['organsDonatedCount']['Heart'] : __('gamedata.Not Yet Donated') }}
                                </span>
                            </li>

                            <li>
                                <img alt="Kidney" class="img-fluid organDonate" src="/assets/images/organs/kidney.png">
                                <br/>
                                <span class="">
                                    {{ ($value['organsDonatedCount']['Kidney'] > 0) ? __('gamedata.Donated').' : '.$value['organsDonatedCount']['Kidney'] : __('gamedata.Not Yet Donated') }}
                                </span>
                            </li>

                            <li>
                                <img alt="Lungs" class="img-fluid organDonate" src="/assets/images/organs/lungs.png">
                                <br/>
                                <span class="">
                                    {{ ($value['organsDonatedCount']['Lungs'] > 0) ? __('gamedata.Donated').' : '.$value['organsDonatedCount']['Lungs'] : __('gamedata.Not Yet Donated') }}
                                </span>
                            </li>

                            <li>
                                <img alt="Skin" class="img-fluid organDonate" src="/assets/images/organs/skin.png">
                                <br/>
                                <span class="">
                                    {{ ($value['organsDonatedCount']['Skin'] > 0) ? __('gamedata.Donated').' : '.$value['organsDonatedCount']['Skin'] : __('gamedata.Not Yet Donated') }}
                                </span>
                            </li>

                            <li>
                                <img alt="Blood" class="img-fluid organDonate" src="/assets/images/organs/blood.png">
                                <br/>
                                <span class="">
                                    {{ ($value['organsDonatedCount']['bloodDonationCount'] > 0) ?  __('gamedata.Donated') .' '. $value['organsDonatedCount']['bloodDonationCount'] . ' Bottles':  __('gamedata.Not Yet Donated')}}
                                </span>
                            </li>
                        </ul>

                        <div class="card-header">
                            <strong>@lang("gamedata.My Gameplay Data Inner")</strong>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div>
                                    <div class="col-sm-12 text-center">
                                        <span class="">@lang("gamedata.Lives in Progress") </span>
                                        <h2 class="m-t-20">{{ $value['incompleteLives'] }}</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div>
                                    <div class="col-sm-12 text-center">
                                        <span>@lang("gamedata.Lives Completed")</span>
                                        <h2 class="m-t-20">{{  $value['livesCompleted'] }}</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div>
                                    <div class="col-sm-12 text-center">
                                        <span>@lang("gamedata.Male Lives")</span>
                                        <h2 class="m-t-20">{{ $value['maleLives'] }}</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div>
                                    <div class="col-sm-12 text-center">
                                        <span>@lang("gamedata.Female Lives")</span>
                                        <h2 class="m-t-20">{{ $value['femaleLives'] }}</h2>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-3 m-t-10">
                                <div>
                                    <div class="col-sm-12 text-center">
                                        <span>@lang("gamedata.Countries Covered")</span>
                                        <h2 class="m-t-20">{{ $value['countriesCoveredCount'] }}</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3 m-t-10">
                                <div>
                                    <div class="col-sm-12 text-center">
                                        <span>@lang("gamedata.Obituaries Written")</span>
                                        <h2 class="m-t-20">{{ $value['obituariesWritten'] }}</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3 m-t-10">
                                <div>
                                    <div class="col-sm-12 text-center">
                                        <span>@lang("gamedata.Letters Written")</span>
                                        <h2 class="m-t-20">{{ $value['lettersWritten'] }}</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3 m-t-10">
                                <div>
                                    <div class="col-sm-12 text-center">
                                        <span>@lang("gamedata.SDG Comments")</span>
                                        <h2 class="m-t-20">{{ $value['sdgComments'] }}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
