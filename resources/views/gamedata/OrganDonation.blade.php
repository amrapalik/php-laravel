@extends('layouts.vertical', ["page_title"=> "  ".__('gamedata.OrganDonation')])


@section('css')
    <!-- third party css -->
    <link href="{{asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
@endsection

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        @include('layouts.shared/page-title', ['title' => " ".__('gamedata.Vital Organs Donated By Me')])
        @include('layouts.common.errors')

        {{--        @dd($value)--}}
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">


                        <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                            <thead>
                            <tr>
                                <th>@lang('mygameplaydata.Full Name')</th>
                                <th>@lang('mygameplaydata.Age')</th>
                                <th>@lang('mygameplaydata.Sex')</th>
                                <th>@lang('mygameplaydata.Feedback')</th>
                                <th>@lang('mygameplaydata.Life Progress')</th>
                                <th>@lang('mygameplaydata.Donated Organs')</th>
                            </tr>
                            </thead>


                            <tbody>

                            @foreach($value as $val)
                                <tr>
                                    <td>{{ $val['fullName'] }}</td>
                                    <td>{{ $val['age'] }}</td>
                                    <td>{{ $val['sex'] }}</td>
                                    <td>{{ $val['countryName'] }}</td>
                                    <td>{{ $val['lifeProgress'] }}</td>
                                    <td>
                                        @foreach( $val['donatedOrgans'] as $let)

                                            <p>{!! $let !!}  </p>
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->




    </div> <!-- container -->
@endsection

@section('script')
    <!-- third party js -->
    <script src="{{asset('assets/libs/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
    <!-- third party js ends -->
    <!-- demo app -->
    <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script>
    <!-- end demo js-->
@endsection
