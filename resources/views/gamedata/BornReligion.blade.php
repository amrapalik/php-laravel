@extends('layouts.vertical', ["page_title"=> "  ".__('gamedata.BornReligion')])


@section('css')
    <!-- third party css -->
    <link href="{{asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
@endsection

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        @include('layouts.shared/page-title', ['title' => " ".__('gamedata.BornReligion')])
        @include('layouts.common.errors')


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">


                        <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                            <thead>
                            <tr>

                                <th>@lang('mygameplaydata.Full Name')</th>
                                <th>@lang('mygameplaydata.Age')</th>
                                <th>@lang('mygameplaydata.Sex')</th>
                                <th>@lang('mygameplaydata.Country Name')</th>
                                <th>@lang('mygameplaydata.Born Religion')</th>

                            </tr>
                            </thead>


                            <tbody>

                            @foreach($value as $val)
{{--                                @dd( $val['countryName'])--}}
                                <tr>
                                    <td>{{ $val['fullName'] }}</td>
                                    <td>{{ $val['age'] }}</td>
                                    <td>{{ $val['sex'] }}</td>
                                    <td>{{ $val['countryName'] }}</td>
                                    <td>{{ $val['religion'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->




    </div> <!-- container -->
@endsection

@section('script')
    <!-- third party js -->
    <script src="{{asset('assets/libs/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
    <!-- third party js ends -->
    <!-- demo app -->
    <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script>
    <!-- end demo js-->
@endsection
