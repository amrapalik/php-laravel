<!-- Organization Orgid Field -->
<div class="col-sm-12">
    {!! Form::label('organization_orgid', __('schools.fields.organization_orgid').':') !!}
    <p>{{ $school->organization_orgid }}</p>
</div>


<!-- Sch Name Field -->
<div class="col-sm-12">
    {!! Form::label('sch_name', __('schools.fields.sch_name').':') !!}
    <p>{{ $school->sch_name }}</p>
</div>


<!-- About Field -->
<div class="col-sm-12">
    {!! Form::label('about', __('schools.fields.about').':') !!}
    <p>{{ $school->about }}</p>
</div>


<!-- No Of Teachers Field -->
<div class="col-sm-12">
    {!! Form::label('no_of_teachers', __('schools.fields.no_of_teachers').':') !!}
    <p>{{ $school->no_of_teachers }}</p>
</div>


<!-- No Of Students Field -->
<div class="col-sm-12">
    {!! Form::label('no_of_students', __('schools.fields.no_of_students').':') !!}
    <p>{{ $school->no_of_students }}</p>
</div>


<!-- Sch Website Field -->
<div class="col-sm-12">
    {!! Form::label('sch_website', __('schools.fields.sch_website').':') !!}
    <p>{{ $school->sch_website }}</p>
</div>


<!-- Facebook Field -->
<div class="col-sm-12">
    {!! Form::label('facebook', __('schools.fields.facebook').':') !!}
    <p>{{ $school->facebook }}</p>
</div>


<!-- Linkedin Field -->
<div class="col-sm-12">
    {!! Form::label('linkedin', __('schools.fields.linkedin').':') !!}
    <p>{{ $school->linkedin }}</p>
</div>


<!-- Latitude Field -->
<div class="col-sm-12">
    {!! Form::label('latitude', __('schools.fields.latitude').':') !!}
    <p>{{ $school->latitude }}</p>
</div>


<!-- Longitude Field -->
<div class="col-sm-12">
    {!! Form::label('longitude', __('schools.fields.longitude').':') !!}
    <p>{{ $school->longitude }}</p>
</div>


<!-- Logo Field -->
<div class="col-sm-12">
    {!! Form::label('logo', __('schools.fields.logo').':') !!}
    <p>{{ $school->logo }}</p>
</div>


<!-- Address1 Field -->
<div class="col-sm-12">
    {!! Form::label('address1', __('schools.fields.address1').':') !!}
    <p>{{ $school->address1 }}</p>
</div>


<!-- Address2 Field -->
<div class="col-sm-12">
    {!! Form::label('address2', __('schools.fields.address2').':') !!}
    <p>{{ $school->address2 }}</p>
</div>


<!-- City Field -->
<div class="col-sm-12">
    {!! Form::label('city', __('schools.fields.city').':') !!}
    <p>{{ $school->city }}</p>
</div>


<!-- State Field -->
<div class="col-sm-12">
    {!! Form::label('state', __('schools.fields.state').':') !!}
    <p>{{ $school->state }}</p>
</div>


<!-- Country Field -->
<div class="col-sm-12">
    {!! Form::label('country', __('schools.fields.country').':') !!}
    <p>{{ $school->country }}</p>
</div>


<!-- Zipcode Field -->
<div class="col-sm-12">
    {!! Form::label('zipcode', __('schools.fields.zipcode').':') !!}
    <p>{{ $school->zipcode }}</p>
</div>


<!-- Sch Phone Field -->
<div class="col-sm-12">
    {!! Form::label('sch_phone', __('schools.fields.sch_phone').':') !!}
    <p>{{ $school->sch_phone }}</p>
</div>


<!-- Sch Mobile Field -->
<div class="col-sm-12">
    {!! Form::label('sch_mobile', __('schools.fields.sch_mobile').':') !!}
    <p>{{ $school->sch_mobile }}</p>
</div>


<!-- Available Licenses Field -->
<div class="col-sm-12">
    {!! Form::label('available_licenses', __('schools.fields.available_licenses').':') !!}
    <p>{{ $school->available_licenses }}</p>
</div>


<!-- Used Licenses Field -->
<div class="col-sm-12">
    {!! Form::label('used_licenses', __('schools.fields.used_licenses').':') !!}
    <p>{{ $school->used_licenses }}</p>
</div>


<!-- Purchased Licenses Field -->
<div class="col-sm-12">
    {!! Form::label('purchased_licenses', __('schools.fields.purchased_licenses').':') !!}
    <p>{{ $school->purchased_licenses }}</p>
</div>


