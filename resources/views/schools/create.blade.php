@extends('layouts.vertical', ["page_title"=> __('general.Create').__('schools.singular')])

@section('content')
<!-- Start Content-->
<div class="container-fluid">

    @include('layouts.shared/page-title', ['title' => __('general.Create').__('schools.singular')])
    @include('layouts.common.errors')
    @include('flash::message')
    <!-- Start Form  -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">@lang('schools.singular')</h4>

                     {!! Former::vertical_open_for_files()->id('create-schools-form')->method('POST')->route('schools.store') !!}

                        @include('schools.fields')

                    {!! Former::close() !!}

                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end form -->
</div> <!-- container -->

@endsection


@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

   {!! JsValidator::formRequest('App\Http\Requests\CreateSchoolRequest') !!}
   <script>
        $(document).ready(function () {
           /*  $('#country_code').on('change', function () {
                var country_code = this.value;
               $("#country").html('');
                $.ajax({
                    url: "{{url('fetchCountry')}}",
                    type: "POST",
                    data: {
                        country_code: country_code,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {

                        $('#country').html();
                        $.each(result.country, function (key, value) {
                            $("#country").append('<option selected data-id="' + value
                                .id + '" value="' + value
                                .name + '">' + value.name + '</option>');
                        });

                       $('#state').html('<option value="">Select State</option>');
                        $.each(result.states, function (key, value) {
                            $("#state").append('<option data-id="' + value
                                .id + '" value="' + value
                                .name + '">' + value.name + '</option>');
                        });
                        $('#city').html('<option value="">Select City</option>');
                    }
                });
            });*/

            $('#country').on('change', function () {

                 var selected = $(this).find('option:selected');
                 var idCountry = selected.data('id');
               // var idCountry = this.value;
                $("#state").html('');
                $.ajax({
                    url: "{{url('fetchState')}}",
                    type: "POST",
                    data: {
                        country_id: idCountry,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {
                        $('#state').html('<option value="">Select State</option>');
                        $.each(result.states, function (key, value) {
                            $("#state").append('<option data-id="' + value
                                .id + '" value="' + value
                                .name + '">' + value.name + '</option>');
                        });
                        $('#city').html('<option value="">Select City</option>');
                    }
                });
            });

            $('#state').on('change', function () {
                 var selected = $(this).find('option:selected');
                 var idState = selected.data('id');

                $("#city").html('');
                $.ajax({
                    url: "{{url('fetchCity')}}",
                    type: "POST",
                    data: {
                        state_id: idState,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (res) {
                        $('#city').html('<option value="">Select City</option>');
                        $.each(res.cities, function (key, value) {
                            $("#city").append('<option value="' + value
                                .name + '">' + value.name + '</option>');
                        });
                    }
                });
            });
        });

    </script>

@endsection
