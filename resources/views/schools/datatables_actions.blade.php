{!! Form::open(['route' => ['schools.destroy', $schid], 'method' => 'delete']) !!}
<div class='btn-group'>

    <a href="/setschool?schoolid={{ $schid }}" class='btn btn-default btn-xs'>
        <i class="fa fa-hand-point-up" title="Go to Dashboard"></i>
    </a>

{{--    <a href="{{ route('schools.show', $schid) }}" class='btn btn-default btn-xs'>--}}
{{--        <i class="fa fa-eye"></i>--}}
{{--    </a>--}}


    <a href="{{ route('schools.edit', $schid) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
{{--    {!! Form::button('<i class="fa fa-trash"></i>', [--}}
{{--        'type' => 'submit',--}}
{{--        'class' => 'btn btn-danger btn-xs',--}}
{{--        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'--}}
{{--    ]) !!}--}}
</div>
{!! Form::close() !!}
