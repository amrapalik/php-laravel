<div class="row">
    <!-- Sch Name Field -->
    {!! Former::text('sch_name')->label(__('schools.fields.sch_name').':')->addGroupClass('col-sm-6')->placeholder(__('schools.fields.sch_name').':')->required() !!}

    <!-- Sch Website Field -->
    {!! Former::text('sch_website')->label(__('schools.fields.sch_website').':')->addGroupClass('col-sm-6')->placeholder(__('schools.fields.sch_website').':') !!}
</div>

<div class="row">
    <!-- About Field -->
    {!! Former::textarea('about')->label(__('schools.fields.about').':')->addGroupClass('col-sm-12 col-lg-12')->placeholder(__('schools.fields.about').':') !!}


</div>



<div class="row">
    <!-- Address1 Field -->
    {!! Former::text('address1')->label(__('schools.fields.address1').':')->addGroupClass('col-sm-6')->placeholder(__('schools.fields.address1').':') !!}

    <!-- Address2 Field -->
    {!! Former::text('address2')->label(__('schools.fields.address2').':')->addGroupClass('col-sm-6')->placeholder(__('schools.fields.address2').':') !!}
</div>



<div class="row">

    <!-- Country Field -->
  <!--   {!! Former::select('country')->fromQuery(App\Models\Countries::get(), 'name','name')->label(__('users.fields.country').':')->addGroupClass('col-sm-3')->placeholder('Country') !!} -->

   <div class="form-group col-md-3">
      <label for="country" class="col-form-label">Country:</label>


       <?php
           $countryss = App\Models\Countries::get();
       ?>


       <select id="country" name="country" class="form-control" required>
           <option value="">Country</option>

           @if(Request::is('schools/create') )
               @foreach($countryss as $value)
                   <option data-id="{{ $value->id }}" value="{{ $value->name }}">{{ $value->name }}</option>
               @endforeach
           @else
               @foreach($countryss as $value)
                   @if($school->country == $value->name)
                        <option data-id="{{ $value->id }}" value="{{ $value->name }}" selected="selected">{{ $value->name }} </option>
                   @else
                       <option data-id="{{ $value->id }}" value="{{ $value->name }}">{{ $value->name }}</option>
                   @endif
               @endforeach
           @endif

       </select>





    </div>

     @if(Request::is('schools/create') )

     <!-- State Field -->
    {!! Former::select('state')->label(__('users.fields.state').':')->addGroupClass('col-sm-3')->placeholder('State') !!}

     @else

     <?php
     $country = App\Models\Countries::where('name',$school->country)->first();
        ?>

      {!! Former::select('state')->fromQuery(App\Models\State::where('country_id',$country->id)->get(), 'name','name')->label(__('users.fields.state').':')->addGroupClass('col-sm-3')->placeholder('State') !!}

     @endif


    @if(Request::is('schools/create') )

     {!! Former::select('city')->label(__('users.fields.city').':')->addGroupClass('col-sm-3')->placeholder('City') !!}

    @else
    <?php
     $state =   App\Models\State::where('name',$school->state)->first();
        ?>
     <!-- City Field -->
    {!! Former::select('city')->fromQuery(App\Models\City::where('state_id',$state->id)->get(), 'name','name')->label(__('users.fields.city').':')->addGroupClass('col-sm-3')->placeholder('City') !!}

    @endif

    <!-- fromQuery(App\Models\City::where('state_id','22')->get(), 'name','name') -->

    <!-- Zipcode Field -->
    {!! Former::text('zipcode')->label(__('users.fields.zipcode').':')->addGroupClass('col-sm-3')->placeholder('Zipcode') !!}


</div>
<div class="row">
    <!-- No Of Teachers Field -->
    {!! Former::number('no_of_teachers')->label(__('schools.fields.no_of_teachers').':')->addGroupClass('col-sm-3')->placeholder(__('schools.fields.no_of_teachers').':') !!}

    <!-- No Of Students Field -->
    {!! Former::number('no_of_students')->label(__('schools.fields.no_of_students').':')->addGroupClass('col-sm-3')->placeholder(__('schools.fields.no_of_students').':') !!}

<?php


$countryss = App\Models\Countries::get();

$countries =[];
foreach ($countryss as $value) {
    # code...


    $countries[$value->phonecode] = $value->name.' (+'.$value->phonecode.')';

    //$countries [] =  $countriesd;
}
?>
<!-- Country Code Field -->
{!! Former::select('country_code')->options($countries)->label(__('users.fields.country_code').':')->addGroupClass('col-sm-3')->placeholder('Country Code') !!}

<!-- Sch Phone Field -->
    {!! Former::text('sch_phone')->label(__('schools.fields.sch_phone').':')->addGroupClass('col-sm-3')->placeholder(__('schools.fields.sch_phone').':') !!}


    {{--    <!-- Purchased Licenses Field -->--}}
    {{--    {!! Former::number('purchased_licenses')->label(__('schools.fields.purchased_licenses').':')->addGroupClass('col-sm-3')->placeholder(__('schools.fields.purchased_licenses').':') !!}--}}


</div>
<div class="row">
    <!-- Facebook Field -->
    {!! Former::text('facebook')->label(__('schools.fields.facebook').':')->addGroupClass('col-sm-6')->placeholder(__('schools.fields.facebook').':') !!}

    <!-- Linkedin Field -->
    {!! Former::text('linkedin')->label(__('schools.fields.linkedin').':')->addGroupClass('col-sm-6')->placeholder(__('schools.fields.linkedin').':') !!}
</div>


	@if (Route::is('schools.create'))
        @include('schools.schooladmin_fields')
	@endif
<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-info']) !!}
    <a href="{{ route('schools.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
