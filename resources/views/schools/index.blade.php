@extends('layouts.vertical', ["page_title"=> __('schools.plural')])

@section('css')
    <!-- third party css -->
    <link href="{{asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
@endsection

<?php
$user = auth()->user();

$school = \App\Models\School::find($user->school_schid);
$org = \App\Models\Organization::find($user->organization_orgid);

?>
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
    @if($org && $org->org_type == "university")    
        @include('layouts.shared/page-title', ['title' => "  ".__('schools.Your University Departments')])
        <!-- end page title -->
    @else    
        @include('layouts.shared/page-title', ['title' => "  ".__('schools.Schools in your organisation')])
    @endif    


        @include('flash::message')

        <div class="clearfix"></div>

        <div class="row">  <!-- Data table-->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-9">
                                @if($org && $org->org_type == "university")    
                                    <h4 class="header-title" style="float: left; margin-top: 10px">@lang('schools.Departments')</h4>
                                @else    
                                    <h4 class="header-title" style="float: left; margin-top: 10px">@lang('schools.plural')</h4>
                                @endif    
    
                            </div>
                            <div class="col-3">
                                @if($org && $org->org_type == "university")    
                                    <a href="{{ route('schools.create') }}" style="float: right" class="btn btn-success rounded-pill waves-effect waves-light">  @lang('schools.Create a Department')</a>
                                <!-- end page title -->
                                @else    
                                    <a href="{{ route('schools.create') }}" style="float: right" class="btn btn-success rounded-pill waves-effect waves-light">  @lang('schools.Create a school')</a>
                                @endif    
                        
                            </div>
                        </div>


                        <p class="text-muted font-13 mb-4" >
                        @if($org && $org->org_type == "university")    
                            <span style="color:red">@lang('general.Click on') <i class="fa fa-hand-point-up"></i> @lang('schools.to select a department') </span>
                        @else    
                            <span style="color:red">@lang('general.Click on') <i class="fa fa-hand-point-up"></i> @lang('schools.to select a school') </span>
                        @endif    
                        </p>

                        {!! $dataTable->table(['width' => '100%', 'class' => 'table dt-responsive nowrap w-100 ']) !!}
                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->
    </div> <!-- end container -->
@endsection


@section('script')
    @include('layouts.datatables_js')
    {{--    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>--}}
    {!! $dataTable->scripts() !!}
@endsection
