<!-- Prod Name Field -->
<div class="col-sm-12">
    {!! Form::label('prod_name', __('products.fields.prod_name').':') !!}
    <p>{{ $product->prod_name }}</p>
</div>


<!-- No Of Licenses Field -->
<div class="col-sm-12">
    {!! Form::label('no_of_licenses', __('products.fields.no_of_licenses').':') !!}
    <p>{{ $product->no_of_licenses }}</p>
</div>


<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('products.fields.description').':') !!}
    <p>{{ $product->description }}</p>
</div>


<!-- Price Field -->
<div class="col-sm-12">
    {!! Form::label('price', __('products.fields.price').':') !!}
    <p>{{ $product->price }}</p>
</div>


<!-- Renew Price Field -->
<div class="col-sm-12">
    {!! Form::label('renew_price', __('products.fields.renew_price').':') !!}
    <p>{{ $product->renew_price }}</p>
</div>


<!-- User Type Field -->
<div class="col-sm-12">
    {!! Form::label('user_type', __('products.fields.user_type').':') !!}
    <p>{{ $product->user_type }}</p>
</div>


<!-- Max Lives Field -->
<div class="col-sm-12">
    {!! Form::label('max_lives', __('products.fields.max_lives').':') !!}
    <p>{{ $product->max_lives }}</p>
</div>


<!-- Validity Field -->
<div class="col-sm-12">
    {!! Form::label('validity', __('products.fields.validity').':') !!}
    <p>{{ $product->validity }}</p>
</div>


<!-- Currency Field -->
<div class="col-sm-12">
    {!! Form::label('currency', __('products.fields.currency').':') !!}
    <p>{{ $product->currency }}</p>
</div>


<!-- Featured Field -->
<div class="col-sm-12">
    {!! Form::label('featured', __('products.fields.featured').':') !!}
    <p>{{ $product->featured }}</p>
</div>


<!-- Suspended Field -->
<div class="col-sm-12">
    {!! Form::label('suspended', __('products.fields.suspended').':') !!}
    <p>{{ $product->suspended }}</p>
</div>


