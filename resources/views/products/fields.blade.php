<div class="row">
    <!-- Prod Name Field -->
    {!! Former::text('prod_name')->label(__('products.fields.prod_name').':')->addGroupClass('col-sm-6')->placeholder(__('products.fields.prod_name').':') !!}
<!-- Description Field -->
    {!! Former::text('description')->label(__('products.fields.description').':')->addGroupClass('col-sm-6')->placeholder(__('products.fields.description').':') !!}

</div>



<div class="row">
    <!-- No Of Licenses Field -->
    {!! Former::number('no_of_licenses')->label(__('products.fields.no_of_licenses').':')->addGroupClass('col-sm-4')->placeholder(__('products.fields.no_of_licenses').':') !!}

    <!-- Price Field -->
    {!! Former::number('price')->label(__('products.fields.price').':')->addGroupClass('col-sm-4')->placeholder(__('products.fields.price').':') !!}

    <!-- Renew Price Field -->
    {!! Former::number('renew_price')->label(__('products.fields.renew_price').':')->addGroupClass('col-sm-4')->placeholder(__('products.fields.renew_price').':') !!}
</div>



<div class="row">
    <!-- User Type Field -->
{!! Former::select('user_type')->options([null=>'Select User Type','School'=>'School','Gamer'=>'Gamer', 'Family'=>'Family','University_dept'=>'University_dept'])->label(__('products.fields.user_type').':')->addGroupClass('col-sm-4')

->state('warning') !!}

    <!-- Validity Field -->
    {!! Former::number('validity')->label(__('products.fields.validity').':')->addGroupClass('col-sm-4')->placeholder(__('products.fields.validity').':') !!}

<!-- Suspended Field -->
    {!! Former::select('suspended')->class("form-select")->label(__('products.fields.suspended').':')->options(['No'=>'No','Yes'=>'Yes'], 1)->addGroupClass('col-sm-4')

    ->state('warning') !!}
</div>



<div class="row">
    <!-- Currency Field -->
    {!! Former::text('currency')->label(__('products.fields.currency').':')->addGroupClass('col-sm-4')->placeholder(__('products.fields.currency').':') !!}

    <!-- Featured Field -->
    {!! Former::text('featured')->label(__('products.fields.featured').':')->addGroupClass('col-sm-4')->placeholder(__('products.fields.featured').':') !!}
<!-- Max Lives Field -->
{!! Former::number('max_lives')->label(__('products.fields.max_lives').':')->addGroupClass('col-sm-4')->placeholder(__('products.fields.max_lives').':') !!}

{!! Former::select('expiry_type')->options([null=>'Select Expiry Type','max_lives'=>'Max Lives','date'=>'Date'])->label(__('Select Expiry Type').':')->addGroupClass('col-sm-4')

->state('warning') !!}

 </div>


<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-info']) !!}
    <a href="{{ route('products.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
