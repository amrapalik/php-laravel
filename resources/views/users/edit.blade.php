@extends('layouts.vertical', ["page_title"=> __('general.Edit').__('users.singular')])

@section('content')
<!-- Start Content-->
<div class="container-fluid">
<?php

$user_type = app('request')->input('utype');

?>

@if($user_type == 'student')
    @include('layouts.shared/page-title', ['title' => __('general.Edit')])
@elseif($user_type == 'teacher')
    @include('layouts.shared/page-title', ['title' => __('general.Edit')])
@else
    @include('layouts.shared/page-title', ['title' => __('general.Edit')])
@endif

    <!-- Start Form  -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    {{-- @if($user_type == 'student')
                        <h4 class="header-title">@lang('Student')</h4>
                    @elseif($user_type == 'teacher')
                        <h4 class="header-title">@lang('Teacher')</h4>
                    @else
                        <h4 class="header-title">@lang('users.singular')</h4>
                    @endif --}}


                    {!! Former::vertical_open_for_files()->id('edit-users-form')->populate($user)->setOption('live_validation', true)
                    ->method('PATCH')->route('users.update', $user->id) !!}

                        @include('users.fields')



                    {!! Former::close() !!}

                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end form -->
</div> <!-- container -->

@endsection


@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

    {!! JsValidator::formRequest('App\Http\Requests\UpdateUserRequest') !!}

    <script>
        $(document).ready(function () {
            /* $('#country_code').on('change', function () {
                var country_code = this.value;
               $("#country").html('');
                $.ajax({
                    url: "{{url('fetchCountry')}}",
                    type: "POST",
                    data: {
                        country_code: country_code,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {

                        $('#country').html();
                        $.each(result.country, function (key, value) {
                            $("#country").append('<option selected data-id="' + value
                                .id + '" value="' + value
                                .name + '">' + value.name + '</option>');
                        });

                       $('#state').html('<option value="">Select State</option>');
                        $.each(result.states, function (key, value) {
                            $("#state").append('<option data-id="' + value
                                .id + '" value="' + value
                                .name + '">' + value.name + '</option>');
                        });
                        $('#city').html('<option value="">Select City</option>');
                    }
                });
            });*/

            $('#country').on('change', function () {

                 var selected = $(this).find('option:selected');
                 var idCountry = selected.data('id');
               // var idCountry = this.value;
                $("#state").html('');
                $.ajax({
                    url: "{{url('fetchState')}}",
                    type: "POST",
                    data: {
                        country_id: idCountry,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {

                            $('#country_code').html();
                 $.each(result.country, function (key, value) {
                    $("#country_code").append('<option selected value="' + value
                        .phonecode + '">' + value.name +' (+'+value.phonecode+')' + '</option>');
                });

                        $('#state').html('<option value="">Select State</option>');
                        $.each(result.states, function (key, value) {
                            $("#state").append('<option data-id="' + value
                                .id + '" value="' + value
                                .name + '">' + value.name + '</option>');
                        });
                        $('#city').html('<option value="">Select City</option>');
                    }
                });
            });

            $('#state').on('change', function () {
                 var selected = $(this).find('option:selected');
                 var idState = selected.data('id');

                $("#city").html('');
                $.ajax({
                    url: "{{url('fetchCity')}}",
                    type: "POST",
                    data: {
                        state_id: idState,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (res) {
                        $('#city').html('<option value="">Select City</option>');
                        $.each(res.cities, function (key, value) {
                            $("#city").append('<option value="' + value
                                .name + '">' + value.name + '</option>');
                        });
                    }
                });
            });
        });

    </script>
@endsection
