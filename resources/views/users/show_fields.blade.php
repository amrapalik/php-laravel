<!-- Organization Orgid Field -->
<div class="col-sm-12">
    {!! Form::label('organization_orgid', __('users.fields.organization_orgid').':') !!}
    <p>{{ $user->organization_orgid }}</p>
</div>


<!-- School Schid Field -->
<div class="col-sm-12">
    {!! Form::label('school_schid', __('users.fields.school_schid').':') !!}
    <p>{{ $user->school_schid }}</p>
</div>


<!-- Username Field -->
<div class="col-sm-12">
    {!! Form::label('username', __('users.fields.username').':') !!}
    <p>{{ $user->username }}</p>
</div>


<!-- Firstname Field -->
<div class="col-sm-12">
    {!! Form::label('firstname', __('users.fields.firstname').':') !!}
    <p>{{ $user->firstname }}</p>
</div>


<!-- Lastname Field -->
<div class="col-sm-12">
    {!! Form::label('lastname', __('users.fields.lastname').':') !!}
    <p>{{ $user->lastname }}</p>
</div>


<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('users.fields.name').':') !!}
    <p>{{ $user->name }}</p>
</div>


<!-- Type Field -->
<div class="col-sm-12">
    {!! Form::label('type', __('users.fields.type').':') !!}
    <p>{{ $user->type }}</p>
</div>


<!-- Password Field -->
<div class="col-sm-12">
    {!! Form::label('password', __('users.fields.password').':') !!}
    <p>{{ $user->password }}</p>
</div>


<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', __('users.fields.email').':') !!}
    <p>{{ $user->email }}</p>
</div>


<!-- Drole Field -->
<div class="col-sm-12">
    {!! Form::label('drole', __('users.fields.drole').':') !!}
    <p>{{ $user->drole }}</p>
</div>


<!-- Grade Field -->
<div class="col-sm-12">
    {!! Form::label('grade', __('users.fields.grade').':') !!}
    <p>{{ $user->grade }}</p>
</div>


<!-- Phone No Field -->
<div class="col-sm-12">
    {!! Form::label('phone_no', __('users.fields.phone_no').':') !!}
    <p>{{ $user->phone_no }}</p>
</div>


<!-- Address Field -->
<div class="col-sm-12">
    {!! Form::label('address', __('users.fields.address').':') !!}
    <p>{{ $user->address }}</p>
</div>


<!-- Country Field -->
<div class="col-sm-12">
    {!! Form::label('country', __('users.fields.country').':') !!}
    <p>{{ $user->country }}</p>
</div>


<!-- Country Code Field -->
<div class="col-sm-12">
    {!! Form::label('country_code', __('users.fields.country_code').':') !!}
    <p>{{ $user->country_code }}</p>
</div>


<!-- Purchaseflag Field -->
<div class="col-sm-12">
    {!! Form::label('purchaseflag', __('users.fields.purchaseflag').':') !!}
    <p>{{ $user->purchaseflag }}</p>
</div>


<!-- Gender Field -->
<div class="col-sm-12">
    {!! Form::label('gender', __('users.fields.gender').':') !!}
    <p>{{ $user->gender }}</p>
</div>


<!-- Avatar Field -->
<div class="col-sm-12">
    {!! Form::label('avatar', __('users.fields.avatar').':') !!}
    <p>{{ $user->avatar }}</p>
</div>


<!-- Alternate Address Field -->
<div class="col-sm-12">
    {!! Form::label('alternate_address', __('users.fields.alternate_address').':') !!}
    <p>{{ $user->alternate_address }}</p>
</div>


<!-- City Field -->
<div class="col-sm-12">
    {!! Form::label('city', __('users.fields.city').':') !!}
    <p>{{ $user->city }}</p>
</div>


<!-- State Field -->
<div class="col-sm-12">
    {!! Form::label('state', __('users.fields.state').':') !!}
    <p>{{ $user->state }}</p>
</div>


<!-- Zipcode Field -->
<div class="col-sm-12">
    {!! Form::label('zipcode', __('users.fields.zipcode').':') !!}
    <p>{{ $user->zipcode }}</p>
</div>


<!-- Designation Field -->
<div class="col-sm-12">
    {!! Form::label('designation', __('users.fields.designation').':') !!}
    <p>{{ $user->designation }}</p>
</div>


<!-- Alternate Email Field -->
<div class="col-sm-12">
    {!! Form::label('alternate_email', __('users.fields.alternate_email').':') !!}
    <p>{{ $user->alternate_email }}</p>
</div>


<!-- Facebook Profile Field -->
<div class="col-sm-12">
    {!! Form::label('facebook_profile', __('users.fields.facebook_profile').':') !!}
    <p>{{ $user->facebook_profile }}</p>
</div>


<!-- Linkedin Profile Field -->
<div class="col-sm-12">
    {!! Form::label('linkedIn_profile', __('users.fields.linkedIn_profile').':') !!}
    <p>{{ $user->linkedIn_profile }}</p>
</div>


<!-- Email Verified At Field -->
<div class="col-sm-12">
    {!! Form::label('email_verified_at', __('users.fields.email_verified_at').':') !!}
    <p>{{ $user->email_verified_at }}</p>
</div>


<!-- Remember Token Field -->
<div class="col-sm-12">
    {!! Form::label('remember_token', __('users.fields.remember_token').':') !!}
    <p>{{ $user->remember_token }}</p>
</div>


<!-- Dob Field -->
<div class="col-sm-12">
    {!! Form::label('dob', __('users.fields.dob').':') !!}
    <p>{{ $user->dob }}</p>
</div>


<!-- Last Loggedin Field -->
<div class="col-sm-12">
    {!! Form::label('last_loggedin', __('users.fields.last_loggedin').':') !!}
    <p>{{ $user->last_loggedin }}</p>
</div>


<!-- Account Expiry Date Field -->
<div class="col-sm-12">
    {!! Form::label('account_expiry_date', __('users.fields.account_expiry_date').':') !!}
    <p>{{ $user->account_expiry_date }}</p>
</div>


<!-- Account Status Field -->
<div class="col-sm-12">
    {!! Form::label('account_status', __('users.fields.account_status').':') !!}
    <p>{{ $user->account_status }}</p>
</div>


<!-- Deallocated Field -->
<div class="col-sm-12">
    {!! Form::label('deallocated', __('users.fields.deallocated').':') !!}
    <p>{{ $user->deallocated }}</p>
</div>


<!-- Account Expired Field -->
<div class="col-sm-12">
    {!! Form::label('account_expired', __('users.fields.account_expired').':') !!}
    <p>{{ $user->account_expired }}</p>
</div>


