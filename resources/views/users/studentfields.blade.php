<?php
    $gender = [ null =>'Select Gender', 'Male'=>'Male','Female'=>'Female','Other'=>'Other'];
    ?>

<div class="row">

    <!-- Firstname Field -->
    {!! Former::text('firstname')->label(__('users.fields.firstname').':')->addGroupClass('col-sm-3')->placeholder('Firstname') ->required()!!}

    <!-- Lastname Field -->
    {!! Former::text('lastname')->label(__('users.fields.lastname').':')->addGroupClass('col-sm-3')->placeholder('Lastname')->required() !!}

     <!-- Username Field -->
     {!! Former::text('username')->label(__('users.fields.username').':')->addGroupClass('col-sm-3')->value($user->username)->placeholder('Username') !!}

    {!! Former::select('gender')->label(__('users.fields.gender').':')->options($gender)->addGroupClass('col-sm-3')

->state('warning')->required() !!}

</div>



<div class="row">


    {!! Former::text('grade')->label(__('users.fields.grade').':')->addGroupClass('col-sm-3')->placeholder('Grade') !!}

     <!-- Email Field -->
    {!! Former::email('email')->label(__('users.fields.email').':')->addGroupClass('col-sm-3')->placeholder('Email') !!}

</div>


<div class="row">

    <div class="col-sm-12">
        <br> <br> <br>
        <h5> @lang('passwords.If you want to change password fill it otherwise left it blank')</h5>
    </div>
    <!-- Password Field -->
{!! Former::password('password')->label(__('users.fields.password').':')->addGroupClass('col-sm-3')->placeholder('Password')->required() !!}

<!-- Password Field -->
    {!! Former::password('password_confirmation')->label(__('users.fields.confirmpassword').':')->addGroupClass('col-sm-3')->placeholder(__('users.fields.confirmpassword'))->required() !!}


</div>

<div class="row">

<!-- Gender Field -->
</div>
<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-info']) !!}
    <a href="/gamedashboard" class="btn btn-default">@lang('crud.cancel')</a>
</div>
