
<!-- Lastname Field -->
{!! Former::hidden('utype')->label(__('usertype').':')->addGroupClass('col-sm-3')->value($user_type)  !!}


<div class="row">

    <!-- Firstname Field -->
    {!! Former::text('firstname')->label(__('users.fields.firstname').':')->addGroupClass('col-sm-3')->placeholder('Firstname') ->required()!!}

    <!-- Lastname Field -->
    {!! Former::text('lastname')->label(__('users.fields.lastname').':')->addGroupClass('col-sm-3')->placeholder('Lastname')->required() !!}

     <!-- Username Field -->
    {!! Former::text('username')->label(__('users.fields.username').':')->addGroupClass('col-sm-3')->placeholder('Username')->required() !!}



    <?php

    $RoleOptions = ['Organization Admin'=>'Organization Admin','School Admin'=>'School Admin','Teacher'=>'Teacher','Student'=>'Student','Gamer'=>'Gamer'];
    $role = auth()->user()->drole;

    $options =[];
    if($role == 'Organization Admin' || $role == 'School Admin')
    {
        $options = [ null =>'Select user Type', 'School Admin'=>'School Admin','Teacher'=>'Teacher','Student'=>'Student'];
    }

    if($role == 'Teacher')
    {
        $options = [ null =>'Select user Type', 'Student'=>'Student'];
    }
 


    $gender = [ null =>'Select Gender', 'Male'=>'Male','Female'=>'Female','Other'=>'Other'];

//    if($role == 'School Admin')
//    {
//        $options = [ 'Teacher'=>'Teacher','Student'=>'Student'];
//    }
//
//    if($role == 'School Admin')
//    {
//        $options = [ 'Teacher'=>'Teacher','Student'=>'Student'];
//    }



    ?>


    @if($user_type != 'teacher')

<!-- Type Field -->
    {!! Former::select('drole')->label(__('users.fields.drole').':')->options($RoleOptions)->addGroupClass('col-sm-3')

  ->state('warning')->required() !!}

        @else
        <!-- Type Field -->
            {!! Former::hidden('drole')->label(__('usertype').':')->addGroupClass('col-sm-3')->value('Teacher')  !!}
        @endif
        <!-- Email Field -->
    {!! Former::email('email')->label(__('users.fields.email').':')->addGroupClass('col-sm-3')->placeholder('Email') !!}


  

</div>




<div class="row">
    <!-- Password Field -->
    {!! Former::password('password')->label(__('users.fields.password').':')->addGroupClass('col-sm-3')->placeholder('Password')->required() !!}

     {!! Former::password('password_confirmation')->label(__('users.fields.confirmpassword').':')->addGroupClass('col-sm-3')->placeholder(__('users.fields.confirmpassword'))->required()  !!}


    <!-- Dob Field -->
    {!! Former::date('dob')->label(__('users.fields.dob').':')->addGroupClass('col-sm-3')->placeholder('Dob') !!}

    @push('page_scripts')
        <script type="text/javascript">
            $('#dob').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                useCurrent: true,
                sideBySide: true
            })
        </script>
    @endpush

     @if($user_type == 'student')
    <!-- Grade Field -->
{!! Former::text('grade')->label(__('users.fields.grade').':')->addGroupClass('col-sm-6')->placeholder('Grade') !!}
    @else
<!-- Designation Field -->
    {!! Former::text('designation')->label(__('users.fields.designation').':')->addGroupClass('col-sm-3')->placeholder('Designation') !!}
        @endif
    </div>






<div class="row">
    <!-- Address Field -->
    {!! Former::text('address')->label(__('users.fields.address').':')->addGroupClass('col-sm-6')->placeholder('Address') !!}

      <!-- Alternate Address Field -->
{!! Former::text('alternate_address')->label(__('users.fields.alternate_address').':')->addGroupClass('col-sm-6')->placeholder('Alternate Address') !!}



</div>

<div class="row">

    <!-- Country Field -->
   <!--  {!! Former::select('country')->fromQuery(App\Models\Countries::get(), 'name','name')->label(__('users.fields.country').':')->addGroupClass('col-sm-3')->placeholder('Country') !!} -->


    <div class="form-group col-md-3">
      <label for="country" class="col-form-label">Country:</label>
      <select id="country" name="country" class="form-control" required>
      <option value="">Country</option>

       <?php

        $countryss = App\Models\Countries::get();

        $countries =[];
        foreach ($countryss as $value)
        { ?>

        <option <?php if(isset($user->country) && $user->country==$value->name) echo "selected" ?> data-id="{{ $value->id }}" value="{{ $value->name }}">{{ $value->name }}</option>

        <?php

        }
        ?>
      </select>
    </div>

     @if(Request::is('users/create') )

     <!-- State Field -->
    {!! Former::select('state')->label(__('users.fields.state').':')->addGroupClass('col-sm-3')->placeholder('State') !!}

     @elseif(!empty($user->country))

     <?php

     $country =   App\Models\Countries::where('name',$user->country)->first();
        ?>

      {!! Former::select('state')->fromQuery(App\Models\State::where('country_id',$country->id)->get(), 'name','name')->label(__('users.fields.state').':')->addGroupClass('col-sm-3')->placeholder('State') !!}

     @else

     {!! Former::select('state')->label(__('users.fields.state').':')->addGroupClass('col-sm-3')->placeholder('State') !!}

     @endif




    @if(Request::is('users/create') )

     {!! Former::select('city')->label(__('users.fields.city').':')->addGroupClass('col-sm-3')->placeholder('City') !!}

    @elseif(!empty($user->state))
    <?php
     $state =   App\Models\State::where('name',$user->state)->first();
        ?>
     <!-- City Field -->
    {!! Former::select('city')->fromQuery(App\Models\City::where('state_id',$state->id)->get(), 'name','name')->label(__('users.fields.city').':')->addGroupClass('col-sm-3')->placeholder('City') !!}
    @else

    {!! Former::select('city')->label(__('users.fields.city').':')->addGroupClass('col-sm-3')->placeholder('City') !!}

    @endif

    <!-- fromQuery(App\Models\City::where('state_id','22')->get(), 'name','name') -->

    <!-- Zipcode Field -->
    {!! Former::text('zipcode')->label(__('users.fields.zipcode').':')->addGroupClass('col-sm-3')->placeholder('Zipcode') !!}


</div>


<?php


$countryss = App\Models\Countries::get();

$countries =[];
foreach ($countryss as $value) {
    # code...


     $countries[$value->phonecode] = $value->name.' (+'.$value->phonecode.')';

     //$countries [] =  $countriesd;
}
 ?>
<div class="row">

  <!-- Gender Field -->

{!! Former::select('gender')->label(__('users.fields.gender').':')->options($gender)->addGroupClass('col-sm-3')

->state('warning')->required() !!}

<!-- Alternate Email Field -->
    {!! Former::text('alternate_email')->label(__('users.fields.alternate_email').':')->addGroupClass('col-sm-3')->placeholder('Alternate Email') !!}


<!-- Country Code Field -->
    {!! Former::select('country_code')->options($countries)->label(__('users.fields.country_code').':')->addGroupClass('col-sm-3')->placeholder('Country Code') !!}

    <!-- Phone No Field -->
    {!! Former::text('user_mobile')->label(__('users.fields.user_mobile').':')->addGroupClass('col-sm-3')->placeholder('Mobile No') !!}



</div>

<div class="row">
    <!-- Facebook Profile Field -->
    {!! Former::text('facebook_profile')->label(__('users.fields.facebook_profile').':')->addGroupClass('col-sm-6')->placeholder('Facebook Profile') !!}

    <!-- Linkedin Profile Field -->
    {!! Former::text('linkedIn_profile')->label(__('users.fields.linkedIn_profile').':')->addGroupClass('col-sm-6')->placeholder('Linkedin Profile') !!}
</div>







<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-info']) !!}
    <a href="{{URL::previous()}}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
