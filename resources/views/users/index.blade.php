@extends('layouts.vertical', ["page_title"=> __('users.plural')])

@section('css')
    <!-- third party css -->
    <link href="{{asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
@endsection


@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
{{--                    <div class="page-title-right">--}}
{{--                        <ol class="breadcrumb m-0">--}}
{{--                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>--}}
{{--                            <li class="breadcrumb-item"><a href="javascript: void(0);">Tables</a></li>--}}
{{--                            <li class="breadcrumb-item active">Datatables</li>--}}
{{--                        </ol>--}}
{{--                    </div>--}}
                    <h4 class="page-title"> @lang('users.plural')</h4>

                </div>
            </div>
        </div>
        <!-- end page title -->


        @include('flash::message')

        <div class="clearfix"></div>

        <div class="row">  <!-- Data table-->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-9">

                                <h4 class="header-title" style="float: left; margin-top: 10px">@lang('users.plural')</h4>
                            </div>
                                <div class="col-3">
                                    <a href="{{ route('users.create') }}" style="float: right" class="btn btn-success rounded-pill waves-effect waves-light"> @lang('pagination.datatable.create') @lang('users.singular')</a>

                                @can('Create Users')
                                    <a href="{{ route('users.create') }}" style="float: right" class="btn btn-success rounded-pill waves-effect waves-light"> @lang('pagination.datatable.create') @lang('users.singular')</a>
                                @endcan
                                </div>
                        </div>



                        <p class="text-muted font-13 mb-4">

                        </p>

                        {!! $dataTable->table(['width' => '100%', 'class' => 'table dt-responsive nowrap w-100 ']) !!}
                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->
    </div> <!-- end container -->
@endsection


@section('script')
    @include('layouts.datatables_js')
    {{--    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>--}}
    <script>
        //dataTableBuilder
        $('#btn').on('click',function (){
            $('#dataTableBuilder').DataTable().ajax.reload();
        })
       // $('#dataTableBuilder').DataTable().ajax.reload();
        function assignAdmin(e, recid)
        {
            e.preventDefault();
    
            $('#loading-indicator').show();
    
            uid = {{ auth()->user()->id }};
    
            if(uid == recid)
            {
                toastr.error(  "@lang('teachers.role_cant_update_own_role')", {timeOut: 6000});
                return 0;
            }
    
            $.ajax({
                type: "GET",
                url : "/toggleActivateUser?userid="+recid,
                cache: false,
                success: function (data) {
                    $('#loading-indicator').hide();
                     toastr.success(  "User Updated Successfully", {timeOut: 6000});
    
                  //  location.reload();
                    //TODO:: Reload table
                    $('#dataTableBuilder').DataTable().ajax.reload();
                    //dataTableBuilder.ajax.reload( null, false );
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert (textStatus);
                    $('#loading-indicator').hide();
                }
            });
        }
    </script>
    {!! $dataTable->scripts() !!}
@endsection
