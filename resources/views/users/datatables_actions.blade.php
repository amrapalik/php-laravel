{!! Form::open(['route' => ['users.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('users.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    <a href="{{ route('users.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>

    <a href="#" onclick="assignAdmin(event,{{$id}})" title="Activate/Deactivate User"   class='btn btn-default btn-xs'>
        @if($purchaseflag == 'Yes')
            <i class="fa fa-hand-point-up" style="color:rgb(8, 226, 37)" ></i>
        @else
            <i class="fa fa-hand-point-up" style="color:red"></i>
        @endif
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
</div>
{!! Form::close() !!}
