@extends('layouts.master')

@section('css')
<!-- third party css -->
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- third party css end -->
@endsection

@section('content')

<!-- Start Content-->
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">SDG's Chosen While Creating Life</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <table id="alternative-page-datatable" class="table table-striped dt-responsive nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>Character Name</th>
                                <th>Sustainable Development Goals (Name - Number)</th>
                                <th>Sex</th>
                                <th>Country name</th>
                            </tr>
                        </thead>


                        <tbody>
                            @foreach ($selectedSdgs as $person)
                            <tr>
                                <td>{{ $person['full_name'] }}</td>
                                <td>{{ $person['SDG_title'] }} - {{ $person['SDG_Id'] }}</td>
                                <td>{{ $person['sex'] }}</td>
                                <td>{{ $person['country']['country'] }}</td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
</div>
<!-- end row-->
@endsection
@section('script')

<!-- third party js -->
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<!-- third party js ends -->

<!-- Datatables init -->
<script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>

@endsection