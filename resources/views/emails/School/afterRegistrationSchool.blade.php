<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />

    <title>Page title</title>

    <style type="text/css">
        @import url('https://fonts.googleapis.com/css?family=Biryani:200,400,700&subset=devanagari,latin-ext');

        #outlook a {
            padding: 0;
        }

        body {
            width: 100% !important;
            -webkit-text;
            size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            margin: 0;
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .backgroundTable {
            margin: 0 auto;
            padding: 0;
            width: 100%;
            !important;
        }

        table td {
            border-collapse: collapse;
            background-size: cover;
        }

        .ExternalClass * {
            line-height: 115%;
        }

        /* These are our tablet/medium screen media queries */

        @media screen and (max-width: 630px) {
            /* Display block allows us to stack elements */
            *[class="mobile-column"] {
                display: block;
            }

            /* Some more stacking elements */
            *[class="mob-column"] {
                float: none !important;
                width: 100% !important;
            }

            /* Hide stuff */
            *[class="hide"] {
                display: none !important;
            }

            /* This sets elements to 100% width and fixes the height issues too, a god send */
            *[class="100p"] {
                width: 100% !important;
                height: auto !important;
            }

            /* For the 2x2 stack */
            *[class="condensed"] {
                padding-bottom: 40px !important;
                display: block;
            }

            /* Centers content on mobile */
            *[class="center"] {
                text-align: center !important;
                width: 100% !important;
                height: auto !important;
            }

            /* 100percent width section with 20px padding */
            *[class="100pad"] {
                width: 100% !important;
                padding: 20px;
            }

            /* 100percent width section with 20px padding left & right */
            *[class="100padleftright"] {
                width: 100% !important;
                padding: 0 20px 0 20px;
            }

            /* 100percent width section with 20px padding top & bottom */
            *[class="100padtopbottom"] {
                width: 100% !important;
                padding: 20px 0px 20px 0px;
            }
        }
    </style>

</head>

<body style="padding:0;margin:0 auto;background: #262626; text-align: center;">
    <table width="640" cellspacing="0" cellpadding="20" bgcolor="#262626" class="100p" style="margin: 0 auto;">
        <tr>
            <td>
                <table width="640" cellspacing="0" cellpadding="0" bgcolor="#" class="100p">
                    <tr>
                        <td background="https://reallivesworld.com/game/assets/start_web_bg.jpg" bgcolor="#000000" width="640" valign="top" class="100p">
                            <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                            <v:fill type="tile" src="images/header-bg.jpg" color="#3b464e" />
                            <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                            <![endif]-->
                            <div>
                                <table width="640" border="0" cellspacing="0" cellpadding="20" class="100p">
                                    <tr>
                                        <td valign="top">
                                            <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                <tr>
                                                    <td align="center" width="50%" class="100p">
                                                        <img src="https://reallivesworld.com/game/assets/images/RL_logo_big.png" alt="Logo" border="0" style="display:block;width: 280px;height: auto;text-align: center;" />
                                                    </td>
                                                    <!--<td width="50%" class="hide" align="right" style="font-size:16px; color:#FFFFFF;">
                                                        <font face="'Biryani', sans-serif">
                                                            <a href="#" style="color:#FFFFFF; text-decoration:none;">HOME</a> &nbsp; &nbsp; &nbsp; | &nbsp; &nbsp; &nbsp;
                                                            <a href="#" style="color:#FFFFFF; text-decoration:none;">HOME</a> &nbsp; &nbsp; &nbsp; | &nbsp; &nbsp; &nbsp;
                                                            <a href="#" style="color:#FFFFFF; text-decoration:none;">HOME</a>
                                                        </font>
                                                    </td>-->
                                                </tr>
                                            </table>
                                            <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                <tr>
                                                    <td height="35"></td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="color:#0f95ac; font-size:24px;">
                                                        <font face="'Biryani', sans-serif">
                                                            <span style="font-size: 18px;letter-spacing: 7px;font-weight: 100;">LIVE&nbsp;&nbsp;|&nbsp;&nbsp;EMPATHIZE&nbsp;&nbsp;|&nbsp;&nbsp;ACT</span><br />
                                                            <br />
                                                        </font>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td align="left" style="color:#0f95ac; font-size:24px;">
                                                        <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                            <tr>
                                                                <td style="padding: 0 100px;">
                                                                    <font face="'Biryani', sans-serif">
                                                                        <span style="font-size: 14px; font-weight: 100; text-align: left; color: #ffffff">
                                                                            <span style="font-size: 18px; font-weight: 400">Dear&nbsp;{{$fullname}},</span><br/><br/>
                                                                            <span style="font-size: 18px; font-weight: 400"> {{ $school_name }},</span><br/><br/>
                                                                            We appreciate your interest in RealLives!<br/> <br/>

                                                                            @if( $school_type != 'family')
                                                                            You've successfully registered your {{ $school_type }} for an academic license on  <a href="www.reallivesworld.com " style="color:#3da2b7">www.reallivesworld.com </a>, but you haven't purchased the license yet. <br/><br/>

                                                                            RealLivesWorld, the world's largest gamified simulation engine of human life, is the best place to learn empathy, social-emotional skills, and global citizenship! In RealLives simulations, you'll learn how ordinary people live and thrive around the world. Thus, RealLives puts a human face on real-life statistics, making it a real-life experience!<br/><br/>

                                                                            Students are changemakers! As a team, we are delighted that your students will gain a thorough understanding of the real world and its complexity, be empowered to take leadership roles, learn 21C skills, sustainable development, multiculturalism, and peace, and be empowered to become changemakers! RealLives allows students to practise making important decisions that require critical thinking in a low-risk environment, but with enough simulation to generate an understanding of empathy.<br/><br/>

                                                                            Please let us know how you would like to proceed to purchase the {{ $school_type }} licence and bring this game-changing application to your classrooms where teachers and students can learn and make sense of the world around them!
                                                                            @else

                                                                            You've successfully registered your family.<br/><br/>
                                                                            Using RealLivesWorld, the world's largest gamified simulation engine of human life, you can learn empathy, social-emotional skills, and global citizenship! RealLives uses real-world statistical data from 193 countries around the world to simulate the lives of ordinary people across the globe. A Nobel Laureate, Dr. Lee Hartwell described RealLives as a simulation game that puts a 'face' on real-world statistical data, making it an immersive real-life experience!<br/><br/>

                                                                            Young people have great abilities, and they can surely be groomed as change-makers and leaders who can shape the world around them into a better and justified place! As a RealLives team, we are delighted that your children will gain a thorough understanding of the real world and its complexities, be empowered to take leadership roles, learn 21C skills, awareness of sustainable development, and facets of multiculturalism, and understand peace and stability. RealLives allows young people to practise making important life decisions that require testing one's critical thinking abilities in a low-risk environment like in the RealLives game.<br/><br/>

                                                                            We are promoting the 'family license' methodology to make your family go through a wonderful learning experience about the real world and discuss issues relating to humanity as well as issues and challenges that are still bothering us all. Your purchase is also going to help a social-serious simulation game like RealLives which is dedicated to bringing nobler instincts and good values to young & youth.<br/><br/>

                                                                            RealLives team once again congratulates you for taking these creative steps to bring a learning ecosystem into your home and make your home the most engaging learning place for everyone! <br/><br/>

                                                                            We will also be interested in knowing your feedback and your comments if any to constantly improve both RealLives and learning design!
                                                                            @endif

                                                                            <br/><br/>
                                                                            <span style="font-size: 14px; font-weight: 100;">
                                                                                 Your username : {!! $username !!}<br>
                                                                                Your email : {!! $email !!}<br>  <br/>

                                                                                Best regards,<br/>
                                                                                RealLives Support Team<br/>
                                                                               <a style="color:#3da2b7" >“We must be willing to let go of the life we planned so as to have the life that is waiting for us.” Joseph Campbell</a>                                                                        <!--<br/>Password: (Password)-->
                                                                            </span>
                                                                        </span>
                                                                    </font>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td height="15"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--[if gte mso 9]>
                        </v:textbox>
                        </v:rect>
                        <![endif]-->
                        </td>
                    </tr>
                </table>
                <br/>
                <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#2a8e9d" class="100p">
                    <tr>
                        <td align="center" style="font-size:20px; color:#ffffff; font-weight: 100">
                            <!--<font face="'Biryani', sans-serif">Nothing is more urgent than empathy</font>-->
                            <font face="'Biryani', sans-serif">
                                <a href="https://reallivesworld.com" style="color:#ffffff; text-decoration: none; letter-spacing: 7px; text-transform: uppercase;">On to the Experience >>></a>
                            </font>
                        </td>
                    </tr>
                </table>
                <!--<table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#262626" class="100p">
                    <tr>
                        <td align="left" style="font-size:16px;color:#ffffff;font-weight: 100;padding: 15px 100px;">
                            <font face="'Biryani', sans-serif"><span style="color:#2a8e9d; font-size:20px; font-weight: 700">RealLives' Aims</span><br />
                                <br />
                                <span style="font-size:12px;">Create global impact for sustainable development, multiculturalism, and peace. For this it aims to provide empathy building experiences to all, especially the millennials. It aims to provide an Empathy to Action platform for empowering people to connect with purpose, learn and teach social emotional skills, and mobilize teams so that they become change agents</span></font>
                        </td>
                    </tr>
                </table>-->
                <br/>
                <table width="640" border="0" cellspacing="0" cellpadding="5" bgcolor="#000000" class="100p">
                    <tr>
                        <td align="left" style="padding: 5px 20px;">
                            <font face="'Biryani', sans-serif">
                                <span style="color:#585858; font-size:10px; font-weight: 400">&copy; RealLivesworld {{ date('Y') }}</span>
                            </font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</body>

</html>
