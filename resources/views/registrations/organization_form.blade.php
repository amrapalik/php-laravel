
<div class="row">
	<input type="hidden" name="org_type" value="{{ $org_type }}" />
    <!-- Org Name Field -->
    {!! Former::text('org_name')->label(__('organizations.fields_'.$org_type.'.org_name').':')->addGroupClass('col-sm-6')->placeholder(__('organizations.fields_'.$org_type.'.org_name'))->required() !!}

    <!-- Org Email Field -->
    {!! Former::text('org_email')->label(__('organizations.fields_'.$org_type.'.org_email').':')->addGroupClass('col-sm-6')->placeholder(__('organizations.fields.org_email')) !!}
</div>

<div class="row">
    <!-- About Field -->
    {!! Former::textarea('about')->label(__('organizations.fields_'.$org_type.'.about').':')->addGroupClass('col-sm-12 col-lg-12')->placeholder(__('organizations.fields.about')) !!}
</div>

<div class="row">
    <!-- Address1 Field -->
    {!! Former::text('address1')->label(__('organizations.fields.address1').':')->addGroupClass('col-sm-6')->placeholder(__('organizations.fields.address1'))->required() !!}

    <!-- Address2 Field -->
    {!! Former::text('address2')->label(__('organizations.fields.address2').':')->addGroupClass('col-sm-6')->placeholder(__('organizations.fields.address2'))->required() !!}
</div>

<div class="row">

    <!-- Country Field -->
    <!-- {!! Former::select('country')->fromQuery(App\Models\Countries::get(), 'name','name')->label(__('users.fields.country').':')->addGroupClass('col-sm-3')->placeholder('Country')->required() !!} -->

     <div class="form-group col-md-3">
      <label for="country" class="col-form-label">Country:</label>
      <select id="country" name="country" class="form-control" required>
      <option value="">Country</option>

       <?php

        $countryss = App\Models\Countries::get();

        $countries =[];
        foreach ($countryss as $value)
        { ?>

        <option <?php if(isset($user->country) && $user->country==$value->name) echo "selected" ?> data-id="{{ $value->id }}" value="{{ $value->name }}">{{ $value->name }}</option>

        <?php

        }
        ?>
      </select>
    </div>

    <!-- State Field -->
    {!! Former::select('state')->label(__('users.fields.state').':')->addGroupClass('col-sm-3')->placeholder(__('users.fields.state'))->required() !!}
    <!-- ->fromQuery(App\Models\State::where('name','state')->get(), 'name','name') -->
    <!-- City Field -->
    {!! Former::select('city')->label(__('users.fields.city').':')->addGroupClass('col-sm-3')->placeholder(__('users.fields.city'))->required() !!}
    <!-- fromQuery(App\Models\City::where('state_id','22')->get(), 'name','name') -->

    <!-- Zipcode Field -->
    {!! Former::text('zipcode')->label(__('users.fields.zipcode').':')->addGroupClass('col-sm-3')->placeholder(__('users.fields.zipcode')) !!}


</div>

<div class="row">
    <!-- Website Field -->
    {!! Former::text('website')->label(__('organizations.fields_'.$org_type.'.website').':')->addGroupClass('col-sm-6')
    ->help(__('organizations.fields.website_help'))

->placeholder(__('organizations.fields_'.$org_type.'.website')) !!}

<?php
    $countryss = App\Models\Countries::get();

    $countries =[];
    foreach ($countryss as $value) {
        # code...


         $countries[$value->phonecode] = $value->name.' (+'.$value->phonecode.')';

         //$countries [] =  $countriesd;
    }
 ?>
<!-- Country Code Field -->
    {!! Former::select('country_code')->options($countries)->label(__('users.fields.country_code').':')->addGroupClass('col-sm-3')->placeholder(__('users.fields.country_code'))->required() !!}

    <!-- Phone Field -->
    {!! Former::text('phone')->label(__('organizations.fields.phone').':')->addGroupClass('col-sm-3')->placeholder(__('organizations.fields.phone'))->required() !!}


</div>


<div class="row">

<!-- Facebook Field -->
{!! Former::text('facebook')->label(__('organizations.fields.facebook').':')->addGroupClass('col-sm-6')
    ->help(__('organizations.fields.facebook_help'))->placeholder(__('organizations.fields.facebook')) !!}

 <!-- Linkedin Field -->
    {!! Former::text('linkedin')->label(__('organizations.fields.linkedin').':')->addGroupClass('col-sm-6')
    ->help(__('organizations.fields.linkedin_help'))->placeholder(__('organizations.fields.linkedin')) !!}
   </div>

<br><br><br><br>

<hr>
<div class="row text-center">
    <h4>
        Information about the admin who will manage the RealLives account
    </h4>
</div>
<hr>
<br>

<h4 class="header-title">{{ __('organizations.'.$org_type) }} Admin Information</h4>

<div class="row">
    <!-- Firstname Field -->
    {!! Former::text('firstname')->label(__('users.fields.firstname').':')->addGroupClass('col-sm-3')->placeholder('Firstname')->required() !!}

    <!-- Lastname Field -->
    {!! Former::text('lastname')->label(__('users.fields.lastname').':')->addGroupClass('col-sm-3')->placeholder('Lastname')->required() !!}

     <!-- Username Field -->
    {!! Former::text('username')->label(__('users.fields.username').':')->addGroupClass('col-sm-3')->placeholder('Username')->required() !!}

       <!-- Gender Field -->

{!! Former::select('gender')->options(['Male'=>'Male','Female'=>'Female','Other'=>'Other'], 1)->addGroupClass('col-sm-3')

  ->state('warning') !!}
</div>



<div class="row">
     <!-- Email Field -->
    {!! Former::email('email')->label(__('users.fields.email').':')->addGroupClass('col-sm-6')->placeholder('Email')->required() !!}


      <?php


$countryss = App\Models\Countries::get();

$countries =[];
foreach ($countryss as $value) {
    # code...


     $countries[$value->phonecode] = $value->name.' (+'.$value->phonecode.')';

     //$countries [] =  $countriesd;
}
 ?>
<!-- Country Code Field -->
    {!! Former::select('country_code')->options($countries)->label(__('users.fields.country_code').':')->addGroupClass('col-sm-3')->placeholder('Country Code') !!}

    <!-- Phone No Field -->
    {!! Former::text('user_mobile')->label(__('users.fields.user_mobile').':')->addGroupClass('col-sm-3')->placeholder('Mobile No') !!}
</div>

<div class="row">

  <!-- Password Field -->
    {!! Former::password('password')->label(__('users.fields.password').':')->addGroupClass('col-sm-3')->placeholder('Password')->required() !!}

    <!-- Password Field -->
    {!! Former::password('password_confirmation')->label(__('users.fields.confirmpassword').':')->addGroupClass('col-sm-3')->placeholder(__('users.fields.confirmpassword'))->required() !!}

 <!-- Dob Field -->
    {!! Former::date('dob')->label(__('users.fields.dob').':')->addGroupClass('col-sm-3')->placeholder('Dob') !!}

    <!-- Designation Field -->
    {!! Former::text('designation')->label(__('users.fields.designation').':')->addGroupClass('col-sm-3')->placeholder('Designation') !!}
</div>
<br>




@push('page_scripts')
    <script type="text/javascript">
        $('#dob').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<script src="https://www.google.com/recaptcha/api.js" 
                            async defer>
</script>

<div class="g-recaptcha" id="feedback-recaptcha" 
    data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}">
</div>
<br><br>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('registrations.Register Your '.$org_type), ['class' => 'btn btn-info']) !!}
{{--    <a href="{{ route('users.index') }}" class="btn btn-default">@lang('crud.cancel')</a>--}}
</div>
