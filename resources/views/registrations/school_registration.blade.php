<!DOCTYPE html>
<html lang="en">

<head>

    @include('layouts.shared/title-meta', ['title' => "Log In"])
    @include('layouts.shared/head-css', ["mode" => $mode ?? '', "demo" => $demo ?? ''])
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <style>

        .game-background{
            background: url(../assets/images/RL-people-graphic.jpg);
            -khtml-opacity:.50;
            -moz-opacity:.50;
            -ms-filter:"alpha(opacity=50)";
            filter:alpha(opacity=50);
            filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0.5);
            opacity:.50;
        }

    </style>

</head>
@php
	$school_type = 'school';
	if(Route::is('register-university-dept')){
		$school_type = 'university_dept';
	}
    else if(Route::is('register-family')){
        $school_type = 'family';
    }
@endphp

<body class="loading game-background">

<div class="account-pages mt-5 mb-5">
    <div class="container">
        @include('layouts.common.errors')
        @include('flash::message')


        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-9 col-xl-9">
                <div class="card bg-pattern">

                    <div class="card-body p-8">

                        <div class="text-center w-75 m-auto">
                            <div class="auth-logo">
                                <a href="{{route('gamedashboard')}}" class="logo logo-dark text-center">
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/rl_logo_black.svg')}}" alt="" height="62">
                                        </span>
                                </a>

                                <a href="{{route('gamedashboard')}}" class="logo logo-light text-center">
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/logo-light.png')}}" alt="" height="62">
                                        </span>
                                </a>
                            </div>
                            <br> <br>
                            <h3>{{ __('registrations.Register Your '.$school_type) }}</h3>

                        </div>


                        @if(session('error'))<div class="alert alert-danger">{{ session('error') }}</div>
                        <br>@endif
                        @if(session('success'))<div class=" alert alert-success">{{ session('success') }}
                        </div>
                        <br>@endif

                        @if (sizeof($errors) > 0)
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li class="text-danger">{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <h5 class="text-muted mb-3 mt-3 text-center">First you can register and then buy license from your dashboard. Your registration will remain active.</h5>
                        <h4 class="header-title">@lang('schools.'.$school_type) Info</h4>

                        {!! Former::vertical_open_for_files()->id('create-school-form')->method('POST')->route('register-school-store') !!}

                        @include('registrations.school_form')

                        {!! Former::close() !!}


                    </div> <!-- end card-body -->
                </div>
                <!-- end card -->

                <div class="row mt-3">
                    <div class="col-12 text-center">
                        <p class="text-white-50">Do you already have an account? <a href="/login" class="text-white ms-1"><b> Login</b></a></p>
                    </div> <!-- end col -->
                </div>
                <!-- end row -->

            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end page -->

{{--<footer class="footer footer-alt">--}}
{{--    2015 - <script>--}}
{{--        document.write(new Date().getFullYear())--}}
{{--    </script> &copy; UBold theme by <a href="" class="text-white-50">Coderthemes</a>--}}
{{--</footer>--}}

@include('layouts.shared/footer-script')


<!-- Laravel Javascript Validation -->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

{!! JsValidator::formRequest('App\Http\Requests\RegisterSchoolRequest') !!}

<script>
        $(document).ready(function () {
            /* $('#country_code').on('change', function () {
                var country_code = this.value;
               $("#country").html('');
                $.ajax({
                    url: "{{url('fetchCountry')}}",
                    type: "POST",
                    data: {
                        country_code: country_code,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {

                        $('#country').html();
                        $.each(result.country, function (key, value) {
                            $("#country").append('<option selected data-id="' + value
                                .id + '" value="' + value
                                .name + '">' + value.name + '</option>');
                        });

                       $('#state').html('<option value="">Select State</option>');
                        $.each(result.states, function (key, value) {
                            $("#state").append('<option data-id="' + value
                                .id + '" value="' + value
                                .name + '">' + value.name + '</option>');
                        });
                        $('#city').html('<option value="">Select City</option>');
                    }
                });
            });*/

            $('#country').on('change', function () {

                 var selected = $(this).find('option:selected');
                 var idCountry = selected.data('id');
               // var idCountry = this.value;
                $("#state").html('');
                $.ajax({
                    url: "{{url('fetchState')}}",
                    type: "POST",
                    data: {
                        country_id: idCountry,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {

                                 $('#country_code').html();
                 $.each(result.country, function (key, value) {
                    $("#country_code").append('<option selected value="' + value
                        .phonecode + '">' + value.name +' (+'+value.phonecode+')' + '</option>');
                });

                        $('#state').html('<option value="">Select State</option>');
                        $.each(result.states, function (key, value) {
                            $("#state").append('<option data-id="' + value
                                .id + '" value="' + value
                                .name + '">' + value.name + '</option>');
                        });
                        $('#city').html('<option value="">Select City</option>');
                    }
                });
            });

            $('#state').on('change', function () {
                 var selected = $(this).find('option:selected');
                 var idState = selected.data('id');

                $("#city").html('');
                $.ajax({
                    url: "{{url('fetchCity')}}",
                    type: "POST",
                    data: {
                        state_id: idState,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (res) {
                        $('#city').html('<option value="">Select City</option>');
                        $.each(res.cities, function (key, value) {
                            $("#city").append('<option value="' + value
                                .name + '">' + value.name + '</option>');
                        });
                    }
                });
            });
        });

    </script>

</body>

</html>
