
<h5 class="text-muted mb-3 mt-3 text-center">First you can register and then buy license from your dashboard. Your registration will remain active.</h5>
<h4 class="header-title">Gamer Information</h4>

<div class="row">
    <!-- Firstname Field -->
{!! Former::text('firstname')->label(__('users.fields.firstname').':')->addGroupClass('col-sm-6')->placeholder('Firstname')->required() !!}

<!-- Lastname Field -->
    {!! Former::text('lastname')->label(__('users.fields.lastname').':')->addGroupClass('col-sm-6')->placeholder('Lastname')->required() !!}
</div>


<div class="row">
    <!-- Email Field -->
{!! Former::email('email')->label(__('users.fields.email').':')->addGroupClass('col-sm-6')->placeholder('Email')->required() !!}


<!-- Username Field -->
{!! Former::text('username')->label(__('users.fields.username').':')->addGroupClass('col-sm-6')->placeholder('Username')->required() !!}

</div>



<div class="row">
    <!-- Gender Field -->

{!! Former::select('gender')->options(['Male'=>'Male','Female'=>'Female','Other'=>'Other'], 1)->addGroupClass('col-sm-3')

  ->state('warning') !!}

<!-- Dob Field -->
{!! Former::date('dob')->label(__('users.fields.dob').':')->addGroupClass('col-sm-3')->placeholder('Dob') !!}

<!-- Designation Field -->
    {!! Former::text('address')->label(__('users.fields.address').':')->addGroupClass('col-sm-6')->placeholder('Address') !!}
</div>
<div class="row">
    
    <!-- Country Field -->
    <!-- {!! Former::select('country')->fromQuery(App\Models\Countries::get(), 'name','name')->label(__('users.fields.country').':')->addGroupClass('col-sm-3')->placeholder('Country')->required() !!} -->

    <div class="form-group col-md-3">
      <label for="country" class="col-form-label">Country:</label>
      <select id="country" name="country" class="form-control" required>
      <option value="">Country</option>
   
       <?php

        $countryss = App\Models\Countries::get();

        $countries =[];
        foreach ($countryss as $value)  
        { ?>
         
        <option <?php if(isset($user->country) && $user->country==$value->name) echo "selected" ?> data-id="{{ $value->id }}" value="{{ $value->name }}">{{ $value->name }}</option>

        <?php   

        }
        ?>
      </select>
    </div>

    <!-- State Field -->
    {!! Former::select('state')->label(__('users.fields.state').':')->addGroupClass('col-sm-3')->placeholder('State')->required() !!}
    <!-- ->fromQuery(App\Models\State::where('name','state')->get(), 'name','name') -->
    <!-- City Field -->
    {!! Former::select('city')->label(__('users.fields.city').':')->addGroupClass('col-sm-3')->placeholder('City')->required() !!}
    <!-- fromQuery(App\Models\City::where('state_id','22')->get(), 'name','name') -->

    <!-- Zipcode Field -->
    {!! Former::text('zipcode')->label(__('users.fields.zipcode').':')->addGroupClass('col-sm-3')->placeholder('Zipcode') !!}


</div>
<div class="row">

   
<?php


$countryss = App\Models\Countries::get();

$countries =[];
foreach ($countryss as $value) {
    # code...

    
     $countries[$value->phonecode] = $value->name.' (+'.$value->phonecode.')';

     //$countries [] =  $countriesd;
}
 ?>
<!-- Country Code Field -->
    {!! Former::select('country_code')->options($countries)->label(__('users.fields.country_code').':')->addGroupClass('col-sm-3')->placeholder('Country Code')->required() !!}

<!-- Phone No Field -->
    {!! Former::text('user_mobile')->label(__('users.fields.user_mobile').':')->addGroupClass('col-sm-3')->placeholder('Mobile No') !!}

     <!-- Password Field -->
{!! Former::password('password')->label(__('users.fields.password').':')->addGroupClass('col-sm-3')->placeholder('Password')->required() !!}

<!-- Password Field -->
{!! Former::password('password_confirmation')->label(__('users.fields.confirmpassword').':')->addGroupClass('col-sm-3')->placeholder(__('users.fields.confirmpassword'))->required() !!}
</div>

<br>
@push('page_scripts')
    <script type="text/javascript">
        $('#dob').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush



<script src="https://www.google.com/recaptcha/api.js" 
                            async defer>
</script>

<div class="g-recaptcha" id="feedback-recaptcha" 
    data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}">
</div><br>
                    {{-- <input type="submit" class="btn btn-info" name="password-reset-token" 
                           class="btn btn-primary"> --}}

<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('Register'), ['class' => 'btn btn-info']) !!}
</div>
