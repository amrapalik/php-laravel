<!DOCTYPE html>
<html lang="en">

<head>

    @include('layouts.shared/title-meta', ['title' => "Log In"])
    @include('layouts.shared/head-css', ["mode" => $mode ?? '', "demo" => $demo ?? ''])

    <style>

        .game-backgrounsdw{
            background: url(../assets/images/RL-people-graphic.jpg);
            -khtml-opacity:.50;
            -moz-opacity:.50;
            -ms-filter:"alpha(opacity=50)";
            filter:alpha(opacity=50);
            filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0.5);
            opacity:.50;
        }


        /*.game-backgrounsd{*/
        /*    background: url(../assets/images/RL-people-graphic.jpg);*/
        /*    -khtml-opacity:.50;*/
        /*    -moz-opacity:.50;*/
        /*    -ms-filter:"alpha(opacity=50)";*/
        /*    filter:alpha(opacity=50);*/
        /*    filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0.5);*/
        /*    opacity:.50;*/
        /*}*/
    </style>

</head>

<body class="loading  game-backgrounsdw">

<div class="account-pages mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">

            {{-- <div class="col-md-3 col-lg-3 col-xl-3">
                <div class="card bg-pattern">

                    <div class="card-body p-8">

                        <div class="text-center w-75 m-auto">
                            <div class="auth-logo">
                                <a href="#" class="logo logo-dark text-center">
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/rl_logo_black.svg')}}" alt="" height="62">
                                        </span>
                                </a>

                                <a href="#" class="logo logo-light text-center">
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/logo-light.png')}}" alt="" height="62">
                                        </span>
                                </a>
                            </div>   <br> <br>
                            <h4>{{__('registrations.gamer.heading')}}</h4>   <br> <br>
                            <h5>{{__('registrations.gamer.subheading')}}</h5>   <br> <br>

                            <a href="/register-gamer" class="btn btn-danger rounded-pill waves-effect waves-light">{{__('registrations.gamer.register')}}</a>

                        </div>




                    </div> <!-- end card-body -->
                </div>
                <!-- end card -->


            </div> <!-- end col --> --}}

            <div class="col-md-3 col-lg-3 col-xl-3">
                <div class="card bg-pattern">

                    <div class="card-body p-8">

                        <div class="text-center w-75 m-auto">
                            <div class="auth-logo">
                                <a href="#" class="logo logo-dark text-center">
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/rl_logo_black.svg')}}" alt="" height="62">
                                        </span>
                                </a>

                                <a href="#" class="logo logo-light text-center">
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/logo-light.png')}}" alt="" height="62">
                                        </span>
                                </a>
                            </div>   <br> <br>
                            <h4>{{__('registrations.organization.heading')}}</h4>   <br> <br>
                            <h5>{{__('registrations.organization.subheading')}}</h5>   <br> <br>

                            <a href="/register-organisation" class="btn btn-primary rounded-pill waves-effect waves-light">{{__('registrations.organization.register')}}</a>
                        </div>




                    </div> <!-- end card-body -->
                </div>
                <!-- end card -->


            </div> <!-- end col -->

            <div class="col-md-3 col-lg-3 col-xl-3">
                <div class="card bg-pattern">

                    <div class="card-body p-8">

                        <div class="text-center w-75 m-auto">
                            <div class="auth-logo">
                                <a href="#" class="logo logo-dark text-center">
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/rl_logo_black.svg')}}" alt="" height="62">
                                        </span>
                                </a>

                                <a href="#" class="logo logo-light text-center">
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/logo-light.png')}}" alt="" height="62">
                                        </span>
                                </a>
                            </div>   <br> <br>
                            <h4>{{__('registrations.school.heading')}}</h4>   <br> <br>
                            <h5>{{__('registrations.school.subheading')}}</h5>   <br> <br>

                            <a href="/register-school" class="btn btn-warning rounded-pill waves-effect waves-light">{{__('registrations.school.register')}}</a>

                        </div>
                    </div> <!-- end card-body -->
                </div>
                <!-- end card -->

            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end page -->

<footer class="footer footer-alt">
{{--    2015 - <script>--}}
{{--        document.write(new Date().getFullYear())--}}
{{--    </script> &copy; Developed by <a href="" class="text-white-50">Webosys</a>--}}
</footer>

@include('layouts.shared/footer-script')



</body>

</html>


@section('content')

    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h1 class="page-title text-center">PLEASE CHOOSE YOUR TYPE OF LICENSE</h1>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-6">
                <div class="card academic">
                    <a href="{{ url('/register-organisation') }}">
                        <div class="card-body text-center">
                            <h4>Organisation</h4>
                            If you have multiple schools in your Organization then click here
                        </div> <!-- end card body-->
                    </a>
                </div> <!-- end card -->
            </div><!-- end col-->
            <div class="col-6">
                <div class="card academic">
                    <a href="{{ url('/register-school') }}">
                        <div class="card-body text-center">
                            <h4>School</h4>
                            If you have a single school then click here
                        </div> <!-- end card body-->
                    </a>
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
    </div>
    <!-- end row-->
@endsection
