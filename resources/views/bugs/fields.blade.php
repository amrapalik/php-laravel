<div class="row">
    <!-- Title Field -->
    {!! Former::text('title')->label(__('bugs.fields.title').':')->addGroupClass('col-sm-6')->placeholder(__('bugs.fields.title').':') !!}
    {!! Former::select('type')->label(__('bugs.fields.type').':')->options([null=>'Select Type','Feedback'=>'Feedback','Bug Report'=>'Bug Report'], 1)->addGroupClass('col-sm-3')
      ->state('warning') !!}
</div>



<div class="row">
    <!-- Description Field -->
    {!! Former::textarea('description')->label(__('bugs.fields.description').':')->addGroupClass('col-sm-12 col-lg-12')->placeholder(__('bugs.fields.description').':') !!}
</div>


<div class="row">
    <!-- Image Field -->
    {!! Former::file('image')->label(__('bugs.fields.image').': &nbsp;&nbsp;&nbsp; ')->class('form-control')->addGroupClass('col-sm-6')->placeholder(__('bugs.fields.image').':') !!}

    <!-- Link Field -->
{{--    {!! Former::text('link')->label(__('bugs.fields.link').':')->addGroupClass('col-sm-6')->placeholder(__('bugs.fields.link').':') !!}--}}
</div>




<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('bugs.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
