<!-- Title Field -->
<div class="col-sm-12">
    {!! Form::label('title', __('bugs.fields.title').':') !!}
    <p>{{ $bugs->title }}</p>
</div>


<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('bugs.fields.description').':') !!}
    <p>{{ $bugs->description }}</p>
</div>


<!-- Image Field -->
<div class="col-sm-12">
    {!! Form::label('image', __('bugs.fields.image').':') !!}
    <p>{{ $bugs->image }}

        <img src="/{{ $bugs->image }}" alt="">

    </p>
</div>


<!-- Link Field -->
<div class="col-sm-12">
    {!! Form::label('link', __('bugs.fields.link').':') !!}
    <p>{{ $bugs->link }}</p>
</div>


{{--<!-- Status Field -->--}}
{{--<div class="col-sm-12">--}}
{{--    {!! Form::label('status', __('bugs.fields.status').':') !!}--}}
{{--    <p>{{ $bugs->status }}</p>--}}
{{--</div>--}}


{{--<!-- Created By` Field -->--}}
{{--<div class="col-sm-12">--}}
{{--    {!! Form::label('created_by`', __('bugs.fields.created_by').':') !!}--}}
{{--    <p>{{ $bugs->created_by }}</p>--}}
{{--</div>--}}


