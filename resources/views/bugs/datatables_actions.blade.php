{!! Form::open(['route' => ['bugs.destroy', $bugid], 'method' => 'delete']) !!}
<div class='btn-group'>


        <a href="{{ route('bugs.show', $bugid) }}" class='btn btn-default btn-xs'>
            <i class="fa fa-eye"></i>
        </a>


    @can('Edit bugs')
        <a href="{{ route('bugs.edit', $bugid) }}" class='btn btn-default btn-xs'>
            <i class="fa fa-edit"></i>
        </a>
    @endcan

    @can('Delete bugs')
        {!! Form::button('<i class="fa fa-trash"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
        ]) !!}
    @endcan
</div>
{!! Form::close() !!}
