
{!! Former::hidden('drole')->label(__('usertype').':')->addGroupClass('col-sm-3')->value('Teacher')  !!}


<div class="row">

    <!-- Firstname Field -->
{!! Former::text('firstname')->label(__('users.fields.firstname').':')->addGroupClass('col-sm-3')->placeholder('Firstname') ->required()!!}

<!-- Lastname Field -->
{!! Former::text('lastname')->label(__('users.fields.lastname').':')->addGroupClass('col-sm-3')->placeholder('Lastname')->required() !!}

<!-- Username Field -->
{!! Former::text('username')->label(__('users.fields.username').':')->addGroupClass('col-sm-3')->placeholder('Username')->required() !!}



<?php

 $role = auth()->user()->drole;

$gender = [ null =>'Select Gender', 'Male'=>'Male','Female'=>'Female','Other'=>'Other'];

//    if($role == 'School Admin')
//    {
//        $options = [ 'Teacher'=>'Teacher','Student'=>'Student'];
//    }
//
//    if($role == 'School Admin')
//    {
//        $options = [ 'Teacher'=>'Teacher','Student'=>'Student'];
//    }



?>



<!-- Email Field -->
    {!! Former::email('email')->label(__('users.fields.email').':')->addGroupClass('col-sm-3')->placeholder('Email')->required() !!}

</div>



<div class="row">
    <!-- Password Field -->
{!! Former::password('password')->label(__('users.fields.password').':')->addGroupClass('col-sm-3')->placeholder('Password')->required() !!}

{!! Former::password('password_confirmation')->label(__('users.fields.confirmpassword').':')->addGroupClass('col-sm-3')->placeholder(__('Password Confirmation'))->required()  !!}


<!-- Gender Field -->
{!! Former::select('gender')->label(__('users.fields.gender').':')->options($gender)->addGroupClass('col-sm-3')

->state('warning')->required() !!}

    <!-- Designation Field -->
        {!! Former::text('designation')->label(__('users.fields.designation').':')->addGroupClass('col-sm-3')->placeholder('Designation') !!}

</div>







<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-info']) !!}
    <a href="{{URL::previous()}}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
