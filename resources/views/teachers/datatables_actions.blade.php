{!! Form::open(['route' => ['users.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
{{--    <a href="{{ route('users.show', $id) }}" class='btn btn-default btn-xs'>--}}
{{--        <i class="fa fa-eye"></i>--}}
{{--    </a>--}}



    <!-- TODO:// Use spatie permission  -->
    @if(auth()->user()->drole == 'School Admin'|| auth()->user()->drole == 'Organization Admin' )


        <a href="{{ route('teacherdashboard', $id) }}" title="Dashboard"  class='btn btn-default btn-xs'>
            <i class="fa fa-tachometer-alt"></i>
        </a>

        <a href="#" onclick="assignAdmin(event,{{$id}})" title="Change Role"   class='btn btn-default btn-xs'>
            @if($drole == 'School Admin')
                <i class="fa fa-hand-point-up" style="color:red" ></i>
            @else
                <i class="fa fa-hand-point-up"></i>
            @endif
        </a>


        <a href="{{ route('users.edit', $id) }}?utype=teacher" title="Edit" class='btn btn-default btn-xs'>
            <i class="fa fa-edit"></i>
        </a>

    @endif




{{--    {!! Form::button('<i class="fa fa-trash"></i>', [--}}
{{--        'type' => 'submit',--}}
{{--        'class' => 'btn btn-danger btn-xs',--}}
{{--        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'--}}
{{--    ]) !!}--}}
</div>
{!! Form::close() !!}
