@extends('layouts.vertical', ["page_title"=> __('teachers.plural')])

@section('css')
    <!-- third party css -->
    <link href="{{asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->

    <style>

        .dt-button-collection
        {
            left: 0px !important;
        }
    </style>
@endsection


@section('content')

<?php
$schoolid =  session('school_schid');

$school =  \App\Models\School::find($schoolid);

?> 
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        @if($school->school_type == 'school')
            @include('layouts.shared/page-title', ['title' => "  ".__('teachers.Teachers of').__($school->sch_name)])
        @elseif($school->school_type == 'university_dept')
            @include('layouts.shared/page-title', ['title' => "  ".__('teachers.Faculties at').__($school->sch_name)])
        @else    
            @include('layouts.shared/page-title', ['title' => "  ".__('teachers.Parents')])
        @endif
        <!-- end page title -->

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="row">  <!-- Data table-->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-9">
                            @if($school->school_type == 'school')
                                <h4 class="header-title" style="float: left; margin-top: 10px">@lang('teachers.create teacher')</h4>
                            @elseif($school->school_type == 'university_dept')
                                <h4 class="header-title" style="float: left; margin-top: 10px">@lang('teachers.Faculty List')</h4>
                            @endif
                    
                            </div>
                            <div class="col-3">


                                @if(in_array(auth()->user()->drole, ['School Admin','Organization Admin']))
                                    <a href="{{ route('teachers_create_form') }}" style="float: right" class="btn btn-success rounded-pill waves-effect waves-light">
                                    @if($school->school_type == 'school')
                                        @lang('teachers.Add new teacher')</a>
                                    @elseif($school->school_type == 'university_dept')
                                        @lang('teachers.Add a faculty member')</a>
                                    @else
                                        @lang('teachers.Add a Parent')</a>
                                    @endif
                                @endif
                        </div>
                        <p class="text-muted font-13 mb-4">
                             <span style="color:red">

                                   @if(in_array(auth()->user()->drole, ['School Admin','Organization Admin']) && $school->school_type != 'family')
                                        @if($school->school_type == 'school')
                                            @lang('teachers.Click on') <i class="fa fa-hand-point-up"></i> @lang('teachers.Hand icon to change role School admin <-> Teacher.')</span>
                                        @else    
                                            @lang('teachers.Click on') <i class="fa fa-hand-point-up"></i> @lang('teachers.Hand icon to change role Dept admin <-> Faculty member.')</span>
                                        @endif    
                                     @endif

                        </p>
                        {!! $dataTable->table(['width' => '100%', 'class' => 'table dt-responsive nowrap w-100 ']) !!}
                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->
    </div> <!-- end container -->
@endsection


@section('script')
    @include('layouts.datatables_js')
    {{--    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>--}}
    {!! $dataTable->scripts() !!}
    <script>
        //dataTableBuilder
        $('#btn').on('click',function (){
            $('#dataTableBuilder').DataTable().ajax.reload();
        })
       // $('#dataTableBuilder').DataTable().ajax.reload();
        function assignAdmin(e, recid)
        {
            e.preventDefault();

            $('#loading-indicator').show();

            uid = {{ auth()->user()->id }};

            if(uid == recid)
            {
                toastr.error(  "@lang('teachers.role_cant_update_own_role')", {timeOut: 6000});
                return 0;
            }

            $.ajax({
                type: "GET",
                url : "/toggleSchoolAdmin?userid="+recid,
                cache: false,
                success: function (data) {
                    $('#loading-indicator').hide();
                     toastr.success(  "@lang('teachers.role_updated')", {timeOut: 6000});

                  //  location.reload();
                    //TODO:: Reload table
                    $('#dataTableBuilder').DataTable().ajax.reload();
                    //dataTableBuilder.ajax.reload( null, false );
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert (textStatus);
                    $('#loading-indicator').hide();
                }
            });
        }
    </script>
@endsection
