@extends('layouts.vertical', ["page_title"=> __('general.Create').__('teachers.create teacher')])

@section('content')
<!-- Start Content-->
<div class="container-fluid">



    <?php
    $user_type = app('request')->input('utype');
    $school = \App\Models\School::find(auth()->user()->school_schid);

    ?>

    @if(!empty($school)) 
        @if($school->school_type == 'school')
            @include('layouts.shared/page-title', ['title' => "".__('teachers.teacherlist')])
        @elseif($school->school_type == 'university_dept')   
            @include('layouts.shared/page-title', ['title' => "".__('teachers.Faculty Member')])
        @else
            @include('layouts.shared/page-title', ['title' => "".__('teachers.Family Member')])
        @endif
    @else
        <?php
        $org = \App\Models\Organization::find(auth()->user()->organization_orgid);
        ?>
        @if($org->org_type == 'organization')
            @include('layouts.shared/page-title', ['title' => "".__('teachers.teacherlist')])
        @else
            @include('layouts.shared/page-title', ['title' => "".__('teachers.Faculty Member')])  
        @endif    
    @endif
    @include('layouts.common.errors')
    @include('flash::message')
    <!-- Start Form  -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                            
                     {!! Former::vertical_open_for_files()->id('create-users-form')->method('POST')->route('teachers_store') !!}




                               @include('teachers.teacherfields')



                    {!! Former::close() !!}

                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end form -->
</div> <!-- container -->

@endsection


@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>


        {!! JsValidator::formRequest('App\Http\Requests\CreateTeacherRequest') !!}




<script>
        $(document).ready(function () {
           /*  $('#country_code').on('change', function () {
                var country_code = this.value;
               $("#country").html('');
                $.ajax({
                    url: "{{url('fetchCountry')}}",
                    type: "POST",
                    data: {
                        country_code: country_code,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {

                        $('#country').html();
                        $.each(result.country, function (key, value) {
                            $("#country").append('<option selected data-id="' + value
                                .id + '" value="' + value
                                .name + '">' + value.name + '</option>');
                        });

                       $('#state').html('<option value="">Select State</option>');
                        $.each(result.states, function (key, value) {
                            $("#state").append('<option data-id="' + value
                                .id + '" value="' + value
                                .name + '">' + value.name + '</option>');
                        });
                        $('#city').html('<option value="">Select City</option>');
                    }
                });
            });*/

            $('#country').on('change', function () {

                 var selected = $(this).find('option:selected');
                 var idCountry = selected.data('id');
               // var idCountry = this.value;
                $("#state").html('');
                $.ajax({
                    url: "{{url('fetchState')}}",
                    type: "POST",
                    data: {
                        country_id: idCountry,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {

                         $('#country_code').html();
                 $.each(result.country, function (key, value) {
                    $("#country_code").append('<option selected value="' + value
                        .phonecode + '">' + value.name +' (+'+value.phonecode+')' + '</option>');
                });

                        $('#state').html('<option value="">Select State</option>');
                        $.each(result.states, function (key, value) {
                            $("#state").append('<option data-id="' + value
                                .id + '" value="' + value
                                .name + '">' + value.name + '</option>');
                        });
                        $('#city').html('<option value="">Select City</option>');
                    }
                });
            });

            $('#state').on('change', function () {
                 var selected = $(this).find('option:selected');
                 var idState = selected.data('id');

                $("#city").html('');
                $.ajax({
                    url: "{{url('fetchCity')}}",
                    type: "POST",
                    data: {
                        state_id: idState,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (res) {
                        $('#city').html('<option value="">Select City</option>');
                        $.each(res.cities, function (key, value) {
                            $("#city").append('<option value="' + value
                                .name + '">' + value.name + '</option>');
                        });
                    }
                });
            });
        });

    </script>

@endsection
