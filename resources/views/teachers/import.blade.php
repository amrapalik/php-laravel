@extends('layouts.vertical', ["page_title"=>  __('users.import teachers')])

@section('content')
    <!-- Start Content-->
    <?php
        $schoolid =  session('school_schid');

        $school =  \App\Models\School::find($schoolid);

    ?> 
    <div class="container-fluid">

    @if($school->school_type == 'school')
        @include('layouts.shared/page-title', ['title' => "".__('teachers.Import Teachers for').__(($school->sch_name))])
    @else   
        @include('layouts.shared/page-title', ['title' => "".__('teachers.Import Faculty Members of').__(($school->sch_name))])
    @endif


    @include('layouts.common.errors')

    @include('flash::message')
    <p class="text-muted font-15 mb-4">
                             <span style="color:#00acc1">
                                @if($school->school_type == 'school')
                                    @lang('teachers.freeLicense')</span>
                                @else   
                                    @lang('teachers.freeLicense2')</span>
                                @endif
                                       

                        </p>

                       

    <!-- Start Form  -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">
                            @if($school->school_type == 'school')
                                @lang('teachers.Import Teachers')</h4>
                            @else   
                                @lang('teachers.Import Faculty Members')</h4>
                            @endif
                        <p class="sub-header">
                            @lang('teachers.Teachers import page heading')<br><br>
                            <a href="/assets/sample_excel/Sample_Teachers_Excel.xlsx" class="btn btn-danger"> @lang('teachers.Download Sample Excel')</a>
                        </p>

                        <p>
                            <span style="color:#00acc1"> @lang('students.instruction2')</span> 
                        </p>


                        @if(isset($errors) && $errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    {!! $error !!}
                                @endforeach
                            </div>
                        @endif



                        @if(session()->has('failures'))

                            <table class="table table-danger">
                                <tr>
                                    <th>Row</th>
                                    <th>Attribute</th>
                                    <th>Errors</th>
                                    <th>Value</th>
                                </tr>

                                @foreach(session()->get('failures') as $validation)
                                    <tr>
                                        <td>{!! $validation->row() !!}</td>
                                        <td>{!! $validation->attribute() !!}</td>
                                        <td>{!! $validation->row() !!}</td>
                                        <td>
                                            <ul>
                                                @foreach($validation->errors() as $e)
                                                    <li> {!! $e !!}</li>
                                                @endforeach
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif

                        {!! Former::vertical_open_for_files()->id('create-students-form')->method('POST')->route('importTeachers') !!}

                        <div class="row">
                            {!! Former::file('file')->label(__('teachers.teachers excel').':')->addGroupClass('col-sm-3')->placeholder('File') !!}
                        </div>
                        <br> <br>
                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit(__('crud.save'), ['class' => 'btn btn-info']) !!}

                        </div>

                        {!! Former::close() !!}



                        <h5>  @lang( 'teachers.Note') </h5>
                        <p>

                            @lang( 'teachers.Teacher Import instructions')
                        </p>
                        


                    </div> <!-- end card-body -->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
        <!-- end form -->
    </div> <!-- container -->

@endsection


@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

    {!! JsValidator::formRequest('App\Http\Requests\CreateStudentRequest') !!}
@endsection
