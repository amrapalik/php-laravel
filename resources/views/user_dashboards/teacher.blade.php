@extends('layouts.vertical', ["page_title"=> "Dashboard 2"])

@section('css')
    <!-- third party css -->
    <link href="{{asset('assets/libs/admin-resources/admin-resources.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
@endsection

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <?php
                            $schid = get_school_id();

                            $school =  \App\Models\School::find($schid);

                           // dd($teacherid);

                           // $total_studentse= \App\Models\Classes::where('created_by','=',$teacherid)->get();

                            // dd($total_studentse);
                            $total_classes = count(\App\Models\Classes::where('created_by','=',$teacherid)->where('type','=','lesson')->get());
                            $total_assignment_classes = count(\App\Models\Classes::where('created_by','=',$teacherid)->where('type','=','Assignment')->get());

                            $total_lessonplans = count(\App\Models\LessonPlan::where('created_by','=',$teacherid)->get());

$teacher = \App\Models\User::find($teacherid);
                            //                        $total_studentse= \App\Models\Student::where('school_schid','=',$schid)->where('drole','=','Student')->get();

                             ?>
                            <li class="breadcrumb-item active">{{  $school->sch_name }}</li>
                        </ol>
                    </div>
                    <h4 class="page-title">  {{$teacher->name}}</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
{{--                            <div class="col-3">--}}
{{--                                <div class="avatar-sm bg-blue rounded">--}}
{{--                                    --}}{{--                                <i data-feather="users" class=" avatar-title font-15 text-white"> ></i>--}}
{{--                                    <i class="fe-users avatar-title font-22 text-white"></i>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="col-12">
                                <div class="text">
                                    <h3 class="text-dark my-1"> <span data-plugin="counterup">{{$total_classes}}</span></h3>
                                    <p class="text-muted mb-1 text-truncate"> Total Class with lessonplan</p>
                                </div>
                            </div>
                        </div>
                        <div class="mt-3 text-center">
                        </div>
                    </div>
                </div> <!-- end card-->
            </div> <!-- end col -->

            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
{{--                            <div class="col-3">--}}
{{--                                <div class="avatar-sm bg-info rounded">--}}
{{--                                    --}}{{--                                <i data-feather="users" class=" avatar-title font-15 text-white"> ></i>--}}
{{--                                    <i class="fe-users avatar-title font-22 text-white"></i>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="col-12">
                                <div class="text">
                                    <h3 class="text-dark my-1"> <span data-plugin="counterup">{{$total_assignment_classes}}</span></h3>
                                    <p class="text-muted mb-1 text-truncate">  {{__('Class with Assignment')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="mt-3 text-center">




                        </div>
                    </div>
                </div> <!-- end card-->
            </div> <!-- end col -->

            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
{{--                            <div class="col-3">--}}
{{--                                <div class="avatar-sm bg-warning rounded">--}}
{{--                                    --}}{{--                                <i data-feather="users" class=" avatar-title font-15 text-white"> ></i>--}}
{{--                                    <i class="fe-dollar-sign avatar-title font-22 text-white"></i>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="col-12">
                                <div class="text">
                                    <h3 class="text-dark my-1"> <span data-plugin="counterup">{{$total_lessonplans}}</span></h3>
                                    <p class="text-muted mb-1 text-truncate"> {{__('Total Lesson Plans')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="mt-3 text-center">
                        </div>
                    </div>
                </div> <!-- end card-->
            </div> <!-- end col -->

{{--            <div class="col-md-6 col-xl-3">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-body">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-3">--}}
{{--                                <div class="avatar-sm bg-success rounded">--}}
{{--                                    --}}{{-- <i data-feather="users" class="avatar-title font-15 text-white"> ></i> --}}
{{--                                    <i class="fe-dollar-sign avatar-title font-22 text-white"></i>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-9">--}}
{{--                                <div class="text">--}}
{{--                                    <h3 class="text-dark my-1"> <span data-plugin="counterup">{{$school->available_licenses}}</span></h3>--}}
{{--                                    <p class="text-muted mb-1 text-truncate">License Available </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="mt-3 text-center">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div> <!-- end card-->--}}
{{--            </div> <!-- end col -->--}}
        </div>
        <!-- end row -->


{{--        <div class="row">--}}
{{--            <div class="col text-center">--}}
{{--                <a href="/students" class="btn btn-info"> Show Students</a>--}}
{{--            </div>--}}
{{--            <div class="col text-center">--}}
{{--                <a href="/teachers_list" class="btn btn-danger"> Show Teachers</a>--}}
{{--            </div>--}}
{{--        </div>--}}


        {{--    <div class="row">--}}




        {{--        <div class="col-xl-4 col-md-6">--}}
        {{--            <div class="card">--}}
        {{--                <div class="card-body">--}}
        {{--                    <div class="card-widgets">--}}
        {{--                        <a href="javascript: void(0);" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>--}}
        {{--                        <a data-bs-toggle="collapse" href="#cardCollpase3" role="button" aria-expanded="false" aria-controls="cardCollpase3"><i class="mdi mdi-minus"></i></a>--}}
        {{--                        <a href="javascript: void(0);" data-toggle="remove"><i class="mdi mdi-close"></i></a>--}}
        {{--                    </div>--}}
        {{--                    <h4 class="header-title mb-0">Total Users</h4>--}}

        {{--                    <div id="cardCollpase3" class="collapse pt-3 show">--}}
        {{--                        <div class="text-center">--}}
        {{--                            <div id="total-users" data-colors="#00acc1,#4b88e4,#e3eaef,#fd7e14"></div>--}}
        {{--                            <div class="row mt-3">--}}
        {{--                                <div class="col-4">--}}
        {{--                                    <p class="text-muted font-15 mb-1 text-truncate">Target</p>--}}
        {{--                                    <h4><i class="fe-arrow-down text-danger me-1"></i>18k</h4>--}}
        {{--                                </div>--}}
        {{--                                <div class="col-4">--}}
        {{--                                    <p class="text-muted font-15 mb-1 text-truncate">Last week</p>--}}
        {{--                                    <h4><i class="fe-arrow-up text-success me-1"></i>3.25k</h4>--}}
        {{--                                </div>--}}
        {{--                                <div class="col-4">--}}
        {{--                                    <p class="text-muted font-15 mb-1 text-truncate">Last Month</p>--}}
        {{--                                    <h4><i class="fe-arrow-up text-success me-1"></i>28k</h4>--}}
        {{--                                </div>--}}
        {{--                            </div> <!-- end row -->--}}
        {{--                        </div>--}}
        {{--                    </div> <!-- collapsed end -->--}}
        {{--                </div> <!-- end card-body -->--}}
        {{--            </div> <!-- end card-->--}}
        {{--        </div> <!-- end col-->--}}
        {{--    </div>--}}
        {{--    <!-- end row -->--}}



    </div> <!-- container -->
@endsection

@section('script')
    <!-- third party js -->
    <script src="{{asset('assets/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
    <script src="{{asset('assets/libs/admin-resources/admin-resources.min.js')}}"></script>
    <!-- third party js ends -->

    <!-- demo app -->
    {{--<script src="{{asset('assets/js/pages/dashboard-2.init.js')}}"></script>--}}
    <!-- end demo js-->


    <script>

        $( document ).ready(function() {

            var DrawSparkline = function() {
                // Line Chart
                var colors = ['#00acc1', '#f1556c'];
                var dataColors = $("#lifetime-sales").data('colors');
                if (dataColors) {
                    colors = dataColors.split(",");
                }
                $('#lifetime-sales').sparkline([0, 23, 43, 35, 44, 45, 56, 37, 40], {
                    type: 'line',
                    width: "100%",
                    height: '220',
                    chartRangeMax: 50,
                    lineColor: colors[0],
                    fillColor: hexToRGB(colors[0], 0.3),
                    highlightLineColor: 'rgba(0,0,0,.1)',
                    highlightSpotColor: 'rgba(0,0,0,.2)',
                    maxSpotColor: false,
                    minSpotColor: false,
                    spotColor: false,
                    lineWidth: 1
                });

                $('#lifetime-sales').sparkline([25, 23, 26, 24, 25, 32, 30, 24, 19], {
                    type: 'line',
                    width: "100%",
                    height: '220',
                    chartRangeMax: 40,
                    lineColor: colors[1],
                    fillColor: hexToRGB(colors[1], 0.3),
                    composite: true,
                    highlightLineColor: 'rgba(0,0,0,.1)',
                    highlightSpotColor: 'rgba(0,0,0,.2)',
                    maxSpotColor: false,
                    minSpotColor: false,
                    spotColor: false,
                    lineWidth: 1
                });

                // Bar Chart
                var colors = ['#00acc1'];
                var dataColors = $("#income-amounts").data('colors');
                if (dataColors) {
                    colors = dataColors.split(",");
                }
                $('#income-amounts').sparkline([3, 6, 7, 8, 6, 4, 7, 10, 12, 7, 4, 9, 12, 13, 11, 12], {
                    type: 'bar',
                    height: '220',
                    barWidth: '10',
                    barSpacing: '3',
                    barColor: colors
                });

                // Pie Chart
                var colors = ['#00acc1','#4b88e4','#e3eaef','#fd7e14'];
                var dataColors = $("#total-users").data('colors');
                if (dataColors) {
                    colors = dataColors.split(",");
                }
                $('#total-users').sparkline([30, 10], {
                    type: 'pie',
                    width: '220',
                    height: '220',
                    sliceColors: colors
                });
            };

            DrawSparkline();

            var resizeChart;

            $(window).resize(function(e) {
                clearTimeout(resizeChart);
                resizeChart = setTimeout(function() {
                    DrawSparkline();
                }, 300);
            });
        });

        // Vector map
        //various examples
        var colors = ['#6658dd'];
        var dataColors = $("#world-map-markers").data('colors');
        if (dataColors) {
            colors = dataColors.split(",");
        }
        $('#world-map-markers').vectorMap({
            map : 'world_mill_en',
            normalizeFunction : 'polynomial',
            hoverOpacity : 0.7,
            hoverColor : false,
            regionStyle : {
                initial : {
                    fill : '#ced4da'
                }
            },
            markerStyle: {
                initial: {
                    r: 9,
                    'fill': colors[0],
                    'fill-opacity': 0.9,
                    'stroke': '#fff',
                    'stroke-width' : 7,
                    'stroke-opacity': 0.4
                },

                hover: {
                    'stroke': '#fff',
                    'fill-opacity': 1,
                    'stroke-width': 1.5
                }
            },
            backgroundColor : 'transparent',
            markers : [{
                latLng : [41.90, 12.45],
                name : 'Vatican City'
            }, {
                latLng : [43.73, 7.41],
                name : 'Monaco'
            }, {
                latLng : [-0.52, 166.93],
                name : 'Nauru'
            }, {
                latLng : [-8.51, 179.21],
                name : 'Tuvalu'
            }, {
                latLng : [43.93, 12.46],
                name : 'San Marino'
            }, {
                latLng : [47.14, 9.52],
                name : 'Liechtenstein'
            }, {
                latLng : [7.11, 171.06],
                name : 'Marshall Islands'
            }, {
                latLng : [17.3, -62.73],
                name : 'Saint Kitts and Nevis'
            }, {
                latLng : [3.2, 73.22],
                name : 'Maldives'
            }, {
                latLng : [35.88, 14.5],
                name : 'Malta'
            }, {
                latLng : [12.05, -61.75],
                name : 'Grenada'
            }, {
                latLng : [13.16, -61.23],
                name : 'Saint Vincent and the Grenadines'
            }, {
                latLng : [13.16, -59.55],
                name : 'Barbados'
            }, {
                latLng : [17.11, -61.85],
                name : 'Antigua and Barbuda'
            }, {
                latLng : [-4.61, 55.45],
                name : 'Seychelles'
            }, {
                latLng : [7.35, 134.46],
                name : 'Palau'
            }, {
                latLng : [42.5, 1.51],
                name : 'Andorra'
            }, {
                latLng : [14.01, -60.98],
                name : 'Saint Lucia'
            }, {
                latLng : [6.91, 158.18],
                name : 'Federated States of Micronesia'
            }, {
                latLng : [1.3, 103.8],
                name : 'Singapore'
            }, {
                latLng : [0.33, 6.73],
                name : 'SÃ£o TomÃ© and PrÃ­ncipe'
            }]
        });

        /* utility function */

        function hexToRGB(hex, alpha) {
            var r = parseInt(hex.slice(1, 3), 16),
                g = parseInt(hex.slice(3, 5), 16),
                b = parseInt(hex.slice(5, 7), 16);

            if (alpha) {
                return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
            } else {
                return "rgb(" + r + ", " + g + ", " + b + ")";
            }
        }
    </script>
@endsection
