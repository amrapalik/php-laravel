@extends('layouts.vertical', ["page_title"=> __('general.Edit').__('roles.singular')])

@section('content')
<!-- Start Content-->
<div class="container-fluid">

    @include('layouts.shared/page-title', ['title' => __('general.Edit').__('roles.singular')])

    <!-- Start Form  -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">@lang('roles.singular')</h4>


                    {!! Former::vertical_open_for_files()->id('edit-roles-form')->populate($role)->setOption('live_validation', true)
                    ->method('PATCH')->route('roles.update', $role->id) !!}

                        @include('roles.fields')



                    {!! Former::close() !!}

                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end form -->
</div> <!-- container -->

@endsection


@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

    {!! JsValidator::formRequest('App\Http\Requests\UpdateRoleRequest') !!}
@endsection
