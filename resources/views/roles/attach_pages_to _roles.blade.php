@extends('layouts.app')

@section('css')
    <style>
        a.delete {
            color: white;
        }

        .perm{
            line-height: 2;
            margin-bottom: 5px;
        }
        .permdelete{
            margin-bottom: 12px;
        }
    </style>
    @endsection

@section('content')
    <section class="content-header">
        <h1>
            Assign Page access to roles
        </h1>
    </section>
    <div class="content">
        @if(!empty($errors))
            @if($errors->any())
                <ul class="alert alert-danger" style="list-style-type: none">
                    @foreach($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            @endif
        @endif
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">

                    {!! Former::vertical_open_for_files()->id('assign_pages_to_roles')->method('POST')->route('assign_pages_to_roles') !!}



                    <?php

                    $roles =  \App\Models\Role::all()->pluck('name','id')->prepend('Select Role', null);

                    $permissions =  \App\Models\Permission::all()->pluck('name','id')->prepend('Select Permission', null);

                    ?>


                    {!! Former::select('role_id')->label('Role: *')->addGroupClass('col-sm-6')->options($roles) !!}


                    {!! Former::select('permission_id')->label('Select Permission : *')->addGroupClass('col-sm-6')->options($permissions) !!}
                    <div class="col-sm-1" >

                    </div>

                <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="#" id="closeForm" class="btn btn-default">Cancel</a>
                    </div>

                    {!! Former::close() !!}

                </div>
            </div>
        </div>


            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Pages & Permissions assigned to Roles</h3>
                </div>
            <div class="box-body">
                <table class="table table-bordered" >
                    <tbody id="roles-permission-List">


                    </tbody></table>
            </div>
    </div>




    </div>


@endsection

@section('scripts')

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>


    <!-- Client Side Validation  -->
{{--    {!! $validatorUserForm = JsValidator::make(['role_id'=>'required|integer','page_id'=>'required|integer']) !!}--}}
{{--    {!! $validatorUserForm->selector('#assign_pages_to_roles') !!}--}}
    <!-- / Client Side Validation -->


    <script src="https://unpkg.com/vue@2.6.12/dist/vue.js"></script>

    <script>

        grolerr = 0;

        var app = new Vue({
            el: '#app',
            data: {
                message: 'Hello Vue!',
                products:[]
            },
            methods: {

                getEditorContents: function (event) {
                    if (event) {
                        event.preventDefault()
                    }

                    {{--var url = '{{ route('get_roles_pages_list') }}?role_id='+grolerr;--}}

                    fetch(url)
                        .then(response => response.json())
                        .then(json => {
                                this.products = json.data
                            }
                    )
                }
            }
        })

        $(document).ready(function(){



            $('#role_id').on('change',function(){

                grolerr = $('#role_id').val();
                updateTable();

            });



            function updateTable(){
                grole = $('#role_id').val();


                {{--var url = '{{ route('get_roles_pages_list') }}?role_id='+grole;--}}

                $.ajax({
                    type:'get',
                    url: url,
                    // data:'country_id='+countryID,
                    success:function(data){
                        $('#loading-indicator').hide();
                        options = data.data
                        //console.log(options)

                        html = `<tr>
                                    <th style="width: 10px">Sr.No</th>
                                    <th>Assigned Page</th>
                                    <th>Role Permissions</th>
                                    <th style="width: 40px">Label</th>
                                </tr>`
                        count = 0;

                        for (const [key, value] of Object.entries(options)) {
                            console.log(`${key}: ${value}`);

                            count++;

                           html += '<tr><td>'+ count+'</td><td>'+ key+'</td><td>';

                          // html += '<tr><td>asdasd</td><td>adaasd</td><td>asdsa</td><td> <a class="delete" href="{{ route('unlink_page_from_role') }}?role_id='+grole+'&page_id=2">Remove Permission</a> </td></tr>';



                            value.forEach(function(option) {
                                html += '<span class="perm">'+ option.name+"</span><br>"
                                    console.log(option.name);
                               });

                            html +='</td><td>';

                            value.forEach(function(option) {

                                html +=   '<span class="badge bg-red permdelete"><a class="delete" href="{{ route('unlink_page_from_role') }}?role_id='+grole+'&page_id='+option.id+'">Remove Permission</a></span> <br>'
                                console.log(option.name);
                            });

                               html += ' </td></tr>';
                        }

                        {{--options.forEach(function(option){--}}
                        {{--    count ++;--}}
                        {{--    html += '<tr><td>'+ count+'</td><td>'+ option.frompage+'</td><td>'+ option.name+'</td><td> <a class="delete" href="{{ route('unlink_page_from_role') }}?role_id='+grole+'&page_id='+option.id+'">Remove Permission</a> </td></tr>';--}}
                        {{--});--}}

                        $('#roles-permission-List').html(html);

                    }
                });
            }


            $(document).on('click', 'a.delete', function(e) {
                e.preventDefault();



                $.ajax({
                    type: 'get',
                    url: $(this).attr('href'),
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        "_token": "{{ csrf_token() }}",

                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        $('#loading-indicator').hide();
                        let serverErrorCount =0;
                        if(data.errors)
                        {
                            serverErrorCount = Object.keys(data.errors).length;
                        }

                        if (serverErrorCount<1) {
                            //alert("Record added Successfully")


                            news = 'New'

                            if (data.success == true)
                            {
                                updateTable();
                            }

                            $('#modal-create').modal('hide');
                            toastr.success(data.message, 'Success Alert', {timeOut: 7000});
                            //  location.reload();

                        }else {
                            alert("Error");
                        }

                    },
                    error: function (xhr, textStatus, errorThrown) {
                        $('#loading-indicator').hide();
                        alert("Error: There Are some Errors");
                        toastr.error('There are some errors.', 'Danger Alert', {timeOut: 7000});

                        let serverErrorCount =0;
                        data = JSON.parse(xhr.responseText);
                        if(data.errors)
                        {
                            serverErrorCount = Object.keys(data.errors).length;
                        }

                        if (serverErrorCount>0) {
                            showErrorsFromServer(data)
                        }
                    }
                });


                updateTable();

            });


            <!-- Event when click on submit create_Link_roles_form -->
            $(document).on('submit', 'form#assign_pages_to_roles', function (event) {
                event.preventDefault();
                var form    = $(this);
                var data    = new FormData($(this)[0]);
                var url     = form.attr("action");
                var method  = form.attr('method');
                var message = "Created"

                $('#loading-indicator').show();
                submitForm(url,data,method,message)
                return false;
            });



            <!-- Close create Link_roles form -->
            $(document).on('click', '#closeForm', function (event) {
                $('#modal-create').modal('hide');
            });


            <!-- Submit form logic -->
            function submitForm(url,data,method,message)
            {
                $.ajax({
                    type: method,
                    url: url,
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        $('#loading-indicator').hide();
                        let serverErrorCount =0;
                        if(data.errors)
                        {
                            serverErrorCount = Object.keys(data.errors).length;
                        }

                        if (serverErrorCount<1) {
                            //alert("Record added Successfully")



                            if (data.success == true)
                            {
                                updateTable();
                            }

                            $('#modal-create').modal('hide');
                            toastr.success(data.message, 'Success Alert', {timeOut: 12000});
                            //  location.reload();

                        }else {
                            alert("Error");
                        }

                    },
                    error: function (xhr, textStatus, errorThrown) {
                        $('#loading-indicator').hide();
                        alert("Error: There Are some Errors");
                        toastr.error('There are some errors.', 'Danger Alert', {timeOut: 12000});

                        let serverErrorCount =0;
                        data = JSON.parse(xhr.responseText);
                        if(data.errors)
                        {
                            serverErrorCount = Object.keys(data.errors).length;
                        }

                        if (serverErrorCount>0) {
                            showErrorsFromServer(data)
                        }
                    }
                });
            }


            function  showErrorsFromServer(data)
            {
                toastr.error('There are some errors.', 'Danger Alert', {timeOut: 12000});

                for (control in data.errors) {
                    $('#'+control).closest('.form-group').removeClass('has-success').addClass('has-error');
                    $('#'+control+'-2').closest('.form-group').removeClass('has-success').addClass('has-error');
                    $cc = "<span id=\""+control+"-error\" class=\"help-block error-help-block\">"+data.errors[control][0]+"</span>";

                    console.log(control);
                    console.log($cc);
                    $($cc).insertAfter('#' + control);
                    $($cc).insertAfter('#create_' + control);
                    $($cc).insertAfter('#' + control+'-2');
                }

            }


        })

    </script>

@endsection

