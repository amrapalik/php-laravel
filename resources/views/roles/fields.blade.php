<div class="row">
    <!-- Name Field -->
    {!! Former::text('name')->label(__('roles.fields.name').':')->addGroupClass('col-sm-6')->placeholder(__('roles.fields.name').':') !!}
</div>



<div class="row">
    <!-- Guard Name Field -->
    {!! Former::text('guard_name')->label(__('roles.fields.guard_name').':')->addGroupClass('col-sm-6')->placeholder(__('roles.fields.guard_name').':') !!}
</div>


<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-info']) !!}
    <a href="{{ route('roles.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
