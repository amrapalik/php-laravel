<div class='btn-group'>
@if (!empty($link))
<a href="{{ $link }}" class="btn btn-default btn-xs" title="View"><i class="fa fa-eye" title="View"></i></a>
@endif
{!! Form::open(['route' => ['notifications.delete'], 'method' => 'post']) !!}
		<input type="hidden" name="id" value="{{ $id }}" />
		{{ Form::button('<i class="fa fa-trash" title="Delete"></i>', 
			['type'=>'submit', 'class'=>'btn btn-danger btn-xs']);
		}}
{!! Form::close() !!}
</div>
