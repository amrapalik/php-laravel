<div class="row">
    <!-- Asname Field -->
    {!! Former::text('asname')->label(__('assignments.fields.asname').':')->addGroupClass('col-sm-6')->placeholder(__('assignments.fields.asname').':') !!}

<!-- Category Field -->
  {!! Former::select('category')->class("form-select")->fromQuery(App\Models\Category::get(), 'catname','catname')->label(__('assignments.fields.category').':')->addGroupClass('col-sm-3')->placeholder(__('assignments.fields.category')) !!}
 </div>

<div class="row">
    <!-- Countryid Field -->
    {!! Former::number('countryid')->label(__('assignments.fields.countryid').':')->addGroupClass('col-sm-6')->placeholder(__('assignments.fields.countryid').':') !!}

    <!-- Country Name Field -->
    {!! Former::text('country_name')->label(__('assignments.fields.country_name').':')->addGroupClass('col-sm-6')->placeholder(__('assignments.fields.country_name').':') !!}
</div>



<div class="row">
    <!-- City Id Field -->
    {!! Former::number('city_id')->label(__('assignments.fields.city_id').':')->addGroupClass('col-sm-6')->placeholder(__('assignments.fields.city_id').':') !!}

    <!-- City Name Field -->
    {!! Former::text('city_name')->label(__('assignments.fields.city_name').':')->addGroupClass('col-sm-6')->placeholder(__('assignments.fields.city_name').':') !!}
</div>



<div class="row">
    <!-- Gender Field -->
    {!! Former::select('gender')->class("form-select")->label(__('assignments.fields.gender').':')->options(['Male'=>'Male','Female'=>'Female','Other'=>'Other'], 1)->addGroupClass('col-sm-3')

  ->state('warning') !!}

    <!-- Religion Field -->
    {!! Former::text('religion')->label(__('assignments.fields.religion').':')->addGroupClass('col-sm-6')->placeholder(__('assignments.fields.religion').':') !!}
</div>

<div class="row">


<!-- Gender Field -->
{!! Former::select('type')->class("form-select")->label(__('assignments.fields.type').':')->options([null=>'Select','School'=>'School','Gamer'=>'Gamer'])->addGroupClass('col-sm-3')

->state('warning') !!}

<!-- Religion Field -->
    {!! Former::text('tag')->label(__('assignments.fields.tag').':')->addGroupClass('col-sm-6')->placeholder(__('assignments.fields.tag').':') !!}
</div>

<div class="row">
    <!-- Urban Rural Field -->

    {!! Former::text('urban_rural')->label(__('assignments.fields.urban_rural').':')->addGroupClass('col-sm-6')->placeholder(__('assignments.fields.urban_rural').':') !!}
    {!! Former::hidden('status')->value('Active')->label(__('assignments.fields.urban_rural').':')->addGroupClass('col-sm-6')->placeholder(__('assignments.fields.urban_rural').':') !!}

    {{--    {!! Former::select('status')->class("form-select")->label(__('assignments.fields.status').':')->options([null=>'Select','Active'=>'Active','Inactive'=>'Inactive'])->addGroupClass('col-sm-3')--}}

{{--    ->state('warning') !!}--}}
</div>


<br><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('assignments.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
