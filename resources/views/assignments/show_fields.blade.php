<!-- Asname Field -->
<div class="col-sm-12">
    {!! Form::label('asname', __('assignments.fields.asname').':') !!}
    <p>{{ $assignment->asname }}</p>
</div>


<!-- Category Field -->
<div class="col-sm-12">
    {!! Form::label('category', __('assignments.fields.category').':') !!}
    <p>{{ $assignment->category }}</p>
</div>


<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', __('assignments.fields.status').':') !!}
    <p>{{ $assignment->status }}</p>
</div>


<!-- Countryid Field -->
<div class="col-sm-12">
    {!! Form::label('countryid', __('assignments.fields.countryid').':') !!}
    <p>{{ $assignment->countryid }}</p>
</div>


<!-- Country Name Field -->
<div class="col-sm-12">
    {!! Form::label('country_name', __('assignments.fields.country_name').':') !!}
    <p>{{ $assignment->country_name }}</p>
</div>


<!-- City Id Field -->
<div class="col-sm-12">
    {!! Form::label('city_id', __('assignments.fields.city_id').':') !!}
    <p>{{ $assignment->city_id }}</p>
</div>


<!-- City Name Field -->
<div class="col-sm-12">
    {!! Form::label('city_name', __('assignments.fields.city_name').':') !!}
    <p>{{ $assignment->city_name }}</p>
</div>


<!-- Gender Field -->
<div class="col-sm-12">
    {!! Form::label('gender', __('assignments.fields.gender').':') !!}
    <p>{{ $assignment->gender }}</p>
</div>


<!-- Religion Field -->
<div class="col-sm-12">
    {!! Form::label('religion', __('assignments.fields.religion').':') !!}
    <p>{{ $assignment->religion }}</p>
</div>


<!-- Urban Rural Field -->
<div class="col-sm-12">
    {!! Form::label('urban_rural', __('assignments.fields.urban_rural').':') !!}
    <p>{{ $assignment->urban_rural }}</p>
</div>
