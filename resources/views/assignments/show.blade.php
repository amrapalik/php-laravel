@extends('layouts.vertical', ["page_title"=> __("classes.My Assigned Classes")])

@section('content')
 <!-- Start Content-->
    <div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">

                </div>
                <h4 class="page-title">@lang('assignments.singular')</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="content px-3">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    @include('assignments.show_fields')
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
