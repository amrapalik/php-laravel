{!! Form::open(['route' => ['assignments.destroy', $asid], 'method' => 'delete']) !!}
<div class='btn-group'>



    @if(auth()->user()->drole != 'Admin')
    <a href="{{ route('classes.create') }}?assignment=true&asid={{$asid}}" title="Create Class" class='btn btn-default btn-xs'>
        <i class="fa fa-plus"></i>
    </a>
    @else
        <a href="{{ route('assignments.show', $asid) }}" class='btn btn-default btn-xs'>
            <i class="fa fa-eye"></i>
        </a>

        <a href="{{ route('assignments.edit', $asid) }}" class='btn btn-default btn-xs'>
            <i class="fa fa-edit"></i>
        </a>
    @endif


{{--    @can('View assignments')--}}
{{--        <a href="{{ route('assignments.show', $asid) }}" class='btn btn-default btn-xs'>--}}
{{--            <i class="fa fa-eye"></i>--}}
{{--        </a>--}}
{{--    @endcan--}}

{{--    @can('Edit assignments')--}}
{{--        <a href="{{ route('assignments.edit', $asid) }}" class='btn btn-default btn-xs'>--}}
{{--            <i class="fa fa-edit"></i>--}}
{{--        </a>--}}
{{--    @endcan--}}

    @can('Delete assignments')
        {!! Form::button('<i class="fa fa-trash"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
        ]) !!}
    @endcan
</div>
{!! Form::close() !!}
