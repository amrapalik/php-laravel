@extends('layouts.vertical', ["page_title"=> __('general.Edit').__('assignments.singular')])

@section('content')
<!-- Start Content-->
<div class="container-fluid">

    @include('layouts.shared/page-title', ['title' => __('general.Edit').__('assignments.singular')])

    @include('layouts.common.errors')

    <!-- Start Form  -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">@lang('assignments.singular')</h4>


                    {!! Former::vertical_open_for_files()->id('edit-assignments-form')->populate($assignment)->setOption('live_validation', true)
                    ->method('PATCH')->route('assignments.update', $assignment->asid) !!}

                        @include('assignments.fields')



                    {!! Former::close() !!}

                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end form -->
</div> <!-- container -->

@endsection


@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

    {!! JsValidator::formRequest('App\Http\Requests\UpdateAssignmentRequest') !!}
@endsection
