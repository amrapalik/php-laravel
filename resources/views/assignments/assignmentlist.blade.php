@extends('layouts.vertical', ["page_title"=> __('assignments.plural')])

@section('css')
    <!-- third party css -->
    <link href="{{asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
@endsection

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        @include('layouts.shared/page-title', ['title' => " ".__('assignments.select an Assignment')])
        @include('layouts.common.errors')

        {{--        @dd($value)--}}
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">


                        <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                            <thead>
                            <tr>
                                <th>@lang('assignments.fields.asid')</th>
                                <th>@lang('assignments.fields.asname')</th>
                                <th>@lang('crud.action')</th>

                            </tr>
                            </thead>


                            <tbody>

                            @foreach($value as $val)
                                <tr>
                                    <td>{{ @$val['asid'] }}</td>
                                    <td>{{ @$val['asname'] }}</td>
                                    <td>

                                        {!! Form::open(['route' => ['assignments.destroy', @$val['asid']], 'method' => 'delete']) !!}
                                        <div class='btn-group'>



                                            @if(auth()->user()->drole != 'Admin')
												@if (request()->get('create') == 'lesson_plan')
                                                <a href="{{ route('lessonPlans.create') }}?assignment=true&asid={{@$val['asid']}}" title="Create Lesson Plan" class='btn btn-default btn-xs'>
                                                    <i class="fa fa-plus"></i>
                                                </a>
												@else
                                                <a href="{{ route('classes.create') }}?assignment=true&asid={{@$val['asid']}}" title="Create Class" class='btn btn-default btn-xs'>
                                                    <i class="fa fa-plus"></i>
                                                </a>
												@endif
                                            @else
                                                <a href="{{ route('assignments.show', $val['asid']) }}" class='btn btn-default btn-xs'>
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                                <a href="{{ route('assignments.edit', $val['asid']) }}" class='btn btn-default btn-xs'>
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endif

                                        </div>
                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->




    </div> <!-- container -->
@endsection

@section('script')
    <!-- third party js -->
    <script src="{{asset('assets/libs/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
    <!-- third party js ends -->
    <!-- demo app -->
    <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script>
    <!-- end demo js-->
@endsection
