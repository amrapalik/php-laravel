@extends('layouts.vertical', ["page_title"=> __('general.Create').__('assignments.singular')])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

    @include('layouts.shared/page-title', ['title' => __('assignments.choose a subject')])

    @include('layouts.common.errors')

<!--        --><?php
//
//        $categories = \App\Models\Category::all();
//        ?>

        <div class="row">
            @foreach($categories as $cat)
            <div class="col-md-6 col-xl-3">
                <a href="{{ route('assignments.index') }}?cat={{@$cat['catid'] }}&create={{ $create }}">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-12">

                                         <h5 style="text-align: center" > {{@$cat['catname'] }}</h5 >

                                </div>
                            </div>

                        </div>
                    </div> <!-- end card-->
                </a>
            </div> <!-- end col -->

            @endforeach



        </div>
        <!-- end row -->
        <!-- end form -->
    </div> <!-- container -->

@endsection


@section('scripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

    {{--   {!! JsValidator::formRequest('App\Http\Requests\CreateAssignmentRequest') !!}--}}
@endsection
