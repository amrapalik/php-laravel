@extends('layouts.master')

@section('css')
<!-- third party css -->
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- third party css end -->
@endsection

@section('content')

<!-- Start Content-->
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">{{ __('SDG Comments By Me') }}</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <table id="alternative-page-datatable" class="table table-striped dt-responsive nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>{{ __('Character Name') }}</th>
                                <th>{{ __('Age') }}</th>
                                <th>{{ __('Sex') }}</th>
                                <th>{{ __('Country name') }}</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>


                        <tbody>
                            @foreach ($persons as $person)
                            <tr>
                                <td>{{ $person->full_name }}</td>
                                <td>{{ $person->age }}</td>
                                <td>{{ $person->sex }}</td>
                                <td>{{ $person->country['country'] }}</td>
                                <td><span class="cursor-pointer" data-toggle="modal" data-target=".bs-example-modal-lg-{{ $person->game_id }}" for="{{ $person->game_id }}">{{ __('View') }}</span></td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div> <!-- end card body-->
                @foreach ($sdgComments as $sdgComment)
                <div class="modal fade bs-example-modal-lg-{{ $sdgComment->game_id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;" for="{{ $sdgComment->game_id }}">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myLargeModalLabel">{{ __('My SDG Comments') }}</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                @forelse ($sdgComment['my_sdg_comments'] as $mySdgComment)
                                {{ $mySdgComment }}
                                <p />
                                @empty
                                {{ __('No Comments') }}
                                @endforelse
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                @endforeach
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
</div>
<!-- end row-->
@endsection
@section('script')

<!-- third party js -->
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<!-- third party js ends -->

<!-- Datatables init -->
<script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>

<!-- Modal-Effect -->
<script src="{{ URL::asset('assets/libs/custombox/custombox.min.js')}}"></script>

@endsection