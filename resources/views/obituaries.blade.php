@extends('layouts.master')

@section('css')
<!-- third party css -->
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- third party css end -->
@endsection

@section('content')

<!-- Start Content-->
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">{{ __('Obituaries Written By Me') }}</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <table id="alternative-page-datatable" class="table table-striped dt-responsive nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>{{ __('Character Name') }}</th>
                                <th>{{ __('Age') }}</th>
                                <th>{{ __('Sex') }}</th>
                                <th>{{ __('Country name') }}</th>
                                <th>{{ __('Obituary') }}</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($persons as $person)
                            <tr>
                                <td>{{ $person->full_name }}</td>
                                <td>{{ $person->age }}</td>
                                <td>{{ $person->sex }}</td>
                                <td>{{ $person->country['country'] }}</td>
                                <td>
                                    @if (in_array($person->game_id, $gameIds))
                                    <span class="cursor-pointer" data-toggle="modal" data-target=".bs-example-modal-lg-s-{{ $person->game_id }}" for="s-{{ $person->game_id }}">{{ __('System Generated') }}</span>
                                    @else
                                    {{ __('No Obituary Generated') }}
                                    @endif
                                    &nbsp; - &nbsp;
                                    @if (in_array($person->game_id, $gameIds))
                                    <span class="cursor-pointer" data-toggle="modal" data-target=".bs-example-modal-lg-u-{{ $person->game_id }}" for="u-{{ $person->game_id }}">{{ __('User Generated') }}</span>
                                    @else
                                    {{ __('No Obituary Written') }}
                                    @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
                @foreach ($obituaries as $obituary)
                <div class="modal fade bs-example-modal-lg-s-{{ $obituary['game_id'] }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;" for="s-{{ $obituary['game_id'] }}">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myLargeModalLabel">{{ __('System Generated Obituary') }}</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                @forelse ($obituary['my_obituary'] as $myObituary)
                                @if (is_array($myObituary))
                                <p>
                                    {{ $myObituary['bornString'] }}
                                </p>
                                <p>
                                    {{ $myObituary['familyString'] }}
                                </p>
                                <p>
                                    {{ $myObituary['residenceString'] }}
                                </p>
                                <p>
                                    {{ $myObituary['educationString'] }}
                                </p>
                                <p>
                                    {{ $myObituary['careerString'] }}
                                </p>
                                <p>
                                    {{ $myObituary['leisureString'] }}
                                </p>
                                <p>
                                    {{ $myObituary['diseaseString'] }}
                                </p>
                                <p>
                                    {{ $myObituary['vicesString'] }}
                                </p>
                                <p>
                                    {{ $myObituary['residenceString'] }}
                                </p>
                                <p>
                                    {{ $myObituary['charityString'] }}
                                </p>
                                <p>
                                    {{ $myObituary['dietString'] }}
                                </p>
                                <p>
                                    {{ $myObituary['loanInvestmentString'] }}
                                </p>
                                <p>
                                    {{ $myObituary['amenitiesString'] }}
                                </p>
                                <p>
                                    {{ $myObituary['statusString'] }}
                                </p>
                                @endif
                                @empty
                                {{ __('No Obituary Generated') }}
                                @endforelse

                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <div class="modal fade bs-example-modal-lg-u-{{ $obituary['game_id'] }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;" for="u-{{ $obituary['game_id'] }}">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myLargeModalLabel">{{ __('Obituary Written By Me') }}</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                @forelse ($obituary['my_obituary'] as $userWritten)
                                @if (!is_array($userWritten))
                                <p>
                                    {{ $userWritten }}
                                </p>
                                @endif
                                @empty
                                <p>
                                    {{ __('No Obituary Written') }}
                                </p>
                                @endforelse
                                <p />
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                @endforeach
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
</div>
<!-- end row-->
@endsection
@section('script')

<!-- third party js -->
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<!-- third party js ends -->

<!-- Datatables init -->
<script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>

<!-- Modal-Effect -->
<script src="{{ URL::asset('assets/libs/custombox/custombox.min.js')}}"></script>

@endsection